﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="HRAResult.aspx.vb" Inherits="Ergonomic.HRAResult" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style>
    @-webkit-keyframes PULSE {
      from {
        opacity: 1
      }

      to {
        opacity: .75
      }
    }

    @keyframes PULSE {
      from {
        opacity: 1
      }

      to {
        opacity: .75
      }
    }

    #Layer_1[_ngcontent-gty-c30] {
      width: 100%
    }

    #Layer_1[_ngcontent-gty-c30] .body-part[_ngcontent-gty-c30] {
      fill: #ccc;
      opacity: .97
    }

    #Layer_1[_ngcontent-gty-c30] .body-part.selected[_ngcontent-gty-c30] {
      fill: #0a94b1
    }

    #Layer_1[_ngcontent-gty-c30] .body-part.selected[_ngcontent-gty-c30]:hover {
      opacity: 1;
      cursor: pointer
    }

    #Layer_1[_ngcontent-gty-c30] .body-part[_ngcontent-gty-c30]:hover {
      opacity: .25;
      cursor: pointer
    }

    #Layer_1[_ngcontent-gty-c30] .body-part.low[_ngcontent-gty-c30] {
      fill: #0aa24a
    }

    #Layer_1[_ngcontent-gty-c30] .body-part.medium[_ngcontent-gty-c30] {
      fill: #fec148
    }

    #Layer_1[_ngcontent-gty-c30] .body-part.high[_ngcontent-gty-c30] {
      fill: #e42b6e;
      -webkit-animation: .75s infinite PULSE;
      animation: .75s infinite PULSE
    }
  </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
    <h1>HRA Evaluation Result</h1>
  </section>
  <section class="content">
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Personal data
              <asp:HiddenField ID="hdUID" runat="server" />   <asp:HiddenField ID="hdPersonUID" runat="server" />
            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label>Code</label>
                  <asp:TextBox ID="txtCode" runat="server" cssclass="form-control text-center"  BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>

              </div>

              <div class="col-md-10">
                <div class="form-group">
                  <label>Name</label>
                  <asp:TextBox ID="txtName" runat="server" cssclass="form-control text-center"  BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Position</label>
                 <asp:TextBox ID="txtPosition" runat="server" cssclass="form-control text-center"  BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-9">
                <div class="form-group">
                  <label>Department</label>
                  <asp:TextBox ID="txtDepartment" runat="server" cssclass="form-control text-center"  BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Section</label>
                  <asp:TextBox ID="txtSection" runat="server" cssclass="form-control text-center"  BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Remark</label>
                  <asp:TextBox ID="txtRemark" runat="server" cssclass="form-control text-center"  BackColor="White"></asp:TextBox>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>
         

    </div>
    <div class="row">
      <section class="col-lg-6 connectedSortable">

        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">อาการผิดปกติทางระบบโครงร่างและกล้ามเนื้อ</h3>
          </div>

          <div class="box-body">

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">


                 <table id="tbdatabody" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th rowspan="2" class="text-center"  >ส่วนของร่างกาย</th>
                        <th colspan="2" class="text-center">ซ้าย</th>
                           <th colspan="2" class="text-center">ขวา</th>

                      </tr>
                         <tr>
                        <th class="text-center"> ระดับ</th>
                        <th class="text-center">ความถี่</th>
                        <th class="text-center">ระดับ</th>
                        <th class="text-center">ความถี่</th>
                      </tr>
                    </thead>
                    <tbody>
                 <% For Each row As DataRow In dtPersonRisk.Rows %>
                <tr>
                  <td><% =String.Concat(row("BodyPart")) %></td> 
                  <td class="text-center"> <% If String.Concat(row("ScoreL")) >= 3 Then %> <span class="text-bold" style="color:red"> <% =String.Concat(row("ScoreL")) %></span>  <% Else %>  <% =String.Concat(row("ScoreL")) %>  <% End If %></td> 
                  <td class="text-center"><% If String.Concat(row("FreqL")) >= 3 Then %> <span class="text-bold" style="color:red"> <% =String.Concat(row("FreqL")) %></span>  <% Else %>  <% =String.Concat(row("FreqL")) %>  <% End If %></td> 
                  <td class="text-center"><% If String.Concat(row("ScoreR")) >= 3 Then %> <span class="text-bold" style="color:red"> <% =String.Concat(row("ScoreR")) %></span>  <% Else %>  <% =String.Concat(row("ScoreR")) %>  <% End If %></td> 
                   <td class="text-center"><% If String.Concat(row("FreqR")) >= 3 Then %> <span class="text-bold" style="color:red"> <% =String.Concat(row("FreqR")) %></span>  <% Else %>  <% =String.Concat(row("FreqR")) %>  <% End If %></td> 
                </tr>
            <%  Next %>
                    </tbody>
                  </table>
               
<!--
                     <asp:GridView ID="grdBodyRisk" runat="server" AutoGenerateColumns="False" CellPadding="0" CssClass="table table-bordered table-hover"  Width="100%">
                          <RowStyle BackColor="#F7F7F7" />
                          <columns>
                              <asp:BoundField DataField="BodyPart" HeaderText="ส่วนของร่างกาย">
                              </asp:BoundField>
                              <asp:BoundField DataField="Score" HeaderText="ความรุนแรง" />
                              <asp:BoundField DataField="Freq" HeaderText="ความถี่" />
                          </columns>
                          <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                          <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                          <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                          <headerstyle VerticalAlign="Middle" />
                          <EditRowStyle BackColor="#2461BF" />
                          <AlternatingRowStyle BackColor="White" />
                      </asp:GridView>
-->

                </div>
              </div>
              <div class="col-md-6">               
                  <table style="width: 100%;">
                    <tr>                     
                      <td class="text-center">
                          <img src="images/body.png" />
<!--
                        <div _ngcontent-gty-c28="" class="flex flex1 justify-content-center">
                          <app-body-segments _ngcontent-gty-c28="" _nghost-gty-c30="">
                            <div _ngcontent-gty-c30="" class="flex align-items-center">
                              <svg _ngcontent-gty-c30="" xml:space="preserve" xmlns:xlink="http://www.w3.org/1999/xlink"
                                id="Layer_1" style="enable-background:new 0 0 1125 2436;" version="1.1"
                                viewBox="0 0 1125 2436" x="0px" xmlns="http://www.w3.org/2000/svg" y="0px">
                                <style type="text/css">
                                  .st0 {
                                    fill: #eee;
                                  }

                                  .st1 {
                                    fill: #dddcdb;
                                  }

                                  .st2 {
                                    opacity: 3e-2;
                                  }

                                  .st3 {
                                    fill: #ece7e4;
                                  }


                                  .st0 {
                                    fill: #eee;
                                  }

                                  .st1 {
                                    fill: #dddcdb;
                                  }

                                  .st2 {
                                    opacity: 3e-2;
                                  }

                                  .st3 {
                                    fill: #ece7e4;
                                  }
                                </style>
                                <path _ngcontent-gty-c30="" class="st0"
                                  d="M963.7,1401.4c-10.2-57.5-9.2-275.6-19.3-316.3c-10.2-40.6-20.3-159.1-54.1-314.8  c-33.9-155.7-182.7-176-226.7-189.6c-0.2-0.1-0.3-0.1-0.5-0.2c-6.6-1.3-13.1-2.9-19.7-4.9c-4.8-0.6-9.6-1.2-14.4-1.9l-0.4,0.1  c-2.2-5.1-3.9-10.9-5.2-16.9c47.4-21.6,71.5-71.1,83.6-111.9c2.3-6.8,4.2-13.7,5.9-20.9c0.8,0.4,1.7,0.6,2.5,0.7  c10.6,1.6,21.8-14.7,25-36.4c3.2-21.7-2.9-40.5-13.5-42.1c-1.8-0.3-3.7,0-5.5,0.7c1.7-28.4,2.8-58.6,2.8-58.6  s-6.5-112.9-128.3-128.3c-56.5-7.1-95.5,4-121.5,18.9c0,0-12.2,5.9-25.9,19.4c-8.8,8.5-14.3,16.4-17.3,21.4  c-16.8,26.3-28.3,66.6-11.3,124.9c-1.5-0.6-3-0.9-4.4-0.8c-9.7,1-16,19.1-13.9,40.4c2.1,21.4,11.6,38,21.4,37  c2.5-0.2,4.8-1.6,6.8-3.9c8,39.5,28.2,102.4,78,132.8c-1.2,7.7-2.9,15.1-5.5,21.5l-1.6,3.6c-6,1.5-12,3-18,4.2  c-12.4,2.6-25,4.5-37.7,6.1c-57.6,14.8-178.1,46.4-208.2,184.5c-33.9,155.7-44,274.2-54.1,314.8c-10.2,40.6-9.2,258.7-19.4,316.3  c-10.2,57.5,2.4,127.2,16,154.3c13.5,27.1,28.1,49.8,37.2,54.2c18.7,8.9,32-5.4,20.3-23.7c-22.1-34.5-20.3-60.9-16.9-77.9  c3.4-16.9,20.3-20.3,20.3-20.3s1.9,47.2,13.5,50.8c15.4,4.7,21.6-11.4,18.3-55.4c-3.4-44-36-40.2-36-87.5  c0-47.4,61.8-256.5,44.8-314c0,0,69.2-178.7,79.4-263.4c0,0,8.7,148.3,25.6,188.9c11.8,28.4,3.8,155.9-2.1,230.1  c-0.9-0.1-1.3-0.1-1.3-0.1l-3.4,54.3c0,0-7.3,454.1-3.9,501.4c3.4,47.4,41.9,482.4,35.1,509.5c0,0-137.3,31.9-137.3,65.1  c0,36.5,62.1,14.3,102.7,17.7c40.6,3.4,109.5,10.8,140-2.8c30.5-13.5-2.4-92.1-2.4-92.1s7.8-416.6,4.4-453.9  c-3-33.2,37.3-345.6,45.7-420.7c0.1,0,0.2,0,0.3,0c0.2,0,0.4,0,0.6,0c8.4,75.1,48.5,387.6,45.5,420.7c-3.4,37.2,2,453.4,2,453.4  s-30.5,79-0.1,92.6c30.5,13.5,99.4,6.1,140,2.8c40.6-3.4,102.7,18.7,102.7-17.7c0-33.2-137.3-65.1-137.3-65.1  c-6.8-27.1,31.7-462.1,35.1-509.5c3.2-44.7-3.1-451.7-3.8-497.3c0.8-0.7,1.3-1,1.3-1l-4.2-57.5c-0.6,0-1.2,0.1-1.9,0.1  c-5.9-74.3-13.9-201.8-2.1-230.2c16.9-40.6,25.3-191.5,25.3-191.5c10.2,84.6,79.6,266,79.6,266c-16.9,57.5,44.8,266.6,44.8,314  c0,47.4-32.6,43.5-36,87.5c-3.4,44,2.9,60.1,18.3,55.4c11.7-3.6,13.5-50.8,13.5-50.8s16.9,3.4,20.3,20.3  c3.4,16.9,5.2,43.3-16.9,77.9c-11.7,18.3,1.6,32.6,20.3,23.7c9.2-4.4,23.7-27.1,37.2-54.2C961.3,1528.6,973.9,1459,963.7,1401.4z">
                                </path>
                                <path _ngcontent-gty-c30="" class="st1"
                                  d="M570.1,1418.4c5.1,6.8,9.9,13.7,14.8,20.5c4.9,6.8,9.7,13.5,14.8,20.1c5.1,6.5,10.4,12.8,16.3,18.4  c2.9,2.8,6.1,5.4,9.5,7.6c3.4,2.2,7.2,3.9,11.4,4.6c-4.2,0.9-8.8,0.1-12.9-1.5c-4.2-1.5-8-3.9-11.5-6.4c-7-5.2-13-11.6-18.3-18.4  c-5.3-6.8-9.9-14-14-21.5C576.2,1434.3,572.6,1426.6,570.1,1418.4z">
                                </path>
                                <path _ngcontent-gty-c30="" class="st1"
                                  d="M552.2,1415.1c-2,8.6-5.1,16.8-8.7,24.8c-3.6,8-7.9,15.7-12.8,23.1c-2.5,3.7-5.2,7.2-8.1,10.6  c-2.9,3.4-6,6.6-9.5,9.5c-3.5,2.9-7.2,5.5-11.4,7.4c-4.1,1.8-8.7,3-13.1,2.5c4.3-1.1,8-3.1,11.4-5.5c3.4-2.5,6.4-5.3,9.3-8.4  c5.7-6.1,10.7-13,15.5-20c4.8-7,9.3-14.3,13.7-21.7C543.2,1430,547.6,1422.5,552.2,1415.1z">
                                </path>
                                <g _ngcontent-gty-c30="" class="st2">
                                  <path _ngcontent-gty-c30=""
                                    d="M563.5,1074.5c1.4,11.9,2.2,23.8,2.7,35.8c0.5,11.9,0.8,23.8,0.8,35.8c0,11.9-0.2,23.8-0.7,35.8   c-0.5,11.9-1.3,23.8-2.8,35.8c-1.5-11.9-2.2-23.8-2.8-35.8c-0.5-11.9-0.8-23.8-0.7-35.8c0-11.9,0.2-23.8,0.8-35.8   C561.3,1098.3,562.1,1086.4,563.5,1074.5z">
                                  </path>
                                </g>
                                <path _ngcontent-gty-c30="" class="st3"
                                  d="M512.6,2181.7c-0.2,1-0.4,2-0.6,3c-0.4,11.1-0.5,22.1-0.3,33.2c0.8,3.1,1.6,6.2,2.4,9.2  c0.3-17.4,0.6-37.6,1-59.4C514.4,2172.4,513.5,2177.1,512.6,2181.7z">
                                </path>
                                <g _ngcontent-gty-c30="" class="st2">
                                  <path _ngcontent-gty-c30=""
                                    d="M582,671c2.3,6.2,4.7,12.3,7.5,18.3c2.7,6,5.6,11.8,8.8,17.5c3.2,5.7,6.6,11.2,10.2,16.5c3.6,5.4,7.5,10.5,11.7,15.4   c8.3,9.8,17.8,18.6,28.3,26.1l2,1.4l2,1.3l4.1,2.6c2.8,1.6,5.6,3.4,8.5,4.8c5.7,3.2,11.8,5.6,17.9,8.1c-6.5-1.1-13.1-2.6-19.3-5   c-3.2-1.1-6.2-2.5-9.3-3.9c-1.5-0.7-3-1.5-4.5-2.3l-2.2-1.2l-2.2-1.3c-1.4-0.9-2.9-1.7-4.3-2.6l-4.2-2.9c-1.4-0.9-2.7-2-4-3.1   c-1.3-1-2.7-2.1-3.9-3.2c-5.2-4.4-9.9-9.3-14.3-14.4c-2.2-2.6-4.2-5.2-6.3-7.9c-1.9-2.8-3.9-5.5-5.6-8.4   c-3.6-5.7-6.8-11.6-9.5-17.7c-2.8-6.1-5.1-12.4-7.1-18.7C584.4,684.1,582.8,677.6,582,671z">
                                  </path>
                                </g>
                                <g _ngcontent-gty-c30="" class="st2">
                                  <path _ngcontent-gty-c30=""
                                    d="M551.4,671c-0.2,3-0.6,6.1-1,9.1c-0.5,3-1.1,6-1.7,9c-1.3,6-3,11.9-5,17.6c-4,11.6-9.3,22.8-16,33.2   c-1.7,2.6-3.5,5.1-5.4,7.6l-1.4,1.8c-0.5,0.6-1,1.2-1.5,1.8c-1,1.2-2,2.4-3,3.5c-4.1,4.6-8.7,8.9-13.6,12.7   c-4.9,3.8-10.2,7.1-15.8,9.8l-4.2,1.9c-1.4,0.6-2.9,1.1-4.3,1.6c-2.9,1.1-5.9,1.7-8.8,2.4c5.5-2.7,10.8-5.5,15.7-8.8   c4.9-3.4,9.6-6.9,13.9-11c4.3-4,8.4-8.3,12.1-12.8c0.9-1.1,1.8-2.3,2.7-3.5c0.5-0.6,0.9-1.1,1.4-1.7l1.3-1.8c1.8-2.3,3.4-4.8,5-7.3   c6.4-9.9,12-20.4,16.8-31.4C543.4,693.8,547.7,682.6,551.4,671z">
                                  </path>
                                </g>
                                <path _ngcontent-gty-c30="" class="body-part low"
                                  d="M760.5,783C731.7,740.8,735,614,735,614c-96-33.7-129-33.5-129-33.5l-11-25.3c-10.3,2.4-20.4,3.4-29.9,3.5  c-10.1-0.1-20.9-1.2-31.9-3.6l-10.2,25.6c-10.7,1-45,6.2-117.7,33.3c0,0,3.1,126.8-24,169c0,0,9.9,34.2,8.5,69.5  c0,0,18.4-15.9,38.1-20.3l0.1-0.2c12.7-2.4,159.8-29.2,283.1,0.2c0,0-5.1-11.9-10.4-32.9l10.4,32.9c20.9,4.4,40.5,20.3,40.5,20.3  C749.9,817.2,760.5,783,760.5,783z"
                                  xmlns="http://www.w3.org/2000/svg" id="body-segment-neckAndUpperBack"></path>
                                <path _ngcontent-gty-c30="" class="body-part low"
                                  d="M734.5,1300c0-3.1-0.3-6.5-0.8-10.2c-1.9-21.3-4.4-42.8-7.9-60.8c-7.5-65.4-8.2-100.9-8.2-100.9l0,0  c-0.3-61.3,5.4-93.4,5.4-93.4c-189,45-319.2,0-319.2,0c10.4,51.7-2.2,160.1-2.2,160.1s-16,137.5-11,191.4c0,0,235-34.8,348,0  C738.5,1386.2,738,1345.1,734.5,1300z"
                                  xmlns="http://www.w3.org/2000/svg" id="body-segment-trunkAndLowerBack"></path>
                                <path _ngcontent-gty-c30="" class="body-part low"
                                  d="M876.1,783L876.1,783c-23-124.4-114.6-161-123.9-164.4l-0.4-0.6c0,0.2,0,0.3,0,0.5c-0.2-0.1-0.3-0.1-0.3-0.1  s0.1,1.5,0.3,4.1c-0.7,65,20.4,160.5,20.4,160.5c14.8,24.3,23.4,63.3,28,91.9c54.6,14.6,93.9,0,93.9,0C894,831,876.1,783,876.1,783z  "
                                  xmlns="http://www.w3.org/2000/svg" id="body-segment-rightUpperArmAndShoulder">
                                </path>
                                <path _ngcontent-gty-c30="" class="body-part low"
                                  d="M934.4,1085.5c0,0-4.1-39.3-13-74.4c0,0,0,0,0,0c-10.5-65.9-17.4-102.3-21.8-122.4c-20.1,6-41.1,8.6-63,7.8  c-11.8,0-23.3-0.8-34.7-2.4c10.1,58.4,46.1,155,46.1,155c7.1,17.6,9,36,9.1,49c-0.4,4.5-0.8,10.4-0.8,17.7c0,0,0,0,0,0l0,0  c0,18.4,2.1,45.8,10.2,83.4c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0c0.5,2.3,1,4.4,1.6,6.4c3.7,13.3,10,44.5,16.4,78.4  c15.9-7.6,32.9-11.1,51.1-10.4c0.4,0,0.7,0,1.1,0c-0.1-1.3-0.1-2.7-0.2-4C932.2,1168.8,937.7,1113.7,934.4,1085.5z"
                                  xmlns="http://www.w3.org/2000/svg" id="body-segment-rightForearmAndElbow"></path>
                                <path _ngcontent-gty-c30="" class="body-part low"
                                  d="M953.9,1382c-2.5-55.5-10.3-97.8-10.3-97.8c-40.5-8.2-57.4,10.4-57.4,10.4c25.9,122.8,4.9,136.2,4.9,136.2  c-8.7,9.7-29.7,40.4-30.5,64.2c-0.9,30,0.5,28.4,3.8,30.7c0,0,0,0,0,0c1,1.3,2.1,2.1,3.3,2.3c9,1,8-26,9-39c1-13,7-12,7-12  s27.1-5,32.1,44s-17,57-21.1,70s10,9,10,9c24.7-20.4,41.5-72.3,45.9-87.4c0.4-1.3,0.8-2.6,1.2-4.2l0,0c2.4-10.1,3.6-25.5,4.1-40.7  c0-0.3,0-0.6,0-1.1c0,0,0,0,0,0C956.3,1457.9,957.5,1413.4,953.9,1382z"
                                  xmlns="http://www.w3.org/2000/svg" id="body-segment-rightWristAndFingers"></path>
                                <path _ngcontent-gty-c30="" class="body-part legs low"
                                  d="M624.3,2296.5l1.3-3l-0.1-3.3c-0.1-4-7.3-393.3-4.7-448.2c0.4-89.3-24.8-231.3-24.8-231.3  c63,19.1,136.6,0,136.6,0c9.5,78.7-3.8,229.8-3.8,229.8c-0.5,6-1,12.5-1.6,19.3l-0.1,0.6c-25.6,307.4-33.5,428.2-29.1,445.8l2.3,9.2  l9.2,2.1c0.3,0.1,34.1,8,67.1,19.6c49.6,17.5,57.3,29.2,58.2,30.9c-0.1,1.2-0.3,2.2-0.5,2.5c-5,4.6-30.3,2.2-43.9,0.9  c-11-1-22.3-2.1-33.1-2.1c-3.9,0-7.5,0.1-10.9,0.4c-3.2,0.3-6.6,0.6-10.1,0.9l-0.4,0c-36.5,3.2-97.6,8.4-121.9-2.4  c-3.1-1.4-6-11.5-1.4-33.8C616.6,2315.1,624.2,2296.7,624.3,2296.5z"
                                  xmlns="http://www.w3.org/2000/svg" id="body-segment-rightleg"></path>
                                <path _ngcontent-gty-c30="" class="body-part low"
                                  d="M248.6,781.8L248.6,781.8c27.7-123.9,128.1-158.4,138.3-161.6l0.5-0.6c0,0.2,0,0.3,0,0.5  c0.2-0.1,0.4-0.1,0.4-0.1s-0.1,1.5-0.4,4.1c-0.6,65-25.5,160-25.5,160c-16.6,24-26.8,62.8-32.4,91.3c-59.8,13.3-102.3-2.1-102.3-2.1  C228.1,829.4,248.6,781.8,248.6,781.8z"
                                  xmlns="http://www.w3.org/2000/svg" id="body-segment-leftUpperArmAndShoulder">
                                </path>
                                <path _ngcontent-gty-c30="" class="body-part low"
                                  d="M188.8,1085.1c0,0,4.3-39.2,13.5-74.3c0,0,0,0,0,0c11-65.8,18.1-102.2,22.7-122.3c20.2,6.1,41.4,8.8,63.6,8.1  c11.9,0,23.6-0.7,35.1-2.2c-10.6,58.3-47.4,154.7-47.4,154.7c-7.3,17.5-9.2,35.9-9.5,48.9c0.4,4.5,0.7,10.4,0.7,17.7c0,0,0,0,0,0  l0,0c-0.1,18.4-2.4,45.8-10.8,83.4c0,0.1,0,0.1,0,0.2c0,0,0,0,0,0c-0.5,2.3-1,4.4-1.6,6.4c-3.9,13.3-10.4,44.5-17,78.3  c-16-7.7-33.2-11.3-51.5-10.7c-0.4,0-0.7,0-1.1,0c0.1-1.3,0.1-2.7,0.2-4C190.6,1168.5,185.2,1113.3,188.8,1085.1z"
                                  xmlns="http://www.w3.org/2000/svg" id="body-segment-leftForearmAndElbow"></path>
                                <path _ngcontent-gty-c30="" class="body-part low"
                                  d="M170.9,1382c2.5-55.5,10.3-97.8,10.3-97.8c40.3-8.2,57.1,10.4,57.1,10.4c-25.8,122.8-4.9,136.2-4.9,136.2  c8.6,9.7,29.6,40.4,30.4,64.2c0.9,30-0.5,28.4-3.8,30.7c0,0,0,0,0,0c-1,1.3-2.1,2.1-3.3,2.3c-9,1-8-26-9-39c-1-13-7-12-7-12  s-27-5-32,44s17,57,21,70c4,13-10,9-10,9c-24.6-20.4-41.3-72.3-45.7-87.4c-0.4-1.3-0.8-2.6-1.2-4.2l0,0c-2.4-10.1-3.6-25.5-4.1-40.7  c0-0.3,0-0.6,0-1.1c0,0,0,0,0,0C168.5,1457.9,167.3,1413.4,170.9,1382z"
                                  xmlns="http://www.w3.org/2000/svg" id="body-segment-leftWristAndFingers"></path>
                                <path _ngcontent-gty-c30="" class="body-part legs low"
                                  d="M497.8,2296.5l-1.3-3l0.1-3.3c0.1-4,7.3-393.3,4.7-448.2c-0.4-89.3,24.8-231.3,24.8-231.3  c-63,19.1-136.6,0-136.6,0c-9.5,78.7,3.8,229.8,3.8,229.8c0.5,6,1,12.5,1.6,19.3l0.1,0.6c25.6,307.4,33.5,428.2,29.1,445.8l-2.3,9.2  l-9.2,2.1c-0.3,0.1-34.1,8-67.1,19.6c-49.6,17.5-57.3,29.2-58.2,30.9c0.1,1.2,0.3,2.2,0.5,2.5c5,4.6,30.3,2.2,43.9,0.9  c11-1,22.3-2.1,33.1-2.1c3.9,0,7.5,0.1,10.9,0.4c3.2,0.3,6.6,0.6,10.1,0.9l0.4,0c36.5,3.2,97.6,8.4,121.9-2.4  c3.1-1.4,6-11.5,1.4-33.8C505.5,2315.1,497.8,2296.7,497.8,2296.5z"
                                  xmlns="http://www.w3.org/2000/svg" id="body-segment-leftleg"></path>
                              </svg>

                            </div>
                          </app-body-segments>
                        </div>

                          -->
                      </td>
                                         </tr>
                  </table>
              
              </div>
            </div>
          </div>
        </div>
      </section> 

   <!-- </div>
    <div class="row">-->

      <section class="col-lg-6 connectedSortable">
           
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">ความผิดปกติของระบบโครงร่างและกล้ามเนื้อ</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
               <h5 class="text-blue text-bold">ความผิดปกติของระบบโครงร่างและกล้ามเนื้อ</h5>
            <div class="row">

              <div class="col-md-6">
                <div class="form-group">
                  <label class="text-blue">คะแนน</label>
                  <asp:TextBox ID="txtBodyScore" runat="server" cssclass="form-control text-bold text-center"
                    placeholder="Kg." Font-Size="12" BackColor="White" ReadOnly="True" AutoPostBack="True"></asp:TextBox>  
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="text-blue">ระดับความเสี่ยง</label>
                  <asp:TextBox ID="txtBodyRisk" runat="server" cssclass="form-control text-bold text-center"
                    placeholder="0.00" Font-Size="12" BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
            </div>


              
              <h5 class="text-blue text-bold">Evaluation Result</h5>
                  <div class="row">
              <div class="col-md-12">
                <div class="form-group">                  

                     <asp:GridView ID="grdEvaluation" runat="server" AutoGenerateColumns="False" CellPadding="0" CssClass="table table-hover table-bordered text-center"  Width="100%">
                          <RowStyle BackColor="#F7F7F7" />
                          <columns>
<asp:BoundField DataField="TaskNo" HeaderText="Job ID">
                              <ItemStyle HorizontalAlign="Center" />
                              </asp:BoundField>
                              <asp:BoundField DataField="TaskName" HeaderText="Detail">
                         
                              <ItemStyle HorizontalAlign="left" />
                              </asp:BoundField>
                              <asp:BoundField DataField="FinalScore" HeaderText="Evaluation Score" >
                              <HeaderStyle HorizontalAlign="Center" />
                              <ItemStyle HorizontalAlign="Center" />
                              </asp:BoundField>
                              <asp:BoundField DataField="HRALevel" HeaderText="HRA Score">
                              <HeaderStyle HorizontalAlign="Center" />
                              <ItemStyle HorizontalAlign="Center" />
                              </asp:BoundField>
                              <asp:BoundField DataField="HRAResult" HeaderText="HRA Result" >
                              <HeaderStyle HorizontalAlign="Center" />
                              <ItemStyle HorizontalAlign="Center" />
                              </asp:BoundField>
                          </columns>
                          <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                          <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                          <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                          <headerstyle VerticalAlign="Middle" />
                          <EditRowStyle BackColor="#2461BF" />
                          <AlternatingRowStyle BackColor="White" />
                      </asp:GridView>


                </div>
              </div>
            </div>





            <h4 class="text-blue text-bold">Recommendations</h4>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <asp:HyperLink ID="lblRecommend"  cssclass="form-control" runat="server"></asp:HyperLink>
                     
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
              <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
        <asp:Button ID="cmdBack" CssClass="btn btn-primary" runat="server" Text="Back" Width="120px" />
        </div>
    </div>
  </section>
</asp:Content>