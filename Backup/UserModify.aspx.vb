﻿Public Class UserModify
    Inherits System.Web.UI.Page
    Dim ctlE As New PersonController
    Dim ctlU As New UserController
    Dim enc As New CryptographyEngine
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            lblUserID.Text = ""
            BindUserGroup()
            LoadPerson()


            If Request.Cookies("Ergo")("ROLE_ADM") = True Or Request.Cookies("Ergo")("ROLE_SPA") = True Then
                LoadCompany()
            Else
                chkGroup.Items.Remove(2)
                chkGroup.Items.Remove(3)
            End If

            If Not Request("uid") = Nothing Then
                EditUser()
            End If
        End If

        Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
        cmdDelete.Attributes.Add("onClick", scriptString)

    End Sub

    Private Sub BindUserGroup()

        With chkGroup
            .Items.Clear()

            If Request.Cookies("Ergo")("ROLE_ADM") = True Or Request.Cookies("Ergo")("ROLE_SPA") = True Then
                .Items.Add("User")
                .Items(0).Value = "1"
                .Items.Add("Customer Admin")
                .Items(1).Value = "2"
                .Items.Add("NPC - SE Admin")
                .Items(2).Value = "3"
                .Items.Add("Super Administrator")
                .Items(3).Value = "9"
            ElseIf Request.Cookies("Ergo")("ROLE_CAD") = True Then
                .Items.Add("User")
                .Items(0).Value = "1"
                .Items.Add("Admin")
                .Items(1).Value = "2"
            End If
        End With

    End Sub


    Private Sub LoadCompany()
        Dim ctlC As New CompanyController
        ddlCompany.DataSource = ctlC.Company_GetActive()
        ddlCompany.DataTextField = "CompanyName"
        ddlCompany.DataValueField = "UID"
        ddlCompany.DataBind()
    End Sub

    Private Sub EditUserCompanyToGrid()
        Dim dtC As New DataTable

        If lblUserID.Text <> "" Then
            dtC = ctlU.UserCompany_GetByUserID(StrNull2Zero(lblUserID.Text))
        Else
            dtC = ctlU.UserCompany_GetByUsername(txtUsername.Text)
        End If


        If dtC.Rows.Count > 0 Then
            With grdCompany
                .Visible = True
                .DataSource = dtC
                .DataBind()
            End With
        Else
            grdCompany.DataSource = Nothing
            grdCompany.Visible = False
        End If
        dtC = Nothing

    End Sub

    Private Sub LoadPerson()
        Dim dtE As New DataTable

        'If Request("uid") Is Nothing Then
        '    dtE = ctlE.Person_GetNotUser(Request.Cookies("Ergo")("LoginCompanyUID"))
        'Else
        If Request.Cookies("Ergo")("ROLE_ADM") = True Or Request.Cookies("Ergo")("ROLE_SPA") = True Then
            dtE = ctlE.Person_GetForSelection("", "A")
        Else
            dtE = ctlE.Person_GetForSelection(Request.Cookies("Ergo")("LoginCompanyUID"), "A")
        End If
        'End If
        ddlPerson.DataSource = dtE
        ddlPerson.DataTextField = "PersonName"
        ddlPerson.DataValueField = "PersonUID"
        ddlPerson.DataBind()
    End Sub
    Private Sub EditUser()
        Dim dt As New DataTable
        dt = ctlU.User_GetByID(Request("uid"))

        If dt.Rows.Count > 0 Then
            lblUserID.Text = Request("uid")
            txtName.Text = String.Concat(dt.Rows(0)("Name"))
            txtUsername.Text = String.Concat(dt.Rows(0)("Username"))
            txtPassword.Text = "****" ' enc.DecryptString(String.Concat(dt.Rows(0)("Passwords")), True)

            ddlPerson.SelectedValue = DBNull2Zero(dt.Rows(0)("PersonUID"))
            chkActive.Checked = ConvertStatusFlag2CHK(String.Concat(dt.Rows(0)("StatusFlag")))

            chkGroup.SelectedValue = String.Concat(dt.Rows(0)("UserGroupUID"))


            'EditUserGroup()

            If Request.Cookies("Ergo")("ROLE_ADM") = True Or Request.Cookies("Ergo")("ROLE_SPA") = True Then
                EditUserCompanyToGrid()
            End If


        End If
    End Sub

    'Private Sub EditUserGroup()
    '    Dim dtG As New DataTable
    '    Dim ctlR As New UserRoleController
    '    chkGroup.ClearSelection()

    '    dtG = ctlR.UserAccessGroup_Get(lblUserID.Text)
    '    If dtG.Rows.Count > 0 Then
    '        For i = 0 To dtG.Rows.Count - 1
    '            For n = 0 To chkGroup.Items.Count - 1
    '                If dtG.Rows(i)("UserGroupUID") = chkGroup.Items(n).Value Then
    '                    chkGroup.Items(n).Selected = True
    '                End If
    '            Next
    '        Next
    '    End If
    'End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtName.Text = "" Or txtUsername.Text = "" Or txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุข้อมูลที่จำเป็นให้ครบถ้วน');", True)
            Exit Sub
        End If

        If chkGroup.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุกลุ่มผู้ใช้งานก่อน');", True)
            Exit Sub
        End If

        If lblUserID.Text = "" Then
            If ctlU.User_CheckDuplicate(txtUsername.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username นี้มีในระบบแล้ว');", True)
                Exit Sub
            End If
        End If
        Dim Password As String = ""
        If txtPassword.Text <> "****" Then
            Password = enc.EncryptString(txtPassword.Text, True)
        Else
            Password = "****"
        End If

        ctlU.User_Save(StrNull2Zero(lblUserID.Text), txtName.Text, txtUsername.Text, Password, ddlPerson.SelectedValue, ConvertBoolean2StatusFlag(chkActive.Checked), Request.Cookies("Ergo")("uid"), chkGroup.SelectedValue)

        Dim UserID As Integer
        UserID = ctlU.User_GetUID(txtUsername.Text)

        'For i = 0 To chkGroup.Items.Count - 1
        'ctlU.UserRole_Save(UserID, chkGroup.Items(i).Value, ConvertBoolean2YN(chkGroup.Items(i).Selected), Request.Cookies("Ergo")("uid"))
        ctlU.UserRole_Save(UserID, chkGroup.SelectedValue, "Y", Request.Cookies("Ergo")("uid"))
        'Next

        Response.Redirect("Users?ActionType=user&ItemType=u")

    End Sub

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlU.User_Delete(lblUserID.Text)
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการทำงาน','ลบข้อมูลเรียบร้อย');", True)

        Response.Redirect("Users?ActionType=user&ItemType=u")
    End Sub

    Protected Sub grdCompany_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCompany.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel"
                    If ctlU.UserCompany_Delete(e.CommandArgument) Then
                        ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), "DEL", "UserCompany", "ลบ UserCompany :" & e.CommandArgument, "")
                        EditUserCompanyToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If
            End Select
        End If
    End Sub

    Protected Sub cmdAddCompany_Click(sender As Object, e As EventArgs) Handles cmdAddCompany.Click
        If ddlCompany.SelectedValue = "0" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกบริษัทที่ให้สิทธิก่อน');", True)
            Exit Sub
        End If

        ctlU.UserCompany_Save(StrNull2Zero(lblUserID.Text), txtUsername.Text, ddlCompany.SelectedValue)
        ddlCompany.SelectedIndex =0
        EditUserCompanyToGrid()
    End Sub
End Class