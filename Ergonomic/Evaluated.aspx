﻿<%@ Page Title="Person" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Evaluated.aspx.vb" Inherits="Ergonomic.Evaluated" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">    
        function openModals(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#btnConfirm').attr('onclick', "$('#mdEditPerson').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            $('.editemp').click(function () {
             var fname = $(this).attr('data-fname');
             $('#fname').val(fname);
             $('#mdEditPerson').modal('show');

            });
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
     <section class="content-header">
      <h1>Evaluated List</h1>     
    </section>
    <section class="content">   
        
        <div class="row no-print">
        <div class="col-xs-12">
          <a href="ErgonomicAssessment?ActionType=asm" target="_blank" class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> Add Assessment</a>
        </div>
      </div>
        <br />

          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">ข้อมูลรายการทำประเมิน</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
              <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled"></th>    
                    <th>Code</th>
                  <th>Task no.</th>
                      <th>Task Name</th>
                  <th>Type</th>
                  <th>Employee Name</th>             
                  <th>Date</th>
                    <th>Score</th>
                     <th>Result</th>
                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtAsm.Rows %>
                <tr>
                     <td><a class="editemp" href="AsmModify?ActionType=<% =Request("ActionType") %>&id=<% =String.Concat(row("UID")) & "&code=" & String.Concat(row("Code")) %>" ><img src="images/icon-edit.png"/></a>                       
                  
                    </td>

                  <td><% =String.Concat(row("Code")) %></td>
                  <td><% =String.Concat(row("TaskNo")) %>    </td>
                     <td><% =String.Concat(row("TaskName")) %>    </td>
                  <td><% =String.Concat(row("AsmType")) %></td>
                  <td><% =String.Concat(row("PersonName")) %></td>
                  <td><% =String.Concat(row("AsmDate")) %></td>
                    <td><% =String.Concat(row("FinalScore")) %></td>
                    <td><% =String.Concat(row("ResultText")) %></td>
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div> 
    </section>
</asp:Content>
