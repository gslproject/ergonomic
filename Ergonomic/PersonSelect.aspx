﻿<%@ Page Title="Person" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PersonSelect.aspx.vb" Inherits="Ergonomic.PersonSelect" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">    
        function openModals(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#btnConfirm').attr('onclick', "$('#mdEditPerson').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            $('.editemp').click(function () {
             var fname = $(this).attr('data-fname');
             $('#fname').val(fname);
             $('#mdEditPerson').modal('show');

            });
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
     <section class="content-header">
      <h1>Person Selection</h1>     
    </section>
    <section class="content">  
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">เลือกพนักงาน</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
              <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th>เลือก</th>   
                    <th>รหัส</th>          
                  <th>ชื่อ - นามสกุล</th>              
                    <th>อายุ</th>                   
                     <th>ตำแหน่ง</th>
                     <th>ฝ่าย/แผนก</th>                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtES.Rows %>
                <tr>
                     <td>
                         <% If Request("ActionType") = "hraasm" Then %>
                                <a href="HRAResult?pid=<% =String.Concat(row("PersonUID")) %>" ><img src="images/select.png"/></a>
                         <% Else %>
                               <a href="PersonalHealth?pid=<% =String.Concat(row("PersonUID")) %>" ><img src="images/select.png"/></a>
                        <% End If %>

                     </td>

                  <td><% =String.Concat(row("Code")) %></td>                  
                  <td><% = String.Concat(row("PrefixName")) & " " & String.Concat(row("NameTH")) & " " & String.Concat(row("SurnameTH")) %></td>              
                    <td><% =String.Concat(row("Age")) %></td>
                    <td><% =String.Concat(row("PositionName")) %></td>
                    <td><% =String.Concat(row("DepartmentName")) %></td>                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div>
 
    <!-- Modal HTML -->
    <div id="mdEditPerson" class="modal fade" role="dialog" data-backdrop="static">
      <div class="modal-dialog modal-lg">
          <form action="Save.vb" method="post">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <div class="icon-box">
              <i class="fa fa-user-circle"></i> Person infomation.
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            
          </div>
          <div class="modal-body">
            <h4><span id="spnTitle"></span></h4>    
              
              <input type="hidden" name="id" id="id" value="" />
                 <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Business Unit</label>
                  <asp:TextBox ID="txtBU" runat="server" cssclass="form-control" placeholder="BU Code"></asp:TextBox>   
                 </div>
              <!-- /.form-group -->
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Name (ไทย)</label> <asp:TextBox ID="txtNameTH" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
                  <input type="text" name="fname" id="fname">
                 </div>
              <!-- /.form-group -->
            </div>
                     <div class="col-md-6">
              <div class="form-group">
                <label>Name (English)</label> <asp:TextBox ID="txtNameEN" runat="server" cssclass="form-control" placeholder="English Name"></asp:TextBox>
                 </div>
              <!-- /.form-group -->
            </div>
</div>
                <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>Address no.</label> <asp:TextBox ID="txtAddressNo" runat="server" cssclass="form-control" placeholder="บ้านเลขที่"></asp:TextBox>
                 </div>
              <!-- /.form-group -->
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Lane</label> <asp:TextBox ID="txtLane" runat="server" cssclass="form-control" placeholder="ซอย"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Road</label> <asp:TextBox ID="txtRoad" runat="server" cssclass="form-control" placeholder="ถนน"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Sub District</label> <asp:TextBox ID="txtSubDistrict" runat="server" cssclass="form-control" placeholder="ตำบล/แขวง"></asp:TextBox>
                 </div>
            </div>
            </div>
                   <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>district</label> <asp:TextBox ID="txtDistrict" runat="server" cssclass="form-control" placeholder="อำเภอ"></asp:TextBox>
                 </div>
              <!-- /.form-group -->
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Province</label> 
                  <asp:DropDownList ID="ddlProvince" runat="server" cssclass="form-control select2" Width="100%" ></asp:DropDownList>
                   
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Zip Code</label> <asp:TextBox ID="txtZipcode" runat="server" cssclass="form-control" placeholder="รหัสไปรษณีย์"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Country</label> 
                  <asp:DropDownList ID="ddlCountry" runat="server" cssclass="form-control select2" Width="100%" >
                      <asp:ListItem Selected="True" Value="TH">ประเทศไทย</asp:ListItem>
                  </asp:DropDownList>
                   
                 </div>
            </div>     
            </div>
            <div class="row">             
          
               <div class="col-md-3">
              <div class="form-group">
                <label>Telephone</label> <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Fax</label> <asp:TextBox ID="txtFax" runat="server" cssclass="form-control" placeholder="แฟกซ์"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>E-mail</label> <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
                 </div>
            </div>
 
                     <div class="col-md-3">
              <div class="form-group">
                <label>Website</label> <asp:TextBox ID="txtWebsite" runat="server" cssclass="form-control" placeholder="เว็บไซต์"></asp:TextBox>
                 </div>
            </div>
 
            </div>
          <h5 class="text-info">Address display (for report)</h5>
    <div class="row">             
          
               <div class="col-md-6">
              <div class="form-group">
                <label>ภาษาไทย</label> <asp:TextBox ID="txtAddressTha" runat="server" cssclass="form-control" placeholder="ที่อยู่ภาษาไทย"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-6">
              <div class="form-group">
                <label>English</label> <asp:TextBox ID="txtAddressEng" runat="server" cssclass="form-control" placeholder="ที่อยู่ภาษาอังกฤษ"></asp:TextBox>
                 </div>
            </div>               
 
            </div> 
              
             
          </div>
        </div>
     
        </form> 

      </div>
    </div>
    <!--- End Modal ---> 
    </section>
</asp:Content>
