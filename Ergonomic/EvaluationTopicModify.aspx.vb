﻿Public Class EvaluationTopicModify
    Inherits System.Web.UI.Page

    Dim ctlLG As New ElearningController
    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            ClearData()
            pnAnswer.Visible = False
            cmdDelete.Visible = False
            cmdSaveA.Visible = False
            cmdSaveQ.Visible = True
            LoadCourse()

            If Not Request("id") Is Nothing Then
                EditData(Request("id"))
            End If
        End If
        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")
    End Sub
    Private Sub LoadCourse()
        ddlCourse.DataSource = ctlLG.Course_GetActive
        ddlCourse.DataTextField = "Name"
        ddlCourse.DataValueField = "UID"
        ddlCourse.DataBind()
    End Sub
    Private Sub EditData(ByVal pID As Integer)

        dt = ctlLG.EvaluationTopic_GetByUID(pID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Me.lblID.Text = DBNull2Str(.Item("UID"))
                ddlCourse.SelectedValue = .Item("CourseUID")
                txtName.Text = DBNull2Str(.Item("Descriptions"))
                chkStatus.Checked = ConvertStatusFlag2CHK(DBNull2Str(.Item("StatusFlag")))
                txtDisplayOrder.Text = DBNull2Str(.Item("DisplayOrder"))
            End With

            pnAnswer.Visible = True
            cmdDelete.Visible = True
            cmdSaveA.Visible = True
            cmdSaveQ.Visible = False

        End If

        dt = ctlLG.EvaluationAnswer_Get(lblID.Text)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                With dt.Rows(i)
                    Select Case DBNull2Zero(.Item("AnswerID"))
                        Case 1
                            txtAnswer1.Text = String.Concat(.Item("Descriptions"))
                        Case 2
                            txtAnswer2.Text = String.Concat(.Item("Descriptions"))
                        Case 3
                            txtAnswer3.Text = String.Concat(.Item("Descriptions"))
                        Case 4
                            txtAnswer4.Text = String.Concat(.Item("Descriptions"))
                        Case 5
                            txtAnswer5.Text = String.Concat(.Item("Descriptions"))
                    End Select

                    If String.Concat(.Item("isAnswer")) = "Y" Then
                        ddlAnswer.SelectedValue = .Item("AnswerCode")
                    End If

                End With
            Next
        End If

        dt = Nothing
    End Sub
    Private Sub ClearData()
        Me.lblID.Text = ""
        txtName.Text = ""
        chkStatus.Checked = True
        ddlCourse.SelectedIndex = 0
        txtDisplayOrder.Text = "0"

        txtAnswer1.Text = ""
        txtAnswer2.Text = ""
        txtAnswer3.Text = ""
        txtAnswer4.Text = ""
        txtAnswer5.Text = ""
        ddlAnswer.SelectedIndex = 0

        pnAnswer.Visible = False
        cmdDelete.Visible = False
        cmdSaveA.Visible = False
        cmdSaveQ.Visible = True
    End Sub


    Protected Sub cmdSaveQ_Click(sender As Object, e As EventArgs) Handles cmdSaveQ.Click
        If ddlCourse.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','กรุณาหลักสูตร/บทเรียนก่อน');", True)
            Exit Sub
        End If

        If txtName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','กรุณากรอกคำถามก่อน');", True)
            Exit Sub
        End If

        ctlLG.EvaluationTopic_Save(StrNull2Zero(lblID.Text), ddlCourse.SelectedValue, txtName.Text, StrNull2Zero(txtDisplayOrder.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))

        lblID.Text = ctlLG.EvaluationTopic_GetUID(ddlCourse.SelectedValue, txtName.Text).ToString()


        pnAnswer.Visible = True
        cmdDelete.Visible = True
        cmdSaveA.Visible = True
        cmdSaveQ.Visible = False
    End Sub
    Protected Sub cmdSaveA_Click(sender As Object, e As EventArgs) Handles cmdSaveA.Click

        ctlLG.EvaluationTopic_Save(StrNull2Zero(lblID.Text), ddlCourse.SelectedValue, txtName.Text, StrNull2Zero(txtDisplayOrder.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))


        If txtAnswer1.Text <> "" Then
            ctlLG.EvaluationAnswer_Save(1, ddlCourse.SelectedValue, StrNull2Zero(lblID.Text), txtAnswer1.Text, "ก", IIf(ddlAnswer.SelectedValue = "ก", "Y", "N"))
        End If
        If txtAnswer2.Text <> "" Then
            ctlLG.EvaluationAnswer_Save(2, ddlCourse.SelectedValue, StrNull2Zero(lblID.Text), txtAnswer2.Text, "ข", IIf(ddlAnswer.SelectedValue = "ข", "Y", "N"))
        End If
        If txtAnswer3.Text <> "" Then
            ctlLG.EvaluationAnswer_Save(3, ddlCourse.SelectedValue, StrNull2Zero(lblID.Text), txtAnswer3.Text, "ค", IIf(ddlAnswer.SelectedValue = "ค", "Y", "N"))
        End If
        If txtAnswer4.Text <> "" Then
            ctlLG.EvaluationAnswer_Save(4, ddlCourse.SelectedValue, StrNull2Zero(lblID.Text), txtAnswer4.Text, "ง", IIf(ddlAnswer.SelectedValue = "ง", "Y", "N"))
        End If
        If txtAnswer5.Text <> "" Then
            ctlLG.EvaluationAnswer_Save(5, ddlCourse.SelectedValue, StrNull2Zero(lblID.Text), txtAnswer5.Text, "จ", IIf(ddlAnswer.SelectedValue = "จ", "Y", "N"))
        End If



        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub

    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        ClearData()
    End Sub

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlLG.EvaluationTopic_Delete(lblID.Text)
        ClearData()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
    End Sub
End Class

