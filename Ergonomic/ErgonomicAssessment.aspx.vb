﻿Public Class ErgonomicAssessment
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    'Dim ctlE As New CompanyController
    'Dim ctlM As New MasterController
    Dim ctlT As New TaskController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadTask()
        End If
    End Sub

    Private Sub LoadTask()

        ddlTask.DataSource = ctlT.Task_Get(Request.Cookies("Ergo")("LoginCompanyUID"))
        ddlTask.DataTextField = "TaskName"
        ddlTask.DataValueField = "UID"
        ddlTask.DataBind()
    End Sub


    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        'If txtCompanyID.Text = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุรหัสบริษัท');", True)
        '    DisplayMessage(Me.Page, "กรุณาระบุรหัสบริษัท")
        'End If
        'If txtName.Text = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อบริษัท');", True)
        '    DisplayMessage(Me.Page, "กรุณาระบุชื่อบริษัท")
        'End If


        'ctlE.Company_Save(StrNull2Zero(hdCompUID.Value), txtCompanyID.Text, txtName.Text, txtBranch.Text, txtAddress.Text, txtTel.Text, txtEmail.Text, ConvertBoolean2StatusFlag(chkStatus.Checked), Request.Cookies("Ergo")("userid"))

        'Dim ctlU As New UserController
        'ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_UPD, "Customer", "บันทึก/แก้ไข ประวัติส่วนตัว :{uid=" & hdCompUID.Value & "}{code=" & txtCompanyID.Text & "}", "")

        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        Session("asmtask") = ddlTask.SelectedValue
        Session("asmtype") = optAsmType.SelectedValue

        Response.Redirect("AsmMenu.aspx?t=" & ddlTask.SelectedValue & "&ptype=" & optAsmType.SelectedValue)

    End Sub
End Class