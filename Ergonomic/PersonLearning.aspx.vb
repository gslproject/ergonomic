﻿Public Class PersonLearning
    Inherits System.Web.UI.Page
    Public dtLearn As New DataTable
    Dim ctlP As New PersonController
    'Dim ctlM As New MasterController
    Dim ctlE As New ElearningController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadPersonLearning(Request.Cookies("Ergo")("LoginPersonUID"))
        End If
    End Sub


    Private Sub LoadPersonLearning(PersonUID As Integer)
        dtLearn = ctlE.PersonLearning_Get(PersonUID)
    End Sub
End Class