﻿Imports Microsoft.ApplicationBlocks.Data

Public Class ReportController
    Inherits BaseClass
    Dim ds As New DataSet

#Region "Chart"

    Public Function RPT_PersonBodyRisk_GetCount(CompanyUID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_PersonBodyRisk_GetCount"), CompanyUID)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function
    Public Function RPT_PersonBodyRisk_GetCountByDivision(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_PersonBodyRisk_GetCountByDivision"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_PersonBodyRisk_GetCountByDepartment(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_PersonBodyRisk_GetCountByDepartment"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function RPT_PersonBodyRisk_GetCountScore(CompanyUID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_PersonBodyRisk_GetCountScore"), CompanyUID)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_MSDs_GetRiskCount(CompanyUID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_MSDs_GetRiskCount"), CompanyUID)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_Ergonomic_GetRiskCount(CompanyUID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Ergonomic_GetRiskCount"), CompanyUID)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function
    Public Function RPT_MSD(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_MSD"), CompanyUID)
        Return ds.Tables(0)
    End Function


#End Region


End Class
