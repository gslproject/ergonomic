﻿Imports Microsoft.ApplicationBlocks.Data
Public Class AssessmentController
    Inherits BaseClass
    Public ds As New DataSet
    Public Function AssessmentAnswer_GetScore(Code As String, TopicUID As Integer, AnswerValue As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AssessmentAnswer_GetScore"), Code, TopicUID, AnswerValue)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function ScoreROSA_GetScore(Code As String, RowScore As Integer, ColScore As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("ScoreROSA_GetScore"), Code, RowScore, ColScore)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function

    Public Function Assessment_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_Get"))
        Return ds.Tables(0)
    End Function

    Public Function Assessment_GetByUID(Code As String, UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetByUID"), Code, UID)
        Return ds.Tables(0)
    End Function

    Public Function Assessment_GetByPersonUID(PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetByPersonUID"), PersonUID)
        Return ds.Tables(0)
    End Function
    Public Function Assessment_GetByCompany(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Assessment_GetByCompany"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function AsmROSA_Save(ByVal UID As Integer, ByVal TaskNo As String, ByVal AsmType As String, ByVal PersonUID As Integer, ByVal AsmDate As String, ByVal Remark As String, ByVal MonitorUse As String, ByVal PhoneUser As String, ByVal KeyboardUse As String, ByVal MouseUse As String, ByVal S1 As String, ByVal S2 As String, ByVal S3 As String, ByVal S4 As String, ByVal S5 As String, ByVal S6 As String, ByVal S7 As String, ByVal S8 As String, S1A As String, S2A As String, S3A As String, S4A As String, S5A As String, S6A As String, S7A As String, S8A As String, ByVal FinalScore As Integer, ByVal ResultText As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmROSA_Save"), UID, TaskNo, AsmType, PersonUID, AsmDate, Remark, MonitorUse, PhoneUser, KeyboardUse, MouseUse, S1, S2, S3, S4, S5, S6, S7, S8, S1A, S2A, S3A, S4A, S5A, S6A, S7A, S8A, FinalScore, ResultText, CUser)

    End Function

    Public Function AsmREBA_Save(ByVal UID As Integer, ByVal TaskNo As String, ByVal AsmType As String, ByVal PersonUID As Integer, ByVal AsmDate As String, ByVal Remark As String, ByVal S1 As String, ByVal S2 As String, ByVal S3 As String, ByVal S5 As String, ByVal S7 As String, ByVal S8 As String, S9 As String, S11 As String, S13 As String, S1A As String, S2A As String, S3A As String, S5A As String, S7A As String, S9A As String, ByVal FinalScore As Integer, ByVal ResultText As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmREBA_Save"), UID, TaskNo, AsmType, PersonUID, AsmDate, Remark, S1, S2, S3, S5, S7, S8, S9, S11, S13, S1A, S2A, S3A, S5A, S7A, S9A, FinalScore, ResultText, CUser)

    End Function

    Public Function AsmRULA_Save(ByVal UID As Integer, ByVal TaskNo As String, ByVal AsmType As String, ByVal PersonUID As Integer, ByVal AsmDate As String, ByVal Remark As String, ByVal S1 As String, ByVal S2 As String, ByVal S3 As String, ByVal S4 As String, ByVal S6 As String, ByVal S7 As String, ByVal S9 As String, ByVal S10 As String, ByVal S11 As String, ByVal S13 As String, ByVal S14 As String, S1A As String, S2A As String, S3A As String, S9A As String, S10A As String, ByVal FinalScore As Integer, ByVal ResultText As String, ByVal CUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmRULA_Save"), UID, TaskNo, AsmType, PersonUID, AsmDate, Remark, S1, S2, S3, S4, S6, S7, S9, S10, S11, S13, S14, S1A, S2A, S3A, S9A, S10A, FinalScore, ResultText, CUser)
    End Function


    Public Function AsmROSA_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmROSA_Delete"), UID)
    End Function
    Public Function AsmREBA_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmREBA_Delete"), UID)
    End Function
    Public Function AsmRULA_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmRULA_Delete"), UID)
    End Function
    Public Function AsmNIOSH_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmNIOSH_Delete"), UID)
    End Function




    Public Function ScoreREBA_GetScore(Code As String, RowScore As Integer, ColScore1 As Integer, ColScore2 As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("ScoreREBA_GetScore"), Code, RowScore, ColScore1, ColScore2)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function

    Public Function ScoreRULA_GetScore(Code As String, RowScore1 As Integer, RowScore2 As Integer, ColScore1 As Integer, ColScore2 As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("ScoreRULA_GetScore"), Code, RowScore1, RowScore2, ColScore1, ColScore2)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function


    Public Function NIOSH_GetFMScore(F As Double, W As Integer, V As Integer) As Double
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("NIOSH_GetFMScore"), F, W, V)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Dbl(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function AsmNIOSH_Save(ByVal UID As Integer, ByVal TaskNo As String, ByVal AsmType As String, ByVal PersonUID As Integer, ByVal AsmDate As String, ByVal Remark As String, ByVal L As Double, ByVal H As Double, ByVal V As Double, ByVal D As Double, ByVal A As Double, ByVal C As Double, ByVal Dur As Double, ByVal F As Double, ByVal LC As Double, ByVal HM As Double, ByVal VM As Double, DM As Double, AM As Double, CM As Double, FM As Double, RWL As Double, ByVal LI As Double, ByVal RecommendText As String, ByVal CUser As Integer, ByVal FIRWL As Double, ByVal FILI As Double) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmNIOSH_Save"), UID, TaskNo, AsmType, PersonUID, AsmDate, Remark, L, H, V, D, A, C, Dur, F, LC, HM, VM, DM, AM, CM, FM, RWL, LI, RecommendText, CUser, FIRWL, FILI)
    End Function

#Region "HRA"
    Public Function AsmHRA_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AsmHRA_Get"), CompanyUID)

        Return ds.Tables(0)

    End Function
    Public Function AsmHRA_GetByUID(UID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AsmHRA_GetByUID"), UID)

        Return ds.Tables(0)

    End Function

    Public Function AsmHRA_GetByPerson(PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("AsmHRA_GetByPerson"), PersonUID)

        Return ds.Tables(0)

    End Function

    Public Function AsmHRA_Save(ByVal UID As Long, ByVal PersonUID As Integer, ByVal ErgoScore As Double, ByVal ErgoLevel As Integer, ByVal ErgoResultText As String, ByVal HRAScore As Double, ByVal HRALevel As Integer, ByVal HRAResultText As String, ByVal Remark As String, ByVal MUser As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("AsmHRA_Save"), UID, PersonUID, ErgoScore, ErgoLevel, ErgoResultText, HRAScore, HRALevel, HRAResultText, Remark, MUser)
    End Function


#End Region

End Class
