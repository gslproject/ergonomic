﻿Imports Microsoft.ApplicationBlocks.Data
Public Class TaskController
    Inherits BaseClass
    Public ds As New DataSet
    Public Function Task_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Task_Get"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function Task_GetByCompany(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Task_GetByCompany"), CompanyUID)
        Return ds.Tables(0)
    End Function
    Public Function Task_GetByUser(UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Task_GetByUser"), UserID)
        Return ds.Tables(0)
    End Function
    Public Function Task_GetActive(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Task_GetActive"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function Task_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Task_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function

    Public Function Task_GetUIDByCode(CompanyUID As Integer, pCode As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Task_GetUIDByCode"), CompanyUID, pCode)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function

    Public Function Task_Save(ByVal UID As Integer, ByVal TaskNo As String, ByVal TaskName As String, ByVal MachineUID As String, ByVal Descriptions As String, CompanyUID As String, SectionUID As String, DepartmentUID As String, PositionUID As Integer, PPE As String, Hot As String, Cold As String, Noise As String, Chemical As String, Biological As String, REBA As String, ROSA As String, RULA As String, NIOSH As String, Status As String, ByVal UpdBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Task_Save"), UID, TaskNo, TaskName, MachineUID, Descriptions, CompanyUID, SectionUID, DepartmentUID, PositionUID, PPE, Hot, Cold, Noise, Chemical, Biological, REBA, ROSA, RULA, NIOSH, Status, UpdBy)
    End Function

    Public Function Task_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Task_Delete"), UID)
    End Function


    Public Function Task_UpdateFileName(ByVal UID As Integer, ByVal TaskNo As String, ByVal Name As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Task_UpdateFileName"), UID, TaskNo, Name)
    End Function

    Public Function TaskImage_Get(TaskUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("TaskImage_Get"), TaskUID)
        Return ds.Tables(0)
    End Function

    Public Function TaskImage_Delete(ByVal UID As Integer, ByVal TaskNo As String, ByVal Name As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("TaskImage_Delete"), UID, TaskNo, Name)
    End Function
    'Public Function Task_UpdateFileName(ByVal UID As Integer, ByVal TaskNo As String, ByVal Name As String) As Integer
    '    Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Task_UpdateFileName"), UID, TaskNo, Name)
    'End Function


    Public Function JobOwner_Get(CompanyUID As Integer, TaskUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("JobOwner_Get"), CompanyUID, TaskUID)
        Return ds.Tables(0)
    End Function
    Public Function JobOwner_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("JobOwner_GetByUID"), UID)
        Return ds.Tables(0)
    End Function

    Public Function JobOwner_Save(ByVal UID As Integer, ByVal TaskUID As Integer, ByVal PersonUID As Integer, CompanyUID As Integer, LevelID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("JobOwner_Save"), UID, TaskUID, PersonUID, CompanyUID, LevelID)
    End Function

    Public Function JobOwner_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("JobOwner_Delete"), UID)
    End Function

    Public Function Task_Problem_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Task_Problem_Get"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function TaskAction_Get(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("TaskAction_Get"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function TaskOwner_GetEmailAlert(CompanyUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("TaskOwner_GetEmailAlert"), CompanyUID)
        Return ds.Tables(0)
    End Function

    Public Function SendAlert_Save(ByVal CompanyUID As Integer, TaskUID As Integer, PersonUID As Integer, email As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SendAlert_Save"), CompanyUID, TaskUID, PersonUID, email, Status)
    End Function
    Public Function SendAlert_UpdateStatus(ByVal CompanyUID As Integer, TaskUID As Integer, PersonUID As Integer, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("SendAlert_UpdateStatus"), CompanyUID, TaskUID, PersonUID, Status)
    End Function



    Public Function TaskAction_GetByTaskUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("TaskAction_GetByTaskUID"), pUID)
        Return ds.Tables(0)
    End Function


    Public Function TaskAction_Save(ByVal UID As Integer, ByVal CompanyUID As Integer, ByVal TaskUID As Integer, ByVal ActionUID As String, ByVal OwnerUID As String, ByVal Duedate As String, ActionStatus As String, Comment As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("TaskAction_Save"), UID, CompanyUID, TaskUID, ActionUID, OwnerUID, Duedate, ActionStatus, Comment)
    End Function

    Public Function Event_GetByAdmin(ByVal CompanyUID As Integer, ByVal PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Event_GetByAdmin"), CompanyUID, PersonUID)
        Return ds.Tables(0)
    End Function
    Public Function Event_GetByUser(ByVal CompanyUID As Integer, ByVal PersonUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Event_GetByUser"), CompanyUID, PersonUID)
        Return ds.Tables(0)
    End Function


    Public Function RPT_Task_Assessment(ByVal CompanyUID As Integer, Bdate As Integer, Edate As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("RPT_Task_Assessment"), CompanyUID, Bdate, Edate)
        Return ds.Tables(0)
    End Function

End Class
