﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AsmROSA.aspx.vb" Inherits="Ergonomic.AsmROSA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>การประเมินความเสี่ยงโดยวิธี ROSA</h1>     
    </section>
    <section class="content">  

  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">ErgonomicAssessment<asp:HiddenField ID="hdUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

      <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>Task No</label>
              <asp:TextBox ID="txtTaskNo" runat="server" cssclass="form-control" placeholder="Task no." ReadOnly="True"></asp:TextBox>
          </div>

        </div>

        <div class="col-md-10">
          <div class="form-group">
            <label>Task Name</label>
              <asp:TextBox ID="txtTaskName" runat="server" cssclass="form-control" placeholder="Task no." ReadOnly="True"></asp:TextBox>
          </div>

        </div>
      
      </div>
      <div class="row">
  <div class="col-md-3">
          <div class="form-group">
            <label>Date</label>
              <asp:TextBox ID="txtAsmDate" runat="server"  CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
          </div>

        </div> 

        <div class="col-md-9">
          <div class="form-group">
            <label>Type</label>
              <asp:RadioButtonList ID="optAsmType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="S">Self-assessment</asp:ListItem>
                  <asp:ListItem Value="O">Observer assessment</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 

      </div>
   
        <div class="row">        
       <div class="col-md-12">
          <div class="form-group">
            <label>ผู้รับการประเมิน</label>
              <asp:DropDownList ID="ddlPerson" runat="server" cssclass="form-control select2"  placeholder="Select Person" Width="100%">
            </asp:DropDownList> 
          </div>

        </div> 
</div>
          <div class="row">        
       <div class="col-md-12">
          <div class="form-group">
            <label>Remark</label>
              <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" Height="50px" TextMode="MultiLine"></asp:TextBox>
          </div>

        </div> 
</div>

        <div class="row">
             <div class="col-md-3">
          <div class="form-group">
            <label class="text-blue">The Monitor in used</label>
              <asp:DropDownList ID="ddlUseMonitor" runat="server" cssclass="form-control select2"  placeholder="Select Person" Width="100%">
                  <asp:ListItem Value="-1">ใช้อุปกรณ์ต่ำกว่า 1 ชม./วัน (ไม่ต่อเนื่อง) หรือ ใช้ต่ำกว่า 30 นาทีอย่างต่อเนื่อง</asp:ListItem>
                  <asp:ListItem Value="0">ใช้อุปกรณื 1-4 ชม./วัน (ไม่ต่อเนื่อง) หรือใช้ 30 นาที - 1 ชม. อย่างต่อเนื่อง</asp:ListItem>
                  <asp:ListItem Value="1">ใช้อุปกรณ์มากว่า 4 ชม./วัน (ไม่ต่อเนื่อง) หรือใช้มากกว่า 1 ชม. อย่างต่อเนื่อง</asp:ListItem>
            </asp:DropDownList> 
          </div>

        </div> 

               <div class="col-md-3">
          <div class="form-group">
            <label class="text-blue">The Phone in used</label>
              <asp:DropDownList ID="ddlUsePhone" runat="server" cssclass="form-control select2"  placeholder="Select Person" Width="100%">
                  <asp:ListItem Value="-1">ใช้อุปกรณ์ต่ำกว่า 1 ชม./วัน (ไม่ต่อเนื่อง) หรือ ใช้ต่ำกว่า 30 นาทีอย่างต่อเนื่อง</asp:ListItem>
                  <asp:ListItem Value="0">ใช้อุปกรณื 1-4 ชม./วัน (ไม่ต่อเนื่อง) หรือใช้ 30 นาที - 1 ชม. อย่างต่อเนื่อง</asp:ListItem>
                  <asp:ListItem Value="1">ใช้อุปกรณ์มากว่า 4 ชม./วัน (ไม่ต่อเนื่อง) หรือใช้มากกว่า 1 ชม. อย่างต่อเนื่อง</asp:ListItem>
            </asp:DropDownList> 
          </div>

        </div> 
               <div class="col-md-3">
          <div class="form-group">
            <label class="text-blue">The Keyboard in used</label>
              <asp:DropDownList ID="ddlUseKeyboard" runat="server" cssclass="form-control select2"  placeholder="Select Person" Width="100%">
                  <asp:ListItem Value="-1">ใช้อุปกรณ์ต่ำกว่า 1 ชม./วัน (ไม่ต่อเนื่อง) หรือ ใช้ต่ำกว่า 30 นาทีอย่างต่อเนื่อง</asp:ListItem>
                  <asp:ListItem Value="0">ใช้อุปกรณื 1-4 ชม./วัน (ไม่ต่อเนื่อง) หรือใช้ 30 นาที - 1 ชม. อย่างต่อเนื่อง</asp:ListItem>
                  <asp:ListItem Value="1">ใช้อุปกรณ์มากว่า 4 ชม./วัน (ไม่ต่อเนื่อง) หรือใช้มากกว่า 1 ชม. อย่างต่อเนื่อง</asp:ListItem>
            </asp:DropDownList> 
          </div>

        </div> 
               <div class="col-md-3">
          <div class="form-group">
            <label class="text-blue">The Mouse in used</label>
              <asp:DropDownList ID="ddlUseMouse" runat="server" cssclass="form-control select2"  placeholder="Select Person" Width="100%">
                  <asp:ListItem Value="-1">ใช้อุปกรณ์ต่ำกว่า 1 ชม./วัน (ไม่ต่อเนื่อง) หรือ ใช้ต่ำกว่า 30 นาทีอย่างต่อเนื่อง</asp:ListItem>
                  <asp:ListItem Value="0">ใช้อุปกรณื 1-4 ชม./วัน (ไม่ต่อเนื่อง) หรือใช้ 30 นาที - 1 ชม. อย่างต่อเนื่อง</asp:ListItem>
                  <asp:ListItem Value="1">ใช้อุปกรณ์มากว่า 4 ชม./วัน (ไม่ต่อเนื่อง) หรือใช้มากกว่า 1 ชม. อย่างต่อเนื่อง</asp:ListItem>
            </asp:DropDownList> 
          </div>

        </div> 

        </div>

 <h5 class="text-blue text-bold">ขั้นตอนที่ 1 การประเมินความสูงของเก้าอี้ (Chair height)</h5>
        <div class="row">
             
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRosa1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img  height="140" src="images/rosa/rosa1_01.jpg" title="เก้าอี้สูงพอเหมาะ" class="vtip" /><br /><br />เก้าอี้สูงพอเหมาะ ข้อพับเข่ามีมุม 90 ํ(1)</asp:ListItem>
                    <asp:ListItem Value="2"><img  height="140" src="images/rosa/rosa1_02.jpg" title="เก้าอี้ต่ำเกินไป" class="vtip" /><br /><br />เก้าอี้ต่ำเกินไป(2)</asp:ListItem>
                    <asp:ListItem Value="3"><img  height="140" src="images/rosa/rosa1_03.jpg" title="เก้าอี้สูงมากเกินไป" class="vtip" /><br /><br />เก้าอี้สูงมากเกินไป(2)</asp:ListItem>
                    <asp:ListItem Value="4"><img  height="140" src="images/rosa/rosa1_04.jpg" title="เก้าอี้สูงมากจนทำให้เท้าของผู้นั่งแตะไม่ถึงพื้น" class="vtip" /><br /><br />เก้าอี้สูงมากจนทำให้เท้าของ<br />ผู้นั่งแตะไม่ถึงพื้น(3)</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 

                  <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
              <asp:CheckBoxList ID="chkRosaAdd1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
 <asp:ListItem Value="A1"><img title="พื้นที่ใต้โต๊ะคับแคบไม่สามารถไขว้ขาได้" class="vtip"   height="140" src="images/rosa/rosa1_05.jpg" /> <br /><br />พื้นที่ใต้โต๊ะคับแคบไม่สามารถไขว้ขาได้(+1)</asp:ListItem>
                    <asp:ListItem Value="A2"><img title="เก้าอี้ไม่สามารถปรับความสูงได้" class="vtip"   height="140" src="images/asmblank.jpg" /><br /><br/>เก้าอี้ไม่สามารถปรับความสูงได้(+1)</asp:ListItem>
              </asp:CheckBoxList>
             
          </div>

        </div> 

            </div>
        <h5 class="text-blue text-bold">ขั้นตอนที่ 2 การประเมินความลึกของที่นั่ง (Pan depth)</h5>
        <div class="row">
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRosa2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="ช่องว่างระหว่างข้อพับเข่าและขอบของที่นั่งประมาณ 5-7 ซม." class="vtip"   height="140" src="images/rosa/rosa2_01.jpg" /> <br /><br />ช่องว่างระหว่างข้อพับเข่าและ<br />ขอบของที่นั่งประมาณ 5-7 ซม.(1)</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ที่นั่งยาวเกินไป นั่นคือ ช่องว่าง น้อยกว่า 5 ซม." class="vtip"   height="140" src="images/rosa/rosa2_02.jpg" /> <br /><br />ที่นั่งยาวเกินไป ช่องว่าง < 5 ซม.(2)</asp:ListItem>
                    <asp:ListItem Value="3"><img title="ที่นั่งสั้นเกินไป นั่นคือ ช่องว่าง  มากกว่า 7 ซม." class="vtip"   height="140" src="images/rosa/rosa2_03.jpg" /> <br /><br />ที่นั่งสั้นเกินไป ช่องว่าง > 7 ซม.(2)</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
                 <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRosaAdd2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                    <asp:ListItem Value="A1"><img title="ไม่สามารถปรับระยะระหว่างข้อพับเข่า และขอบที่นั่งได้" class="vtip"   height="140" src="images/asmblank.jpg" /> <br /><br />ไม่สามารถปรับระยะระหว่างข้อพับเข่า และขอบที่นั่งได้ (+1)</asp:ListItem>
              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
        <h5 class="text-blue text-bold">ขั้นตอนที่ 3 การประเมินที่พักแขน (Armrest)</h5>
         <div class="row">
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRosa3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="ลักษณะข้อศอกมีมุมประมาณ 90 ํ และไหล่ดูผ่อนคลาย" class="vtip"   height="140" src="images/rosa/rosa3_01.jpg" /> <br /><br />ลักษณะข้อศอกมีมุมประมาณ 90 ํ<br />และไหล่ดูผ่อนคลาย(1)</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ที่พักแขนสูงเกินไป ไหล่อยู่ในลักษณะยกขึ้น" class="vtip"   height="140" src="images/rosa/rosa3_02.jpg" /> <br /><br />ที่พักแขนสูงเกินไป<br />ไหล่อยู่ในลักษณะยกขึ้น(2)</asp:ListItem>
                    <asp:ListItem Value="3"><img title="ที่พักแขนต่ำเกินไป ข้อศอกไม่มีที่รองรับ" class="vtip"   height="140" src="images/rosa/rosa3_01.jpg" /> <br /><br />ที่พักแขนต่ำเกินไป <br />ข้อศอกไม่มีที่รองรับ</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
    <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRosaAdd3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                    <asp:ListItem Value="A1"><img title="ที่พักแขนมีพื้นผิวแข็งเกินไปหรือ ชำรุดเสียหาย ทำให้วางได้ไม่เต็มทั้งแขน" class="vtip"   height="140" src="images/rosa/rosa3_03.jpg" /> <br /><br />ที่พักแขนมีพื้นผิวแข็งเกินไปหรือ<br />ชำรุดเสียหายทำให้วางได้ไม่เต็มทั้งแขน(+1)</asp:ListItem>
                    <asp:ListItem Value="A2"><img title="ระยะของที่พักแขนกว้างเกินไป" class="vtip"   height="140" src="images/rosa/rosa3_04.jpg" /> <br /><br />ระยะของที่พักแขนกว้างเกินไป(+1)</asp:ListItem>
                     <asp:ListItem Value="A3"><img title="ที่พักแขนปรับไม่ได้" class="vtip"   height="140" src="images/asmblank.jpg" /> <br /><br />ที่พักแขนปรับไม่ได้(+1)</asp:ListItem>

              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
        <h5 class="text-blue text-bold">ขั้นตอนที่ 4 การประเมินพนักพิง (Backrest)</h5>
           <div class="row">
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRosa4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="มีพนักพิงที่เหมาะสม มีที่รองเอว พนักพิงเอียง 95-100 องศา" class="vtip"   height="140" src="images/rosa/rosa4_01.jpg" /> <br /><br />มีพนักพิงที่เหมาะสม มีที่รองเอว <br />พนักพิงเอียง 95-100 ํ (1)</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ไม่มีที่รองเอว หรือที่รองเอวไม่ได้อยู่ในตำแหน่งที่เหมาะสม" class="vtip"   height="140" src="images/rosa/rosa4_02.jpg" /> <br /><br />ไม่มีที่รองเอว หรือที่รองเอวไม่ได้<br />อยู่ในตำแหน่งที่เหมาะสม(2)</asp:ListItem>
                    <asp:ListItem Value="3"><img title="พนักพิงเอียง >110 หรือ <95 องศา" class="vtip"   height="140" src="images/rosa/rosa4_03.jpg" /> <br /><br />พนักพิงเอียง >110 หรือ <95 องศา(2)</asp:ListItem>
                    <asp:ListItem Value="4"><img title="ไม่มีพนักพิง (มีท่าทางการนั่งที่ไม่เหมาะสม)" class="vtip"   height="140" src="images/rosa/rosa4_04.jpg" /> <br /><br />ไม่มีพนักพิง มีท่าทางการนั่งที่ไม่เหมาะสม(2)</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
    <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRosaAdd4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                     <asp:ListItem Value="A1"><img title="พื้นโต๊ะทำงานสูงเกินไป (ใช้อยู่ในลักษณะยกไหล่)" class="vtip"   height="140" src="images/rosa/rosa4_05.jpg" /> <br /><br />พื้นโต๊ะทำงานสูงเกินไป (ใช้อยู่ในลักษณะยกไหล่)(+1)</asp:ListItem>
                    <asp:ListItem Value="A2"><img title="พนักพิงปรับไม่ได้" class="vtip"   height="140" src="images/asmblank.jpg" /> <br /><br />พนักพิงปรับไม่ได้(+1)</asp:ListItem>
              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
           <h5 class="text-blue text-bold">ขั้นตอนที่ 5 การประเมินหน้าจอ (Monitor)</h5>
     <div class="row">
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRosa5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="หน้าจอมีระยะประมาณความยาวแขน (40-75 ซม.) และหน้าจออยู่ระดับสายตาผู้ใช้" class="vtip"   height="140" src="images/rosa/rosa5_01.jpg" /> <br /><br />หน้าจอมีระยะประมาณความยาวแขน<br />(40-75 ซม.) และหน้าจออยู่ระดับสายตาผู้ใช้(1)</asp:ListItem>
                    <asp:ListItem Value="2"><img title="หน้าจอต่ำเกินไป (ทำให้ต้องก้มคอเพื่อมองจอภาพ)" class="vtip"   height="140" src="images/rosa/rosa5_02.jpg" /> <br /><br />หน้าจอต่ำเกินไป <br />(ทำให้ต้องก้มคอเพื่อมองจอภาพ)(2)</asp:ListItem>
                    <asp:ListItem Value="3"><img title="หน้าจอสูงเกินไป (ทำให้ต้องเงยคอเพื่อมองจอภาพ)" class="vtip"   height="140" src="images/rosa/rosa5_03.jpg" /> <br /><br />หน้าจอสูงเกินไป <br />(ทำให้ต้องก้มคอเพื่อมองจอภาพ)(3)</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRosaAdd5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                    <asp:ListItem Value="A1"><img title="ผู้ใช้ต้องหมุนคอเพื่อมองจอภาพ" class="vtip"   height="140" src="images/rosa/rosa5_04.jpg" /> <br /><br />ผู้ใช้ต้องหมุนคอเพื่อมองจอภาพ(+1)</asp:ListItem>
                    <asp:ListItem Value="A2"><img title="มีแสงสะท้อนบนหน้าจอ" class="vtip"   height="140" src="images/asmblank.jpg" /> <br /><br />มีแสงสะท้อนบนหน้าจอ(+1)</asp:ListItem>
                    <asp:ListItem Value="A3"><img title="ไม่มีที่แขวนเอกสาร (ถ้าจำเป็น)" class="vtip" height="140" src="images/asmblank.jpg" /> <br /><br />ไม่มีที่แขวนเอกสาร (ถ้าจำเป็น)(+1)</asp:ListItem>
              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
           <h5 class="text-blue text-bold">ขั้นตอนที่ 6 การประเมินโทรศัพท์ (phone)</h5>
        <div class="row">
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRosa6" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="มีการใช้อุปกรณ์สวมศีรษะ (Headset) หรือจับหูฟังด้วยมือและคออยู่ในท่าทางตรง ตำแหน่งของโทรศัพท์อยู่ห่างไม่เกิน 30 ซม." class="vtip"   height="140" src="images/rosa/rosa6_01.jpg" /> <br /><br />มีการใช้อุปกรณ์สวมศีรษะ (Headset)<br /> หรือจับหูฟังด้วยมือและคออยู่ในท่าทางตรง<br /> ตำแหน่งของโทรศัพท์อยู่ห่างไม่เกิน 30 ซม.(1)</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ระยะโทรศัพท์ห่างเกิน 30 ซม." class="vtip"   height="140" src="images/rosa/rosa6_02.jpg" /><br /><br />ระยะโทรศัพท์ห่างเกิน 30 ซม.(2)</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
                 <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRosaAdd6" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  
                    <asp:ListItem Value="A1"><img title="วางหูฟังโทรศัพท์ระหว่าง คอและไหล่เมื่อใช้งาน" class="vtip"   height="140" src="images/rosa/rosa6_03.jpg" /> <br /><br />วางหูฟังโทรศัพท์ระหว่าง<br /> คอและไหล่เมื่อใช้งาน(+2)</asp:ListItem>
                    <asp:ListItem Value="A2"><img title="โทรศัพท์ไม่มีระบบที่ทำงานโดยไร้มือจับ (Hands free) เช่น ลำโพง (Speaker phone) หรือ อุปกรณ์สวมศีรษะ (Headset)" class="vtip"   height="140" src="images/asmblank.jpg" /> <br /><br />โทรศัพท์ไม่มีระบบที่ทำงานโดยไร้มือจับ (Hands free) <br />เช่น ลำโพง (Speaker phone) <br />หรือ อุปกรณ์สวมศีรษะ (Headset)(+1)</asp:ListItem>
              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
           <h5 class="text-blue text-bold">ขั้นตอนที่ 7 การประเมินเมาส์ (Mouse)</h5>
         <div class="row">
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRosa7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="เมาส์อยู่ในแนวเดียวกับไหล่" class="vtip"   height="140" src="images/rosa/rosa7_01.jpg" /> <br /><br />เมาส์อยู่ในแนวเดียวกับไหล่(1)</asp:ListItem>
                    <asp:ListItem Value="2"><img title="เมาส์ไม่ได้อยู่ในแนวเดียวกับไหล่ การเชื่อมถึงเมาส์ไม่สะดวกเช่น อยู่ห่างจากแป้นพิมพ์" class="vtip"   height="140" src="images/rosa/rosa7_02.jpg" /> <br /><br />เมาส์ไม่ได้อยู่ในแนวเดียวกับไหล่ <br />การเชื่อมถึงเมาส์ไม่สะดวกเช่น <br />อยู่ห่างจากแป้นพิมพ์(2)</asp:ListItem> 

              </asp:RadioButtonList>
          </div>

        </div> 
                  <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRosaAdd7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                 
                    <asp:ListItem Value="A1"><img title="เมาส์กับแป้นพิมพ์อยู่ต่างระดับกัน" class="vtip"   height="140" src="images/rosa/rosa7_03.jpg" /> <br /><br />เมาส์กับแป้นพิมพ์อยู่ต่างระดับกัน(+1)</asp:ListItem>
                    <asp:ListItem Value="A2"><img title="เมาส์มีขนาดเล็กเกินไป ต้องใช้นิ้วมือในการเคลื่อนที่เมาส์มากกว่าการใช้ฝ่ามือ" class="vtip"   height="140" src="images/rosa/rosa7_04.jpg" /> <br /><br />เมาส์มีขนาดเล็กเกินไป ต้องใช้<br />นิ้วมือในการเคลื่อนที่เมาส์<br />มากกว่าการใช้ฝ่ามือ(+2)</asp:ListItem>
                     <asp:ListItem Value="A3"><img title="ไม่มีที่รองข้อมือหรือที่รองข้อมือมีพื้นผิวแข็ง หรือมีจุดกดทับในขณะที่ใช้งานเมาส์" class="vtip"   height="140" src="images/rosa/rosa7_05.jpg" /> <br /><br />ไม่มีที่รองข้อมือหรือที่รองข้อมือมีพื้นผิวแข็ง <br />หรือมีจุดกดทับในขณะที่ใช้งานเมาส์(+1)</asp:ListItem>

              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
           <h5 class="text-blue text-bold">ขั้นตอนที่ 8 การประเมินแป้นพิมพ์ (Key board)</h5>
           <div class="row">
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRosa8" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="ข้อมือตรงและไหล่อยู่ในลักษณะผ่อนคลาย" class="vtip"   height="140" src="images/rosa/rosa8_01.jpg" /> <br /><br />ข้อมือตรงและไหล่อยู่ในลักษณะผ่อนคลาย(1)</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ข้อมืองอขึ้นมากกว่า 15 องศา" class="vtip"   height="140" src="images/rosa/rosa8_02.jpg" /> <br /><br />ข้อมืองอขึ้นมากกว่า 15 องศา(2)</asp:ListItem>
              </asp:RadioButtonList>  
          </div>

        </div> 
                    <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRosaAdd8" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  
                    <asp:ListItem Value="A1"><img title="ข้อมือเบี่ยงออกซ้าย-ขวา ขณะพิมพ์" class="vtip"   height="140" src="images/rosa/rosa8_03.jpg" /> <br /><br />ข้อมือเบี่ยงออกซ้าย-ขวา ขณะพิมพ์(+1)</asp:ListItem>
                    <asp:ListItem Value="A2"><img title="ระดับแป้นพิมพ์สูงเกินไป ไหล่ผู้ใช้อยู่ลักษณะยกขึ้น" class="vtip"   height="140" src="images/rosa/rosa8_04.jpg" /> <br /><br />ระดับแป้นพิมพ์สูงเกินไป <br />ไหล่ผู้ใช้อยู่ลักษณะยกขึ้น(+1)</asp:ListItem> 
                     <asp:ListItem Value="A3"><img title="ต้องมีการใช้งานหรือหยิบอุปกรณ์ที่อยู่ในระดับเหนือศีรษะ" class="vtip"   height="140" src="images/rosa/rosa8_05.jpg" /> <br /><br />ต้องมีการใช้งานหรือหยิบอุปกรณ์<br />ที่อยู่ในระดับเหนือศีรษะ(+1)</asp:ListItem>
                    <asp:ListItem Value="A4"><img title="ที่วางแป้นพิมพ์ปรับระดับไม่ได้" class="vtip"   height="140" src="images/asmblank.jpg" /> <br /><br />ที่วางแป้นพิมพ์ปรับระดับไม่ได้(+1)</asp:ListItem>
              </asp:CheckBoxList>  
          </div>

        </div> 

            </div> 
      
    </div>
    <div class="box-footer clearfix">           
                <!--ที่มา : (Sonne, Villalta, & Andrews, 2012) , http://thai-ergonomic-assessment.blogspot.com-->
            </div>
  </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>             
         
         <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Summary Score         </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

<div class="row">
         <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">ค่าคะแนนการประเมิน</label>
            <h3> <asp:Label ID="lblFinalScore" runat="server" Text="" CssClass="text-blue text-bold" ></asp:Label></h3> 
          </div>

        </div> 
     <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">สรุปแปลผล</label>
            <h3>  <asp:Label ID="lblFinalResult" runat="server" Text=""  CssClass="text-blue text-bold" ></asp:Label></h3>
          </div>

        </div> 

  </div>     

    </div>
             </div>
   </ContentTemplate>

            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="optRosa1" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRosaAdd1" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRosa2" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRosaAdd2" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRosa3" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRosaAdd3" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRosa4" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRosaAdd4" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRosa5" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRosaAdd5" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRosa6" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRosaAdd6" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRosa7" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRosaAdd7" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRosa8" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRosaAdd8" EventName="SelectedIndexChanged" />                
                <asp:AsyncPostBackTrigger ControlID="ddlUseMonitor" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlUsePhone" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlUseKeyboard" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="ddlUseMouse" EventName="SelectedIndexChanged" />
                
            </Triggers>

        </asp:UpdatePanel>
         <div class="row"> 

        <div class="col-md-12 text-center">
  <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="บันทึก" Width="120px" /> 
             &nbsp;<asp:Button ID="cmdCancel" CssClass="btn btn-default" runat="server" Text="ยกเลิก" Width="120px" /> 
            <asp:Button ID="cmdDelete" CssClass="btn btn-danger" runat="server" Text="ลบ" Width="120px" /> 
              </div>
      </div>
</section>
</asp:Content>