﻿Public Class ResultPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        Select Case Request("p")
            Case "person"
                hlkBack.NavigateUrl = "Person?ActionType=emp"
            Case "bio"
                hlkBack.NavigateUrl = "Home?ActionType=h"
            Case "tska"
                hlkBack.NavigateUrl = "TaskAction?ActionType=tsk"
            Case Else
                hlkBack.Visible = False
        End Select


    End Sub

End Class