﻿<%@ Page Title="ข้อมูลบริษัท" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CompanyModify.aspx.vb" Inherits="Ergonomic.CompanyModify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

      <section class="content-header">
      <h1><%: Title %>        
        <small>ข้อมูลบริษัท</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Company</li>
      </ol>
    </section>

    <section class="content">   
           <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Company
                </h3><asp:HiddenField ID="hdCompUID" runat="server" />
            </div>
            <div class="box-body">            

                 <div class="row">
            <div class="col-md-1">
              <div class="form-group">
                <label>Code</label>
                  <asp:TextBox ID="txtCompanyID" runat="server" cssclass="form-control" ></asp:TextBox>   
                 </div> 
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Name (ไทย)</label> <asp:TextBox ID="txtNameTH" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
                 </div> 
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Name (English)</label> <asp:TextBox ID="txtNameEN" runat="server" cssclass="form-control" placeholder="English Name"></asp:TextBox>
                 </div>
            </div>
                      <div class="col-md-2">
              <div class="form-group">
                <label>Branch</label> <asp:TextBox ID="txtBranch" runat="server" cssclass="form-control" placeholder="สาขา"></asp:TextBox>
                 </div>
            </div>
 <div class="col-md-2">
              <div class="form-group">
                <label>TAX ID</label> <asp:TextBox ID="txtTaxID" runat="server" cssclass="form-control" placeholder="เลขประจำตัวผู้เสียภาษี"></asp:TextBox>
                 </div>
            </div> 

</div>
                <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>Address no.</label> <asp:TextBox ID="txtAddressNo" runat="server" cssclass="form-control" placeholder="บ้านเลขที่"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Lane</label> <asp:TextBox ID="txtLane" runat="server" cssclass="form-control" placeholder="ซอย"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Road</label> <asp:TextBox ID="txtRoad" runat="server" cssclass="form-control" placeholder="ถนน"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Sub District</label> <asp:TextBox ID="txtSubDistrict" runat="server" cssclass="form-control" placeholder="ตำบล/แขวง"></asp:TextBox>
                 </div>
            </div>
            </div>
                   <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>district</label> <asp:TextBox ID="txtDistrict" runat="server" cssclass="form-control" placeholder="อำเภอ"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Province</label> 
                  <asp:DropDownList ID="ddlProvince" runat="server" cssclass="form-control select2" Width="100%" ></asp:DropDownList>
                   
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Zip Code</label> <asp:TextBox ID="txtZipcode" runat="server" cssclass="form-control" placeholder="รหัสไปรษณีย์"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Country</label> 
                  <asp:DropDownList ID="ddlCountry" runat="server" cssclass="form-control select2" Width="100%" >
                      <asp:ListItem Selected="True" Value="TH">ประเทศไทย</asp:ListItem>
                  </asp:DropDownList>
                   
                 </div>
            </div>     
            </div>
            <div class="row">             
          
               <div class="col-md-3">
              <div class="form-group">
                <label>Telephone</label> <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Fax</label> <asp:TextBox ID="txtFax" runat="server" cssclass="form-control" placeholder="แฟกซ์"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>E-mail</label> <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
                 </div>
            </div>
 
                     <div class="col-md-3">
              <div class="form-group">
                <label>Website</label> <asp:TextBox ID="txtWebsite" runat="server" cssclass="form-control" placeholder="เว็บไซต์"></asp:TextBox>
                 </div>
            </div> 
                  
            </div>         
        
                <div class="row">
                     <div class="col-md-3">
              <div class="form-group">
                <label>Status</label>  <asp:CheckBox ID="chkStatus" runat="server" Checked="True" Text="Active" />
                 </div>
            </div> 
                </div>
                
            </div> 
             <div class="box-footer  text-center"> 
               
              </div> 
</div>
  <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Company
                </h3><asp:HiddenField ID="HiddenField1" runat="server" />
            </div>
            <div class="box-body">        

                 <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>MAX EMPLOYEE</label>
                  <asp:TextBox ID="txtMaxPerson" runat="server" cssclass="form-control text-center" ></asp:TextBox>   
                 </div> 
            </div>

 </div> 
</div>
    </div> 


        <div class="text-center">
  <asp:Button ID="cmdSave" cssclass="btn btn-primary" runat="server" Text="Save"  data-dismiss="modal" Width="120px" />  
                  <asp:Button ID="cmdDelete" cssclass="btn btn-danger" runat="server" Text="Delete"  data-dismiss="modal" Width="120px" />  
        </div>

      </section>
</asp:Content>