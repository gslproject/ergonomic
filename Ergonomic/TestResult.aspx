﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TestResult.aspx.vb" Inherits="Ergonomic.TestResult" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>ผลการทดสอบหลังบทเรียน
        <small> </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">ผลการทดสอบหลังบทเรียน</li>
      </ol>
    </section>

    <!-- Main content -->   
      <section class="content">  
            
<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title">
          <asp:HiddenField ID="hdCourseUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
          
     <div class="col-lg-12">          
          <div class="text-center"> 
              <h3>
          <asp:Label ID="lblCourseName" runat="server"  cssclass="text-blue" ></asp:Label>  </h3>
          </div>
      </div>     
          
</div>
          <div class="row">
          
     <div class="col-lg-12">          
          <div class="text-center">  
              <h2>
          <asp:Label ID="lblResult" runat="server"  cssclass="text-center" ></asp:Label>  </h2>
          </div>
      </div>     
          
</div>
            <div class="row">
  <div class="col-lg-12 text-center">
        <br/>      
          <asp:Button ID="cmdPrint" CssClass="btn btn-primary" runat="server" Text="พิมพ์ Certificate" />
 
        </div>

             </div>    
    </div>
    <!-- /.box-body -->
  </div>
  </section>
</div>                          
      
      </section>
</asp:Content>