﻿Imports System.Drawing
Public Class HRAResult
    Inherits System.Web.UI.Page
    Dim ctlEmp As New PersonController
    Dim ctlA As New AssessmentController
    Public dtPersonRisk As New DataTable
    Public dtT As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If


        If Not IsPostBack Then
            hdUID.Value = 0
            hdPersonUID.Value = 0

            If Not Request("id") Is Nothing Then
                hdUID.Value = Request("id")
                Dim dtH As New DataTable
                dtH = ctlA.AsmHRA_GetByUID(hdUID.Value)

                If dtH.Rows.Count > 0 Then
                    hdPersonUID.Value = dtH.Rows(0)("PersonUID")
                    hdUID.Value = dtH.Rows(0)("UID")
                End If
            Else
                If Not Request("pid") Is Nothing Then
                    hdPersonUID.Value = Request("pid")
                Else
                    hdPersonUID.Value = Request.Cookies("Ergo")("LoginPersonUID")
                End If
            End If

            LoadPersonData()
            LoadPersonErgonomicRisk()
            CalculatingScore(hdPersonUID.Value)

            If Request.Cookies("Ergo")("ROLE_CUS") = True Then
                cmdSave.Visible = False
                txtRemark.ReadOnly = True
            End If
        End If
    End Sub

    Private Sub LoadPersonErgonomicRisk()
        dtPersonRisk = ctlEmp.PersonBodyRisk_Get(hdPersonUID.Value)

        'grdBodyRisk.DataSource = dtPersonRisk
        'grdBodyRisk.DataBind()

        'For i = 0 To grdBodyRisk.Rows.Count - 1
        '    If grdBodyRisk.Rows(i).Cells(1).Text >= 3 Then
        '        grdBodyRisk.Rows(i).Cells(1).ForeColor = Color.Red
        '    End If

        '    If grdBodyRisk.Rows(i).Cells(2).Text >= 3 Then
        '        grdBodyRisk.Rows(i).Cells(2).ForeColor = Color.Red
        '    End If


        'Next



    End Sub
    Private Sub LoadPersonData()
        Dim dtP As New DataTable
        dtP = ctlEmp.Person_GetByPersonID(hdPersonUID.Value)
        If dtP.Rows.Count > 0 Then
            With dtP.Rows(0)
                hdPersonUID.Value = .Item("PersonUID")
                txtCode.Text = String.Concat(.Item("Code"))
                txtName.Text = String.Concat(.Item("PersonName"))
                txtPosition.Text = String.Concat(.Item("PositionName"))
                txtDepartment.Text = String.Concat(.Item("DepartmentName"))
                txtSection.Text = String.Concat(.Item("SectionName"))
            End With
            LoadTaskAssessment(hdPersonUID.Value)
            LoadAsmHRAData()
        End If
    End Sub

    Private Sub LoadTaskAssessment(PersonUID As Integer)

        dtT = ctlA.Assessment_GetByPersonUID(PersonUID)
        'grdEvaluation.DataSource = dtT
        'grdEvaluation.DataBind()

    End Sub

    Private Sub LoadAsmHRAData()
        Dim dtH As New DataTable
        dtH = ctlA.AsmHRA_GetByPerson(hdPersonUID.Value)
        If dtH.Rows.Count > 0 Then
            With dtH.Rows(0)
                txtRemark.Text = String.Concat(.Item("Remark"))
            End With
        End If
    End Sub


    Private Sub CalculatingScore(PersonUID As Integer)
        Dim dtScore As New DataTable
        Dim BodyRisk, RiskLevel As Double
        'Dim ErgoRisk As Double

        dtScore = ctlEmp.PersonBodyRisk_GetMax(PersonUID)

        If dtScore.Rows.Count > 0 Then
            BodyRisk = DBNull2Dbl(dtScore.Rows(0)("Score")) * DBNull2Dbl(dtScore.Rows(0)("Freq"))
        Else
            BodyRisk = 0
        End If

        RiskLevel = ctlEmp.PersonErgonomic_GetMax(PersonUID)
        'ErgoRisk = BodyRisk * RiskLevel


        txtBodyScore.Text = BodyRisk
        txtBodyRisk.Text = ""


        If BodyRisk <= 0 Then
            hdHRALevel.Value = 0
            txtBodyRisk.Text = "ระดับ 0 ไม่รู้สึกรับรู้ความไม่สบาย"
            txtBodyRisk.ForeColor = ColorTranslator.FromHtml("#004085")
            txtBodyRisk.BackColor = ColorTranslator.FromHtml("#cce5ff")
            txtBodyRisk.BorderColor = ColorTranslator.FromHtml("#b8daff")
        ElseIf BodyRisk >= 1 And BodyRisk <= 2 Then
            hdHRALevel.Value = 1
            txtBodyRisk.Text = "ระดับ 1 รู้สึกไม่สบายรุนแรงเล็กน้อย"
            txtBodyRisk.ForeColor = ColorTranslator.FromHtml("#008d4c")
            txtBodyRisk.BackColor = ColorTranslator.FromHtml("#d4edda")
            txtBodyRisk.BorderColor = ColorTranslator.FromHtml("#c3e6cb")
        ElseIf BodyRisk >= 3 And BodyRisk <= 4 Then
            hdHRALevel.Value = 2
            txtBodyRisk.Text = "ระดับ 2 รู้สึกไม่สบายรุนแรงปานกลาง"
            txtBodyRisk.ForeColor = ColorTranslator.FromHtml("#ff7701")
            txtBodyRisk.BackColor = ColorTranslator.FromHtml("#fff3cd")
            txtBodyRisk.BorderColor = ColorTranslator.FromHtml("#ffeeba")

        ElseIf BodyRisk >= 5 And BodyRisk <= 8 Then
            hdHRALevel.Value = 3
            txtBodyRisk.Text = "ระดับ 3 รู้สึกไม่สบายรุนแรงมาก"
            txtBodyRisk.ForeColor = ColorTranslator.FromHtml("#ff0000")
            txtBodyRisk.BackColor = ColorTranslator.FromHtml("#f8d7da")
            txtBodyRisk.BorderColor = ColorTranslator.FromHtml("#f5c6cb")
        Else
            hdHRALevel.Value = 4
            txtBodyRisk.Text = "ระดับ 4 รู้สึกไม่สบายรุนแรงมากเกินทนไหว"
            txtBodyRisk.ForeColor = Color.White
            txtBodyRisk.BackColor = ColorTranslator.FromHtml("#d33724")
            txtBodyRisk.BorderColor = ColorTranslator.FromHtml("#d33724")
        End If

        'บันทึกให้ อัตโนมัติ
        ctlA.AsmHRA_Save(StrNull2Zero(hdUID.Value), StrNull2Zero(hdPersonUID.Value), StrNull2Double(txtBodyScore.Text), StrNull2Zero(hdHRALevel.Value), txtBodyRisk.Text, StrNull2Double(txtBodyScore.Text), StrNull2Zero(hdHRALevel.Value), txtBodyRisk.Text, txtRemark.Text, Request.Cookies("Ergo")("userid"))



        '        txtErgoScore.Text = ErgoRisk
        '        txtErgoRisk.Text = ""

        '        If ErgoRisk <= 0 Then
        '            txtErgoRisk.Text = "ระดับ 0"
        '            lblRecommend.Text = "1. อบรมให้ความรู้เรื่องการยศาสตร์ในการทำงาน"

        '            txtErgoRisk.ForeColor = ColorTranslator.FromHtml("#004085")
        '            txtErgoRisk.BackColor = ColorTranslator.FromHtml("#cce5ff")
        '            txtErgoRisk.BorderColor = ColorTranslator.FromHtml("#b8daff")


        '            lblRecommend.ForeColor = ColorTranslator.FromHtml("#004085")
        '            lblRecommend.BackColor = ColorTranslator.FromHtml("#cce5ff")
        '            lblRecommend.BorderColor = ColorTranslator.FromHtml("#b8daff")


        '        ElseIf ErgoRisk >= 1 And ErgoRisk <= 2 Then
        '            txtErgoRisk.Text = "ระดับ 1"
        '            lblRecommend.Text = "1. อบรมให้ความรู้เรื่องการยศาสตร์ในการทำงาน"


        '            txtErgoRisk.ForeColor = ColorTranslator.FromHtml("#008d4c")
        '            txtErgoRisk.BackColor = ColorTranslator.FromHtml("#d4edda")
        '            txtErgoRisk.BorderColor = ColorTranslator.FromHtml("#c3e6cb")

        '            lblRecommend.ForeColor = ColorTranslator.FromHtml("#008d4c")
        '            lblRecommend.BackColor = ColorTranslator.FromHtml("#d4edda")
        '            lblRecommend.BorderColor = ColorTranslator.FromHtml("#c3e6cb")

        '        ElseIf ErgoRisk >= 3 And ErgoRisk <= 4 Then

        '            txtErgoRisk.Text = "ระดับ 2"
        '            lblRecommend.Text = "1. กำหนดโปรแกรมการบริหารร่างกายระหว่างช่วงเวลาทำงาน <br/>2. อบรมให้ความรู้เรื่องการยศาสตร์ในการทำงาน"
        '            lblRecommend.Height = 50

        '            txtErgoRisk.ForeColor = ColorTranslator.FromHtml("#ff7701")
        '            txtErgoRisk.BackColor = ColorTranslator.FromHtml("#fff3cd")
        '            txtErgoRisk.BorderColor = ColorTranslator.FromHtml("#ffeeba")

        '            lblRecommend.ForeColor = ColorTranslator.FromHtml("#ff7701")
        '            lblRecommend.BackColor = ColorTranslator.FromHtml("#fff3cd")
        '            lblRecommend.BorderColor = ColorTranslator.FromHtml("#ffeeba")


        '        ElseIf ErgoRisk >= 5 And ErgoRisk <= 8 Then
        '            txtErgoRisk.Text = "ระดับ 3"
        '            lblRecommend.Text = "1. ปรับปรุงสถานีงานให้เหมาะสมตามหลักการยศาสตร์ เช่น การปรับเปลี่ยนเก้าอี้หรือโต๊ะทำงาน และคอมพิวเตอร์ เป็นต้น<br/>
        '2. กำหนดโปรแกรมการบริหารร่างกายระหว่างช่วงเวลาทำงาน<br/>
        '3. อบรมให้ความรู้เรื่องการยศาสตร์ในการทำงาน<br/>
        '4. จัดให้มีการตรวจสอบและประเมินท่าทางการทำงานตามหลักการยศาสตร์ เป็นประจำ"

        '            txtErgoRisk.ForeColor = ColorTranslator.FromHtml("#ff0000")
        '            txtErgoRisk.BackColor = ColorTranslator.FromHtml("#f8d7da")
        '            txtErgoRisk.BorderColor = ColorTranslator.FromHtml("#f5c6cb")

        '            lblRecommend.ForeColor = ColorTranslator.FromHtml("#ff0000")
        '            lblRecommend.BackColor = ColorTranslator.FromHtml("#f8d7da")
        '            lblRecommend.BorderColor = ColorTranslator.FromHtml("#f5c6cb")

        '            lblRecommend.Height = 100
        '        Else
        '            txtErgoRisk.Text = "ระดับ 4"
        '            lblRecommend.Text = "1. ปรับปรุงสถานีงานให้เหมาะสมตามหลักการยศาสตร์ เช่น การปรับเปลี่ยนเก้าอี้หรือโต๊ะทำงาน และคอมพิวเตอร์ เป็นต้น<br/>
        '2. ดำเนินการประเมินความเสี่ยงอีกครั้งหลังจากมีการปรับปรุงสถานีงาน<br/>
        '3. กำหนดโปรแกรมการบริหารร่างกายระหว่างช่วงเวลาทำงาน<br/>
        '4. อบรมให้ความรู้เรื่องการยศาสตร์ในการทำงาน<br/>
        '5. จัดให้มีการตรวจสอบและประเมินท่าทางการทำงานตามหลักการยศาสตร์ เป็นประจำ"


        '            txtErgoRisk.ForeColor = Color.White
        '            txtErgoRisk.BackColor = ColorTranslator.FromHtml("#d33724")
        '            txtErgoRisk.BorderColor = ColorTranslator.FromHtml("#d33724")

        '            lblRecommend.ForeColor = Color.White
        '            lblRecommend.BackColor = ColorTranslator.FromHtml("#d33724")
        '            lblRecommend.BorderColor = ColorTranslator.FromHtml("#d33724")


        '            lblRecommend.Height = 150
        '        End If



        'lblRecommend.NavigateUrl = "#"


        dtScore = Nothing

    End Sub


    Protected Sub cmdBack_Click(sender As Object, e As EventArgs) Handles cmdBack.Click

        If Request.Cookies("Ergo")("ROLE_CUS") = True Then
            Response.Redirect("Home?ActionType=h")
        Else
            Response.Redirect("HRA?ActionType=hraasm")
        End If
        'Response.Redirect("PersonSelect?ActionType=hraasm")
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click


        ctlA.AsmHRA_Save(StrNull2Zero(hdUID.Value), StrNull2Zero(hdPersonUID.Value), StrNull2Double(txtBodyScore.Text), StrNull2Zero(hdHRALevel.Value), txtBodyRisk.Text, StrNull2Double(txtBodyScore.Text), StrNull2Zero(hdHRALevel.Value), txtBodyRisk.Text, txtRemark.Text, Request.Cookies("Ergo")("userid"))

        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        Response.Redirect("HRA?ActionType=hraasm")

    End Sub
End Class


'<!-- ของเดิม
' <h5 class="text-blue text-bold">การประเมินความเสี่ยงสุขภาพต่อความผิดปกติของระบบโครงร่างและกล้ามเนื้อ</h5>
'            <div class="row">
'              <div class="col-md-6">
'                <div class="form-group">
'                  <label class="text-blue">คะแนน</label>
'                  <asp:TextBox ID="txtErgoScore" runat="server" cssclass="form-control text-bold text-center" placeholder="Kg."
'                    Font-Size="12" BackColor="White" ReadOnly="True"></asp:TextBox>
'                </div>
'              </div>
'              <div class="col-md-6">
'                <div class="form-group">
'                  <label class="text-blue">ระดับความเสี่ยง</label>
'                  <asp:TextBox ID="txtErgoRisk" runat="server" cssclass="form-control text-bold text-center"
'                    placeholder="0.00" Font-Size="12" BackColor="White" ReadOnly="True"></asp:TextBox>
'                </div>
'              </div>
'            </div>
' -->