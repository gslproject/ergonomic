﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CourseAssign.aspx.vb" Inherits="Ergonomic.CourseAssign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>E-Learning Assignment
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Person</li>
      </ol>
    </section>

    <!-- Main content -->   
      <section class="content">  
<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Personal Information<asp:HiddenField ID="hdPersonUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">


      <div class="row">
        <div class="col-md-1">
          <div class="form-group">
            <label>Code</label>
            <asp:Label ID="lblCode" runat="server" cssclass="form-control text-center"></asp:Label>
          </div>

        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Name</label>
               <asp:Label ID="lblPersonName" runat="server" cssclass="form-control text-center"></asp:Label> 
          </div>
        </div>                

        <div class="col-md-3">
          <div class="form-group">
            <label>Position</label>

              <asp:Label ID="lblPosition" runat="server" cssclass="form-control text-center"></asp:Label>
          </div>

        </div>
    

        <div class="col-md-2">
          <div class="form-group">
            <label>Department</label>
            <asp:Label ID="lblDepartment" runat="server" cssclass="form-control text-center"></asp:Label>
          </div>

        </div> 
        <div class="col-md-4">
          <div class="form-group">
            <label>Section</label>
            <asp:Label ID="lblSection" runat="server" cssclass="form-control text-center"></asp:Label>      
          </div>

        </div>      
      </div>    
    </div>
    <!-- /.box-body -->
  </div>
  </section>
</div>
   
            <div class="row">
    <section class="col-lg-12 connectedSortable"> 

          <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Course Assignment</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

          <div class="row">
            <div class="col-md-6">
          <div class="form-group">
            <label>Course</label>
            <asp:DropDownList ID="ddlCourse" runat="server" cssclass="form-control select2"  placeholder="--- เลือกหลักสูตร ---" Width="100%"> </asp:DropDownList>
          </div>
        </div>

                 <div class="col-md-2">
          <div class="form-group">
            <label>Duedate</label>
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <asp:TextBox ID="txtDueDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </div>            
          </div>

        </div>

             <div class="col-md-3">
          <div class="form-group">
            <label>Re-Occurrence</label>
              <asp:RadioButtonList ID="optRecure" runat="server" RepeatDirection="Horizontal">
                   <asp:ListItem Value="M">6 month</asp:ListItem>
                <asp:ListItem Value="Y">1 year</asp:ListItem>
                   <asp:ListItem Value="D">2 year</asp:ListItem>
                <asp:ListItem Value="X" Selected="True">none</asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div>
<div class="col-md-1">
          <div class="form-group">
              <br />
<asp:Button ID="cmdAdd" runat="server" Text="Save" Width="100" CssClass="btn btn-primary" />
 </div>
        </div>

</div>
     <div class="row text-center">

         </div>

  <div class="row">
 
                <asp:GridView ID="grdCompany" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-striped" 
                             Font-Bold="False" PageSize="15">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' /> 
                                </ItemTemplate>
                                <ItemStyle Width="20px" />
                            </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server" ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="No." DataField="nRow">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40px" />                            </asp:BoundField>
                        <asp:BoundField DataField="CourseName" HeaderText="Course/หลักสูตร" />
                            <asp:BoundField DataField="Duedate" HeaderText="Duedate" />
                            <asp:BoundField HeaderText="Re-Occurrence" DataField="ReStatus" />
                            <asp:BoundField HeaderText="Status" DataField="LearnStatusName" />
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
     </div>        
         
          </div>
    </div>




        </section>
<section class="col-lg-6 connectedSortable"> 
       
        </section>
                </div>
      <div class="row">
        <div class="col-md-9">
        </div>
      </div>

          </section>
</asp:Content>