﻿Public Class EvaluationTopic
    Inherits System.Web.UI.Page

    Dim ctlLG As New ElearningController
    Public dtT As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        LoadEvaluationTopicToGrid()
    End Sub

    Private Sub LoadEvaluationTopicToGrid()
        dtT = ctlLG.EvaluationTopic_Get
    End Sub
End Class

