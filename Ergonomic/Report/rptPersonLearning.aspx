﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rptPersonLearning.aspx.vb" Inherits="Ergonomic.rptPersonLearning" %>
<%@ Import Namespace="System.Data" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>    
     <meta charset="utf-8" /> 
</head>
<body style="font:12px tahoma #000;margin:0;">
    <form id="form1" runat="server">     
          <table border="1">
                <thead>
                <tr>
                 <th style="background-color:lightblue">No.</th>   
                    <th style="background-color:lightblue">Course</th>
                  <th style="background-color:lightblue">Duedate</th>
                  <th style="background-color:lightblue">Status</th>
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLearn.Rows %>
                <tr>  
                  <td width="40"><% =String.Concat(row("nRow")) %></td>
                  <td>  <% =String.Concat(row("CourseName")) %> </td>
                  <td><% =String.Concat(row("Duedate")) %></td>
                  <td><% =String.Concat(row("LearnStatusName")) %></td>                 
                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>  

    </form>
</body>
</html>
