﻿Public Class rptPersonLearning
    Inherits System.Web.UI.Page
    Public dtLearn As New DataTable
    Dim ctlE As New ElearningController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Request("pid") Is Nothing Then
            LoadPersonLearning(Request.Cookies("Ergo")("LoginPersonUID"))
        Else
            LoadPersonLearning(Request("pid"))
        End If

        'If Request("ex") = 1 Then
        Response.AppendHeader("content-disposition", "attachment;filename=personallearning.xls")
            Response.Charset = ""
            Response.ContentType =  "application/vnd.ms-excel"
        'End If

    End Sub

    Private Sub LoadPersonLearning(PersonUID As Integer)
        dtLearn = ctlE.PersonLearning_Get(PersonUID)
    End Sub

End Class