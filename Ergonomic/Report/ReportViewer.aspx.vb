﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports Microsoft.Reporting.WebForms

Public Class ReportViewer
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Public Shared ReportFormula As String
    Dim ReportFileName As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.Browser.Browser = "Firefox" Then
            Panel1.Visible = True
        ElseIf Request.Browser.Browser = "IE" Then
            Panel1.Visible = True
        Else
            Panel1.Visible = False

            If Not Request("id") Is Nothing Then
                'LoadData(Request("id"))
            Else
                'LoadData("")
            End If

            LoadReport()

        End If



    End Sub

    Private Sub LoadReport()

        'System.Threading.Thread.Sleep(1000)
        'UpdateProgress1.Visible = True

        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True

        Dim xParam As New List(Of ReportParameter)

        Select Case Request("R")
            Case "tasktotal"
                ReportFileName = "TaskTotal" & "_" & Request("BDT")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("TaskTotal")
                xParam.Add(New ReportParameter("CompanyUID", Request("COM").ToString()))
                xParam.Add(New ReportParameter("Bdate", Request("BDT").ToString()))
                xParam.Add(New ReportParameter("Edate", Request("EDT").ToString()))
                xParam.Add(New ReportParameter("Cond", Request("COND").ToString()))
                xParam.Add(New ReportParameter("UserID", Request.Cookies("Ergo")("userid").ToString()))
            Case "personal"
                ReportFileName = "Personal" & "_" & Request("EMP")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("PersonalReport")
                'xParam.Add(New ReportParameter("CompanyUID", Request("COM").ToString()))
                xParam.Add(New ReportParameter("PersonUID", Request("EMP").ToString()))
                xParam.Add(New ReportParameter("UserID", Request.Cookies("Ergo")("userid").ToString()))
            Case "hracomp"
                ReportFileName = "HRA" & "_" & Request("BDT")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("HRA")
                xParam.Add(New ReportParameter("CompanyUID", Request("COM").ToString()))
                'xParam.Add(New ReportParameter("Bdate", Request("BDT").ToString()))
                'xParam.Add(New ReportParameter("Edate", Request("EDT").ToString()))
                xParam.Add(New ReportParameter("DivisionUID", Request("DIVUID").ToString()))
                xParam.Add(New ReportParameter("DepartmentUID", Request("DEPUID").ToString()))
                xParam.Add(New ReportParameter("UserID", Request.Cookies("Ergo")("userid").ToString()))
            Case "acttask"
                ReportFileName = "ActionByTask" & "_" & Request("TASK")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("TaskActionByTask")
                xParam.Add(New ReportParameter("CompanyUID", Request("COM").ToString()))
                xParam.Add(New ReportParameter("TaskUID", Request("TASK").ToString()))
                xParam.Add(New ReportParameter("Bdate", Request("BDT").ToString()))
                xParam.Add(New ReportParameter("Edate", Request("EDT").ToString()))
                xParam.Add(New ReportParameter("UserID", Request.Cookies("Ergo")("userid").ToString()))
            Case "actdt"
                ReportFileName = "ActionByDate" & "_" & Request("BDT")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("TaskActionByDate")
                xParam.Add(New ReportParameter("CompanyUID", Request("COM").ToString()))
                xParam.Add(New ReportParameter("Bdate", Request("BDT").ToString()))
                xParam.Add(New ReportParameter("Edate", Request("EDT").ToString()))
                xParam.Add(New ReportParameter("UserID", Request.Cookies("Ergo")("userid").ToString()))
            Case "plearn"
                ReportFileName = "PersonalLearning" & "_" & Request("EMP")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("PersonalLearning")
                xParam.Add(New ReportParameter("PersonUID", Request("EMP").ToString()))
                xParam.Add(New ReportParameter("UserID", Request.Cookies("Ergo")("userid").ToString()))
            Case "comprog"
                ReportFileName = "CompanyLearning" & "_" & Request("COM")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("CompanyLearning")
                xParam.Add(New ReportParameter("CompanyUID", Request("COM").ToString()))
                xParam.Add(New ReportParameter("UserID", Request.Cookies("Ergo")("userid").ToString()))
            Case "csprog"
                ReportFileName = "CourseProgression" & "_" & Request("CS")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("CourseProgression")
                xParam.Add(New ReportParameter("CompanyUID", Request("COM").ToString()))
                xParam.Add(New ReportParameter("CourseUID", Request("CS").ToString()))
                xParam.Add(New ReportParameter("UserID", Request.Cookies("Ergo")("userid").ToString()))
            Case "cert"
                ReportFileName = "Certificate" & "_" & Request("PID")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("Certificate")
                xParam.Add(New ReportParameter("PersonUID", Request("PID").ToString()))
                xParam.Add(New ReportParameter("CourseUID", Request("CID").ToString()))
            Case "msd"
                If Request("DEPT") = "0" Then
                    ReportFileName = "MSDs" & "_" & Request("COM")
                    ReportViewer1.ServerReport.ReportPath = credential.ReportPath("MSDs")
                    xParam.Add(New ReportParameter("CompanyUID", Request("COM").ToString()))
                Else
                    ReportFileName = "MSDs" & "_" & Request("DEPT")
                    ReportViewer1.ServerReport.ReportPath = credential.ReportPath("MSDsByDepartment")
                    xParam.Add(New ReportParameter("CompanyUID", Request("COM").ToString()))
                    xParam.Add(New ReportParameter("DepartmentUID", Request("DEPT").ToString()))
                End If
                'ReportFileName = "MSDs" & "_" & Request("COM")
                'ReportViewer1.ServerReport.ReportPath = credential.ReportPath("MSDs")
                'xParam.Add(New ReportParameter("CompanyUID", Request("COM").ToString()))
                'xParam.Add(New ReportParameter("Bdate", Request("BDT").ToString()))
                'xParam.Add(New ReportParameter("Edate", Request("EDT").ToString()))
                'xParam.Add(New ReportParameter("Cond", Request("COND").ToString()))
                'xParam.Add(New ReportParameter("UserID", Request.Cookies("Ergo")("userid").ToString()))
            Case Else
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("")
                xParam.Add(New ReportParameter("UserID", Request.Cookies("Ergo")("userid").ToString()))
        End Select



        ReportViewer1.ServerReport.SetParameters(xParam)

        Select Case Request("RPTTYPE") ' FagRPT
            Case "EXCEL"
                ' Variables
                Dim warnings As Warning()
                Dim streamIds As String()
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim extension As String = String.Empty

                ' Setup the report viewer object and get the array of bytes

                Dim bytes As Byte() = ReportViewer1.ServerReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamIds, Nothing)

                ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
                Response.Buffer = True
                Response.Clear()
                Response.ContentType = mimeType
                Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=" & ReportFileName & "." & extension))
                Response.BinaryWrite(bytes)
                ' create the file
                Response.Flush()

                ' send it to the client to download
            Case Else
                ' Variables

                Dim warnings As Warning()
                Dim streamIds As String()
                Dim mimeType As String = String.Empty
                Dim encoding As String = String.Empty
                Dim extension As String = String.Empty


                ' Setup the report viewer object and get the array of bytes

                Dim bytes As Byte() = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, Nothing)

                ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
                Response.Buffer = True
                Response.Clear()
                Response.ContentType = mimeType
                'Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=C:\ttt.pdf"))
                Response.BinaryWrite(bytes)
                ' create the file
                'Response.Flush()
                ' send it to the client to download

        End Select
        '
    End Sub

    'Private Sub LoadData(pID As String)

    '    Dim ctlstd As New StudentController

    '    Select Case Request("m")
    '        Case "cert"
    '            lblReportTitle.Text = "พิมพ์ใบรับรองการผ่านการฝึกปฏิบัติงาน"
    '            Request.Cookies("Ergo")("rptyear") = Request("y")
    '            Request.Cookies("Ergo")("rptcertdate") = Request("d")

    '            If pID <> "" Then
    '                ctlstd.TMP_Student_SET2CERTBySTDID(Request("id"), Request.Cookies("Ergo")("username"), Request("d"))
    '            Else
    '                If ReportFormula <> "" Then
    '                    ctlstd.TMP_Student_SET2CERT(ReportFormula, Request.Cookies("Ergo")("username"), Request("d"))
    '                End If
    '            End If
    '        Case "certregis"
    '            lblReportTitle.Text = "พิมพ์แบบรายงานการฝึกปฏิบัติงานสำหรับผู้สมัครสอบความรู้เพื่อขึ้นทะเบียนเป็นผู้ประกอบวิชาชีพเภสัชกรรม"

    '            If pID <> "" Then
    '                ctlstd.TMP_Student_SET2CERTBySTDID(Request("id"), Request.Cookies("Ergo")("username"), Request("d"))
    '            Else
    '                If ReportFormula <> "" Then
    '                    ctlstd.TMP_Student_SET2CERT(ReportFormula, Request.Cookies("Ergo")("username"), Request("d"))
    '                End If
    '            End If


    '        Case Else
    '            lblReportTitle.Text = "พิมพ์ประวัตินักศึกษา"
    '            If pID <> "" Then
    '                ctlstd.TMP_Student_SET2BIOBySTDID(Request("id"), Request.Cookies("Ergo")("username"))
    '            Else
    '                If ReportFormula <> "" Then
    '                    ctlstd.TMP_Student_SET2BIO(ReportFormula, Request.Cookies("Ergo")("username"))
    '                Else
    '                    ctlstd.TMP_Student_SET2BIOBySTDID(Request.Cookies("Ergo")("username"), Request.Cookies("Ergo")("username"))
    '                End If
    '            End If
    '    End Select



    'End Sub

End Class