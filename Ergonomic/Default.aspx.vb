﻿
Public Class _Default
    Inherits Page
    Dim dt As New DataTable
    Dim acc As New UserController
    Dim enc As New CryptographyEngine
    Dim ctlM As New MasterController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If ctlM.SystemOnline_GetStatus() = False Then
            Exit Sub
        End If

        If Not Request("logout") Is Nothing Then
            If Request("logout") = "y" Then
                Session.Abandon()
                Session.RemoveAll()
                Response.Cookies.Clear()

                Dim delCookie1 As HttpCookie
                delCookie1 = New HttpCookie("Ergo")
                delCookie1.Expires = DateTime.Now.AddDays(-1D)
                Response.Cookies.Add(delCookie1)

                Response.Redirect("Login")
            Else
                RenewSessionFromCookie()
            End If
        End If

        Session.Timeout = 360
        Response.Redirect("Home")
    End Sub

    Private Sub RenewSessionFromCookie()
        If IsNothing(Request.Cookies("Ergo")) Then
            Exit Sub
        End If

        dt = acc.User_GetByUserID(StrNull2Zero(Request.Cookies("Ergo")("userid")))

        If dt.Rows.Count > 0 Then

            If dt.Rows(0).Item("StatusFlag") <> "A" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ID ของท่านไม่สามารถใช้งานได้ชั่วคราว');", True)
                Exit Sub
            End If

            'Response.Cookies("Ergo")("userid") = ""
            'Response.Cookies("Ergo")("username") = ""
            'Response.Cookies("Ergo").Expires = acc.GET_DATE_SERVER().AddDays(1)


            Dim ErgoCookie As HttpCookie = New HttpCookie("Ergo")
            ErgoCookie("userid") = dt.Rows(0).Item("UserID")
            ErgoCookie("username") = String.Concat(dt.Rows(0).Item("USERNAME")).ToString()
            ErgoCookie("Password") = enc.DecryptString(dt.Rows(0).Item("PASSWORDs"), True)
            ErgoCookie("LastLogin") = String.Concat(dt.Rows(0).Item("LastLogin"))
            ErgoCookie("NameOfUser") = String.Concat(dt.Rows(0).Item("Name"))
            ErgoCookie("LoginPersonUID") = dt.Rows(0).Item("PersonUID")
            ErgoCookie("LoginCompanyUID") = dt.Rows(0).Item("CompanyUID")
            ErgoCookie("ROLE_CUS") = False
            ErgoCookie("ROLE_CAD") = False
            ErgoCookie("ROLE_ADM") = False
            ErgoCookie("ROLE_SPA") = False

            ErgoCookie.Expires = acc.GET_DATE_SERVER().AddMinutes(60)
            Response.Cookies.Add(ErgoCookie)


            'Request.Cookies("Ergo")("userid") = dt.Rows(0).Item("UserID")
            'Request.Cookies("Ergo")("username") = dt.Rows(0).Item("USERNAME")
            'Request.Cookies("Ergo")("Password") = enc.DecryptString(dt.Rows(0).Item("PASSWORDs"), True)
            'Request.Cookies("Ergo")("LastLogin") = String.Concat(dt.Rows(0).Item("LastLogin"))
            'Request.Cookies("Ergo")("NameOfUser") = String.Concat(dt.Rows(0).Item("Name"))
            'Request.Cookies("Ergo")("LoginPersonUID") = dt.Rows(0).Item("PersonUID")
            'Request.Cookies("Ergo")("LoginCompanyUID") = dt.Rows(0).Item("CompanyUID")
            'Request.Cookies("Ergo")("ROLE_CUS") = False
            'Request.Cookies("Ergo")("ROLE_CAD") = False
            'Request.Cookies("Ergo")("ROLE_ADM") = False
            'Request.Cookies("Ergo")("ROLE_SPA") = False


            Dim rd As New UserRoleController

            dt = rd.UserAccessGroup_GetByUID(Request.Cookies("Ergo")("userid"))
            If dt.Rows.Count > 0 Then

                For i = 0 To dt.Rows.Count - 1

                    Select Case dt.Rows(i)("UserGroupUID")
                        Case 1 'Customer User
                            ErgoCookie("ROLE_CUS") = True
                            'Request.Cookies("Ergo")("ROLE_CUS") = True
                        Case 2  'Customer Admin
                            ErgoCookie("ROLE_CAD") = True
                            'Request.Cookies("Ergo")("ROLE_CAD") = True
                        Case 3 'Administrator
                            ErgoCookie("ROLE_ADM") = True
                            'Request.Cookies("Ergo")("ROLE_ADM") = True
                        Case 9
                            ErgoCookie("ROLE_SPA") = True
                            'Request.Cookies("Ergo")("ROLE_SPA") = True
                    End Select
                    Next

                End If

            Response.Redirect("Home")

        Else
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username  หรือ Password ไม่ถูกต้อง');", True)
                Exit Sub
            End If

    End Sub


End Class