﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AsmREBA.aspx.vb" Inherits="Ergonomic.AsmREBA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>การประเมินความเสี่ยงโดยวิธี REBA</h1>     
    </section>
    <section class="content">  

  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">ErgonomicAssessment<asp:HiddenField ID="hdUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

      <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>Task No</label>
              <asp:TextBox ID="txtTaskNo" runat="server" cssclass="form-control" placeholder="Task no."></asp:TextBox>
          </div>

        </div>

        <div class="col-md-10">
          <div class="form-group">
            <label>Task Name</label>
              <asp:TextBox ID="txtTaskName" runat="server" cssclass="form-control" placeholder="Task no." ReadOnly="True"></asp:TextBox>
          </div>

        </div>
      
      </div>
      <div class="row">
  <div class="col-md-3">
          <div class="form-group">
            <label>Date</label>
              <asp:TextBox ID="txtAsmDate" runat="server" placeholder="Date of assessment" CssClass="form-control datepicker-dropdown"></asp:TextBox>
          </div>

        </div> 

        <div class="col-md-9">
          <div class="form-group">
            <label>Type</label>
              <asp:RadioButtonList ID="optAsmType" runat="server" RepeatDirection="Horizontal">
                  <asp:ListItem Value="S">Self-assessment</asp:ListItem>
                  <asp:ListItem Value="O">Observer assessment</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 

      </div>
   
        <div class="row">        
       <div class="col-md-12">
          <div class="form-group">
            <label>ผู้รับการประเมิน</label>
              <asp:DropDownList ID="ddlPerson" runat="server" cssclass="form-control select2"  placeholder="Select Person" Width="100%">
            </asp:DropDownList> 
          </div>

        </div> 
</div>
          <div class="row">        
       <div class="col-md-12">
          <div class="form-group">
            <label>Remark</label>
              <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" Height="50px" TextMode="MultiLine"></asp:TextBox>
          </div>

        </div> 
</div>
</div>
      </div>

         <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">กลุ่ม A       </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

 <h5 class="text-blue text-bold">การประเมินส่วนคอ (Neck)</h5>
        <div class="row">
             
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optReba1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="ก้มคอ โดยมีมุม 0-20 องศา" class="vtip"  height="140" src="images/reba/reba1_01.jpg" /><br /><br />ก้มคอ โดยมีมุม 0-20 ํ</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ก้มคอ โดยมีมุม มากกว่า 20 องศา" class="vtip"  height="140"  src="images/reba/reba1_02.jpg" /><br /><br />ก้มคอ โดยมีมุม มากกว่า 20 ํ</asp:ListItem>
                    <asp:ListItem Value="3"><img title="เงยหน้า (คอแอนไปด้านหลัง) มากกว่า 20 องศา" class="vtip"  height="140"  src="images/reba/reba1_03.jpg" /><br /><br />เงยหน้า (คอแอนไปด้านหลัง) มากกว่า 20 ํ</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 

                  <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
              <asp:CheckBoxList ID="chkRebaAdd1" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="A1">มีการหมุนศีรษะ หรือ เอียงศีรษะไปด้านข้าง</asp:ListItem>                
              </asp:CheckBoxList>
             
          </div>

        </div> 

            </div>
        <h5 class="text-blue text-bold">การประเมินส่วนลำตัว (Trunk)</h5>
        <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optReba2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="ลำตัวตั้งตรง" class="vtip"  height="140"  src="images/reba/reba2_01.jpg" /><br /><br />ลำตัวตั้งตรง</asp:ListItem>
                    <asp:ListItem Value="2"><img title="เอนตัวไปด้านหลัง" class="vtip"  height="140"  src="images/reba/reba2_02.jpg" /><br /><br />เอนตัวไปด้านหลัง</asp:ListItem>
                    <asp:ListItem Value="3"><img title="เอนตัวไปด้านหน้า 0-20 องศา" class="vtip"  height="140"  src="images/reba/reba2_03.jpg" /><br /><br />เอนตัวไปด้านหน้า 0-20 ํ</asp:ListItem>
                     <asp:ListItem Value="4"><img title="เอนตัวไปด้านหน้า 20-60 องศา" class="vtip"  height="140"  src="images/reba/reba2_04.jpg" /><br /><br />เอนตัวไปด้านหน้า 20-60 ํ</asp:ListItem>
                     <asp:ListItem Value="5"><img title="เอนตัวไปด้านหน้า มากกว่า 60 องศา" class="vtip"  height="140"  src="images/reba/reba2_05.jpg" /><br /><br />เอนตัวไปด้านหน้า มากกว่า 60 ํ</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
                 <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRebaAdd2" runat="server" AutoPostBack="True">
                      <asp:ListItem Value="A1">มีการหมุนตัว หรือ เอียงไปด้านข้าง</asp:ListItem>               
              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
        <h5 class="text-blue text-bold">การประเมินส่วนขา (Legs)</h5>
         <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optReba3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="ลักษณะขายืนอยู่ในแนวดิ่งตรงและสมดุลทั้ง 2 ข้าง" class="vtip"  height="140"  src="images/reba/reba3_01.jpg" /><br /><br />ลักษณะขายืนอยู่ในแนวดิ่งตรงและสมดุลทั้ง 2 ข้าง</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ขายืนไม่สมดุล" class="vtip"  height="140"  src="images/reba/reba3_02.jpg" /><br /><br />ขายืนไม่สมดุล</asp:ListItem>

              </asp:RadioButtonList>
          </div>

        </div> 
    <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>              
               <asp:CheckBoxList ID="chkRebaAdd3" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="A1">มีการย่อเข่าระหว่าง 30-60 ํ</asp:ListItem>
                    <asp:ListItem Value="A2">มีการย่อเข่า มากกว่า 60 ํ</asp:ListItem> 
              </asp:CheckBoxList>
         
          </div>

        </div> 
            </div>
     

            <h5 class="text-blue text-bold">แรงที่ใช้หรือภาระงาน (Force/Load)</h5>
         <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optReba5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="0">แรงหรือภาระงานที่ใช้น้อยกว่า 11 ปอนด์</asp:ListItem>
                    <asp:ListItem Value="1">แรงหรือภาระงานที่ใช้อยู่ระหว่าง 11-22 ปอนด์</asp:ListItem>
    <asp:ListItem Value="2">แรงหรือภาระงานที่ใช้มากกว่า 22 ปอนด์</asp:ListItem>

              </asp:RadioButtonList>
          </div>

        </div> 
    <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRebaAdd5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                    <asp:ListItem Value="1">ถ้าแรงเป็นแบบกระแทกหรือกระชากเร็วๆ</asp:ListItem>            

              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
            </div>
    <div class="box-footer clearfix">           
                <!--ที่มา : (Sonne, Villalta, & Andrews, 2012) , http://thai-ergonomic-assessment.blogspot.com-->
            </div>
  </div>

  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">กลุ่ม B       </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
          <h5 class="text-blue text-bold">การประเมินแขนส่วนบน (Upper arm)</h5>
         <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optReba7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="แขนอยู่ในตำแหน่งไปข้างหน้า-หลังไม่เกิน 20 องศา" class="vtip"  height="140"  src="images/reba/reba7_01.jpg" /><br /><br />แขนอยู่ในตำแหน่งไปข้างหน้า-หลังไม่เกิน 20ํ</asp:ListItem>
                    <asp:ListItem Value="2"><img title="แขนอยู่ด้านหลัง เกิน 20 องศา" class="vtip"  height="140"  src="images/reba/reba7_02.jpg" /><br /><br />แขนอยู่ด้านหลัง เกิน 20ํ</asp:ListItem> 
                     <asp:ListItem Value="3"><img title="แขนอยู่ด้านหน้า 20-45 องศา" class="vtip"  height="140"  src="images/reba/reba7_03.jpg" /><br /><br />แขนอยู่ด้านหน้า 20-45ํ</asp:ListItem>
                     <asp:ListItem Value="4"><img title="แขนอยู่ด้านหน้า 45-90 องศา" class="vtip"  height="140"  src="images/reba/reba7_04.jpg" /><br /><br />แขนอยู่ด้านหน้า 45-90ํ</asp:ListItem>
                     <asp:ListItem Value="5"><img title="แขนอยู่ในตำแหน่งเหนือไหล่ (มีมุมเกิน 90 องศา เมื่อเทียบกับลำตัว)" class="vtip"  height="140"  src="images/reba/reba7_05.jpg" /><br /><br />แขนอยู่ในตำแหน่งเหนือไหล่ <br />(มีมุมเกิน 90 ํ เมื่อเทียบกับลำตัว)</asp:ListItem>

              </asp:RadioButtonList>
          </div>

        </div> 
                  <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRebaAdd7" runat="server" AutoPostBack="True">
                 
                    <asp:ListItem Value="A1">มีการยกหัวไหล่</asp:ListItem>
                    <asp:ListItem Value="A2">หัวไหล่กางออก</asp:ListItem>
                     <asp:ListItem Value="A3">ถ้ามีที่วางแขน หรือสามารถพาดแขนได้</asp:ListItem>

              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
           <h5 class="text-blue text-bold">การประเมินแขนส่วนล่าง (Lower arm หรือ forearm)</h5>
           <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optReba8" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="แขนส่วนล่างอยู่ในระดับที่มีมุมระหว่าง 60-100 องศา เมื่อเทียบกับแนวดิ่ง" class="vtip"  height="140"  src="images/reba/reba8_01.jpg" /><br /><br />แขนส่วนล่างอยู่ในระดับที่มีมุมระหว่าง 60-100 ํ เมื่อเทียบกับแนวดิ่ง</asp:ListItem>
                    <asp:ListItem Value="2"><img title="แขนส่วนล่างตกลงมาด้านล่างโดยมีมุมน้อยกว่า 60 องศา หรือแขนอยู่ในตำแหน่งยกขึ้นด้านบนทำมุมมากกว่า 100 องศา เมื่อเทียบกับแนวดิ่ง" class="vtip"  height="140"  src="images/reba/reba8_02.jpg" /><br />แขนส่วนล่างตกลงมาด้านล่างโดยมีมุม<60ํ<br />หรือแขนอยู่ในตำแหน่งยกขึ้นด้านบนทำมุม > 100 ํ<br />เมื่อเทียบกับแนวดิ่ง</asp:ListItem>
              </asp:RadioButtonList>  
          </div>

        </div>  

            </div> 

          <h5 class="text-blue text-bold">การประเมินข้อมือ (Wrist)</h5>
           <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optReba9" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="ตำแหน่งของข้อมือ (แนวกระดูกฝ่ามือ) อยู่ในแนวเดียวกับแขนส่วนล่างหรืองอขึ้น หรือลงได้ไม่เกิน 15 องศา" class="vtip"  height="140"  src="images/reba/reba9_01.jpg" /><br /><br />ตำแหน่งของข้อมือ (แนวกระดูกฝ่ามือ)<br /> อยู่ในแนวเดียวกับแขนส่วนล่าง<br />หรืองอขึ้น หรือลงได้ไม่เกิน 15ํ</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ตำแหน่งของข้อมือ (แนวกระดูกฝ่ามือ) หรือลงมากกว่า 15 องศา เมื่อเทียบกับแนวแขนส่วนล่าง" class="vtip"  height="140"  src="images/reba/reba9_02.jpg" /><br /><br />ตำแหน่งของข้อมือ (แนวกระดูกฝ่ามือ) หรือลงมากกว่า 15ํ <br />เมื่อเทียบกับแนวแขนส่วนล่าง</asp:ListItem>
              </asp:RadioButtonList>  
          </div>

        </div> 
                    <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRebaAdd9" runat="server" AutoPostBack="True">
                  
                    <asp:ListItem Value="A1">มีการหมุนข้อมือ</asp:ListItem>
                    <asp:ListItem Value="A2">มีการเอียงข้อมือไปด้านข้าง (ซ้าย-ขวา)</asp:ListItem> 
              </asp:CheckBoxList>  
          </div>

        </div> 

            </div> 
   <h5 class="text-blue text-bold">การประเมินการจับยึดวัตถุ (Coupling)</h5>
         <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optReba11" runat="server" AutoPostBack="True">
                  <asp:ListItem Value="0">วัตถุจับยึดมีมือจับ ผู้ปฏิบัติสามารถจับยึดได้ถนัดมือสามารถกำได้รอบมือ</asp:ListItem>
                    <asp:ListItem Value="1">วัตถุจับยึดมีมือจับ แต่ไม่เหมาะสม ผู้ปฏิบัติไม่สามารถกำได้รอบมือ </asp:ListItem>
                    <asp:ListItem Value="2">ไม่มีมือจับแต่มีจุดที่สามารถสอดนิ้วมือหรืองอนิ้วมือเพื่อจับยึดได้</asp:ListItem>
                    <asp:ListItem Value="3">ไม่มีมือจับและวัตถุจับยึดได้ยากเช่น เปลี่ยนรูปร่างได้เป็นก้อนกลมใหญ่ ผิวลื่นมัน เป็นต้น</asp:ListItem>

              </asp:RadioButtonList>
          </div>

        </div> 
 
            </div>
</div>
      </div>
 <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">การประเมินการเคลื่อนไหวและกิจกรรมของงาน</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
            
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">การประเมินการเคลื่อนไหวและกิจกรรมของงาน</label>
               <asp:RadioButtonList ID="optReba13" runat="server" RepeatDirection="Vertical" AutoPostBack="True">
                  <asp:ListItem Value="1">ร่างกายส่วนใดส่วนหนึ่งอยู่กับที่นานกว่า 1 นาที</asp:ListItem>
                    <asp:ListItem Value="2">มีการเคลื่อนไหวร่างกายส่วนใดส่วนหนึ่งซ้ำๆ มากกว่า 4 ครั้งต่อนาที</asp:ListItem>
                    <asp:ListItem Value="2">มีการเปลี่ยนแปลงตำแหน่งท่าทางของร่างกายมากและเร็ว หรือมีการทรงตัวไม่ดี</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div>  
            </div>
 </div>
      </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Summary Score         </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

<div class="row">
         <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">ค่าคะแนนการประเมิน</label>
            <h3> <asp:Label ID="lblFinalScore" runat="server" Text="" CssClass="text-blue text-bold" ></asp:Label></h3> 
          </div>

        </div> 
     <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">สรุปแปลผล</label>
            <h3>  <asp:Label ID="lblFinalResult" runat="server" Text=""  CssClass="text-blue text-bold" ></asp:Label></h3>
          </div>

        </div> 

  </div>     

    </div>
             </div>
 </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="optReba1" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optReba2" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optReba3" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optReba5" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optReba7" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optReba8" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optReba9" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optReba11" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optReba13" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRebaAdd1" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRebaAdd2" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRebaAdd3" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRebaAdd5" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRebaAdd7" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRebaAdd9" EventName="SelectedIndexChanged" />
            </Triggers>

        </asp:UpdatePanel>

  <div class="row"> 

        <div class="col-md-12 text-center">

          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="บันทึก" Width="120px" /> 
             &nbsp;<asp:Button ID="cmdCancel" CssClass="btn btn-default" runat="server" Text="ยกเลิก" Width="120px" /> 
              <asp:Button ID="cmdDelete" CssClass="btn btn-danger" runat="server" Text="ลบ" Width="120px" /> 
        </div>
      </div>
</section>
</asp:Content>