﻿Imports System.Data
Public Class PersonSelect
    Inherits System.Web.UI.Page
    Dim ctlEmp As New PersonController
    Public dtES As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            If Request.Cookies("Ergo")("ROLE_ADM") = False And Request.Cookies("Ergo")("ROLE_SPA") = False Then
                LoadPersonByCustomer(Request.Cookies("Ergo")("LoginCompanyUID"))
            Else
                LoadPersonByAdmin()
            End If

        End If
    End Sub
    Private Sub LoadPersonByAdmin()
        Select Case Request("ActionType")
            Case "hraasm"
                dtES = ctlEmp.PersonRisk_Get
            Case "health"
                dtES = ctlEmp.Person_GetByStatus("A")
            Case Else
                dtES = ctlEmp.Person_GetAll
        End Select
    End Sub

    Private Sub LoadPersonByCustomer(CompanyUID As Integer)

        Select Case Request("ActionType")
            Case "hraasm"
                dtES = ctlEmp.PersonRisk_Get(CompanyUID)
            Case "health"
                dtES = ctlEmp.Person_GetByCompany(CompanyUID)
            Case Else
                dtES = ctlEmp.Person_GetByCompany(Request.Cookies("Ergo")("LoginCompanyUID"))
        End Select
    End Sub
End Class