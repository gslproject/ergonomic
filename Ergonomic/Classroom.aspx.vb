﻿Public Class Classroom
    Inherits System.Web.UI.Page
    Dim dtC As New DataTable
    Dim ctlE As New ElearningController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not Request("CID") Is Nothing Then
            dtC = ctlE.Course_GetByUID(Request("CID"))
            If dtC.Rows.Count > 0 Then
                With dtC.Rows(0)
                    Select Case .Item("MediaType")
                        Case "PPT", "PDF"
                            Response.Redirect("Classroom1?CID=" & Request("CID"))
                        Case "VDO"
                            Response.Redirect("Classroom2?CID=" & Request("CID"))
                    End Select
                End With
            End If
        End If
    End Sub
End Class