﻿Public Class PostTest
    Inherits System.Web.UI.Page

    Public dtCourse1 As New DataTable
    Dim dtP As New DataTable
    Dim ctlP As New PersonController
    'Dim ctlM As New MasterController
    Dim ctlE As New ElearningController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            lblAlert.Visible = False
            cmdSave.Visible = False

            If Not Request("CID") Is Nothing Then
                hdCourseUID.Value = Request("CID")
                LoadQuest(Request("CID"))
                LoadCourse(Request.Cookies("Ergo")("LoginPersonUID"), Request("CID"))
            End If
        End If
    End Sub
    Private Sub LoadQuest(CourseUID As Integer)
        Dim dtReq As New DataTable
        Dim dtA As New DataTable
        dtReq = ctlE.Question_Get(CourseUID)

        If dtReq.Rows.Count > 0 Then
            cmdSave.Visible = True
            With grdData
                .Visible = True
                .DataSource = dtReq
                .DataBind()
            End With

            For i = 0 To grdData.Rows.Count - 1
                Dim optA As New RadioButtonList

                optA = grdData.Rows(i).Cells(0).FindControl("optAnswer")

                dtA = ctlE.Answer_Get(CourseUID, grdData.DataKeys(i).Value)

                If dtA.Rows.Count > 0 Then
                    With optA
                        .Visible = True
                        .DataSource = dtA
                        .DataTextField = "Descriptions"
                        .DataValueField = "AnswerID"
                        .DataBind()
                    End With
                Else
                    optA.Visible = False
                End If
            Next
        Else
            grdData.Visible = False
            cmdSave.Visible = False
            lblAlert.Text = "ไม่พบแบบทดสอบ"
            lblAlert.Visible = True
        End If

    End Sub


    Private Sub LoadCourse(PersonUID As Integer, CourseUID As Integer)
        dtCourse1 = ctlE.Course_GetForClassroom(PersonUID, CourseUID)

        If dtCourse1.Rows.Count > 0 Then
            With dtCourse1.Rows(0)
                lblCourseName.Text = String.Concat(.Item("CourseName"))
            End With
        Else
            lblAlert.Text = "เกิดข้อผิดพลาดบางอย่าง ขออภัยในความไม่สะดวก"
            lblAlert.Visible = True
        End If

        'grdCompany.DataSource = dtCourse
        'grdCompany.DataBind()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Dim Acount As Integer = 0
        Dim TotalScore As Integer = 0
        Dim Score As Integer = 0
        Dim Rstatus As String = ""
        For i = 0 To grdData.Rows.Count - 1
            Dim optA As New RadioButtonList

            optA = grdData.Rows(i).Cells(0).FindControl("optAnswer")

            If optA.SelectedIndex = -1 Then
                Acount = Acount + 1
            Else
                If StrNull2Zero(optA.SelectedValue) = ctlE.Question_GetCorrectAnswer(hdCourseUID.Value, grdData.DataKeys(i).Value) Then
                    TotalScore = TotalScore + 1
                End If
            End If
        Next

        If Acount > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาทำแบบทดสอบให้ครบทุกข้อ');", True)
            Exit Sub
        End If

        If TotalScore > (grdData.Rows.Count / 2) Then
            Rstatus = "P"
            ctlE.PersonLearning_Save(hdCourseUID.Value, Request.Cookies("Ergo")("LoginPersonUID"), "C") 'Complete
        Else
            Rstatus = "X"
            ctlE.PersonLearning_Save(hdCourseUID.Value, Request.Cookies("Ergo")("LoginPersonUID"), "O") 'Complete
        End If
        ctlE.PersonTest_Save(Request.Cookies("Ergo")("LoginPersonUID"), hdCourseUID.Value, TotalScore, Rstatus)
        Dim PTUID As Integer
        PTUID = ctlE.PersonTest_GetUID(Request.Cookies("Ergo")("LoginPersonUID"), hdCourseUID.Value)
        For i = 0 To grdData.Rows.Count - 1
            Dim optA As New RadioButtonList

            optA = grdData.Rows(i).Cells(0).FindControl("optAnswer")

            If optA.SelectedIndex = -1 Then
                Acount = Acount + 1
            Else
                If StrNull2Zero(optA.SelectedValue) = ctlE.Question_GetCorrectAnswer(hdCourseUID.Value, grdData.DataKeys(i).Value) Then
                    TotalScore = TotalScore + 1
                    Score = 1
                Else
                    Score = 0
                End If
            End If

            ctlE.PersonTestScore_Save(PTUID, grdData.DataKeys(i).Value, optA.SelectedValue, IIf(Score = 1, "Y", "N"), Score)
        Next


        Response.Redirect("TestResult?CID=" & Request("CID"))
    End Sub

End Class