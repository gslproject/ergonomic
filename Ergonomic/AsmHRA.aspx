﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AsmHRA.aspx.vb" Inherits="Ergonomic.AsmHRA" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
    <h1>ผลการประเมินความเสี่ยงสุขภาพ</h1>
  </section>
  <section class="content">
    <div class="row">
      <section class="col-lg-12 connectedSortable">
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Personal data
              <asp:HiddenField ID="hdUID" runat="server" />   <asp:HiddenField ID="hdPersonUID" runat="server" />
            </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label>Code</label>
                  <asp:TextBox ID="txtCode" runat="server" cssclass="form-control text-center"  BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>

              </div>

              <div class="col-md-10">
                <div class="form-group">
                  <label>Name</label>
                  <asp:TextBox ID="txtName" runat="server" cssclass="form-control text-center"  BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>Position</label>
                 <asp:TextBox ID="txtPosition" runat="server" cssclass="form-control text-center"  BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-9">
                <div class="form-group">
                  <label>Department</label>
                  <asp:TextBox ID="txtDepartment" runat="server" cssclass="form-control text-center"  BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Section</label>
                  <asp:TextBox ID="txtSection" runat="server" cssclass="form-control text-center"  BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Remark</label>
                  <asp:TextBox ID="txtRemark" runat="server" cssclass="form-control text-center"  BackColor="White"></asp:TextBox>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="row">
      <section class="col-lg-6 connectedSortable">
          
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">อาการผิดปกติทางระบบโครงร่างและกล้ามเนื้อ</h3>
          </div>

          <div class="box-body">

            <div class="row">
              <div class="col-md-7">
                <div class="form-group">


                 <table id="tbdatabody" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th rowspan="2" class="text-center"  >ส่วนของร่างกาย</th>
                        <th colspan="2" class="text-center">ซ้าย</th>
                           <th colspan="2" class="text-center">ขวา</th>

                      </tr>
                         <tr>
                        <th class="text-center"> ความรุนแรง</th>
                        <th class="text-center">ความถี่</th>
                        <th class="text-center">ความรุนแรง</th>
                        <th class="text-center">ความถี่</th>
                      </tr>
                    </thead>
                    <tbody>

                 <% For Each row As DataRow In dtPersonRisk.Rows %>
                <tr> 
                  <td><% =String.Concat(row("BodyPart")) %></td> 
                  <td class="text-center"> <% If String.Concat(row("ScoreL")) >= "3" Then %> <span class="text-bold" style="color:red"> <% =String.Concat(row("ScoreL")) %></span>  <% Else %>  <% =String.Concat(row("ScoreL")) %>  <% End If %></td> 
                  <td class="text-center"><% If String.Concat(row("FreqL")) >= "3" Then %> <span class="text-bold" style="color:red"> <% =String.Concat(row("FreqL")) %></span>  <% Else %>  <% =String.Concat(row("FreqL")) %>  <% End If %></td> 
                  <td class="text-center"><% If String.Concat(row("ScoreR")) >= "3" Then %> <span class="text-bold" style="color:red"> <% =String.Concat(row("ScoreR")) %></span>  <% Else %>  <% =String.Concat(row("ScoreR")) %>  <% End If %></td> 
                   <td class="text-center"><% If String.Concat(row("FreqR")) >= "3" Then %> <span class="text-bold" style="color:red"> <% =String.Concat(row("FreqR")) %></span>  <% Else %>  <% =String.Concat(row("FreqR")) %>  <% End If %></td> 
                </tr>
            <%  Next %>
                    </tbody>
                  </table>


                </div>
              </div>
              <div class="col-md-5">               
                  <table style="width: 100%;">
                    <tr>                     
                      <td class="text-center">
                          <img src="images/body.png" /> 
                      </td>
                                         </tr>
                  </table>
              
              </div>
            </div>
          </div>
        </div>
      </section> 

   <!-- </div>
    <div class="row">-->

      <section class="col-lg-6 connectedSortable">
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">ความผิดปกติของระบบโครงร่างและกล้ามเนื้อ</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
               <h5 class="text-blue text-bold">ความผิดปกติของระบบโครงร่างและกล้ามเนื้อ</h5>
            <div class="row">

              <div class="col-md-6">
                <div class="form-group">
                  <label class="text-blue">คะแนน</label>
                  <asp:TextBox ID="txtBodyScore" runat="server" cssclass="form-control text-bold text-center"
                    placeholder="Kg." Font-Size="12" BackColor="White" ReadOnly="True" AutoPostBack="True"></asp:TextBox>  
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="text-blue">ระดับความเสี่ยง</label>
                  <asp:TextBox ID="txtBodyRisk" runat="server" cssclass="form-control text-bold text-center"
                    placeholder="0.00" Font-Size="12" BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
            </div>

 <h5 class="text-blue text-bold">การประเมินความเสี่ยงสุขภาพต่อความผิดปกติของระบบโครงร่างและกล้ามเนื้อ</h5>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="text-blue">คะแนน</label>
                  <asp:TextBox ID="txtErgoScore" runat="server" cssclass="form-control text-bold text-center" placeholder="Kg."
                    Font-Size="12" BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="text-blue">ระดับความเสี่ยง</label>
                  <asp:TextBox ID="txtErgoRisk" runat="server" cssclass="form-control text-bold text-center"
                    placeholder="0.00" Font-Size="12" BackColor="White" ReadOnly="True"></asp:TextBox>
                </div>
              </div>
            </div>
            <h4 class="text-blue text-bold">Recommendations</h4>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                    <asp:HyperLink ID="lblRecommend"  cssclass="form-control" runat="server"></asp:HyperLink>
                     
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
              <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
        <asp:Button ID="cmdBack" CssClass="btn btn-primary" runat="server" Text="Back" Width="120px" />
        </div>
    </div>
  </section>
</asp:Content>