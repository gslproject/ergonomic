﻿Imports System.Data
Public Class Company
    Inherits System.Web.UI.Page
    Dim ctlEmp As New CompanyController
    Public dtC As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Request.Cookies("Ergo")("ROLE_ADM") = False And Request.Cookies("Ergo")("ROLE_SPA") = False Then
            Response.Redirect("Error.aspx")
        End If

        If Not IsPostBack Then
            LoadCompany()
        End If
    End Sub
    Private Sub LoadCompany()
        dtC = ctlEmp.Company_Get
    End Sub

End Class