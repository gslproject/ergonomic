﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TaskTrackingNew.aspx.vb" Inherits="Ergonomic.TaskTrackingNew" %>
 

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>Task Actions
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Task Actions</li>
      </ol>
    </section>

    <!-- Main content -->   
      <section class="content">  
<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Task Information<asp:HiddenField ID="hdUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Task Name</label>
               <asp:DropDownList ID="ddlTask" runat="server" cssclass="form-control select2"  placeholder="Select Task" Width="100%" AutoPostBack="True">
            </asp:DropDownList> 
               
          </div>

        </div>
      
       </div>
                   
    </div>
    <!-- /.box-body -->
  </div>
  </section>
</div>
        <div class="row">
    <section class="col-lg-12 connectedSortable"> 
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">Actions</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                    <div class="row">

                            <div class="col-md-3">
          <div class="form-group">
            <label>Actions</label>
            <asp:DropDownList ID="ddlAction" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>

        </div>
       <div class="col-md-3">
          <div class="form-group">
            <label>Duedate</label>
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <asp:TextBox ID="txtDueDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </div>            
          </div>

        </div>
                         
        <div class="col-md-3">
          <div class="form-group">
            <label>Owner</label>
            <asp:DropDownList ID="ddlOwner" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>

        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Status</label>
            <asp:DropDownList ID="ddlStatus" runat="server" cssclass="form-control select2" Width="100%">
                <asp:ListItem Value="O">On going</asp:ListItem>
                <asp:ListItem Value="P">Pending</asp:ListItem>
                <asp:ListItem Value="X">Cancel</asp:ListItem>
                <asp:ListItem Value="A">Close</asp:ListItem>
            </asp:DropDownList>
          </div>

        </div>
         

                           <div class="col-md-12">
          <div class="form-group">
            <label>Comment</label>
               <asp:TextBox ID="txtComment" runat="server" cssclass="form-control" placeholder="Task name"></asp:TextBox>
          </div>

        </div>


      </div>
                   
                
</div> 
        <div class="box-footer clearfix">           
                
            </div>
          </div>
         
        </section>
                </div>

      <div class="row">   
        <div class="col-md-9 text-center">
          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="100px" />
        &nbsp;<asp:Button ID="cmdCancel"  CssClass="btn btn-default" runat="server" Text="ยกเลิก" Width="100px" />
        </div>
      </div>

          </section>
</asp:Content>