﻿
Public Class Users
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim objUser As New UserController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            grdData.PageIndex = 0
            LoadCompany()
            BindUserGroup()
            LoadUserAccountToGrid()

            If Request.Cookies("Ergo")("ROLE_ADM") = False And Request.Cookies("Ergo")("ROLE_SPA") = False Then
                'dtU = objUser.User_GetBySearch(Request.Cookies("Ergo")("LoginCompanyUID"), ddlGroupFind.SelectedValue, txtSearch.Text)
                ddlCompany.Enabled = False
            Else
                ddlCompany.Enabled = True
            End If


        End If
    End Sub
    Private Sub LoadCompany()
        Dim ctlC As New CompanyController
        ddlCompany.DataSource = ctlC.Company_GetActive()
        ddlCompany.DataTextField = "CompanyName"
        ddlCompany.DataValueField = "UID"
        ddlCompany.DataBind()
        ddlCompany.SelectedValue = Request.Cookies("Ergo")("LoginCompanyUID")

    End Sub
    Private Sub BindUserGroup()

        With ddlGroupFind
            .Items.Clear()

            .Items.Add("All")
            .Items(0).Value = "0"

            If Request.Cookies("Ergo")("ROLE_ADM") = True Or Request.Cookies("Ergo")("ROLE_SPA") = True Then
                .Items.Add("User")
                .Items(1).Value = "1"
                .Items.Add("Customer Admin")
                .Items(2).Value = "2"
                .Items.Add("NPC - SE Admin")
                .Items(3).Value = "3"
                .Items.Add("Super Administrator")
                .Items(4).Value = "9"
            ElseIf Request.Cookies("Ergo")("ROLE_CAD") = True Then
                .Items.Add("User")
                .Items(1).Value = "1"
                .Items.Add("Admin")
                .Items(2).Value = "2"
            End If
        End With

    End Sub


    Private Sub LoadUserAccountToGrid()
        Dim dtU As New DataTable
        dtU = objUser.User_GetBySearch(StrNull2Zero(ddlCompany.SelectedValue), ddlGroupFind.SelectedValue, txtSearch.Text)



        If dtU.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dtU
                .DataBind()
            End With
        Else
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If
        dt = Nothing

    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadUserAccountToGrid()
    End Sub
    Private Sub grdData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    Response.Redirect("UserModify?ActionType=user&ItemType=u&uid=" & e.CommandArgument())
                Case "imgDel"
                    If objUser.User_Delete(e.CommandArgument) Then

                        objUser.User_GenLogfile(Request.Cookies("Ergo")("username"), "DEL", "User", "ลบ user :" & e.CommandArgument, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการทำงาน','ลบข้อมูลเรียบร้อย');", True)

                        'DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)

                        'DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If

                    LoadUserAccountToGrid()

            End Select

        End If

    End Sub
    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(1).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d9edf7';")
        '    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        'End If


    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadUserAccountToGrid()
    End Sub

    Protected Sub ddlGroupFind_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroupFind.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadUserAccountToGrid()
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadUserAccountToGrid()
    End Sub
End Class

