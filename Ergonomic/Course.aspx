﻿<%@ Page Title="Course" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Course.aspx.vb" Inherits="Ergonomic.Course" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">    
        function openModals(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#btnConfirm').attr('onclick', "$('#mdEditPerson').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            $('.editemp').click(function () {
             var fname = $(this).attr('data-fname');
             $('#fname').val(fname);
             $('#mdEditPerson').modal('show');

            });
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
     <section class="content-header">
      <h1><%: Title %>        
        <small>ข้อมูลหลักสูตร</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Course</li>
      </ol>
    </section>

    <section class="content">   

        <div class="row no-print">
        <div class="col-xs-12">
          <a href="CourseModify?ActionType=comp"  class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> เพิ่มหลักสูตรใหม่</a>
        </div>
      </div>
        <br />
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">ข้อมูลรายชื่อหลักสูตร</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
              <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled">แก้ไข</th>    
                    <th>No.</th>
                  <th>ชื่อหลักสูตร</th>
                  <th>ประเภท</th>
                  <th>Display Order</th>                      
                     <th>Active</th>
                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtCourse.Rows %>
                <tr>
                 <td width="50px"><a class="editemp" href="CourseModify?cid=<% =String.Concat(row("UID")) %>" ><img src="images/icon-edit.png"/></a>
                    </td>
                     <td><% =String.Concat(row("UID")) %></td>
                  <td><% =String.Concat(row("Name")) %></td>
                  <td><% =String.Concat(row("MediaType")) %>    </td>
                  <td><% =String.Concat(row("DisplayOrder")) %></td> 
                    <td><% =IIf(String.Concat(row("StatusFlag")) = "A", "<img src='images/icon-ok.png'>", "") %> </td>
                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div>
  
    </section>
</asp:Content>
