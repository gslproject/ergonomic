﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TaskModify.aspx.vb" Inherits="Ergonomic.TaskModify" %>

<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
      <link href="css/DragAndDrop.css" rel="stylesheet" type="text/css" />  
         
<script lang="javascript">
       function onUploadControlFileUploadComplete1(s, e) {
            if (e.isValid)
                document.getElementById("uploadedImage1").src = "/imgTask/" + e.callbackData;
            setElementVisible("uploadedImage1", e.isValid);
            document.getElementById("uploadedImage1").className = "img-responsive";
        }

    function onImageLoad1() {
            var externalDropZone = document.getElementById("externalDropZone1");
            var uploadedImage = document.getElementById("uploadedImage1");
            uploadedImage.style.left = (externalDropZone.clientWidth - uploadedImage.width) / 2 + "px";
            uploadedImage.style.top = (externalDropZone.clientHeight - uploadedImage.height) / 2 + "px";
            setElementVisible("dragZone1", false);
            document.getElementById("uploadedImage1").className = "img-responsive";
           
    }

     function onUploadControlFileUploadComplete2(s, e) {
            if (e.isValid)
                document.getElementById("uploadedImage2").src = "/imgTask/" + e.callbackData;
            setElementVisible("uploadedImage2", e.isValid);
            document.getElementById("uploadedImage2").className = "img-responsive";
        }
        function onImageLoad2() {
            var externalDropZone = document.getElementById("externalDropZone2");
            var uploadedImage = document.getElementById("uploadedImage2");
            uploadedImage.style.left = (externalDropZone.clientWidth - uploadedImage.width) / 2 + "px";
            uploadedImage.style.top = (externalDropZone.clientHeight - uploadedImage.height) / 2 + "px";
            setElementVisible("dragZone2", false);
            document.getElementById("uploadedImage2").className = "img-responsive";
           
    }


     function onUploadControlFileUploadComplete3(s, e) {
            if (e.isValid)
                document.getElementById("uploadedImage3").src = "/imgTask/" + e.callbackData;
            setElementVisible("uploadedImage3", e.isValid);
            document.getElementById("uploadedImage3").className = "img-responsive";
        }
        function onImageLoad3() {
            var externalDropZone = document.getElementById("externalDropZone3");
            var uploadedImage = document.getElementById("uploadedImage3");
            uploadedImage.style.left = (externalDropZone.clientWidth - uploadedImage.width) / 2 + "px";
            uploadedImage.style.top = (externalDropZone.clientHeight - uploadedImage.height) / 2 + "px";
            setElementVisible("dragZone3", false);
            document.getElementById("uploadedImage3").className = "img-responsive";
           
    }
         

        function setElementVisible(elementId, visible) {
            document.getElementById(elementId).className = visible ? "" : "hidden";
        }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>Task Register
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Task</li>
      </ol>
    </section>

    <!-- Main content -->   
      <section class="content">  
<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Task Information<asp:HiddenField ID="hdTaskUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">


      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Task No.</label>
            <asp:TextBox ID="txtTaskNo" runat="server" cssclass="form-control" placeholder="Task Number"></asp:TextBox>
          </div>

        </div>
      
      </div>
      <div class="row">

        <div class="col-md-6">
          <div class="form-group">
            <label>Task name</label>
            <asp:TextBox ID="txtName" runat="server" cssclass="form-control" placeholder="Task name"></asp:TextBox>
            &nbsp;
          </div>

        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Machine</label>
            <asp:DropDownList ID="ddlMachine" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
            
          </div>

        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Description</label>
            <asp:TextBox ID="txtDescription" runat="server" cssclass="form-control" placeholder="Task description"></asp:TextBox>
          </div>

        </div>
       </div>

      <div class="row">  
          <div class="col-md-6">
          <div class="form-group">
            <label>PPE Requirement</label>
                <asp:TextBox ID="txtPPE" runat="server" cssclass="form-control" placeholder="ระบุ"></asp:TextBox>
            &nbsp;
          </div>

        </div>

   

        <div class="col-md-6">
          <div class="form-group">
            <label>Other hazard</label>
              <asp:CheckBoxList ID="chkHazard" runat="server" RepeatDirection="Horizontal">
                  <asp:ListItem Value="Hot">Hot</asp:ListItem>
                  <asp:ListItem Value="Cold">Cold</asp:ListItem>
                  <asp:ListItem Value="Noise">Noise</asp:ListItem>
<asp:ListItem Value="Chemical">Chemical</asp:ListItem>
<asp:ListItem Value="Biological">Biological</asp:ListItem>
              </asp:CheckBoxList>
            
          </div>

        </div> 
    </div>
<div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>Recommend Tool</label>
              <asp:CheckBoxList ID="chkTools" runat="server" RepeatDirection="Horizontal">                  
                  <asp:ListItem Value="ROSA">ROSA</asp:ListItem>
                  <asp:ListItem Value="REBA">REBA</asp:ListItem>
                  <asp:ListItem Value="RULA">RULA</asp:ListItem>
                  <asp:ListItem Value="NIOSH">NIOSH</asp:ListItem> 
              </asp:CheckBoxList>
            
          </div>

        </div> 
  
</div>
    

    
    </div>
    <!-- /.box-body -->
  </div>
  </section>
</div>
        <div class="row">
    <section class="col-lg-12 connectedSortable"> 
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">Description</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                    <div class="row">

                            <div class="col-md-3">
          <div class="form-group">
            <label>Company</label>
            <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>

        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Position</label>
            <asp:DropDownList ID="ddlPosition" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>

        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Section</label>
            <asp:DropDownList ID="ddlDivision" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>

        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Department</label>
            <asp:DropDownList ID="ddlDepartment" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>

        </div>
         
      </div>
                   
                
</div> 
        <div class="box-footer clearfix">           
                
            </div>
          </div>

    <div class="box box-success">
        <div class="box-header with-border">
          <h2 class="box-title">Task Picture</h2>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            
          </div>
        </div>
        <div class="box-body" align="center">
               
              <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label></label>      
               <div id="externalDropZone1" class="dropZoneExternal">
        <div id="dragZone1">
            <span class="dragZoneText">ลากรูปมาวางที่นี่</span>
        </div>
      <img id="uploadedImage1" src='<% Response.Write("/" & Ergonomic.PictureTask & "/" & Request.Cookies("Ergo")("taskimg1")) %>' width="200" height="200" class="img-responsive" alt="" onload="onImageLoad1()" />      
        <div id="dropZone1" class="hidden">
            <span class="dropZoneText">Drop an image here</span>
        </div>
    </div>
    <dx:ASPxUploadControl ID="UploadControl" ClientInstanceName="UploadControl" runat="server" UploadMode="Auto" AutoStartUpload="True" Width="80%" 
        ShowProgressPanel="True" CssClass="uploadControl" DialogTriggerID="externalDropZone1" OnFileUploadComplete="UploadControl_FileUploadComplete1" >
        <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" ExternalDropZoneID="externalDropZone1" DropZoneText="" />
        <ValidationSettings MaxFileSize="4194304" AllowedFileExtensions=".jpg, .jpeg, .gif, .png" ErrorStyle-CssClass="validationMessage" />
        <BrowseButton Text="เลือกรูปสำหรับอัพโหลด..." />
        <DropZoneStyle CssClass="uploadControlDropZone" />
        <ProgressBarStyle CssClass="uploadControlProgressBar" />
        <ClientSideEvents 
            DropZoneEnter="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', true); }" 
            DropZoneLeave="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', false); }" 
            FileUploadComplete="onUploadControlFileUploadComplete1">
        </ClientSideEvents>
    </dx:ASPxUploadControl>


          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label></label>
                 <div id="externalDropZone2" class="dropZoneExternal">
        <div id="dragZone2">
            <span class="dragZoneText">ลากรูปมาวางที่นี่</span>
        </div>
      <img id="uploadedImage2" src='<% Response.Write("/" & Ergonomic.PictureTask & "/" & Request.Cookies("Ergo")("taskimg2")) %>' width="200" height="200" class="img-responsive" alt="" onload="onImageLoad2()" />      
        <div id="dropZone2" class="hidden">
            <span class="dropZoneText">Drop an image here</span>
        </div>
    </div>
    <dx:ASPxUploadControl ID="ASPxUploadControl1" ClientInstanceName="UploadControl" runat="server" UploadMode="Auto" AutoStartUpload="True" Width="80%" 
        ShowProgressPanel="True" CssClass="uploadControl" DialogTriggerID="externalDropZone2" OnFileUploadComplete="UploadControl_FileUploadComplete2" >
        <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" ExternalDropZoneID="externalDropZone2" DropZoneText="" />
        <ValidationSettings MaxFileSize="4194304" AllowedFileExtensions=".jpg, .jpeg, .gif, .png" ErrorStyle-CssClass="validationMessage" />
        <BrowseButton Text="เลือกรูปสำหรับอัพโหลด..." />
        <DropZoneStyle CssClass="uploadControlDropZone" />
        <ProgressBarStyle CssClass="uploadControlProgressBar" />
        <ClientSideEvents 
            DropZoneEnter="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', true); }" 
            DropZoneLeave="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', false); }" 
            FileUploadComplete="onUploadControlFileUploadComplete2">
        </ClientSideEvents>
    </dx:ASPxUploadControl>

          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label></label>
                 <div id="externalDropZone3" class="dropZoneExternal">
        <div id="dragZone3">
            <span class="dragZoneText">ลากรูปมาวางที่นี่</span>
        </div>
      <img id="uploadedImage3" src='<% Response.Write("/" & Ergonomic.PictureTask & "/" & Request.Cookies("Ergo")("taskimg3")) %>' width="200" height="200" class="img-responsive" alt="" onload="onImageLoad3()" />      
        <div id="dropZone3" class="hidden">
            <span class="dropZoneText">Drop an image here</span>
        </div>
    </div>
    <dx:ASPxUploadControl ID="ASPxUploadControl2" ClientInstanceName="UploadControl" runat="server" UploadMode="Auto" AutoStartUpload="True" Width="80%" 
        ShowProgressPanel="True" CssClass="uploadControl" DialogTriggerID="externalDropZone3" OnFileUploadComplete="UploadControl_FileUploadComplete3" >
        <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="True" EnableMultiSelect="True" ExternalDropZoneID="externalDropZone3" DropZoneText="" />
        <ValidationSettings MaxFileSize="4194304" AllowedFileExtensions=".jpg, .jpeg, .gif, .png" ErrorStyle-CssClass="validationMessage" >
<ErrorStyle CssClass="validationMessage"></ErrorStyle>
        </ValidationSettings>
        <BrowseButton Text="เลือกรูปสำหรับอัพโหลด..." />
        <DropZoneStyle CssClass="uploadControlDropZone" />
        <ProgressBarStyle CssClass="uploadControlProgressBar" />
        <ClientSideEvents 
            DropZoneEnter="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', true); }" 
            DropZoneLeave="function(s, e) { if(e.dropZone.id == 'externalDropZone') setElementVisible('dropZone', false); }" 
            FileUploadComplete="onUploadControlFileUploadComplete3">
        </ClientSideEvents>
    </dx:ASPxUploadControl>

          </div>
        </div>
      </div>      


             <div class="pull-left">            </div> 
           <div class="pull-right">            </div>         
                                 
             </div> 
     <div class="box-footer">  <asp:Label ID="lblNoSuccess" runat="server" ForeColor="Red" Text="Upload รูปไม่สำเร็จ กรุณาตรวจสอบไฟล์ แล้วลองใหม่อีกครั้ง" Visible="False"></asp:Label>
         </div>
      </div>
        </section>
                </div>

      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Status</label>
              <asp:CheckBox ID="chkStatus" runat="server" Text="   Active" />
          </div>
        </div>

        <div class="col-md-9">

          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
            
          <asp:Button ID="cmdDelete" CssClass="btn btn-danger" runat="server" Text="Delete" Width="120px" />

        </div>
      </div>

          </section>
</asp:Content>