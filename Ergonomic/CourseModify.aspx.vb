﻿Imports System.IO
Public Class CourseModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New ElearningController
    Dim ctlM As New MasterController
    'Dim sFile As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Request.Cookies("Ergo")("ROLE_ADM") = False And Request.Cookies("Ergo")("ROLE_SPA") = False Then
            Response.Redirect("Error.aspx")
        End If

        If Not IsPostBack Then
            LoadCourseData()
        End If

        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")
    End Sub

    Private Sub LoadCourseData()
        dt = ctlE.Course_GetByUID(Request("cid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdCompUID.Value = String.Concat(.Item("UID"))
                txtCourseID.Text = String.Concat(.Item("UID"))
                txtName.Text = String.Concat(.Item("Name"))
                ddlMediaType.SelectedValue = String.Concat(.Item("MediaType"))
                txtMediaCount.Text = String.Concat(.Item("MediaCount"))
                txtLink.Text = String.Concat(.Item("MediaLink"))
                hlnkImage.Text = String.Concat(.Item("PicturePath"))
                hlnkImage.NavigateUrl = CourseImage & "/" & String.Concat(.Item("PicturePath"))

                txtSort.Text = String.Concat(.Item("DisplayOrder"))
                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(.Item("StatusFlag")))
            End With

        Else

        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtCourseID.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุรหัสบริษัท');", True)
            DisplayMessage(Me.Page, "กรุณาระบุรหัสบริษัท")
        End If
        If txtName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อบริษัท');", True)
            DisplayMessage(Me.Page, "กรุณาระบุชื่อบริษัท")
        End If

        ctlE.Course_Save(StrNull2Zero(txtCourseID.Text), txtName.Text, ddlMediaType.SelectedValue, txtMediaCount.Text, txtSort.Text, txtLink.Text, hlnkImage.Text, ConvertBoolean2StatusFlag(chkStatus.Checked))

        UploadFile(FileUploadImg, CourseImage)
        'UploadFile(FileUploadMedia, ElearningResource)
        'Dim ctlU As New UserController
        'ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_UPD, "Customer", "บันทึก/แก้ไข ประวัติส่วนตัว :{uid=" & hdCompUID.Value & "}{code=" & txtCourseID.Text & "}", "")

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Response.Redirect("Course.aspx?p=complete")
    End Sub

    Sub UploadFile(ByVal Fileupload As Object, sFilePath As String)
        Dim FileFullName As String = Fileupload.PostedFile.FileName
        Dim FileNameInfo As String = Path.GetFileName(FileFullName)
        Dim filename As String = Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As FileInfo = New FileInfo(Server.MapPath(sFilePath))
        If FileNameInfo <> "" Then
            'Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & filename)
            'Path.GetExtension(FileFullName)
            Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & sFilePath & "\" & filename)

            If sFilePath = "imgCourse" Then
                ctlE.Course_UpdateFilename(txtCourseID.Text, filename)
            End If
        End If
        objfile = Nothing
    End Sub


    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlE.Course_Delete(StrNull2Zero(hdCompUID.Value))
    End Sub
End Class