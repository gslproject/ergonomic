﻿Imports System.Data
Public Class Person
    Inherits System.Web.UI.Page
    Dim ctlEmp As New PersonController
    Public dtE As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadPerson()
        End If
    End Sub
    Private Sub LoadPerson()
        If Request.Cookies("Ergo")("ROLE_ADM") = False And Request.Cookies("Ergo")("ROLE_SPA") = False Then
            dtE = ctlEmp.Person_GetAllByCompany(Request.Cookies("Ergo")("LoginCompanyUID"))
        Else
            dtE = ctlEmp.Person_GetAll
        End If
    End Sub
End Class