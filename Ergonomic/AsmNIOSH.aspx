﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AsmNIOSH.aspx.vb" Inherits="Ergonomic.AsmNIOSH" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>การประเมินความเสี่ยงโดยวิธี NIOSH Lifting</h1>     
    </section>
    <section class="content">   
<div class="row">
   <section class="col-lg-12 connectedSortable">
       <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">ErgonomicAssessment<asp:HiddenField ID="hdUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

      <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>Task No</label>
              <asp:TextBox ID="txtTaskNo" runat="server" cssclass="form-control" placeholder="Task no."></asp:TextBox>
          </div>

        </div>

        <div class="col-md-10">
          <div class="form-group">
            <label>Task Name</label>
              <asp:TextBox ID="txtTaskName" runat="server" cssclass="form-control" ></asp:TextBox>
               
          </div>

        </div>
      
      </div>
      <div class="row">
  <div class="col-md-3">
          <div class="form-group">
            <label>Date</label>
              <asp:TextBox ID="txtAsmDate" runat="server" placeholder="Date of assessment" CssClass="form-control datepicker-dropdown"></asp:TextBox>
          </div>

        </div> 

        <div class="col-md-9">
          <div class="form-group">
            <label>Type</label>
              <asp:RadioButtonList ID="optAsmType" runat="server" RepeatDirection="Horizontal">
                  <asp:ListItem Value="S">Self-assessment</asp:ListItem>
                  <asp:ListItem Value="O">Observer assessment</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 

      </div>
   
        <div class="row">        
       <div class="col-md-12">
          <div class="form-group">
            <label>ผู้รับการประเมิน</label>
              <asp:DropDownList ID="ddlPerson" runat="server" cssclass="form-control select2"  placeholder="Select Person" Width="100%">
            </asp:DropDownList> 
          </div>

        </div> 
</div>
          <div class="row">        
       <div class="col-md-12">
          <div class="form-group">
            <label>Remark</label>
              <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" Height="50px" TextMode="MultiLine"></asp:TextBox>
          </div>

        </div> 
</div>
</div>
      </div>
</section>
</div>
        
       <div class="row">
  <section class="col-lg-5 connectedSortable">

         <div class="box box-solid">
    <div class="box-header">
      <h3 class="box-title"></h3>
    </div> 
    <div class="box-body">
          <div class="row">             
             <div class="col-md-6">
        <img    src="images/NIOSH/NIOSH01.jpg" />

        </div>          
              <div class="col-md-6">
               <img    src="images/NIOSH/NIOSH02.jpg" />

        </div>      
            </div>

  </div>
    
  </div>
      </section>

   <section class="col-lg-4 connectedSortable">

         <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Model Inputs </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        
        <div class="row">             
             <div class="col-md-6">
          <div class="form-group">
            <label class="h5 text-blue">Load Weight (L)</label><small>(กก.)</small>
               <asp:TextBox ID="txtL" runat="server" cssclass="form-control text-center" placeholder="" AutoPostBack="True"></asp:TextBox>
          </div>

        </div>          

            </div>

        
        <div class="row">
             
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Horizontal location (H)</label><small>(25 - 64 ซม.)</small>
             <asp:TextBox ID="txtH" runat="server" cssclass="form-control text-center" placeholder="" AutoPostBack="True"></asp:TextBox>
          </div>

        </div> 


            </div>
        
        
        <div class="row">
             
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Vertical location (V)</label><small>  (0 - 178 ซม.)</small>
               <asp:TextBox ID="txtV" runat="server" cssclass="form-control text-center" placeholder="" AutoPostBack="True"></asp:TextBox>
          </div>

        </div>          

            </div>
         
        <div class="row">
             
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Travel distance (D)</label><small>(25-178 ซม.)</small>
               <asp:TextBox ID="txtD" runat="server" cssclass="form-control text-center" placeholder="" AutoPostBack="True"></asp:TextBox>
          </div>

        </div>          

            </div>
         
        <div class="row">
             
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Asymmetry angle (A)</label> <small>(0-135 องศา)</small>
              <asp:DropDownList ID="ddlA" runat="server" CssClass="form-control select2 text-center" AutoPostBack="True">
                       <asp:ListItem Value="1">0</asp:ListItem>
<asp:ListItem Value="0.95">15</asp:ListItem>
<asp:ListItem Value="0.9">30</asp:ListItem>
<asp:ListItem Value="0.86">45</asp:ListItem>
<asp:ListItem Value="0.81">60</asp:ListItem>
<asp:ListItem Value="0.76">75</asp:ListItem>
<asp:ListItem Value="0.71">90</asp:ListItem>
<asp:ListItem Value="0.66">105</asp:ListItem>
<asp:ListItem Value="0.62">120</asp:ListItem>
<asp:ListItem Value="0.57">135</asp:ListItem>
<asp:ListItem Value="0">>135</asp:ListItem>

              </asp:DropDownList> 
          </div>

        </div>          

            </div>


        <div class="row">
             
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Coupling (C)</label> <small></small>
<asp:RadioButtonList ID="optC" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Selected="True" Value="1">Good</asp:ListItem>
                        <asp:ListItem Value="2">Fair</asp:ListItem>
                        <asp:ListItem Value="3">Poor</asp:ListItem>
                    </asp:RadioButtonList>
          </div>

        </div> 
             
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Duration (Dur)</label> <small>  </small>
              <asp:RadioButtonList ID="optDur" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1" Selected="True">1 hr</asp:ListItem>
                        <asp:ListItem Value="2">1-2 hr</asp:ListItem>
                        <asp:ListItem Value="8">2-8 hr</asp:ListItem>
                    </asp:RadioButtonList>
          </div>

        </div>          

        </div>


        <div class="row">   
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Frequency (F)</label><small>  </small>
              <asp:DropDownList ID="ddlF" runat="server" CssClass="form-control select2" AutoPostBack="True">
                       <asp:ListItem Value="0.2">&lt;=0.2</asp:ListItem>
                        <asp:ListItem Value="0.5">0.5</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                       <asp:ListItem>2</asp:ListItem>
                       <asp:ListItem>3</asp:ListItem>
                       <asp:ListItem>4</asp:ListItem>
                       <asp:ListItem>5</asp:ListItem>
                       <asp:ListItem>6</asp:ListItem>
                       <asp:ListItem>7</asp:ListItem>
                       <asp:ListItem>8</asp:ListItem>
                       <asp:ListItem>9</asp:ListItem>
                       <asp:ListItem>10</asp:ListItem>
                       <asp:ListItem>11</asp:ListItem>
                       <asp:ListItem>12</asp:ListItem>
                       <asp:ListItem>13</asp:ListItem>
                       <asp:ListItem>14</asp:ListItem>
                       <asp:ListItem>15</asp:ListItem>
                       <asp:ListItem Value="16">&gt;15</asp:ListItem>
              </asp:DropDownList> 
          </div>

        </div>          

            </div>
                          
    
    </div>
    <div class="box-footer clearfix">           
                <!--ที่มา : (Sonne, Villalta, & Andrews, 2012) , http://thai-ergonomic-assessment.blogspot.com-->
            </div>
  </div>
</section>
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        
<section class="col-lg-3 connectedSortable">

  
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Multiplier </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        <table class="table table-hover h4" style="width: 100%;">
            <tr>
                <td>HM</td>
                <td><asp:Label ID="lblHM" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td>VM</td>
                <td><asp:Label ID="lblVM" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td>DM</td>
                <td><asp:Label ID="lblDM" runat="server" Text=""></asp:Label></td>
            </tr>
             <tr>
                <td>AM</td>
                <td><asp:Label ID="lblAM" runat="server" Text=""></asp:Label></td>
            </tr>
             <tr>
                <td>CM</td>
                <td><asp:Label ID="lblCM" runat="server" Text=""></asp:Label></td>
            </tr>
            
             <tr>
                <td>FM</td>
                <td><asp:Label ID="lblFM" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>

         
</div>
      </div>

   

  
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Model Output </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">




       <div class="row">             
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Recomemd weight limit (RWL) </label>
               <asp:TextBox ID="txtRWL" runat="server" cssclass="form-control text-bold text-center" placeholder="Kg." Font-Size="12" BackColor="White" ReadOnly="True" AutoPostBack="True"></asp:TextBox>
          </div>

        </div>   
                         
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Lifting Index (LI=Load/RWL)</label>
               <asp:TextBox ID="txtLI" runat="server" cssclass="form-control text-bold text-center" placeholder="0.00" Font-Size="12" BackColor="White" ReadOnly="True"></asp:TextBox>
          </div>
             
        </div>

        </div>

          <div class="row">             
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Frequency Independent RWL</label>
               <asp:TextBox ID="txtFI" runat="server" cssclass="form-control text-bold text-center" placeholder="Kg." Font-Size="12" BackColor="White" ReadOnly="True"></asp:TextBox>
          </div>

        </div>             
                   
             <div class="col-md-6">
          <div class="form-group">
            <label class="text-blue">Frequency Independent LI</label>
               <asp:TextBox ID="txtFILI" runat="server" cssclass="form-control text-bold text-center" placeholder="0.00" Font-Size="12" BackColor="White" ReadOnly="True"></asp:TextBox>
          </div>

        </div>             
        </div>

       
     
         <h4 class="text-blue text-bold">Recommendations</h4>
         <div class="row">
             <div class="col-md-12">
          <div class="form-group">
              <asp:TextBox ID="txtRecommend" cssclass="form-control text-bold text-center"  runat="server" Font-Size="12" BackColor="White" ReadOnly="True"></asp:TextBox>
          </div>

        </div>  
            </div>

         
</div>
      </div>

    </section>
   </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="txtL" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtH" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtV" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtD" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlA" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="optC" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="optDur" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlF" EventName="SelectedIndexChanged" /> 

        </Triggers>
    </asp:UpdatePanel>     
           
       
       </div>

  <div class="row"> 

        <div class="col-md-12 text-center">

          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="บันทึก" Width="120px" /> 
             &nbsp;<asp:Button ID="cmdCancel" CssClass="btn btn-default" runat="server" Text="ยกเลิก" Width="120px" /> 
              <asp:Button ID="cmdDelete" CssClass="btn btn-danger" runat="server" Text="ลบ" Width="120px" /> 
        </div>
      </div>
 </section>
</asp:Content>