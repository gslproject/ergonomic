﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="EvaluationTopicModify.aspx.vb" Inherits="Ergonomic.EvaluationTopicModify" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<section class="content-header">
      <h1>จัดการหัวข้อการประเมิน</h1>   
    </section>

<section class="content">  
    <div class="row">
   <section class="col-lg-12 connectedSortable">
         <div class="box box-primary">

            <div class="box-header">
              <i class="fa fa-filter"></i>

              <h3 class="box-title">เพิ่ม/แก้ไข ข้อสอบ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                            <div class="row">
        <div class="col-md-1">
          <div class="form-group">
            <label>ID</label>            
              <asp:Label ID="lblID"  cssclass="form-control text-center" runat="server"></asp:Label>  
          </div>

        </div>
  <div class="col-md-11">
          <div class="form-group">
            <label>หลักสูตร/บทเรียน</label>
               <asp:DropDownList ID="ddlCourse" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>
        </div> 
                                </div>
    <div class="row">
      <div class="col-md-12">
          <div class="form-group">
            <label>หัวข้อ</label>
            <asp:TextBox ID="txtName" runat="server" cssclass="form-control" placeholder="คำถาม" MaxLength="1000"></asp:TextBox>
          </div>
        </div>
  </div>
           <div class="row">
         <div class="col-md-1">
          <div class="form-group">
            <label>ลำดับ</label>
            <asp:TextBox ID="txtDisplayOrder" runat="server" cssclass="form-control text-center" ></asp:TextBox>
          </div>

        </div>
         <div class="col-md-1">
          <div class="form-group">
            <label>Status</label>
              <br />
            <asp:CheckBox ID="chkStatus" runat="server" Text="Active"  Checked="True" />
          </div>

        </div>

         <div class="col-md-2">
          <div class="form-group text-center">
            <label></label>
          </div>
                     </div>

</div>

               
</div>
            <div class="box-footer text-center clearfix">
           
            </div>
          </div>

  </section>
 <asp:Panel ID="pnAnswer" runat="server"> 

          <section class="col-lg-12 connectedSortable">
         <div class="box box-primary">

            <div class="box-header">
              <i class="fa fa-filter"></i>

              <h3 class="box-title">จัดการคำตอบ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                            <div class="row">
                                <table class="table table-hover">
                                    <tr>
                                        <td width="40px" class="text-center" >ก.</td>
                                        <td> <asp:TextBox ID="txtAnswer1" runat="server" cssclass="form-control" ></asp:TextBox></td>                                       
                                    </tr>
                                     <tr>
                                        <td class="text-center">ข.</td>
                                        <td> <asp:TextBox ID="txtAnswer2" runat="server" cssclass="form-control" ></asp:TextBox></td>                                       
                                    </tr>
                                     <tr>
                                        <td class="text-center">ค.</td>
                                        <td> <asp:TextBox ID="txtAnswer3" runat="server" cssclass="form-control" ></asp:TextBox></td>                                       
                                    </tr>
                                     <tr>
                                        <td class="text-center">ง.</td>
                                        <td> <asp:TextBox ID="txtAnswer4" runat="server" cssclass="form-control" ></asp:TextBox></td>                                       
                                    </tr>
                                     <tr>
                                        <td class="text-center">จ.</td>
                                        <td> <asp:TextBox ID="txtAnswer5" runat="server" cssclass="form-control" ></asp:TextBox></td>                                       
                                    </tr>
                                  
                                </table>
                                </div>
    <div class="row">
      <div class="col-md-3">
          <div class="form-group">
            <label>คำตอบที่ถูก</label>
              <asp:DropDownList ID="ddlAnswer" runat="server" cssclass="form-control select2" Width="100%">
                  <asp:ListItem>ก</asp:ListItem>
                  <asp:ListItem>ข</asp:ListItem>
                  <asp:ListItem>ค</asp:ListItem>
                  <asp:ListItem>ง</asp:ListItem>
                  <asp:ListItem>จ</asp:ListItem>
            </asp:DropDownList>
          </div>
        </div>
  </div>     
               
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

  </section>       

        </asp:Panel>

        <div class="text-center">
            <asp:Button ID="cmdSaveQ" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
            <asp:Button ID="cmdSaveA" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
            <asp:Button ID="cmdCancel" CssClass="btn btn-default" runat="server" Text="Cancel" Width="120px" />
             <asp:Button ID="cmdDelete" CssClass="btn btn-danger" runat="server" Text="Delete" Width="120px" />
          
      </div>
   </div>
   </section>   
</asp:Content>
