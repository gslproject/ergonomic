﻿Public Class PersonalHealth
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlP As New PersonController
    Dim ctlM As New MasterController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadSafetyToolsToCheckList()

            If Request("pid") Is Nothing Then
                LoadPersonData(Request.Cookies("Ergo")("LoginPersonUID"))
            Else
                LoadPersonData(Request("pid"))
            End If

        End If
    End Sub

    'Private Sub LoadBusinessUnit()
    '    Dim ctlOrg As New OrganizationController
    '    ddlBU.DataSource = ctlOrg.Organization_Get()
    '    ddlBU.DataTextField = "UnitName"
    '    ddlBU.DataValueField = "OrganizationCode"
    '    ddlBU.DataBind()
    'End Sub

    'Private Sub LoadDivision()
    '    ddlDivision.DataSource = ctlM.Division_Get(Request.Cookies("Ergo")("LoginCompanyUID"))
    '    ddlDivision.DataTextField = "DivisionName"
    '    ddlDivision.DataValueField = "DivisionUID"
    '    ddlDivision.DataBind()
    'End Sub
    'Private Sub LoadDepartment()
    '    ddlDepartment.DataSource = ctlM.Department_GetByDivisionUID(Request.Cookies("Ergo")("LoginCompanyUID"), ddlDivision.SelectedValue)
    '    ddlDepartment.DataTextField = "DepartmentName"
    '    ddlDepartment.DataValueField = "DepartmentUID"
    '    ddlDepartment.DataBind()
    'End Sub
    'Private Sub LoadPosition()
    '    ddlPosition.DataSource = ctlM.Position_Get(Request.Cookies("Ergo")("LoginCompanyUID"))
    '    ddlPosition.DataTextField = "PositionName"
    '    ddlPosition.DataValueField = "PositionUID"
    '    ddlPosition.DataBind()
    'End Sub
    Private Sub LoadROWL()
        dt = ctlP.PersonROWL_Get(hdPersonUID.Value)
        If dt.Rows.Count > 0 Then
            optROWL1.SelectedValue = String.Concat(dt.Rows(0)("ROWL1"))
            optROWL2.SelectedValue = String.Concat(dt.Rows(0)("ROWL2"))
            optROWL3.SelectedValue = String.Concat(dt.Rows(0)("ROWL3"))
            optROWL4.SelectedValue = String.Concat(dt.Rows(0)("ROWL4"))
            optROWL5.SelectedValue = String.Concat(dt.Rows(0)("ROWL5"))
            optROWL6.SelectedValue = String.Concat(dt.Rows(0)("ROWL6"))
            optROWL7.SelectedValue = String.Concat(dt.Rows(0)("ROWL7"))
            optROWL8.SelectedValue = String.Concat(dt.Rows(0)("ROWL8"))
        End If
    End Sub

    Private Sub LoadPersonErgonomicRisk()
        dt = ctlP.PersonErgonomicRisk_Get(hdPersonUID.Value)
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                Select Case dt.Rows(i)("BodyPartCode")
                    Case BODYPART_NECK
                        If dt.Rows(i)("Side") = "L" Then
                            optNeck_Score_L.SelectedValue = dt.Rows(i)("Score")
                            optNeck_Freq_L.SelectedValue = dt.Rows(i)("Freq")
                        End If
                        If dt.Rows(i)("Side") = "R" Then
                            optNeck_Score_R.SelectedValue = dt.Rows(i)("Score")
                            optNeck_Freq_R.SelectedValue = dt.Rows(i)("Freq")
                        End If

                    Case BODYPART_SHOULDER
                        If dt.Rows(i)("Side") = "L" Then
                            optShoulder_Score_L.SelectedValue = dt.Rows(i)("Score")
                            optShoulder_Freq_L.SelectedValue = dt.Rows(i)("Freq")
                        End If
                        If dt.Rows(i)("Side") = "R" Then
                            optShoulder_Score_R.SelectedValue = dt.Rows(i)("Score")
                            optShoulder_Freq_R.SelectedValue = dt.Rows(i)("Freq")
                        End If

                    Case BODYPART_UPPERBACK
                        If dt.Rows(i)("Side") = "L" Then
                            optUpperBack_Score_L.SelectedValue = dt.Rows(i)("Score")
                            optUpperBack_Freq_L.SelectedValue = dt.Rows(i)("Freq")
                        End If
                        If dt.Rows(i)("Side") = "R" Then
                            optUpperBack_Score_R.SelectedValue = dt.Rows(i)("Score")
                            optUpperBack_Freq_R.SelectedValue = dt.Rows(i)("Freq")
                        End If

                    Case BODYPART_LOWERBACK
                        If dt.Rows(i)("Side") = "L" Then
                            optLowerBack_Score_L.SelectedValue = dt.Rows(i)("Score")
                            optLowerBack_Freq_L.SelectedValue = dt.Rows(i)("Freq")
                        End If
                        If dt.Rows(i)("Side") = "R" Then
                            optLowerBack_Score_R.SelectedValue = dt.Rows(i)("Score")
                            optLowerBack_Freq_R.SelectedValue = dt.Rows(i)("Freq")
                        End If

                    Case BODYPART_FOREARM
                        If dt.Rows(i)("Side") = "L" Then
                            optForearm_Score_L.SelectedValue = dt.Rows(i)("Score")
                            optForearm_Freq_L.SelectedValue = dt.Rows(i)("Freq")
                        End If
                        If dt.Rows(i)("Side") = "R" Then
                            optForearm_Score_R.SelectedValue = dt.Rows(i)("Score")
                            optForearm_Freq_R.SelectedValue = dt.Rows(i)("Freq")
                        End If

                    Case BODYPART_HAND
                        If dt.Rows(i)("Side") = "L" Then
                            optHand_Score_L.SelectedValue = dt.Rows(i)("Score")
                            optHand_Freq_L.SelectedValue = dt.Rows(i)("Freq")
                        End If
                        If dt.Rows(i)("Side") = "R" Then
                            optHand_Score_R.SelectedValue = dt.Rows(i)("Score")
                            optHand_Freq_R.SelectedValue = dt.Rows(i)("Freq")
                        End If

                    Case BODYPART_HIP
                        If dt.Rows(i)("Side") = "L" Then
                            optHip_Score_L.SelectedValue = dt.Rows(i)("Score")
                            optHip_Freq_L.SelectedValue = dt.Rows(i)("Freq")
                        End If
                        If dt.Rows(i)("Side") = "R" Then
                            optHip_Score_R.SelectedValue = dt.Rows(i)("Score")
                            optHip_Freq_R.SelectedValue = dt.Rows(i)("Freq")
                        End If

                    Case BODYPART_KNEE
                        If dt.Rows(i)("Side") = "L" Then
                            optKnee_Score_L.SelectedValue = dt.Rows(i)("Score")
                            optKnee_Freq_L.SelectedValue = dt.Rows(i)("Freq")
                        End If
                        If dt.Rows(i)("Side") = "R" Then
                            optKnee_Score_R.SelectedValue = dt.Rows(i)("Score")
                            optKnee_Freq_R.SelectedValue = dt.Rows(i)("Freq")
                        End If

                    Case BODYPART_CALF
                        If dt.Rows(i)("Side") = "L" Then
                            optCalf_Score_L.SelectedValue = dt.Rows(i)("Score")
                            optCalf_Freq_L.SelectedValue = dt.Rows(i)("Freq")
                        End If
                        If dt.Rows(i)("Side") = "R" Then
                            optCalf_Score_R.SelectedValue = dt.Rows(i)("Score")
                            optCalf_Freq_R.SelectedValue = dt.Rows(i)("Freq")
                        End If

                    Case BODYPART_FEET
                        If dt.Rows(i)("Side") = "L" Then
                            optFeet_Score_L.SelectedValue = dt.Rows(i)("Score")
                            optFeet_Freq_L.SelectedValue = dt.Rows(i)("Freq")
                        End If
                        If dt.Rows(i)("Side") = "R" Then
                            optFeet_Score_R.SelectedValue = dt.Rows(i)("Score")
                            optFeet_Freq_R.SelectedValue = dt.Rows(i)("Freq")
                        End If
                End Select


            Next
        End If
    End Sub



    Private Sub LoadPersonData(PersonUID As Integer)

        dt = ctlP.Person_GetByPersonID(PersonUID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdPersonUID.Value = String.Concat(.Item("PersonUID"))
                lblCode.Text = String.Concat(.Item("Code"))
                lblName.Text = String.Concat(.Item("PersonName"))
                lblSex.Text = String.Concat(.Item("Sex"))
                lblAge.Text = String.Concat(.Item("Age"))
                txtHight.Text = String.Concat(.Item("Hight"))
                txtWeight.Text = String.Concat(.Item("Weight"))
                lblPosition.Text = String.Concat(.Item("PositionName"))
                lblDivision.Text = String.Concat(.Item("SectionName"))
                lblDepartment.Text = String.Concat(.Item("DepartmentName"))

                'ddlBU.SelectedValue = .Item("BU")
                lblWY.Text = String.Concat(.Item("WorkingAge"))
                'lblWorkDayPerWeek.Text = String.Concat(.Item("WorkDayPerWeek"))
                lblWorkPeriod.Text = String.Concat(.Item("WorkPeriodName"))

                optSmoking.SelectedValue = String.Concat(.Item("IsSmoking"))
                txtSmokeQTY.Text = String.Concat(.Item("SmokeQTY"))

                optBodyHurt.SelectedValue = String.Concat(.Item("IsBodyHurt"))
                txtBodyHurt.Text = String.Concat(.Item("BodyHurt"))

                optLegHurt.SelectedValue = String.Concat(.Item("IsHurtInFactory"))
                txtLegHurt.Text = String.Concat(.Item("HurtInFactory"))

                optIsMedicalHistory.SelectedValue = String.Concat(.Item("isCongenitalDisease"))
                txtMedicalHistory.Text = String.Concat(.Item("DiseaseDetail"))

                optIsErgoMedical.SelectedValue = String.Concat(.Item("IsErgonomicMedical"))
                txtErgoMedical.Text = String.Concat(.Item("ErgonomicMedical"))

                optIsAccident.SelectedValue = String.Concat(.Item("IsErgonomicAccident"))
                txtAccident.Text = String.Concat(.Item("ErgonomicAccident"))

                optJobAccident.SelectedValue = String.Concat(.Item("IsJobAccident"))
                txtJobAccident.Text = String.Concat(.Item("JobAccident"))

                optWorkRepeatedly.SelectedValue = String.Concat(.Item("IsWorkRepeatedly"))
                If String.Concat(.Item("IsWorkRepeatedly")) = "Y" Then
                    If String.Concat(.Item("WorkRepeatedly1")) = "Y" Then
                        chkWorkRepeatedly.Items(0).Selected = True
                    End If
                    If String.Concat(.Item("WorkRepeatedly2")) = "Y" Then
                        chkWorkRepeatedly.Items(1).Selected = True
                    End If
                End If

                optHardLook.SelectedValue = String.Concat(.Item("IsHardLook"))

                optLifting.SelectedValue = String.Concat(.Item("IsLifting"))
                txtLiftTime.Text = String.Concat(.Item("LiftTime"))
                txtLiftLoad.Text = String.Concat(.Item("LiftLoad"))

                optBodyPosition.SelectedValue = String.Concat(.Item("BodyPosition"))


                LoadPersonalActivityData(lblCode.Text)
                LoadPersonalSafetyTools(hdPersonUID.Value)
                LoadROWL()
                LoadPersonErgonomicRisk()

            End With
        End If

    End Sub

    Private Sub LoadPersonalActivityData(pid As String)

        dt = ctlP.Person_GetActivity(pid)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                optHobby.SelectedValue = String.Concat(.Item("isHobby"))
                txtHobby.Text = String.Concat(.Item("HobbyRemark"))
                optActivity.SelectedValue = String.Concat(.Item("isActivity"))
                txtActivityTime.Text = String.Concat(.Item("ActivityTime"))
                txtActivityFeq.Text = String.Concat(.Item("ActivityFeq"))
            End With
        End If

    End Sub

    Private Sub LoadSafetyToolsToCheckList()
        dt = ctlM.ReferenceValue_GetByDomainCode(REF_PPE)
        With chkPPE
            .Visible = True
            .DataSource = dt
            .DataTextField = "DisplayName"
            .DataValueField = "UID"
            .DataBind()
        End With
    End Sub

    Private Sub LoadPersonalSafetyTools(pid As String)
        dt = ctlP.Person_GetPersonalSafety(pid)
        chkPPE.ClearSelection()
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                For n = 0 To chkPPE.Items.Count - 1
                    If chkPPE.Items(n).Value = String.Concat(dt.Rows(i)("PPEUID")) Then
                        chkPPE.Items(n).Selected = True
                    End If
                Next
            Next
        End If

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        Dim WorkRepeatedly1, WorkRepeatedly2 As String
        WorkRepeatedly1 = ""
        WorkRepeatedly2 = ""
        If chkWorkRepeatedly.Items(0).Selected = True Then
            WorkRepeatedly1 = "Y"
        End If
        If chkWorkRepeatedly.Items(1).Selected = True Then
            WorkRepeatedly2 = "Y"
        End If

        ctlP.Person_UpdateGeneralData(hdPersonUID.Value, optHobby.SelectedValue, txtHobby.Text, optActivity.SelectedValue, StrNull2Zero(txtActivityTime.Text), StrNull2Zero(txtActivityFeq.Text), optSmoking.SelectedValue, StrNull2Zero(txtSmokeQTY.Text), optBodyHurt.SelectedValue, txtBodyHurt.Text, optLegHurt.SelectedValue, txtLegHurt.Text, optIsMedicalHistory.SelectedValue, txtMedicalHistory.Text, optIsErgoMedical.SelectedValue, txtErgoMedical.Text, optIsAccident.SelectedValue, txtAccident.Text, optJobAccident.SelectedValue, txtJobAccident.Text, StrNull2Zero(Request.Cookies("Ergo")("userid")), StrNull2Double(txtHight.Text), StrNull2Double(txtWeight.Text), optWorkRepeatedly.SelectedValue, WorkRepeatedly1, WorkRepeatedly2, optHardLook.SelectedValue, optLifting.SelectedValue, txtLiftTime.Text, txtLiftLoad.Text, optBodyPosition.SelectedValue)

        For i = 0 To chkPPE.Items.Count - 1
            ctlP.PersonPPE_Save(StrNull2Zero(hdPersonUID.Value), chkPPE.Items(i).Value, ConvertBoolean2YN(chkPPE.Items(i).Selected), Request.Cookies("Ergo")("userid"))
        Next

        ctlP.PersonROWL_Save(StrNull2Zero(hdPersonUID.Value), ROWL1, StrNull2Zero(optROWL1.SelectedValue))
        ctlP.PersonROWL_Save(StrNull2Zero(hdPersonUID.Value), ROWL2, StrNull2Zero(optROWL2.SelectedValue))
        ctlP.PersonROWL_Save(StrNull2Zero(hdPersonUID.Value), ROWL3, StrNull2Zero(optROWL3.SelectedValue))
        ctlP.PersonROWL_Save(StrNull2Zero(hdPersonUID.Value), ROWL4, StrNull2Zero(optROWL4.SelectedValue))
        ctlP.PersonROWL_Save(StrNull2Zero(hdPersonUID.Value), ROWL5, StrNull2Zero(optROWL5.SelectedValue))
        ctlP.PersonROWL_Save(StrNull2Zero(hdPersonUID.Value), ROWL6, StrNull2Zero(optROWL6.SelectedValue))
        ctlP.PersonROWL_Save(StrNull2Zero(hdPersonUID.Value), ROWL7, StrNull2Zero(optROWL7.SelectedValue))
        ctlP.PersonROWL_Save(StrNull2Zero(hdPersonUID.Value), ROWL8, StrNull2Zero(optROWL8.SelectedValue))


        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_NECK, "L", optNeck_Score_L.SelectedValue, optNeck_Freq_L.SelectedValue)
        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_NECK, "R", optNeck_Score_R.SelectedValue, optNeck_Freq_R.SelectedValue)

        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_SHOULDER, "L", optShoulder_Score_L.SelectedValue, optShoulder_Freq_L.SelectedValue)
        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_SHOULDER, "R", optShoulder_Score_R.SelectedValue, optShoulder_Freq_R.SelectedValue)

        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_UPPERBACK, "L", optUpperBack_Score_L.SelectedValue, optUpperBack_Freq_L.SelectedValue)
        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_UPPERBACK, "R", optUpperBack_Score_R.SelectedValue, optUpperBack_Freq_R.SelectedValue)

        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_LOWERBACK, "L", optLowerBack_Score_L.SelectedValue, optLowerBack_Freq_L.SelectedValue)
        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_LOWERBACK, "R", optLowerBack_Score_R.SelectedValue, optLowerBack_Freq_R.SelectedValue)

        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_FOREARM, "L", optForearm_Score_L.SelectedValue, optForearm_Freq_L.SelectedValue)
        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_FOREARM, "R", optForearm_Score_R.SelectedValue, optForearm_Freq_R.SelectedValue)

        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_HAND, "L", optHand_Score_L.SelectedValue, optHand_Freq_L.SelectedValue)
        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_HAND, "R", optHand_Score_R.SelectedValue, optHand_Freq_R.SelectedValue)

        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_HIP, "L", optHip_Score_L.SelectedValue, optHip_Freq_L.SelectedValue)
        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_HIP, "R", optHip_Score_R.SelectedValue, optHip_Freq_R.SelectedValue)

        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_KNEE, "L", optKnee_Score_L.SelectedValue, optKnee_Freq_L.SelectedValue)
        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_KNEE, "R", optKnee_Score_R.SelectedValue, optKnee_Freq_R.SelectedValue)

        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_CALF, "L", optCalf_Score_L.SelectedValue, optCalf_Freq_L.SelectedValue)
        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_CALF, "R", optCalf_Score_R.SelectedValue, optCalf_Freq_R.SelectedValue)

        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_FEET, "L", optFeet_Score_L.SelectedValue, optFeet_Freq_L.SelectedValue)
        ctlP.PersonErgonomicRisk_Save(StrNull2Zero(hdPersonUID.Value), BODYPART_FEET, "R", optFeet_Score_R.SelectedValue, optFeet_Freq_R.SelectedValue)


        Dim ctlU As New UserController
        ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_UPD, "Health data", "บันทึก/แก้ไข Health data :{uid=" & hdPersonUID.Value & "}{code=" & lblCode.Text & "}", "")

        'If Request("pid") Is Nothing Then
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        'Else
        '    Response.Redirect("Person.aspx")
        'End If

    End Sub

End Class