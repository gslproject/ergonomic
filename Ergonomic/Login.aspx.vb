﻿Imports System.Web

Public Class Login
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim acc As New UserController
    Dim enc As New CryptographyEngine
    Dim ctlM As New MasterController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If ctlM.SystemOnline_GetStatus() = False Then
            Response.Redirect("default.aspx")
        End If

        If Request("logout") = "y" Then
            Session.Contents.RemoveAll()
            Session.Abandon()
            Session.RemoveAll()
            Response.Cookies.Clear()

            Dim delCookie1 As HttpCookie
            delCookie1 = New HttpCookie("Ergo")
            delCookie1.Expires = DateTime.Now.AddDays(-1D)
            Response.Cookies.Add(delCookie1)
        End If

        Session.Timeout = 60

        If Not IsPostBack Then
            If Request("logout") = "y" Then
                Session.Abandon()
                Request.Cookies("Ergo")("userid") = Nothing
            End If
            txtUsername.Focus()
        End If
    End Sub

    Private Sub CheckUser()
        dt = acc.User_CheckLogin(txtUsername.Text, enc.EncryptString(txtPassword.Text, True))

        If dt.Rows.Count > 0 Then

            If dt.Rows(0).Item("StatusFlag") <> "A" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ID ของท่านไม่สามารถใช้งานได้ชั่วคราว');", True)
                Exit Sub
            End If

            '--------------------------------------------------------------------------------------------

            Dim ErgoCookie As HttpCookie = New HttpCookie("Ergo")
            ErgoCookie("userid") = dt.Rows(0).Item("UserID")
            ErgoCookie("username") = String.Concat(dt.Rows(0).Item("USERNAME")).ToString()
            ErgoCookie("Password") = enc.DecryptString(dt.Rows(0).Item("PASSWORDs"), True)
            ErgoCookie("LastLogin") = String.Concat(dt.Rows(0).Item("LastLogin"))
            ErgoCookie("NameOfUser") = String.Concat(dt.Rows(0).Item("Name"))
            ErgoCookie("LoginPersonUID") = DBNull2Zero(dt.Rows(0).Item("PersonUID"))
            ErgoCookie("LoginCompanyUID") = DBNull2Zero(dt.Rows(0).Item("CompanyUID"))
            ErgoCookie("ROLE_CUS") = False
            ErgoCookie("ROLE_CAD") = False
            ErgoCookie("ROLE_ADM") = False
            ErgoCookie("ROLE_SPA") = False

            ErgoCookie.Expires = acc.GET_DATE_SERVER().AddMinutes(60)
            Response.Cookies.Add(ErgoCookie)


            'If Request.Cookies("Ergo")("UserProfileID") <> 1 Then
            '    Dim ctlC As New CourseController
            '    Request.Cookies("Ergo")("ROLE_ADV") = ctlC.CoursesCoordinator_CheckStatus(Request.Cookies("Ergo")("ProfileID"))
            'End If

            'Dim ctlCfg As New SystemConfigController()
            'Request.Cookies("Ergo")("EDUYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_EDUYEAR)
            'Request.Cookies("Ergo")("ASSMYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_ASSMYEAR)

            'genLastLog()

            'acc.User_GenLogfile(txtUsername.Value, ACTTYPE_LOG, "Users", "", "")

            Dim rd As New UserRoleController

            dt = rd.UserAccessGroup_GetByUID(Request.Cookies("Ergo")("userid"))
            If dt.Rows.Count > 0 Then

                For i = 0 To dt.Rows.Count - 1
                    Select Case dt.Rows(i)("UserGroupUID")
                        Case 1 'Customer User
                            ErgoCookie("ROLE_CUS") = True
                            Request.Cookies("Ergo")("ROLE_CUS") = True
                        Case 2  'Customer Admin
                            ErgoCookie("ROLE_CAD") = True
                            Request.Cookies("Ergo")("ROLE_CAD") = True
                        Case 3 'Administrator
                            ErgoCookie("ROLE_ADM") = True
                            Request.Cookies("Ergo")("ROLE_ADM") = True
                        Case 9
                            ErgoCookie("ROLE_SPA") = True
                            Request.Cookies("Ergo")("ROLE_SPA") = True
                    End Select
                Next

            End If
            genLastLog()

            'Select Case Request.Cookies("Ergo")("UserProfileID")
            '    Case 1 'Customer
            '        Response.Redirect("Default.aspx")
            '    Case 2, 3 'Teacher
            '        Response.Redirect("Default.aspx")
            '    Case 4 'Admin
            'Response.Redirect("Home")
            Response.Redirect("LoginPolicy")
            'End Select

        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username  หรือ Password ไม่ถูกต้อง');", True)
            Exit Sub
        End If
    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        'txtUsername.Value = "admin"
        'txtPassword.Value = "112233"
        'Request.Cookies("Ergo")("userid") = 1
        'Request.Cookies("Ergo")("ROLE_ADM") = True
        'Response.Redirect("Home")
        If txtUsername.Text = "" Or txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!','กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน');", True)
            txtUsername.Focus()
            Exit Sub
        End If

        Session.Contents.RemoveAll()
        CheckUser()
    End Sub

    Private Sub genLastLog() ' เก็บวันเวลาที่ User เข้าใช้ระบบครั้งล่าสุด
        acc.User_LastLog_Update(txtUsername.Text)
    End Sub
End Class