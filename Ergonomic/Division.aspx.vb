﻿
Public Class Division
    Inherits System.Web.UI.Page

    Dim ctlLG As New MasterController
    Dim dt As New DataTable
    Dim acc As New UserController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            ClearData()
            txtUID.Text = ""
            LoadDivisionToGrid()
        End If

    End Sub
    Private Sub LoadDivisionToGrid()

        If Trim(txtSearch.Text) <> "" Then
            dt = ctlLG.Division_GetBySearch(Request.Cookies("Ergo")("LoginCompanyUID"), txtSearch.Text)
        Else
            dt = ctlLG.Division_GetAll(Request.Cookies("Ergo")("LoginCompanyUID"))
        End If
        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()
        End With

    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlLG.Division_Delete(Request.Cookies("Ergo")("LoginCompanyUID"), e.CommandArgument) Then

                        acc.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_DEL, "Division", "ลบชื่อฝ่าย :" & txtName.Text, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                        LoadDivisionToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)

                    End If


            End Select


        End If
    End Sub
    Private Sub EditData(ByVal pID As String)
        dt = ctlLG.Division_GetByUID(pID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                Me.txtUID.Text = DBNull2Str(dt.Rows(0)("DivisionUID"))
                txtCode.Text = String.Concat(dt.Rows(0)("DivisionCode"))
                txtName.Text = DBNull2Str(dt.Rows(0)("DivisionName"))
                txtSort.Text = DBNull2Str(dt.Rows(0)("SortOrder"))
                chkStatus.Checked = ConvertStatusFlag2CHK(dt.Rows(0)("StatusFlag"))
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub ClearData()
        Me.txtUID.Text = ""
        txtUID.Text = ""
        txtCode.Text = ""
        txtName.Text = ""
        txtSort.Text = "0"
        chkStatus.Checked = True

    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(4).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d9edf7';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกข้อมูลให้ครบถ้วน');", True)
            Exit Sub
        End If

        Dim item As Integer

        If txtUID.Text = "" Then
            If ctlLG.Division_CheckDuplicate(Request.Cookies("Ergo")("LoginCompanyUID"), txtName.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ชื่อฝ่ายนี้มีอยู่่ในระบบแล้ว');", True)
                Exit Sub
            End If
            item = ctlLG.Division_Add(Request.Cookies("Ergo")("LoginCompanyUID"), txtCode.Text, txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
        Else
            item = ctlLG.Division_Update(StrNull2Zero(txtUID.Text), Request.Cookies("Ergo")("LoginCompanyUID"), txtCode.Text, txtName.Text, StrNull2Zero(txtSort.Text), ConvertBoolean2StatusFlag(chkStatus.Checked))
        End If


        LoadDivisionToGrid()
        ClearData()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','บันทึกข้อมูลเรียบร้อย');", True)


    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        LoadDivisionToGrid()
    End Sub

    Protected Sub grdData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadDivisionToGrid()
    End Sub
End Class

