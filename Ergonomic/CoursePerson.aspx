﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CoursePerson.aspx.vb" Inherits="Ergonomic.CoursePerson" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>My E-Learning Course
        <small>หลักสูตรของคุณ</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">My Course</li>
      </ol>
    </section>
     
      <section class="content">  
<!--
<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Personal Information<asp:HiddenField ID="hdPersonUID" runat="server" />
        </h3>
    </div> 
    <div class="box-body">
      <div class="row">
        <div class="col-md-1">
          <div class="form-group">
            <label>Code</label>
            <asp:Label ID="lblCode" runat="server" cssclass="form-control text-center" ></asp:Label>
          </div>

        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Name</label>
               <asp:Label ID="lblPersonName" runat="server" cssclass="form-control text-center"  ></asp:Label> 
          </div>
        </div>                

        <div class="col-md-3">
          <div class="form-group">
            <label>Position</label>

              <asp:Label ID="lblPosition" runat="server" cssclass="form-control text-center"></asp:Label>
          </div>

        </div>
    

        <div class="col-md-2">
          <div class="form-group">
            <label>Department</label>
            <asp:Label ID="lblDepartment" runat="server" cssclass="form-control text-center"  ></asp:Label>
          </div>
        </div>   
        <div class="col-md-4">
          <div class="form-group">
            <label>Section</label>
            <asp:Label ID="lblSection" runat="server" cssclass="form-control text-center"></asp:Label>      
          </div>

        </div>      
      </div>    
    </div> 
  </div>
  </section>
</div>
   <!--
            <div class="row">
    <section class="col-lg-12 connectedSortable"> 

          <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Course</h3>
    </div>
  
    <div class="box-body">      

  <div class="row">
 
                <asp:GridView ID="grdCompany" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-striped" 
                             Font-Bold="False" PageSize="15">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:BoundField HeaderText="No." DataField="nRow">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40px" />                            </asp:BoundField>
                        <asp:BoundField DataField="CourseName" HeaderText="Course/หลักสูตร" />
                            <asp:BoundField DataField="Duedate" HeaderText="Duedate" />
                            <asp:BoundField HeaderText="Re-Occurrence" DataField="ReStatus" />
                            <asp:BoundField HeaderText="Status" DataField="LearnStatusName" />
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
     </div>        
         
          </div>
    </div>
        </section>



                </div>
    -- >

   <h4 class="text-blue text-bold">หลักสูตรของคุณ</h4> 
<br />
 -->   

<div class="row"> 

<% If dtCourse.Rows.Count = 0 Then %>
     <div class="text-blue text-center">
         <h3>ท่านยังไม่ได้รับการจัดหลักสูตรให้</h3>
     </div>
<% End If %>
<% For i = 0 To dtCourse.Rows.Count - 1 %>
     
     <div class="col-lg-4 col-xs-6">         

          <div class="small-box bg-gray-light">
            <div class="inner">

                 <a href='<% Response.Write("Classroom.aspx?CID=" & dtCourse.Rows(i)("CourseUID")) %>' title='<% Response.Write(dtCourse.Rows(i)("CourseName")) %>' rel="bookmark" target="_blank">
                  <img width="100%"src="imgCourse/<% Response.Write(dtCourse.Rows(i)("PicturePath")) %>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="">
              <h5><% Response.Write(dtCourse.Rows(i)("CourseName")) %></h5>
              <small>Duedate : <% Response.Write(dtCourse.Rows(i)("Duedate")) %></small>
            </a>
            </div>
            <div class="icon">
             <% Response.Write(dtCourse.Rows(i)("LearnStatusName")) %>
            </div>
              <div class="small-box-footer text-blue">
            <a href='<% Response.Write("Classroom.aspx?CID=" & dtCourse.Rows(i)("CourseUID")) %>' title='<% Response.Write(dtCourse.Rows(i)("CourseName")) %>' rel="bookmark" target="_blank" >Go to classroom <i class="fa fa-arrow-circle-right"></i></a>
        
        <% If String.Concat(dtCourse.Rows(i)("LStatus")) = "C"  %>
               <a href='<% Response.Write("Report/ReportViewer?R=cert&PID=" & Request.Cookies("Ergo")("LoginPersonUID") & "&CID=" & dtCourse.Rows(i)("CourseUID") & "&RPTTYPE=PDF") %>'  rel="bookmark" target="_blank">Print Certificate <i class="fa fa-print"></i></a>
        <% End If %> 
</div>
          </div>
     
         
     
     </div>      
         
<% Next %>

</div><!--row--> 
                   
          </section>
</asp:Content>