﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CompanyModify
    
    '''<summary>
    '''hdCompUID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdCompUID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''txtCompanyID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCompanyID As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtNameTH control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNameTH As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtNameEN control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNameEN As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtBranch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBranch As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtTaxID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTaxID As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtAddressNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAddressNo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtLane control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLane As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtRoad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRoad As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtSubDistrict control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSubDistrict As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtDistrict control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDistrict As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddlProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProvince As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtZipcode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtZipcode As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddlCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCountry As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtTel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTel As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtFax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFax As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmail As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtWebsite control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWebsite As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''chkStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkStatus As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''HiddenField1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HiddenField1 As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''txtMaxPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMaxPerson As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cmdSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''cmdDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdDelete As Global.System.Web.UI.WebControls.Button
End Class
