﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data
'Imports Subgurim.Controles
Public Class UserImport
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlP As New PersonController
    Dim ctlM As New MasterController
    Dim ctlC As New CompanyController
    Dim ctlU As New UserController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadCompany()
            CheckCompanyQuota(ddlCompany.SelectedValue)
            lblResult.Visible = False
            If Request.Cookies("Ergo")("ROLE_ADM") = True Or Request.Cookies("Ergo")("ROLE_SPA") = True Then
                ddlCompany.Enabled = True
            Else
                ddlCompany.Enabled = False
            End If
        End If
    End Sub
    Private Sub LoadCompany()
        ddlCompany.DataSource = ctlC.Company_GetActive()
        ddlCompany.DataTextField = "CompanyName"
        ddlCompany.DataValueField = "UID"
        ddlCompany.DataBind()
        ddlCompany.SelectedValue = Request.Cookies("Ergo")("LoginCompanyUID")
    End Sub
    Private Sub cmdImport_Click(sender As Object, e As EventArgs) Handles cmdImport.Click
        If (StrNull2Zero(lblRemain.Text) <= 0) Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บริษัทท่านได้เพิ่ม User ครบตามแพคเก็จแล้ว ไม่สามารถเพิ่มได้อีก');", True)
            If (Not FileUploadFile.HasFile) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกไฟล์ Excel ก่อน');", True)
                Exit Sub
            End If
        End If


        System.Threading.Thread.Sleep(3000)
        UpdateProgress1.Visible = True

        Dim connectionString As String = ""
        Try

            lblResult.Visible = False

            Dim fileName As String = Request.Cookies("Ergo")("LoginCompanyUID") + FileUploadFile.FileName
            Dim fileExtension As String = Path.GetExtension(FileUploadFile.PostedFile.FileName)
            Dim fileLocation As String = Server.MapPath("~/" + tmpUpload + "/" + fileName)
            FileUploadFile.SaveAs(Server.MapPath("~/" + tmpUpload + "/" + fileName))


            'Check whether file extension is xls or xslx

            If fileExtension = ".xls" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            ElseIf fileExtension = ".xlsx" Then
                If (Not FileUploadFile.HasFile) Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกไฟล์ Excel เวอร์ชั่น .xls เท่านั้น');", True)
                    Exit Sub
                End If
                Exit Sub
                'connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            End If

            'Create OleDB Connection and OleDb Command

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = dtExcelSheetName.Rows(0)("Table_Name").ToString()
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            Dim strCode As String = ""
            Dim k As Integer = 0
            For i = 0 To dtExcelRecords.Rows.Count - 1
                With dtExcelRecords.Rows(i)
                    If .Item(0).ToString <> "" Then
                        If ctlP.Person_CheckDuplicateByCode(ddlCompany.SelectedValue, String.Concat(.Item(0))) Then
                            strCode = strCode & String.Concat(.Item(0)) & ","
                        Else
                            ctlP.Person_AddByImport(String.Concat(.Item(0)), String.Concat(.Item(1)), String.Concat(.Item(2)), String.Concat(.Item(3)), String.Concat(.Item(4)), String.Concat(.Item(5)), String.Concat(.Item(6)), String.Concat(.Item(7)), ddlCompany.SelectedValue, String.Concat(.Item(8)), String.Concat(.Item(9)), String.Concat(.Item(10)), String.Concat(.Item(11)), String.Concat(.Item(12)), String.Concat(.Item(13)), Request.Cookies("Ergo")("userid"))
                            k = k + 1

                            If k = StrNull2Zero(lblRemain.Text) Then
                                Exit For
                            End If
                        End If
                    End If
                End With
            Next
            'grdData.DataSource = dtExcelRecords
            'grdData.DataBind()


            ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_ADD, "Person & User", "import personal and user : " & k & " คน", "จากไฟล์ excel")

            lblResult.Text = "ข้อมูลทั้งหมด " & k & " รายการ ถูก import เรียบร้อย" & IIf(strCode <> "", vbCrLf & " รหัส " & strCode & " ไม่สามารถเพิ่มได้เนื่องจากมีในฐานข้อมูลแล้ว ", "")
            lblResult.Visible = True
            lblAlert.Visible = False
            dtExcelRecords = Nothing

            UpdateProgress1.Visible = False
        Catch ex As Exception
            lblAlert.Text = "Error : " & ex.Message
            lblAlert.Visible = True
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        CheckCompanyQuota(ddlCompany.SelectedValue)
    End Sub

    Private Sub CheckCompanyQuota(CompanyUID As Integer)

        lblQuota.Text = ctlC.Company_GetMaxLimit(CompanyUID)
        lblRemain.Text = StrNull2Zero(lblQuota.Text) - ctlC.Company_GetPersonCount(CompanyUID)

        If ctlC.Company_CheckOverMaxLimit(CompanyUID) = True Then
            lblAlert.Visible = True
            lblAlert.Text = "บริษัทนี้มีจำนวน User ครบตามกำหนดแล้ว ไม่สามารถเพิ่มได้อีก"
            cmdImport.Enabled = False
        Else
            lblAlert.Visible = False
            cmdImport.Enabled = True
        End If

    End Sub
End Class