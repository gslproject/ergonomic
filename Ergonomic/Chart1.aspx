﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Chart1.aspx.vb" Inherits="Ergonomic.Chart1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
       <link href="assets/styles.css" rel="stylesheet" />
   

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>

    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
         <script>
    var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ]
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
   <section class="content-header">
    <h1>
      สรุปผลจากการตอบแบบประเมินความเสี่ยงอาการผิดปกติของระบบโครงร่างกระดูกและกล้ามเนื้อ
      <small></small>
    </h1>
  
  </section>

  <!-- Main content -->
  <section class="content">   
    <div class="row"> 
      <section class="col-lg-6 connectedSortable">

          <div class="box box-solid">
    <div class="box-header">
 <h5 class="card-title">สรุปภาพรวมทั้งบริษัท</h5>
    </div> 
    <div class="box-body">             
                  <div id="pie1"></div>                 
              </div>
          </div>
</section>   
        <section class="col-lg-6 connectedSortable">
             <div class="box box-solid">
    <div class="box-header">
 <h5 class="card-title">แยกตามฝ่าย</h5>
    </div> 
    <div class="box-body">     

  <div id="chart"></div>
              </div>
          </div>

            </section>
        </div>  
    <div class="row">
      <section class="col-lg-12 connectedSortable">
             <div class="box box-solid">
    <div class="box-header">
 <h5 class="card-title">แยกตามแผนก</h5>
    </div> 
    <div class="box-body">     

  <div id="chart2"></div>
              </div>
          </div>
 
      </section> 
    </div>    
</section>

 
    <script>
      
        var options = {
            series: [<%=datapie %>],
            labels: ['จำนวนพนักงานทั้งหมด', 'จำนวนพนักงานที่ตอบแบบสอบถาม'],
			legend: {position: 'bottom'},
          chart: {
          width: 480,
          type: 'pie',
            },         
        responsive: [{
          breakpoint: 380,
          options: {
            chart: {
              width: 380
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var pie1 = new ApexCharts(document.querySelector("#pie1"), options);
        pie1.render();
      
      
    </script>
        
     <script>
      
        var options = {
            series: [{
              name: 'จำนวนพนักงานทั้งหมด',
          data: [<%=databar1All %>]
            }, {
                    name: 'จำนวนพนักงานที่ตอบแบบสอบถาม',
          data: [<%=databar1Asm %>]
                }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            horizontal: false,
            dataLabels: {
              position: 'top',
            },
          }
        },
        dataLabels: {
          enabled: true,
          offsetX: 0,
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        tooltip: {
          shared: true,
          intersect: false
        },
        xaxis: {
          categories: [<%=catebar1 %>],
        },
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
      
      
    </script>
        <script>
      
        var options = {
            series: [{
              name: 'จำนวนพนักงานทั้งหมด',
          data: [<%=databar2All %>]
            }, {
                    name: 'จำนวนพนักงานที่ตอบแบบสอบถาม',
          data: [<%=databar2Asm %>]
                }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            horizontal: false,
            dataLabels: {
              position: 'top',
            },
          }
        },
        dataLabels: {
          enabled: true,
          offsetX: 0,
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        tooltip: {
          shared: true,
          intersect: false
        },
        xaxis: {
          categories: [<%=catebar2 %>],
        },
        };

        var chart2 = new ApexCharts(document.querySelector("#chart2"), options);
        chart2.render();
      
      
    </script>
        
</asp:Content>