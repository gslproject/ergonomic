﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Classroom2.aspx.vb" Inherits="Ergonomic.Classroom2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>Classroom
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Person</li>
      </ol>
    </section>   
      <section class="content">  
            
<div class="row">
   <section class="col-lg-9 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">
          <asp:Label ID="lblCourseName" runat="server"></asp:Label>          
          <asp:HiddenField ID="hdUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">    
    <%   If dtCourse2.Rows.Count > 0 Then %>

<div class="set--detail">
    <div class="container">  
          <div class="row">  

    <div class="col-md-12">
      
        <video id="studyVid"  onended="vidend()" onclick="playPause()" width="78%" >
            <source src="elearning-resource/<% =String.Concat(dtCourse2.Rows(0)("MediaLink")) %>" type="video/mp4">
        </video>
     

<div class="row margin--top30">
<div class="col-md-12">
    <div class="input-group mb-6">
       <table>  
        <tr>
             <td width="120">
            <button class="btn btn-sm" id="vidbutton" type="button" style="background-color:#fff;padding:0 0 0 0;"><span><img src="images/play-button.png" style="width:20px;"></span></button>&nbsp;&nbsp;
            <span class="text-lowercase" id="curtimetext">00:00</span>&nbsp;/&nbsp;<span class=" text-lowercase" id="durtimetext"><% =String.Concat(dtCourse2.Rows(0)("MediaCount")) %></span>

                   </td>
    
                <td> <button class="btn btn-sm" id="mutebtn" type="button" style="background-color:#fff;padding:0 0 0 0;"><span><img src="images/speaker.png" style="width:20px;"></span></button></td>
                 <td><span><input style="width: 80px;text-align: center;" id="volumeslider" type="range" min="0" max="100" value="100" step="1"></span>   </td>
            </tr>

        </table>

                     
    </div>

    </div>

</div>
 
 
<div class="row ">
    <div class="col-md-5 text-center" style="margin:0 auto">
      <a href="/PostTest?CID=<% =String.Concat(Request("CID")) %>" id="btn1" class="btn btn-lg btn-primary btn-block" style="display: none;
        top: 300px;
        left: 25%;
        right: 25%;
        position: fixed;
        width: 80%;
        max-width: 50%;
        height: 50px;" >ไปทำแบบทดสอบหลังบทเรียน</a>  
    </div>
</div>
        



          </div>
    </div>


            <div class="row">
  <div class="col-lg-12 text-center">
        <br/>
      
          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="ทดสอบหลังบทเรียน" Width="150px" />
            <asp:Button ID="cmdReload" CssClass="btn btn-primary" runat="server" Text="เริ่มต้นใหม่" Width="150px" />
 
        </div>

        <div class="col-lg-12">
          <div class="form-group">
            <label></label>
            <asp:Label ID="lblAlert" runat="server" cssclass="form-control text-center" ></asp:Label>
          </div>

        </div>

      </div>    

        
    </div>
 </div>
    
        <% End If %>  

  </div>
 
      </div>
      
      </section>
    
   <section class="col-lg-3 connectedSortable">
         <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">
       Chapter         
          <asp:HiddenField ID="HiddenField1" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">  

        </div>
             </div>
       </section>
</div>  
          

<script>
    window.addEventListener('focus', function () {

    });

    window.addEventListener('blur', function () {
        studyVid.pause();
        //ppbutton.innerHTML = "<i class='fas fa-play'></i>"
    });
    //window.addEventListener('contextmenu', function (e) {
    //    e.preventDefault();
    //}, false);
</script>

<script>
    studyVid = document.getElementById("studyVid");
    function vidend() {
        try {
            completed();
            document.getElementById('btn1').style.display = 'inline';
            
        } catch { }
        setTimeout(function () {

            alert('ไม่มีการดำเนินการใน 5 นาที');

            //deleteEn();
            console.log('not en');
            location.href = '/CoursePerson';
        }, 300000);
    }

    function goposttest(i) {          
          location.href = '/PostTest?CID=' + i;         
    }

    var ppbutton = document.getElementById("vidbutton");
    ppbutton.addEventListener("click", playPause);


    function playPause() {
        if (studyVid.paused) {
            studyVid.play();
            ppbutton.innerHTML = "<span><img src='images/stop.png' style='width:20px;' /></span>";
        } else {
            studyVid.pause();
            ppbutton.innerHTML = "<span><img src='images/play-button.png' style='width:20px;' /></span>"
        }
    }
    var mutebtn = document.getElementById("mutebtn");
    var volumeslider = document.getElementById("volumeslider");
    mutebtn.addEventListener("click", vidmute);
    volumeslider.addEventListener("change", setvolume);

    function vidmute() {
        if (studyVid.muted) {
            studyVid.muted = false;
            studyVid.volume = 1;
            volumeslider.value = 100
            mutebtn.innerHTML =  "<span><img src='images/speaker.png' style='width:20px;' /></span>";
        } else {
            studyVid.muted = true;
            volumeslider.value = 0;
            mutebtn.innerHTML = "<span><img src='images/mute.png' style='width:20px;' /></span>";
        }
    }

    function setvolume() {
        studyVid.volume = volumeslider.value / 100;
        if (studyVid.volume == 0) {
            vidmute();
        } else {
            mutebtn.innerHTML =  "<span><img src='images/speaker.png' style='width:20px;' /></span>";
        }
    }
    var curtimetext = document.getElementById("curtimetext");
    var durtimetext = document.getElementById("durtimetext");
    studyVid.addEventListener("timeupdate", seektimeupdate, false);

    function seektimeupdate() {
        var nt = studyVid.currentTime * (100 / studyVid.duration);
        var curmins = Math.floor(studyVid.currentTime / 60);
        var cursecs = Math.floor(studyVid.currentTime - curmins * 60);
        var durmins = Math.floor(studyVid.duration / 60);
        var dursecs = Math.floor(studyVid.duration - durmins * 60);


        if (cursecs < 10) {
            cursecs = "0" + cursecs;
        }
        if (dursecs < 10) {
            dursecs = "0" + dursecs;
        }
        if (curmins < 10) {
            curmins = "0" + curmins;
        }
        if (durmins < 10) {
            durmins = "0" + durmins;
        }
        curtimetext.innerHTML = curmins + ":" + cursecs;
        durtimetext.innerHTML = durmins + ":" + dursecs;
    }
    let host = window.location.origin;
    function completed() {
        try {
            var xmlHttp = new XMLHttpRequest();
            let theUrl = `${host}/api/elearning/CompleteStudy`;
            let json = { nat: '111111111' , cid: 1 }
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
                    //callback(xmlHttp.status, xmlHttp.responseText);
                    console.log(xmlHttp.responseText);
                    console.log(typeof(xmlHttp.responseText));
                    //do things
                }
            };
            xmlHttp.onerror = function (ex) {
                console.log('Boom!');
            };
            xmlHttp.onload = function () {

            };
            xmlHttp.open("Post", theUrl, true); // true for asynchronous
            xmlHttp.setRequestHeader("Content-Type", "application/json");
            xmlHttp.send(JSON.stringify(json));
        } catch (ex) {
            console.log('not complete', ex)
        }

    }




</script>

<script>
    var video = document.getElementById('studyVid');
    var supposedCurrentTime = 0;
    video.addEventListener('timeupdate', function () {
        if (!video.seeking) {
            supposedCurrentTime = video.currentTime;
        }
    });
    video.addEventListener('seeking', function () {
        var delta = video.currentTime - supposedCurrentTime;
        if (Math.abs(delta) > 0.01) {
            video.currentTime = supposedCurrentTime;
        }
    });
    video.addEventListener('ended', function () {
        
        supposedCurrentTime = 0;
    });
</script>
               
      
      </section>
</asp:Content>