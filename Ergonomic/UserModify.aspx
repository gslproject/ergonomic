﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="UserModify.aspx.vb" Inherits="Ergonomic.UserModify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

       <section class="content-header">
      <h1>User Account</h1>     
    </section>
    <section class="content">  

<div class="row">    
<section class="col-lg-7 connectedSortable"> 
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">User Account</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

      <div class="row">
            <div class="col-md-2">
          <div class="form-group">
            <label>User ID.</label>
              <asp:Label ID="lblUserID" runat="server" cssclass="form-control"></asp:Label>
          </div>

        </div>

            <div class="col-md-10">
          <div class="form-group">
            <label>Name</label>
              <asp:TextBox ID="txtName" runat="server" cssclass="form-control" placeholder="ชื่อผู้ใช้งาน"></asp:TextBox>
          </div>

        </div>

           
      
      </div>
      <div class="row">
            <div class="col-md-6">
          <div class="form-group">
            <label>Username</label>
              <asp:TextBox ID="txtUsername" runat="server" cssclass="form-control" placeholder="ชื่อ Login" MaxLength="15"></asp:TextBox>
          </div>
        </div>
             <div class="col-md-6">
          <div class="form-group">
            <label>Password</label>
              <asp:TextBox ID="txtPassword" runat="server" cssclass="form-control" placeholder="รหัสผ่าน"></asp:TextBox>
          </div>
        </div>
</div>
        <div class="row">
               <div class="col-md-9">
          <div class="form-group">
            <label>เชื่อมโยงข้อมูลบุคคล</label>
               <asp:DropDownList ID="ddlPerson" runat="server" cssclass="form-control select2"  placeholder="เลือกพนักงาน" Width="100%">
            </asp:DropDownList> 
          </div>
        </div>
     <div class="col-md-3">
          <div class="form-group">
           <label>Status</label> 
              <br />
               <asp:CheckBox ID="chkActive" runat="server"  Text="Active User" Checked="True">               
              </asp:CheckBox> 
          </div>

        </div> 

        </div>
         
    </div>
    </div>
</section>
    <section class="col-lg-5 connectedSortable"> 
         <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">User Group</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <asp:RadioButtonList ID="chkGroup" runat="server">
            <asp:ListItem Value="1">User</asp:ListItem>             
            <asp:ListItem Value="2">Customer Admin</asp:ListItem>
            <asp:ListItem Value="3">NPC-SE Admin</asp:ListItem>
             <asp:ListItem Value="9">Super Administrator</asp:ListItem>
        </asp:RadioButtonList> 
        <br /> <br /> <br /> <br />
          </div>
    </div>
</section>
</div>  

<div class="row text-center">
    <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="Save" Width="100px" /> &nbsp;<asp:Button ID="cmdDelete" runat="server" Text="Delete" CssClass="btn btn-danger" Width="100px" />
    </div>

        </section>
</asp:Content>
