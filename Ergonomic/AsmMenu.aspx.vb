﻿Public Class AsmMenu
    Inherits System.Web.UI.Page
    Dim ctlT As New TaskController
    Dim dt As New DataTable
    Public isROSA, isREBA, isRULA, isNIOSH As Boolean
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        isROSA = False
        isREBA = False
        isRULA = False
        isNIOSH = False

        dt = ctlT.Task_GetByUID(Request("t"))
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)("ROSA") = "Y" Then
                isROSA = True
            End If
            If dt.Rows(0)("REBA") = "Y" Then
                isREBA = True
            End If
            If dt.Rows(0)("RULA") = "Y" Then
                isRULA = True
            End If
            If dt.Rows(0)("NIOSH") = "Y" Then
                isNIOSH = True
            End If
        End If




    End Sub

End Class