﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PostTest.aspx.vb" Inherits="Ergonomic.PostTest" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>แบบทดสอบหลังบทเรียน
        <small>Post test</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Classroom</li>
      </ol>
    </section>

    <!-- Main content -->   
      <section class="content">  
            
<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">
          <asp:Label ID="lblCourseName" runat="server"></asp:Label>  
          <asp:HiddenField ID="hdCourseUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
          
     <div class="col-lg-12">          
          <div class="text-center">             
              <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" DataKeyNames="UID" ShowHeader="False" Width="100%" CssClass="table table-hover" GridLines="None">
                  <Columns>
                      <asp:TemplateField>
                          <ItemTemplate>
                              <table width="100%" class="table table-striped">
                                  <tr>
                                      <td align="left" class="MenuSt">
                                          <asp:Label ID="lblQuestion" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Descriptions") %>' CssClass="text-blue text-bold"></asp:Label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td align="left">
                                          <asp:RadioButtonList ID="optAnswer" runat="server">
                                          </asp:RadioButtonList>
                                      </td>
                                  </tr>
                              </table>
                          </ItemTemplate>
                      </asp:TemplateField>
                  </Columns>
              </asp:GridView>
          </div>
      </div>     
          
</div>
            <div class="row">
  <div class="col-lg-12 text-center">
        <br/>
      
          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="ส่งคำตอบ" Width="150px" />
 
        </div>

        <div class="col-lg-12 text-center">
          <div class="form-group">
            <label></label>
              <h3>
            <asp:Label ID="lblAlert" runat="server" cssclass="text-red" ></asp:Label></h3>
          </div>

        </div>

      </div>    
    </div>
    <!-- /.box-body -->
  </div>
  </section>
</div>                          
      
      </section>
</asp:Content>