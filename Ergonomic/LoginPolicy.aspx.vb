﻿
Public Class LoginPolicy
    Inherits Page
    Dim ctlU As New UserController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            If ctlU.User_CheckPolicyAccept(StrNull2Zero(Request.Cookies("Ergo")("userid"))) = "Y" Then
                Response.Redirect("Home")
            End If
        End If
    End Sub

    Protected Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click

        'If ddlPeriod.SelectedValue = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!','กรุณาเลือก Period ก่อน');", True)
        '    Exit Sub
        'End If
        ctlU.User_AcceptPolicy(StrNull2Zero(Request.Cookies("Ergo")("userid")))
        Response.Redirect("Home")
    End Sub
End Class