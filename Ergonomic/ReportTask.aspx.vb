﻿
Public Class ReportTask
    Inherits System.Web.UI.Page

    Dim ctlM As New MasterController
    Dim dt As New DataTable
    Dim acc As New UserController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadCompany()
            txtStartDate.Text = Today.Date.ToString("dd/MM/yyyy")
            txtEndDate.Text = Today.Date.ToString("dd/MM/yyyy")
        End If

    End Sub

    Private Sub LoadCompany()
        Dim ctlC As New CompanyController

        If Request.Cookies("Ergo")("ROLE_SPA") = True Or Request.Cookies("Ergo")("ROLE_ADM") = True Then
            dt = ctlC.Company_GetActive()
        Else
            dt = ctlC.Company_GetByUID(Request.Cookies("Ergo")("LoginCompanyUID"))
        End If

        With ddlCompany
            .DataSource = dt
            .DataTextField = "CompanyName"
            .DataValueField = "UID"
            .DataBind()
            .SelectedIndex = 0
        End With

    End Sub


    Private Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click
        PrintReport("EXCEL")
    End Sub
    Private Sub PrintReport(Optional ReportType As String = "PDF")
        If txtStartDate.Text = "" Or txtEndDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุช่วงวันที่ก่อน');", True)
            Exit Sub
        End If

        Dim Bdate, Edate, Cond As String
        Bdate = Right(txtStartDate.Text, 4) + Mid(txtStartDate.Text, 4, 2) + Left(txtStartDate.Text, 2)
        Edate = Right(txtEndDate.Text, 4) + Mid(txtEndDate.Text, 4, 2) + Left(txtEndDate.Text, 2)

        If ddlFillter.SelectedValue = "ALL" Then
            Cond = "ALL"
        Else
            Cond = "LAST"
        End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('Report/ReportViewer?R=tasktotal&COM=" & ddlCompany.SelectedValue & "&COND=" & ddlFillter.SelectedValue & "&BDT=" & Bdate & "&EDT=" & Edate & "&RPTTYPE=" & ReportType & "','_blank');", True)
    End Sub
End Class

