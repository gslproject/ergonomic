﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportCompany.aspx.vb" Inherits="Ergonomic.ReportCompany" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>
          <asp:Label ID="lblTitle" runat="server" Text="Total Task Report"></asp:Label><small></small></h1>   
</section>

<section class="content">  

         <div class="box box-solid">
            <div class="box-header">           
            </div>
            <div class="box-body"> 
 <div class="col-lg-2"></div>

<div class="col-lg-8 row" style="background-color:#14539a;color: white">
      <br />   <br />   
                
      <div class="row">
   <div class="col-md-12">
          <div class="form-group">
            <label>Company</label>
              <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="true" cssclass="form-control select2" Width="100%">                   
            </asp:DropDownList>
          </div>

        </div>         
         </div>

 
                <div class="row">      
   <% If Request("r") = "actdt" Then %>
            <div class="col-md-3">
          <div class="form-group">
            <label>Start Date</label>
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </div>

            
          </div>

        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>End Date</label>
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </div>
          </div>

        </div>
   <% End If %>   
   <% If Request("r") = "hracomp" Then %>
 <div class="col-md-12">
          <div class="form-group">
            <label>Division</label>
              <asp:DropDownList ID="ddlDivision" runat="server" AutoPostBack="true" cssclass="form-control select2" Width="100%">                   
            </asp:DropDownList>
          </div>
        </div> 
                     <div class="col-md-12">
          <div class="form-group">
            <label>Department</label>
              <asp:DropDownList ID="ddlDepartment" runat="server" cssclass="form-control select2" Width="100%">                   
            </asp:DropDownList>
          </div>
        </div> 

<% End If %>
          </div>
      
    <br />

  <div class="row">
   <div class="col-md-12 text-center">
       <asp:Button ID="cmdPrint" runat="server" CssClass="btn btn-info" Text="Print Preview" />
     <asp:Button ID="cmdExcel" runat="server" CssClass="btn btn-info" Text="Export to Excel" />
      
       </div>
      </div>

       <br />   <br />  
    </div>
 <div class="col-lg-2"></div>
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

</section>   
</asp:Content>
