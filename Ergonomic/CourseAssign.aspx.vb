﻿Public Class CourseAssign
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlP As New PersonController
    'Dim ctlM As New MasterController
    Dim ctlE As New ElearningController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadCourse()

            If Not Request("pid") Is Nothing Then
                LoadPersonData(Request("pid"))
            End If
        End If
    End Sub
    Private Sub LoadPersonData(PersonUID As Integer)
        dt = ctlP.Person_GetByPersonID(PersonUID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdPersonUID.Value = String.Concat(.Item("PersonUID"))
                lblCode.Text = String.Concat(.Item("Code"))
                lblPersonName.Text = String.Concat(.Item("PersonName"))
                lblPosition.Text = String.Concat(.Item("PositionName"))
                lblDepartment.Text = String.Concat(.Item("DepartmentName"))
                lblSection.Text = String.Concat(.Item("SectionName"))
                LoadCourseAssign(hdPersonUID.Value)

            End With
        Else
        End If
    End Sub


    Private Sub LoadCourseAssign(PersonUID As Integer)
        'chkCourse.ClearSelection()
        'dt = ctlE.CourseAssign_getbyPersonUID(PersonUID)
        'Dim itemCount, dataRow As Integer
        'itemCount = chkCourse.Items.Count
        'dataRow = dt.Rows.Count

        'If dt.Rows.Count > 0 Then
        '    For i = 0 To dataRow - 1
        '        For n = 0 To itemCount - 1
        '            If dt.Rows(i)("CourseUID") = chkCourse.Items(n).Value Then
        '                chkCourse.Items(n).Selected = True
        '            End If
        '        Next
        '    Next
        'End If

        dt = ctlE.CourseAssign_GetByPersonUID(PersonUID)
        grdCompany.DataSource = dt
            grdCompany.DataBind()



    End Sub
    Private Sub LoadCourse()
        ddlCourse.Items.Clear()
        dt = ctlE.Course_GetActive()
        With ddlCourse
            .Enabled = True
            .DataSource = dt
            .DataTextField = "Name"
            .DataValueField = "UID"
            .DataBind()
            .Visible = True
        End With
        dt = Nothing
    End Sub
    'Private Sub SaveCourseAssignment(PersonUID As Integer)
    '    'ctlE.CourseAssign_DeleteByPerson(PersonUID)
    '    For i = 0 To chkCourse.Items.Count - 1
    '        If chkCourse.Items(i).Selected Then

    '            ctlE.CourseAssign_Save(PersonUID, StrNull2Zero(chkCourse.Items(i).Value), ConvertBoolean2StatusFlag(chkCourse.Items(i).Selected))
    '        End If
    '    Next
    'End Sub

    Protected Sub cmdAdd_Click(sender As Object, e As EventArgs) Handles cmdAdd.Click
        Dim Duedate As String

        Duedate = txtDueDate.Text

        If Duedate = "" Or Duedate = "dd/mm/yyyy" Then
            Duedate = ""
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุ Duedate');", True)
            Exit Sub
        Else
            Duedate = ConvertStrDate2ShortDateEN(txtDueDate.Text) 'SetStrDate2DBDateFormat(Duedate)
        End If

        ctlE.CourseAssign_Save(StrNull2Zero(hdPersonUID.Value), StrNull2Zero(ddlCourse.SelectedValue), DueDate, optRecure.SelectedValue)

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        LoadCourseAssign(hdPersonUID.Value)
    End Sub

    Protected Sub grdCompany_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCompany.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlE.CourseAssign_Delete(e.CommandArgument) Then

                        Dim acc As New UserController
                        acc.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_DEL, "Division", "ลบชื่อ Course Assign :{personid=" & hdPersonUID.Value & "}{cid=" & e.CommandArgument() & "}", "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                        LoadCourseAssign(hdPersonUID.Value)
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)

                    End If


            End Select


        End If
    End Sub
    Private Sub EditData(ByVal pID As String)
        dt = ctlE.CourseAssign_GetByUID(pID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                ddlCourse.SelectedValue = String.Concat(dt.Rows(0)("CourseUID"))
                txtDueDate.Text = String.Concat(dt.Rows(0)("Duedate"))
                optRecure.SelectedValue = String.Concat(dt.Rows(0)("RecureStatus"))
            End With
        End If
        dt = Nothing
    End Sub


    Private Sub grdCompany_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdCompany.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(1).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d9edf7';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
End Class