﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Classroom1.aspx.vb" Inherits="Ergonomic.Classroom1" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>Classroom
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Classroom</li>
      </ol>
    </section>

    <!-- Main content -->   
      <section class="content">  
            
<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">
          <asp:Label ID="lblCourseName" runat="server"></asp:Label>  
          <asp:HiddenField ID="hdCourseUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
          
     <div class="col-lg-12">          
          <div class="text-center">
               <% if dtCourse1.Rows.Count > 0 Then %>
              <iframe src="<% =String.Concat(dtCourse1.Rows(0)("MediaLink")) %>" width="962px" height="565px" frameborder="0"></iframe>
               <% End If %>
          </div>
      </div>     
          
</div>
            <div class="row">
  <div class="col-lg-12 text-center">
        <br/>
      
          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="สอบหลังบทเรียน" Width="150px" />
            <asp:Button ID="cmdReload" CssClass="btn btn-primary" runat="server" Text="เริ่มต้นใหม่" Width="150px" />
 
        </div>

        <div class="col-lg-12">
          <div class="form-group">
            <label></label>
            <asp:Label ID="lblAlert" runat="server" cssclass="form-control text-center" ></asp:Label>
          </div>

        </div>

      </div>    
    </div>
    <!-- /.box-body -->
  </div>
  </section>
</div>                          
      
      </section>
</asp:Content>