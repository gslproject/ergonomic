﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PersonLearning.aspx.vb" Inherits="Ergonomic.PersonLearning" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>My E-Learning 
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Person</li>
      </ol>
    </section>
       
      <section class="content">     
            <div class="row">
    <section class="col-lg-12 connectedSortable"> 
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Course/หลักสูตร</h3>
    </div>
    
    <div class="box-body"> 
    <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th>No.</th>   
                    <th>Course</th>
                  <th>Duedate</th>
                  <th>Status</th>    
                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtLearn.Rows %>
                <tr>  
                  <td width="40"><% =String.Concat(row("nRow")) %></td>
                  <td><a class="editemp" href="Media?cid=<% =String.Concat(row("UID")) %>" target="_blank" > <% =String.Concat(row("CourseName")) %></a></td>
                  <td><% =String.Concat(row("Duedate")) %></td>
                  <td><% =String.Concat(row("LearnStatusName")) %></td>                 
                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>         
    </div>
    </div>
        </section> 
                </div>
      <div class="row text-center">
           <a href="Report/rptPersonLearning.aspx" target="_blank"  class="btn btn-success"><i class="fa fa-download"></i> Export</a>
      </div>

          </section>
</asp:Content>