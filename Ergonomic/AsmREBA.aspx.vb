﻿Public Class AsmREBA
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlA As New AssessmentController
    Dim ctlP As New PersonController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadPerson()
            LoadTaskData()
            optAsmType.SelectedValue = Session("asmtype")
            txtAsmDate.Text = Today.Date.ToShortDateString()

            If Session("asmtype") = "O" Then
                ddlPerson.Enabled = True
            Else
                ddlPerson.SelectedValue = Request.Cookies("Ergo")("LoginPersonUID")
                ddlPerson.Enabled = False
            End If

            If Not Request("id") Is Nothing Then
                LoadAssessmentData()
            End If

        End If

        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")
    End Sub

    Private Sub LoadPerson()
        Dim dtE As New DataTable
        dtE = ctlP.Person_GetByCompany(Request.Cookies("Ergo")("LoginCompanyUID"))
        ddlPerson.DataSource = dtE
        ddlPerson.DataTextField = "PersonName"
        ddlPerson.DataValueField = "PersonUID"
        ddlPerson.DataBind()
    End Sub
    Private Sub LoadTaskData()
        Dim ctlT As New TaskController
        dt = ctlT.Task_GetByUID(Session("asmtask"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                txtTaskNo.Text = String.Concat(.Item("TaskNo"))
                txtTaskName.Text = String.Concat(.Item("TaskName"))
            End With
        Else
        End If
    End Sub
    Private Sub LoadAssessmentData()
        dt = ctlA.Assessment_GetByUID("REBA", Request("id"))

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdUID.Value = .Item("UID")
                txtTaskNo.Text = .Item("TaskNo")
                optAsmType.SelectedValue = .Item("AsmType")
                ddlPerson.SelectedValue = .Item("PersonUID")
                txtAsmDate.Text = .Item("AsmDate")
                txtRemark.Text = .Item("Remark")

                optReba1.SelectedValue = .Item("S1")
                optReba2.SelectedValue = .Item("S2")
                optReba3.SelectedValue = .Item("S3")

                optReba5.SelectedValue = .Item("S5")
                optReba7.SelectedValue = .Item("S7")
                optReba8.SelectedValue = .Item("S8")
                optReba9.SelectedValue = .Item("S9")
                optReba11.SelectedValue = .Item("S11")
                optReba13.SelectedValue = .Item("S13")

                Dim str1(), str2(), str3(), str5(), str7(), str9() As String

                str1 = Split(String.Concat(.Item("S1A")), "|")
                For i = 0 To str1.Length - 1
                    For n = 0 To chkRebaAdd1.Items.Count - 1
                        If str1(i) = chkRebaAdd1.Items(n).Value Then
                            chkRebaAdd1.Items(n).Selected = True
                        End If
                    Next
                Next


                str2 = Split(String.Concat(.Item("S2A")), "|")
                For i = 0 To str2.Length - 1
                    For n = 0 To chkRebaAdd2.Items.Count - 1
                        If str2(i) = chkRebaAdd2.Items(n).Value Then
                            chkRebaAdd2.Items(n).Selected = True
                        End If
                    Next
                Next


                str3 = Split(String.Concat(.Item("S3A")), "|")
                For i = 0 To str3.Length - 1
                    For n = 0 To chkRebaAdd3.Items.Count - 1
                        If str3(i) = chkRebaAdd3.Items(n).Value Then
                            chkRebaAdd3.Items(n).Selected = True
                        End If
                    Next
                Next



                str5 = Split(String.Concat(.Item("S5A")), "|")
                For i = 0 To str5.Length - 1
                    For n = 0 To chkRebaAdd5.Items.Count - 1
                        If str5(i) = chkRebaAdd5.Items(n).Value Then
                            chkRebaAdd5.Items(n).Selected = True
                        End If
                    Next
                Next

                str7 = Split(String.Concat(.Item("S7A")), "|")
                For i = 0 To str7.Length - 1
                    For n = 0 To chkRebaAdd7.Items.Count - 1
                        If str7(i) = chkRebaAdd7.Items(n).Value Then
                            chkRebaAdd7.Items(n).Selected = True
                        End If
                    Next
                Next

                str9 = Split(String.Concat(.Item("S9A")), "|")
                For i = 0 To str9.Length - 1
                    For n = 0 To chkRebaAdd9.Items.Count - 1
                        If str9(i) = chkRebaAdd9.Items(n).Value Then
                            chkRebaAdd9.Items(n).Selected = True
                        End If
                    Next
                Next



                lblFinalScore.Text = .Item("FinalScore")
                lblFinalResult.Text = .Item("ResultText")
            End With
        End If


    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtAsmDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุวันที่ประเมินก่อน');", True)
            Exit Sub
        End If
        'If txtName.Text = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อบริษัท');", True)
        '    DisplayMessage(Me.Page, "กรุณาระบุชื่อบริษัท")
        'End If

        CalculateScore()

        '--------------------------
        Dim S1A, S2A, S3A, S5A, S7A, S9A As String

        For i = 0 To chkRebaAdd1.Items.Count - 1
            If chkRebaAdd1.Items(i).Selected Then
                S1A = S1A & chkRebaAdd1.Items(i).Value & "|"
            End If
        Next

        For i = 0 To chkRebaAdd2.Items.Count - 1
            If chkRebaAdd2.Items(i).Selected Then
                S2A = S2A & chkRebaAdd2.Items(i).Value & "|"
            End If
        Next

        For i = 0 To chkRebaAdd3.Items.Count - 1
            If chkRebaAdd3.Items(i).Selected Then
                S3A = S3A & chkRebaAdd3.Items(i).Value & "|"
            End If
        Next



        For i = 0 To chkRebaAdd5.Items.Count - 1
            If chkRebaAdd5.Items(i).Selected Then
                S5A = S5A & chkRebaAdd5.Items(i).Value & "|"
            End If
        Next


        For i = 0 To chkRebaAdd7.Items.Count - 1
            If chkRebaAdd7.Items(i).Selected Then
                S7A = S7A & chkRebaAdd7.Items(i).Value & "|"
            End If
        Next
        For i = 0 To chkRebaAdd9.Items.Count - 1
            If chkRebaAdd9.Items(i).Selected Then
                S9A = S9A & chkRebaAdd9.Items(i).Value & "|"
            End If
        Next


        '--------------------------

        ctlA.AsmREBA_Save(StrNull2Zero(hdUID.Value), txtTaskNo.Text, optAsmType.SelectedValue, ddlPerson.SelectedValue, ConvertStrDate2ShortDateTH(txtAsmDate.Text), txtRemark.Text, optReba1.SelectedValue, optReba2.SelectedValue, optReba3.SelectedValue, optReba5.SelectedValue, optReba7.SelectedValue, optReba8.SelectedValue, optReba9.SelectedValue, optReba11.SelectedValue, optReba13.SelectedValue, S1A, S2A, S3A, S5A, S7A, S9A, StrNull2Zero(lblFinalScore.Text), lblFinalResult.Text, Request.Cookies("Ergo")("userid"))


        'Dim ctlU As New UserController
        'ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_UPD, "Customer", "บันทึก/แก้ไข ประวัติส่วนตัว :{uid=" & hdCompUID.Value & "}{code=" & txtCompanyID.Text & "}", "")

        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Response.Redirect("AsmResult.aspx")
    End Sub

    Private Sub CalculateScore()
        Dim sc1, sc2, sc3, sc5, sc7, sc8, sc9, sc11, sc13 As Integer

        sc1 = ctlA.AssessmentAnswer_GetScore("REBA", 1, optReba1.SelectedValue)
        sc2 = ctlA.AssessmentAnswer_GetScore("REBA", 2, optReba2.SelectedValue)
        sc3 = ctlA.AssessmentAnswer_GetScore("REBA", 3, optReba3.SelectedValue)
        sc5 = StrNull2Zero(optReba5.SelectedValue)
        sc7 = ctlA.AssessmentAnswer_GetScore("REBA", 7, optReba7.SelectedValue)
        sc8 = ctlA.AssessmentAnswer_GetScore("REBA", 8, optReba8.SelectedValue)
        sc9 = ctlA.AssessmentAnswer_GetScore("REBA", 9, optReba9.SelectedValue)
        sc11 = StrNull2Zero(optReba11.SelectedValue)
        If optReba13.SelectedValue Is Nothing Then
            sc13 = 0
        Else
            sc13 = 1
        End If


        For i = 0 To chkRebaAdd1.Items.Count - 1
            If chkRebaAdd1.Items(i).Selected Then
                sc1 = sc1 + ctlA.AssessmentAnswer_GetScore("REBA", 1, chkRebaAdd1.Items(i).Value)
            End If
        Next

        If sc1 > 3 Then 'คอ ไม่เกิน 3
            sc1 = 3
        End If

        For i = 0 To chkRebaAdd2.Items.Count - 1
            If chkRebaAdd2.Items(i).Selected Then
                sc2 = sc2 + ctlA.AssessmentAnswer_GetScore("REBA", 2, chkRebaAdd2.Items(i).Value)
            End If
        Next
        If sc2 > 5 Then 'ลำตัว ไม่เกิน 5
            sc2 = 5
        End If

        For i = 0 To chkRebaAdd3.Items.Count - 1
            If chkRebaAdd3.Items(i).Selected Then
                sc3 = sc3 + ctlA.AssessmentAnswer_GetScore("REBA", 3, chkRebaAdd3.Items(i).Value)
            End If
        Next
        If sc3 > 4 Then 'ขา ไม่เกิน 4
            sc3 = 4
        End If

        If chkRebaAdd5.Items(0).Selected Then
            sc5 = sc5 + StrNull2Zero(chkRebaAdd5.Items(0).Value)
        End If
        If sc5 > 3 Then 'ภาระงานที่ทำ ไม่เกิน 3
            sc5 = 3
        End If

        'Step7
        For i = 0 To chkRebaAdd7.Items.Count - 1
            If chkRebaAdd7.Items(i).Selected Then
                sc7 = sc7 + ctlA.AssessmentAnswer_GetScore("REBA", 7, chkRebaAdd7.Items(i).Value)
            End If
        Next
        If sc7 > 6 Then 'แขนส่วนบน ไม่เกิน 6
            sc7 = 6
        End If


        'Step8
        If sc8 > 2 Then 'แขนส่วนล่าง ไม่เกิน 2
            sc8 = 2
        End If


        'Step9 มือและข้อมือ
        For i = 0 To chkRebaAdd9.Items.Count - 1
            If chkRebaAdd9.Items(i).Selected Then
                sc9 = sc9 + ctlA.AssessmentAnswer_GetScore("REBA", 9, chkRebaAdd9.Items(i).Value)
            End If
        Next
        If sc9 > 3 Then 'มือและข้อมือ ไม่เกิน 3
            sc9 = 3
        End If


        Dim ScoreA, ScoreB, ScoreC, ScoreFinal As Integer
        'Step4-6 นำคะแนนจาก 1-3 มาหาคะแนนจากตาราง A และ รวมกับ 5
        ScoreA = ctlA.ScoreREBA_GetScore("A", sc2, sc1, sc3) + sc5

        'Step10-12 นำคะแนนจาก 7-9 มาเปิดตาราง B (แขนบน,แขนล่าง,ข้อมือ) + 11
        ScoreB = ctlA.ScoreREBA_GetScore("B", sc7, sc8, sc9) + sc11

        'Step14 นำคะแนน A ที่รวมกับ 5 และ B ที่รวม 11 แล้วไปเปิดตาราง C
        ScoreC = ctlA.ScoreREBA_GetScore("C", ScoreA, ScoreB, 0)

        'Step15  นำคะแนน 13 + C = REBA
        ScoreFinal = ScoreC + sc13

        lblFinalScore.Text = ScoreFinal.ToString()

        'If ScoreFinal = 1 Then
        '    lblFinalResult.Text = "ความเสี่ยงน้อยมาก"
        'ElseIf ScoreFinal >= 2 And ScoreFinal <= 3 Then
        '    lblFinalResult.Text = "ความเสี่ยงน้อย ยังต้องมีการปรับปรุงงาน"
        'ElseIf ScoreFinal >= 4 And ScoreFinal <= 7 Then
        '    lblFinalResult.Text = "ความเสี่ยงปานกลาง ควรวิเคราะห์เพิ่มเติมและควรได้รับการปรับปรุง"
        'ElseIf ScoreFinal >= 8 And ScoreFinal <= 10 Then
        '    lblFinalResult.Text = "ความเสี่ยงสูง ควรวิเคราะห์เพิ่มเติมและควรรีบปรับปรุง"
        'Else
        '    lblFinalResult.Text = "ความเสี่ยงสูงมาก ควรปรับปรุงทันที"
        'End If

        If ScoreFinal = 1 Then
            lblFinalResult.Text = "ระดับ 1 ภาวะที่ยอมรับได้"
        ElseIf ScoreFinal >= 2 And ScoreFinal <= 3 Then
            lblFinalResult.Text = "ระดับ 2 งานนั้นควรได้รับการตรวจสอบและศึกษารายละเอียดเพิ่มเติม"
        ElseIf ScoreFinal >= 4 And ScoreFinal <= 7 Then
            lblFinalResult.Text = "ระดับ 3 งานนั้นเริ่มเป็นปัญหาควรตรวจสอบและรีบดำเนินการปรับปรุงให้ดีขึ้น"
        ElseIf ScoreFinal >= 8 Then
            lblFinalResult.Text = "ระดับ 4 งานนั้นเป็นปัญหาควรรีบทำการปรับปรุงหรือแก้ไขโดยทันที"
        End If


    End Sub

    Protected Sub optAsmType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAsmType.SelectedIndexChanged
        If optAsmType.SelectedValue = "O" Then
            ddlPerson.Enabled = True
        Else
            ddlPerson.SelectedValue = Request.Cookies("Ergo")("LoginPersonUID")
            ddlPerson.Enabled = False
        End If
    End Sub

    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Response.Redirect("Evaluated.aspx")
    End Sub

    Protected Sub optReba1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optReba1.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRebaAdd1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRebaAdd1.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optReba2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optReba2.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRebaAdd2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRebaAdd2.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optReba3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optReba3.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRebaAdd3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRebaAdd3.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optReba5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optReba5.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRebaAdd5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRebaAdd5.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optReba7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optReba7.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRebaAdd7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRebaAdd7.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optReba8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optReba8.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optReba9_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optReba9.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRebaAdd9_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRebaAdd9.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optReba11_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optReba11.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optReba13_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optReba13.SelectedIndexChanged
        CalculateScore()
    End Sub
    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlA.AsmREBA_Delete(StrNull2Zero(hdUID.Value))
    End Sub

    'Protected Sub chkRebaAdd3A1_CheckedChanged(sender As Object, e As EventArgs) Handles chkRebaAdd3A1.CheckedChanged
    '    chkRebaAdd3A2.Checked = False
    '    CalculateScore()
    'End Sub

    'Protected Sub chkRebaAdd3A2_CheckedChanged(sender As Object, e As EventArgs) Handles chkRebaAdd3A2.CheckedChanged
    '    chkRebaAdd3A1.Checked = False
    '    CalculateScore()
    'End Sub
End Class