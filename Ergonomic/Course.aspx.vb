﻿Imports System.Data
Public Class Course
    Inherits System.Web.UI.Page
    Dim ctlC As New ElearningController
    Public dtCourse As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Request.Cookies("Ergo")("ROLE_ADM") = False And Request.Cookies("Ergo")("ROLE_SPA") = False Then
            Response.Redirect("Error.aspx")
        End If

        If Not IsPostBack Then
            LoadCourse()
        End If
    End Sub
    Private Sub LoadCourse()
        dtCourse = ctlC.Course_Get
    End Sub

End Class