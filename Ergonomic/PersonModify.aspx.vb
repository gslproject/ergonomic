﻿Public Class PersonModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New PersonController
    Dim ctlM As New MasterController
    Dim ctlC As New CompanyController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            lblAlert1.Visible = False
            lblAlert2.Visible = False

            LoadCompany()
            'LoadBusinessUnit()
            LoadPrefix()
            LoadProvince()
            LoadDivision()
            LoadPosition()
            BindDayToDDL()
            BindMonthToDDL()
            BindYearToDDL()

            If Request("pid") Is Nothing Then
                If Request("t") = "m" Then
                    LoadPersonData(Request.Cookies("Ergo")("LoginPersonUID"))

                Else
                    ' เป็นการเพิ่มใหม่ ตรวจสอบ limit 
                    If Request.Cookies("Ergo")("ROLE_ADM") = True Or Request.Cookies("Ergo")("ROLE_SPA") = True Then

                    Else
                        If ctlC.Company_CheckOverMaxLimit(Request.Cookies("Ergo")("LoginCompanyUID")) = True Then
                            lblAlert1.Visible = True
                            lblAlert2.Visible = True
                            cmdSave.Visible = False
                        Else
                            cmdSave.Visible = True
                            lblAlert1.Visible = False
                            lblAlert2.Visible = False
                        End If


                    End If

                End If
            Else
                ''LoadPersonData(Request.Cookies("Ergo")("LoginPersonUID")) 'Request.Cookies("Ergo")("LoginPersonUID")
                LoadPersonData(Request("pid"))
            End If


            If Request.Cookies("Ergo")("ROLE_ADM") = True Or Request.Cookies("Ergo")("ROLE_SPA") = True Then
                ddlCompany.Enabled = True
                cmdDel.Visible = True
            Else
                ddlCompany.Enabled = False
                cmdDel.Visible = False
            End If



            End If
    End Sub
    Private Sub LoadCompany()

        ddlCompany.DataSource = ctlC.Company_GetActive()
        ddlCompany.DataTextField = "CompanyName"
        ddlCompany.DataValueField = "UID"
        ddlCompany.DataBind()
        ddlCompany.SelectedValue = Request.Cookies("Ergo")("LoginCompanyUID")

    End Sub


    'Private Sub LoadBusinessUnit()
    '    Dim ctlOrg As New OrganizationController
    '    ddlBU.DataSource = ctlOrg.Organization_Get()
    '    ddlBU.DataTextField = "UnitName"
    '    ddlBU.DataValueField = "OrganizationCode"
    '    ddlBU.DataBind()
    'End Sub
    Private Sub LoadPrefix()
        ddlPrefix.DataSource = ctlM.Prefix_GetActive()
        ddlPrefix.DataTextField = "Name"
        ddlPrefix.DataValueField = "UID"
        ddlPrefix.DataBind()
    End Sub

    Private Sub LoadProvince()
        ddlProvince.DataSource = ctlM.Province_Get()
        ddlProvince.DataTextField = "ProvinceName"
        ddlProvince.DataValueField = "ProvinceID"
        ddlProvince.DataBind()
    End Sub
    Private Sub LoadDivision()
        ddlDivision.DataSource = ctlM.Division_Get(ddlCompany.SelectedValue)
        ddlDivision.DataTextField = "DivisionName"
        ddlDivision.DataValueField = "DivisionUID"
        ddlDivision.DataBind()
    End Sub
    Private Sub LoadDepartment()
        ddlDepartment.DataSource = ctlM.Department_GetByDivisionUID(ddlCompany.SelectedValue, ddlDivision.SelectedValue)
        ddlDepartment.DataTextField = "DepartmentName"
        ddlDepartment.DataValueField = "DepartmentUID"
        ddlDepartment.DataBind()
    End Sub
    Private Sub LoadPosition()
        ddlPosition.DataSource = ctlM.Position_Get(ddlCompany.SelectedValue) 'Request.Cookies("Ergo")("LoginCompanyUID"))
        ddlPosition.DataTextField = "PositionName"
        ddlPosition.DataValueField = "PositionUID"
        ddlPosition.DataBind()
    End Sub


    Private Sub BindDayToDDL()
        Dim sD As String = ""

        ddlDay.Items.Add("")
        ddlDay.Items(0).Value = ""

        For i = 1 To 31
            If i < 10 Then
                sD = "0" & i
            Else
                sD = i
            End If

            With ddlDay
                .Items.Add(i)
                .Items(i).Value = sD
                .SelectedIndex = 0
            End With
        Next
    End Sub
    Private Sub BindMonthToDDL()
        Dim sD As String = ""
        ddlMonth.Items.Add("")
        ddlMonth.Items(0).Value = ""
        For i = 1 To 12
            If i < 10 Then
                sD = "0" & i
            Else
                sD = i
            End If

            With ddlMonth
                .Items.Add(DisplayNumber2Month(i))
                .Items(i).Value = sD
                .SelectedIndex = 0
            End With
        Next
    End Sub

    Private Sub BindYearToDDL()
        Dim ctlb As New BaseClass
        Dim sDate As Date
        Dim y As Integer

        ddlYear.Items.Add("")
        ddlYear.Items(0).Value = ""


        sDate = ctlb.GET_DATE_SERVER()

        If sDate.Year < 2500 Then
            y = (sDate.Year + 543)
        Else
            y = sDate.Year
        End If
        Dim i As Integer = 0
        Dim n As Integer = y

        For i = 1 To y - 15
            With ddlYear
                .Items.Add(n)
                .Items(i).Value = n
                .SelectedIndex = 0
            End With
            n = n - 1
            If n < 2500 Then
                Exit For
            End If
        Next
    End Sub

    Private Sub LoadPersonData(PersonUID As Integer)
        dt = ctlE.Person_GetByPersonID(PersonUID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdPersonUID.Value = String.Concat(.Item("PersonUID"))
                txtCode.Text = String.Concat(.Item("Code"))
                ddlPrefix.SelectedValue = String.Concat(.Item("PrefixUID"))
                txtFirstNameTH.Text = String.Concat(.Item("NameTH"))
                txtLastNameTH.Text = String.Concat(.Item("SurnameTH"))
                txtFNameEN.Text = String.Concat(.Item("NameEN"))
                txtLNameEN.Text = String.Concat(.Item("SurnameEN"))

                'BDate = String.Concat(.Item("Code"))
                'txtCardID.Text = String.Concat(.Item("CardID"))
                optSex.SelectedValue = String.Concat(.Item("Gender"))

                ddlDay.SelectedValue = Right(String.Concat(.Item("BirthDateTXT")), 2)
                ddlMonth.SelectedValue = Mid(String.Concat(.Item("BirthDateTXT")), 5, 2)
                ddlYear.SelectedValue = StrNull2Zero(Left(String.Concat(.Item("BirthDateTXT")), 4)) + 543

                txtAge.Text = String.Concat(.Item("Age"))

                ddlCompany.SelectedValue = String.Concat(.Item("CompanyUID"))

                LoadDivision()
                ddlDivision.SelectedValue = String.Concat(.Item("DivisionUID"))
                LoadDepartment()
                ddlDepartment.SelectedValue = String.Concat(.Item("DepartmentUID"))
                LoadPosition()
                ddlPosition.SelectedValue = String.Concat(.Item("PositionUID"))
                If String.Concat(.Item("StartWorkDate")) <> "" Then
                    txtStartDate.Text = DisplayShortDateTH(.Item("StartWorkDate"))
                End If
                If String.Concat(.Item("EndWorkDate")) <> "" Then
                    txtEndDate.Text = DisplayShortDateTH(.Item("EndWorkDate"))
                End If

                ddlWorkStatus.SelectedValue = String.Concat(.Item("EmployeeStatus"))

                'lblWY.Text = String.Concat(.Item("WorkYear")) & " Y " & String.Concat(.Item("WorkMonth")) & " M "

                txtWorkDayPerWeek.Text = String.Concat(.Item("WorkDayPerWeek"))
                ddlWorkPeriod.SelectedValue = String.Concat(.Item("WorkPeriod"))

                ddlEdu.SelectedValue = String.Concat(.Item("Education"))
                txtEduRemark.Text = String.Concat(.Item("EducationRemark"))

                txtAddressNo.Text = String.Concat(.Item("AddressNumber"))
                txtLane.Text = String.Concat(.Item("Lane"))
                txtRoad.Text = String.Concat(.Item("Road"))
                txtSubDistrict.Text = String.Concat(.Item("SubDistrict"))
                txtDistrict.Text = String.Concat(.Item("District"))
                ddlProvince.SelectedValue = String.Concat(.Item("ProvinceID"))
                txtZipcode.Text = String.Concat(.Item("ZipCode"))
                ddlCountry.SelectedValue = String.Concat(.Item("Country"))
                txtTel.Text = String.Concat(.Item("Telephone"))
                txtEmail.Text = String.Concat(.Item("Email"))
                txtAddressTha.Text = String.Concat(.Item("AddressTH"))
                txtAddressEng.Text = String.Concat(.Item("AddressEN"))
                ddlBloodGroup.SelectedValue = String.Concat(.Item("BloodGroup"))
                txtMedicalHistory.Text = String.Concat(.Item("MedicalHistory"))
                txtAllergy.Text = String.Concat(.Item("Allergy"))

                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(.Item("StatusFlag")))

            End With

        Else
        End If
    End Sub

    Protected Sub ddlDivision_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDivision.SelectedIndexChanged
        LoadDepartment()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If StrNull2Zero(hdPersonUID.Value) = 0 Then
            If ctlC.Company_CheckOverMaxLimit(StrNull2Zero(ddlCompany.SelectedValue)) = True Then
                ' เกิน limit ที่ซื้อไว้
                lblAlert1.Visible = True
                lblAlert2.Visible = True
                cmdSave.Visible = False
                cmdDel.Visible = False
                Exit Sub
            Else
                cmdSave.Visible = True
                cmdDel.Visible = True
                lblAlert1.Visible = False
                lblAlert2.Visible = False
            End If
        End If


        Dim DOB, DOBTXT As String
        If ddlDay.SelectedValue <> "" And ddlMonth.SelectedValue <> "" And ddlYear.SelectedValue <> "" Then
            DOB = ddlMonth.SelectedValue & "/" & ddlDay.SelectedValue & "/" & (StrNull2Zero(ddlYear.SelectedValue) - 543).ToString()
            DOBTXT = (StrNull2Zero(ddlYear.SelectedValue) - 543).ToString() & ddlMonth.SelectedValue & ddlDay.SelectedValue
        Else
            DOB = ""
            DOBTXT = ""
        End If
        Dim BWork, EWork As String

        BWork = txtStartDate.Text
        EWork = txtEndDate.Text

        If BWork = "" Or BWork = "dd/mm/yyyy" Then
            BWork = ""
        Else
            BWork = SetStrDate2DBDateFormat(BWork)
        End If


        If EWork = "" Or EWork = "dd/mm/yyyy" Then
            EWork = ""
        Else
            EWork = SetStrDate2DBDateFormat(EWork)
        End If


        ctlE.Person_Save(StrNull2Zero(hdPersonUID.Value), txtCode.Text, StrNull2Zero(ddlPrefix.SelectedValue), txtFirstNameTH.Text, txtLastNameTH.Text, txtFNameEN.Text, txtLNameEN.Text, DOB, DOBTXT, optSex.SelectedValue, "E", StrNull2Zero(ddlCompany.SelectedValue), StrNull2Zero(ddlDivision.SelectedValue), StrNull2Zero(ddlDepartment.SelectedValue), StrNull2Zero(ddlPosition.SelectedValue), BWork, EWork, txtAddressNo.Text, txtLane.Text, txtRoad.Text, txtSubDistrict.Text, txtDistrict.Text, ddlProvince.SelectedValue, txtZipcode.Text, ddlCountry.SelectedValue, txtTel.Text, txtEmail.Text, txtAddressTha.Text, txtAddressEng.Text, ddlBloodGroup.SelectedValue, txtMedicalHistory.Text, txtAllergy.Text, ddlWorkStatus.SelectedValue, ConvertBoolean2StatusFlag(chkStatus.Checked), Request.Cookies("Ergo")("userid"), StrNull2Zero(txtWorkDayPerWeek.Text), ddlWorkPeriod.SelectedValue, ddlEdu.SelectedValue, txtEduRemark.Text)

        'Dim ctlU As New UserController
        'Dim enc As New CryptographyEngine


        ''--------------Add User-----------------
        'Dim iPersonUID As Integer

        'If StrNull2Zero(hdPersonUID.Value) = 0 Then
        '    If txtCode.Text <> "" Then
        '        iPersonUID = ctlE.Person_GetPersonUID(txtCode.Text, txtFirstNameTH.Text.Trim())
        '        ctlU.User_Save(0, txtFirstNameTH.Text & " " & txtLastNameTH.Text, txtCode.Text, enc.EncryptString("1234", True), iPersonUID, "A", Request.Cookies("Ergo")("uid"), 1)

        '        Dim UserID As Integer
        '        UserID = ctlU.User_GetUID(txtCode.Text)
        '        ctlU.UserRole_Save(UserID, 1, "Y", Request.Cookies("Ergo")("uid"))
        '    End If
        'End If
        ''-----------------End Add User-----------


        'ctlU.User_Save(0, txtFirstNameTH.Text & " " & txtLastNameTH.Text, txtCode.Text, "1234", hdPersonUID.Value, "A", Request.Cookies("Ergo")("userid"))

        'ctlU.UserCompany_Save(StrNull2Zero(lblUserID.Text), txtUsername.Text, ddlCompany.SelectedValue)

        Dim ctlU As New UserController
        ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_UPD, "Customer", "บันทึก/แก้ไข ประวัติส่วนตัว :{uid=" & hdPersonUID.Value & "}{code=" & txtCode.Text & "}", "")

        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        If Request("ActionType") = "bio" Then
            Response.Redirect("ResultPage.aspx?p=bio")
        Else
            Response.Redirect("ResultPage.aspx?p=person")
        End If

    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        If ddlDay.SelectedValue <> "" And ddlMonth.SelectedValue <> "" And ddlYear.SelectedValue <> "" Then
            Dim dob As Date
            dob = Date.Parse(ddlDay.SelectedValue & "/" & ddlMonth.SelectedValue & "/" & ddlYear.SelectedValue)
            txtAge.Text = DateDiff("yyyy", Date.Parse(dob), Now.Date).ToString()
        End If
    End Sub

    Private Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadDivision()
        LoadPosition()
    End Sub
End Class