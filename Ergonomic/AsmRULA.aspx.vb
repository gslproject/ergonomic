﻿Public Class AsmRULA
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlA As New AssessmentController
    Dim ctlP As New PersonController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadPerson()
            LoadTaskData()
            optAsmType.SelectedValue = Session("asmtype")
            txtAsmDate.Text = Today.Date.ToShortDateString()

            If Session("asmtype") = "O" Then
                ddlPerson.Enabled = True
            Else
                ddlPerson.SelectedValue = Request.Cookies("Ergo")("LoginPersonUID")
                ddlPerson.Enabled = False
            End If

            If Not Request("id") Is Nothing Then
                LoadAssessmentData()
            End If

        End If

        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")
    End Sub
    Private Sub LoadPerson()
        Dim dtE As New DataTable
        dtE = ctlP.Person_GetByCompany(Request.Cookies("Ergo")("LoginCompanyUID"))
        ddlPerson.DataSource = dtE
        ddlPerson.DataTextField = "PersonName"
        ddlPerson.DataValueField = "PersonUID"
        ddlPerson.DataBind()
    End Sub
    Private Sub LoadTaskData()
        Dim ctlT As New TaskController
        dt = ctlT.Task_GetByUID(Session("asmtask"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                txtTaskNo.Text = String.Concat(.Item("TaskNo"))
                txtTaskName.Text = String.Concat(.Item("TaskName"))
            End With
        Else
        End If
    End Sub
    Private Sub LoadAssessmentData()
        dt = ctlA.Assessment_GetByUID("RULA", Request("id"))

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdUID.Value = .Item("UID")
                txtTaskNo.Text = .Item("TaskNo")
                optAsmType.SelectedValue = .Item("AsmType")
                ddlPerson.SelectedValue = .Item("PersonUID")
                txtAsmDate.Text = .Item("AsmDate")
                txtRemark.Text = .Item("Remark")

                optRULA1.SelectedValue = .Item("S1")
                optRULA2.SelectedValue = .Item("S2")
                optRULA3.SelectedValue = .Item("S3")
                optRULA4.SelectedValue = .Item("S4")
                optRULA6.SelectedValue = .Item("S6")
                optRULA7.SelectedValue = .Item("S7")
                optRULA9.SelectedValue = .Item("S9")
                optRULA10.SelectedValue = .Item("S10")
                optRULA11.SelectedValue = .Item("S11")
                optRULA13.SelectedValue = .Item("S13")
                optRULA14.SelectedValue = .Item("S14")



                Dim str1(), str2(), str3(), str9(), str10() As String

                str1 = Split(String.Concat(.Item("S1A")), "|")
                For i = 0 To str1.Length - 1
                    For n = 0 To chkRulaAdd1.Items.Count - 1
                        If str1(i) = chkRulaAdd1.Items(n).Value Then
                            chkRulaAdd1.Items(n).Selected = True
                        End If
                    Next
                Next


                str2 = Split(String.Concat(.Item("S2A")), "|")
                For i = 0 To str2.Length - 1
                    For n = 0 To chkRulaAdd2.Items.Count - 1
                        If str2(i) = chkRulaAdd2.Items(n).Value Then
                            chkRulaAdd2.Items(n).Selected = True
                        End If
                    Next
                Next


                str3 = Split(String.Concat(.Item("S3A")), "|")
                For i = 0 To str3.Length - 1
                    For n = 0 To chkRulaAdd3.Items.Count - 1
                        If str3(i) = chkRulaAdd3.Items(n).Value Then
                            chkRulaAdd3.Items(n).Selected = True
                        End If
                    Next
                Next

                str9 = Split(String.Concat(.Item("S9A")), "|")
                For i = 0 To str9.Length - 1
                    For n = 0 To chkRulaAdd9.Items.Count - 1
                        If str9(i) = chkRulaAdd9.Items(n).Value Then
                            chkRulaAdd9.Items(n).Selected = True
                        End If
                    Next
                Next

                str10 = Split(String.Concat(.Item("S10A")), "|")
                For i = 0 To str10.Length - 1
                    For n = 0 To chkRulaAdd10.Items.Count - 1
                        If str10(i) = chkRulaAdd10.Items(n).Value Then
                            chkRulaAdd10.Items(n).Selected = True
                        End If
                    Next
                Next

                lblFinalScore.Text = .Item("FinalScore")
                lblFinalResult.Text = .Item("ResultText")

                'CalculateScore()

            End With
        End If


    End Sub


    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtAsmDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุวันที่ประเมินก่อน');", True)
            Exit Sub
        End If
        'If txtName.Text = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อบริษัท');", True)
        '    DisplayMessage(Me.Page, "กรุณาระบุชื่อบริษัท")
        'End If

        If optRULA1.SelectedValue = "" Or optRULA2.SelectedValue = "" Or optRULA3.SelectedValue = "" Or optRULA4.SelectedValue = "" Or optRULA6.SelectedValue = "" Or optRULA7.SelectedValue = "" Or optRULA9.SelectedValue = "" Or optRULA10.SelectedValue = "" Or optRULA11.SelectedValue = "" Or optRULA13.SelectedValue = "" Or optRULA14.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุคะแนนประเมินหลักให้ครบทุกข้อ');", True)
            Exit Sub
        End If


        CalculateScore()

        '--------------------------
        Dim S1A, S2A, S3A, S9A, S10A As String

        For i = 0 To chkRulaAdd1.Items.Count - 1
            If chkRulaAdd1.Items(i).Selected Then
                S1A = S1A & chkRulaAdd1.Items(i).Value & "|"
            End If
        Next

        For i = 0 To chkRulaAdd2.Items.Count - 1
            If chkRulaAdd2.Items(i).Selected Then
                S2A = S2A & chkRulaAdd2.Items(i).Value & "|"
            End If
        Next

        For i = 0 To chkRulaAdd3.Items.Count - 1
            If chkRulaAdd3.Items(i).Selected Then
                S3A = S3A & chkRulaAdd3.Items(i).Value & "|"
            End If
        Next

        For i = 0 To chkRulaAdd9.Items.Count - 1
            If chkRulaAdd9.Items(i).Selected Then
                S9A = S9A & chkRulaAdd9.Items(i).Value & "|"
            End If
        Next


        For i = 0 To chkRulaAdd10.Items.Count - 1
            If chkRulaAdd10.Items(i).Selected Then
                S10A = S10A & chkRulaAdd10.Items(i).Value & "|"
            End If
        Next

        '--------------------------
        ctlA.AsmRULA_Save(StrNull2Zero(hdUID.Value), txtTaskNo.Text, optAsmType.SelectedValue, ddlPerson.SelectedValue, ConvertStrDate2ShortDateTH(txtAsmDate.Text), txtRemark.Text, optRULA1.SelectedValue, optRULA2.SelectedValue, optRULA3.SelectedValue, optRULA4.SelectedValue, optRULA6.SelectedValue, optRULA7.SelectedValue, optRULA9.SelectedValue, optRULA10.SelectedValue, optRULA11.SelectedValue, optRULA13.SelectedValue, optRULA14.SelectedValue, S1A, S2A, S3A, S9A, S10A, StrNull2Zero(lblFinalScore.Text), lblFinalResult.Text, Request.Cookies("Ergo")("userid"))


        'Dim ctlU As New UserController
        'ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_UPD, "Customer", "บันทึก/แก้ไข ประวัติส่วนตัว :{uid=" & hdCompUID.Value & "}{code=" & txtCompanyID.Text & "}", "")

        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Response.Redirect("AsmResult.aspx")
    End Sub

    Private Sub CalculateScore()
        Dim sc1, sc2, sc3, sc4, sc6, sc7, sc9, sc10, sc11, sc13, sc14 As Integer

        sc1 = ctlA.AssessmentAnswer_GetScore("RULA", 1, optRULA1.SelectedValue)
        sc2 = ctlA.AssessmentAnswer_GetScore("RULA", 2, optRULA2.SelectedValue)
        sc3 = ctlA.AssessmentAnswer_GetScore("RULA", 3, optRULA3.SelectedValue)
        sc4 = StrNull2Zero(optRULA4.SelectedValue)
        sc6 = 1 '1 ทั้ง2 ข้อไม่ว่าจะเลือกข้อไหน ctlA.AssessmentAnswer_GetScore("RULA", 6, optRULA6.SelectedValue)
        sc7 = StrNull2Zero(optRULA7.SelectedValue) 'ctlA.AssessmentAnswer_GetScore("RULA", 7, optRULA7.SelectedValue)
        sc9 = ctlA.AssessmentAnswer_GetScore("RULA", 9, optRULA9.SelectedValue)
        sc10 = ctlA.AssessmentAnswer_GetScore("RULA", 10, optRULA10.SelectedValue)
        sc11 = StrNull2Zero(optRULA11.SelectedValue)
        sc13 = 1 'ctlA.AssessmentAnswer_GetScore("RULA", 13, optRULA13.SelectedValue)
        sc14 = StrNull2Zero(optRULA14.SelectedValue) 'ctlA.AssessmentAnswer_GetScore("RULA", 14, optRULA14.SelectedValue)

        'sc11 = optRULA11.SelectedValue



        'If optRULA13.SelectedValue Is Nothing Then
        '    sc13 = 0
        'Else
        '    sc13 = 1
        'End If

        'step1 ไม่เกิน 6
        For i = 0 To chkRulaAdd1.Items.Count - 1
            If chkRulaAdd1.Items(i).Selected Then
                sc1 = sc1 + ctlA.AssessmentAnswer_GetScore("RULA", 1, chkRulaAdd1.Items(i).Value)
            End If
        Next
        If sc1 > 6 Then 'step1 ไม่เกิน 6
            sc1 = 6
        End If



        For i = 0 To chkRulaAdd2.Items.Count - 1
            If chkRulaAdd2.Items(i).Selected Then
                sc2 = sc2 + ctlA.AssessmentAnswer_GetScore("RULA", 2, chkRulaAdd2.Items(i).Value)
            End If
        Next
        If sc2 > 3 Then 'step1 ไม่เกิน 3
            sc2 = 3
        End If


        For i = 0 To chkRulaAdd3.Items.Count - 1
            If chkRulaAdd3.Items(i).Selected Then
                sc3 = sc3 + ctlA.AssessmentAnswer_GetScore("RULA", 3, chkRulaAdd3.Items(i).Value)
            End If
        Next
        If sc3 > 4 Then 'step1 ไม่เกิน 4
            sc3 = 4
        End If

        For i = 0 To chkRulaAdd9.Items.Count - 1
            If chkRulaAdd9.Items(i).Selected Then
                sc9 = sc9 + ctlA.AssessmentAnswer_GetScore("RULA", 9, chkRulaAdd9.Items(i).Value)
            End If
        Next

        For i = 0 To chkRulaAdd10.Items.Count - 1
            If chkRulaAdd10.Items(i).Selected Then
                sc10 = sc10 + ctlA.AssessmentAnswer_GetScore("RULA", 10, chkRulaAdd10.Items(i).Value)
            End If
        Next

        Dim ScoreA, ScoreB, ScoreC, Score8, Score15, ScoreFinal As Integer

        'Step5
        ScoreA = ctlA.ScoreRULA_GetScore("A", sc1, sc2, sc3, sc4)

        'Step8
        Score8 = ScoreA + sc6 + sc7

        'Step12
        ScoreB = ctlA.ScoreRULA_GetScore("B", sc9, 0, sc10, sc11)

        'Step15
        Score15 = ScoreB + sc13 + sc14

        ''Step16
        ScoreC = ctlA.ScoreRULA_GetScore("C", Score8, 0, Score15, 0)
        ScoreFinal = ScoreC


        lblFinalScore.Text = ScoreFinal.ToString()

        'If ScoreFinal >= 1 And ScoreFinal <= 2 Then
        '    lblFinalResult.Text = "ยอมรับได้ แต่อาจจะมีปัญหาทางการยศาสตร์ได้ถ้ามีการทำงานดังกล่าวซ้ำๆต่อเนื่องเป็นเวลานานกว่าเดิม"
        'ElseIf ScoreFinal >= 3 And ScoreFinal <= 4 Then
        '    lblFinalResult.Text = "ควรมีการศึกษาเพิ่มเติมและติดตามวัดผลอย่างต่อเนื่องอาจจะจำเป็นที่จะต้องมีการออกแบบงานใหม่"
        'ElseIf ScoreFinal >= 5 And ScoreFinal <= 6 Then
        '    lblFinalResult.Text = "งานนั้นเริ่มเป็นปัญหา ควรทำการศึกษาเพิ่มเติม และควรรีบปรับปรุง"
        'ElseIf ScoreFinal >= 7 Then
        '    lblFinalResult.Text = "งานนั้นมีปัญหาทางการยศาสตร์ และต้องมีการปรับปรุงทันที"
        'Else
        '    lblFinalResult.Text = "Error ผิดพลาดกรุณาตรวจสอบ หรือ ติดต่อ admin"
        'End If

        If ScoreFinal >= 1 And ScoreFinal <= 2 Then
            lblFinalResult.Text = "ระดับ 1 ความเสี่ยงจากท่าทางที่ยอมรับได้"
        ElseIf ScoreFinal >= 3 And ScoreFinal <= 4 Then
            lblFinalResult.Text = "ระดับ 2 ความเสี่ยงที่ควรตรวจสอบและอาจต้องแก้ไข"
        ElseIf ScoreFinal >= 5 And ScoreFinal <= 6 Then
            lblFinalResult.Text = "ระดับ 3 ความเสี่ยงสูงที่ควรตรวจสอบและแก้ไขโดยเร็ว"
        ElseIf ScoreFinal >= 7 Then
            lblFinalResult.Text = "ระดับ 4 ความเสี่ยงสูงมาก งานนั้นควรตรวจสอบและแก้ไขโดยทันที"
        Else
            lblFinalResult.Text = "Error ผิดพลาดกรุณาตรวจสอบ หรือ ติดต่อ admin"
        End If
    End Sub

    Protected Sub optAsmType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAsmType.SelectedIndexChanged
        If optAsmType.SelectedValue = "O" Then
            ddlPerson.Enabled = True
        Else
            ddlPerson.SelectedValue = Request.Cookies("Ergo")("LoginPersonUID")
            ddlPerson.Enabled = False
        End If
    End Sub

    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Response.Redirect("Evaluated.aspx")
    End Sub

    Protected Sub optRULA1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA1.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRulaAdd1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRulaAdd1.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optRULA2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA2.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRulaAdd2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRulaAdd2.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optRULA3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA3.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRulaAdd3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRulaAdd3.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optRULA4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA4.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optRULA6_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA6.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optRULA7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA7.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optRULA9_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA9.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRulaAdd9_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRulaAdd9.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optRULA10_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA10.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub chkRulaAdd10_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkRulaAdd10.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optRULA11_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA11.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optRULA13_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA13.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optRULA14_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRULA14.SelectedIndexChanged
        CalculateScore()
    End Sub
    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlA.AsmRULA_Delete(StrNull2Zero(hdUID.Value))
    End Sub
End Class