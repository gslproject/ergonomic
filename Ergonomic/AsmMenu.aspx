﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AsmMenu.aspx.vb" Inherits="Ergonomic.AsmMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
     <section class="content-header">
      <h1>ทำแบบประเมิน
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">   
    
     <div class="row">
     <% If Me.isROSA = True %>
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>ROSA</h3>

              <p>Rapid Office Strain Assessment</p>
            </div>
            <div class="icon">
              <i class="ion ion-man"></i>
            </div>
            <a href="AsmROSA.aspx?ActionType=asm&ItemType=ega&t=new" class="small-box-footer">ทำแบบประเมิน<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
     <% End If %> 
     <% If Me.isREBA = True %>    
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-green">
            <div class="inner">
              <h3>REBA</h3>

              <p>Rapid Entire Body Assessment</p>
            </div>
            <div class="icon">
              <i class="ion ion-man"></i>
            </div>
            <a href="AsmREBA.aspx?ActionType=asm&ItemType=ega&t=new" class="small-box-footer">ทำแบบประเมิน<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
     <% End If %> 
     <% If Me.isRULA = True Then %>    
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-purple">
            <div class="inner">
              <h3>RULA</h3>

              <p>Rapid Upper Limp Assessment</p>
            </div>
            <div class="icon">
              <i class="ion ion-man"></i>
            </div>
            <a href="AsmRULA.aspx?ActionType=asm&ItemType=ega&t=new" class="small-box-footer">ทำแบบประเมิน<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
     <% End If %> 
     <% If Me.isNIOSH = True Then  %>          
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-red">
            <div class="inner">
              <h3>NIOSH Lifting</h3>

              <p>The National Institute for Occupational Safety and Health</p>
            </div>
            <div class="icon">
              <i class="ion ion-man"></i>
            </div>
            <a href="AsmNIOSH.aspx?ActionType=asm&ItemType=ega&t=new" class="small-box-footer">ทำแบบประเมิน<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    <% End If %>  
         </div>
     

    </section>
</asp:Content>
