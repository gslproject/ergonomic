﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AsmRULA.aspx.vb" Inherits="Ergonomic.AsmRULA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>การประเมินความเสี่ยงโดยวิธี RULA</h1>     
    </section>
    <section class="content">  

  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">ErgonomicAssessment<asp:HiddenField ID="hdUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

      <div class="row">
           <div class="col-md-2">
          <div class="form-group">
            <label>Task No</label>
              <asp:TextBox ID="txtTaskNo" runat="server" cssclass="form-control" placeholder="Task no."></asp:TextBox>
          </div>

        </div>

        <div class="col-md-10">
          <div class="form-group">
            <label>Task Name</label>
              <asp:TextBox ID="txtTaskName" runat="server" cssclass="form-control" placeholder="Task no."></asp:TextBox>
          </div>

        </div>
      
      </div>
      <div class="row">
  <div class="col-md-3">
          <div class="form-group">
            <label>Date</label>
              <asp:TextBox ID="txtAsmDate" runat="server" placeholder="Date of assessment" CssClass="form-control datepicker-dropdown"></asp:TextBox>
          </div>

        </div> 

        <div class="col-md-9">
          <div class="form-group">
            <label>Type</label>
              <asp:RadioButtonList ID="optAsmType" runat="server" RepeatDirection="Horizontal">
                  <asp:ListItem Value="S">Self-assessment</asp:ListItem>
                  <asp:ListItem Value="O">Observer assessment</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 

      </div>
   
        <div class="row">        
       <div class="col-md-12">
          <div class="form-group">
            <label>ผู้รับการประเมิน</label>
              <asp:DropDownList ID="ddlPerson" runat="server" cssclass="form-control select2"  placeholder="Select Person" Width="100%">
            </asp:DropDownList> 
          </div>

        </div> 
</div>
          <div class="row">        
       <div class="col-md-12">
          <div class="form-group">
            <label>Remark</label>
              <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" Height="50px" TextMode="MultiLine"></asp:TextBox>
          </div>

        </div> 
</div>
</div>
      </div>

         <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">กลุ่ม A       </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

 <h5 class="text-blue text-bold">การประเมินแขนส่วนบน (Upper arm)</h5>
        <div class="row">
             
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRULA1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="แขนอยู่ในตำแหน่งไปข้างหน้า-หลังไม่เกิน 20 องศา" class="vtip"  height="140"  src="images/RULA/RULA1_01.jpg" /><br /><br />แขนอยู่ในตำแหน่งไปข้างหน้า-หลังไม่เกิน 20 ํ</asp:ListItem>
                    <asp:ListItem Value="2"><img title="แขนอยู่ด้านหลัง เกิน 20 องศา" class="vtip"  height="140"  src="images/RULA/RULA1_02.jpg" /><br /><br />แขนอยู่ด้านหลัง เกิน 20 ํ</asp:ListItem>
                    <asp:ListItem Value="3"><img title="แขนอยู่ด้านหน้า 20-45 องศา" class="vtip"  height="140"  src="images/RULA/RULA1_03.jpg" /><br /><br />แขนอยู่ด้านหน้า 20-45 ํ</asp:ListItem>
                   <asp:ListItem Value="4"><img title="แขนอยู่ด้านหน้า 45-90 องศา" class="vtip"  height="140"  src="images/RULA/RULA1_04.jpg" /><br /><br />แขนอยู่ด้านหน้า 45-90 ํ</asp:ListItem>
                    <asp:ListItem Value="5"><img title="แขนอยู่ในตำแหน่งเหนือไหล่ (มีมุมเกิน 90 องศา เมื่อเทียบกับลำตัว)" class="vtip"  height="140"  src="images/RULA/RULA1_05.jpg" /><br /><br />แขนอยู่ในตำแหน่งเหนือไหล่ (มีมุมเกิน 90 ํ เมื่อเทียบกับลำตัว)</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 

                  <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
              <asp:CheckBoxList ID="chkRulaAdd1" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="A1">มีการยกหัวไหล่</asp:ListItem>
                    <asp:ListItem Value="A2">หัวไหล่กางออก </asp:ListItem>
                  <asp:ListItem Value="A3">ถ้ามีที่วางแขน หรือสามารถพาดแขนได้ </asp:ListItem>
              </asp:CheckBoxList>
             
          </div>

        </div> 

            </div>
        <h5 class="text-blue text-bold">การประเมินแขนส่วนล่าง (Lower arm หรือ forearm)</h5>
        <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRULA2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="แขนส่วนล่างอยู่ในระดับที่มีมุมระหว่าง 60-100 องศา เมื่อเทียบกับแนวดิ่ง" class="vtip"  height="140"  src="images/RULA/RULA2_01.jpg" /><br /><br />แขนส่วนล่างอยู่ในระดับที่มีมุมระหว่าง 60-100 ํ เมื่อเทียบกับแนวดิ่ง</asp:ListItem>
                    <asp:ListItem Value="2"><img title="แขนส่วนล่างตกลงมาด้านล่างโดยมีมุมน้อยกว่า 60 องศา" class="vtip"  height="140"  src="images/RULA/RULA2_02.jpg" /><br /><br />แขนส่วนล่างตกลงมาด้านล่างโดยมีมุมน้อยกว่า 60 ํ</asp:ListItem>
                    <asp:ListItem Value="3"><img title="แขนอยู่ในตำแหน่งยกขึ้นด้านบนทำมุมมากกว่า 100 องศา เมื่อเทียบกับแนวดิ่ง" class="vtip"  height="140"  src="images/RULA/RULA2_03.jpg" /><br /><br />แขนอยู่ในตำแหน่งยกขึ้นด้านบนทำมุมมากกว่า 100 ํ เมื่อเทียบกับแนวดิ่ง</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
                 <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRulaAdd2" runat="server" RepeatDirection="Vertical" AutoPostBack="True">
                      <asp:ListItem Value="A1">แขนไขว้เลยแกนกลางของลำตัว หรือแขนกางออกไปด้านข้างของลำตัว </asp:ListItem>
                 
              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
        <h5 class="text-blue text-bold">การประเมินข้อมือ (Wrist)</h5>
         <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRULA3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="ตำแหน่งของข้อมือ (แนวกระดูกฝ่ามือ) อยู่ในแนวเดียวกับแขนส่วนล่าง" class="vtip"  height="140"  src="images/RULA/RULA3_01.jpg" /><br /><br />ตำแหน่งของข้อมือ<br />(แนวกระดูกฝ่ามือ) อยู่ในแนวเดียวกับแขนส่วนล่าง</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ตำแหน่งของข้อมือ (แนวกระดูกฝ่ามือ) ทำมุมขึ้นหรือลงไม่เกิน 15 องศา เมื่อเทียบกับแนวแขนส่วนล่าง" class="vtip"  height="140"  src="images/RULA/RULA3_03.jpg" /><br /><br />ตำแหน่งของข้อมือ (แนวกระดูกฝ่ามือ)<br />ทำมุมขึ้นหรือลงไม่เกิน 15 ํ<br /> เมื่อเทียบกับแนวแขนส่วนล่าง</asp:ListItem>
                    <asp:ListItem Value="3"><img title="ตำแหน่งของข้อมือ (แนวกระดูกฝ่ามือ) ทำมุมขึ้นหรือลงมากกว่า 15 องศา เมื่อเทียบกับแนวแขนส่วนล่าง" class="vtip"  height="140"  src="images/RULA/RULA3_02.jpg" /><br /><br />ตำแหน่งของข้อมือ (แนวกระดูกฝ่ามือ)<br />ทำมุมขึ้นหรือลงมากกว่า 15 ํ <br />เมื่อเทียบกับแนวแขนส่วนล่าง</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
    <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRulaAdd3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                   
                    <asp:ListItem Value="A1">มีการเอียงข้อมือเบี่ยงไปด้านข้าง (ซ้าย–ขวา)</asp:ListItem> 

              </asp:CheckBoxList>
          </div>

        </div> 
            </div>

         <h5 class="text-blue text-bold">การประเมินการหมุนของข้อมือ (Wrist twist)</h5>
         <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRULA4" runat="server" RepeatDirection="Vertical" AutoPostBack="True">
                  <asp:ListItem Value="1">ไม่มีการบิดหรือหมุนข้อมือ หรือหมุนบิดข้อมือเล็กน้อยไม่เกินครึ่ง</asp:ListItem>
                    <asp:ListItem Value="2">มีการหมุนบิดของข้อมือตั้งแต่ครึ่งถึงเกือบสุด </asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
            </div>
           
        
    </div>
    <div class="box-footer clearfix">           
                <!--ที่มา : (Sonne, Villalta, & Andrews, 2012) , http://thai-ergonomic-assessment.blogspot.com-->
            </div>
  </div>

  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">กลุ่ม B       </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

          <h5 class="text-blue text-bold">การประเมินการใช้กล้ามเนื้อแขนหรือมือในการทำงาน</h5>
         <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRULA6" runat="server" RepeatDirection="Vertical" AutoPostBack="True">
                  <asp:ListItem Value="1">แขนหรือมือใช้แรงอยู่นิ่งนานเกิน 1 นาที</asp:ListItem>
                    <asp:ListItem Value="2">แขนหรือมือมีการเคลื่อนไหวซ้ำไปมาตั้งแต่ 4 ครั้งต่อนาทีขึ้นไป</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 
            </div>

          <h5 class="text-blue text-bold">การประเมินแรงหรือภาระงานในส่วนแขนหรือมือ</h5>
         <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRULA7" runat="server" RepeatDirection="Vertical" AutoPostBack="True">
                  <asp:ListItem Value="0">แรงที่ใช้หรือน้ำหนักที่ถือน้อยกว่า 2 กก. (ทำงานไม่ต่อเนื่อง)</asp:ListItem>
                    <asp:ListItem Value="1">แรงที่ใช้หรือน้ำหนักที่ถืออยู่ระหว่าง 2–10 กก. (ทำงานไม่ต่อเนื่อง)</asp:ListItem>
                    <asp:ListItem Value="2">แรงที่ใช้หรือน้ำหนักที่ถืออยู่ระหว่าง 2–10 กก. โดยมีการใช้แรงหรือจับถือน้ำหนักอยู่ตลอดเวลา หรือมีการออกแรงซ้ำไปมาบ่อยๆ</asp:ListItem>
                    <asp:ListItem Value="3">แรงที่ใช้หรือน้ำหนักที่ถือ มากกว่า 10 กก. ไม่ว่าจะเป็นการใช้แรงแบบสถิตหรือเคลื่อนที่ซ้ำไปมาบ่อยๆ หรือมีการใช้แรงแบบกระแทกเป็นครั้งคราว</asp:ListItem> 
              </asp:RadioButtonList>
          </div>

        </div> 
            </div>




          <h5 class="text-blue text-bold">การประเมินส่วนคอ</h5>
         <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRULA9" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="ศีรษะตรงหรือก้มไปข้างหน้าเล็กน้อย (แนวของศีรษะทำมุมกับแนวดิ่งหรือแนวแกนของคอไม่เกิน 10 องศา )" class="vtip"  height="140"  src="images/RULA/RULA9_01.jpg" /><br /><br />ศีรษะตรงหรือก้มไปข้างหน้าเล็กน้อย<br />(แนวของศีรษะทำมุมกับแนวดิ่งหรือแนวแกนของคอไม่เกิน 10 ํ)</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ศีรษะก้มไปข้างหน้าทำมุมกับแนวดิ่งอยู่ระหว่าง 10-20 องศา" class="vtip"  height="140"  src="images/RULA/RULA9_02.jpg" /><br /><br />ศีรษะก้มไปข้างหน้าทำมุม<br />กับแนวดิ่งอยู่ระหว่าง 10-20 ํ</asp:ListItem> 
                     <asp:ListItem Value="3"><img title="ศีรษะก้มไปข้างหน้า ทำมุมกับแนวดิ่ง มากกว่า 20 องศา" class="vtip"  height="140"  src="images/RULA/RULA9_03.jpg" /><br /><br />ศีรษะก้มไปข้างหน้าทำมุมกับ<br />แนวดิ่ง > 20 ํ</asp:ListItem> 
                     <asp:ListItem Value="4"><img title="ศีรษะเงยไปด้านหลัง" class="vtip"  height="140"  src="images/RULA/RULA9_04.jpg" /><br /><br />ศีรษะเงยไปด้านหลัง</asp:ListItem> 
              </asp:RadioButtonList>
          </div>

        </div> 
                  <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRulaAdd9" runat="server" AutoPostBack="True">                 
                    <asp:ListItem Value="A2">มีการหมุนศีรษะด้วย</asp:ListItem>
                    <asp:ListItem Value="A1">มีการเอียงศีรษะไปด้านข้าง </asp:ListItem>
              </asp:CheckBoxList>
          </div>

        </div> 
            </div>
           <h5 class="text-blue text-bold">การประเมินส่วนลำตัว</h5>
           <div class="row">
             <div class="col-md-8">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRULA10" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1"><img title="ลำตัวตั้งตรง" class="vtip"  height="140"  src="images/RULA/RULA10_01.jpg" /><br /><br />ลำตัวตั้งตรง</asp:ListItem>
                    <asp:ListItem Value="2"><img title="ลำตัวเอนไปด้านหน้า 0-20 องศา" class="vtip"  height="140"  src="images/RULA/RULA10_02.jpg" /><br /><br />ลำตัวเอนไปด้านหน้า 0-20 ํ</asp:ListItem>
                   <asp:ListItem Value="3"><img title="ลำตัวเอนไปด้านหน้า 20-60 องศา" class="vtip"  height="140"  src="images/RULA/RULA10_03.jpg" /><br /><br />ลำตัวเอนไปด้านหน้า 20-60 ํ</asp:ListItem>
                   <asp:ListItem Value="4"><img title="ลำตัวเอนไปด้านหน้า มากกว่า 60 องศา" class="vtip"  height="140"  src="images/RULA/RULA10_04.jpg" /><br /><br />ลำตัวเอนไปด้านหน้า มากกว่า 60 </asp:ListItem>
              </asp:RadioButtonList>  
          </div>

        </div>  
                <div class="col-md-4">
          <div class="form-group">
            <label class="text-blue">คะแนนปรับเพิ่ม</label>
               <asp:CheckBoxList ID="chkRulaAdd10" runat="server" AutoPostBack="True">                 
                    <asp:ListItem Value="A1">มีการหมุนตัวด้วย</asp:ListItem>
                    <asp:ListItem Value="A2">มีการเอนตัวไปด้านข้าง</asp:ListItem>
              </asp:CheckBoxList>
          </div>

        </div> 
            </div> 

          <h5 class="text-blue text-bold">การประเมินส่วนขา</h5>
           <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">คะแนนหลัก</label>
               <asp:RadioButtonList ID="optRULA11" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="1">ขาและเท้าทั้ง 2 ข้างอยู่ในท่าทางสมดุลและมีที่รองรับอย่างเหมาะสม

</asp:ListItem>
                    <asp:ListItem Value="2">ขาและเท้าทั้ง 2 ข้างอยู่ในท่าทางไม่เหมาะสมหรือไม่มีที่รองรับเท้า </asp:ListItem>
              </asp:RadioButtonList>  
          </div>

        </div>  

            </div> 
         
</div>
      </div>
 <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">การประเมินการเคลื่อนไหวและกิจกรรมของงาน</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
            
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">การประเมินกล้ามเนื้อขาหรือเท้าในการทำงาน</label>
               <asp:RadioButtonList ID="optRULA13" runat="server" RepeatDirection="Vertical" AutoPostBack="True">
                  <asp:ListItem Value="1">ขาหรือเท้าอยู่ในท่านิ่งนานเกิน 1 นาที</asp:ListItem>
                    <asp:ListItem Value="2">ขาหรือเท้ามีการเคลื่อนไหวหรือใช้แรงแบบซ้ำๆไปมา ตั้งแต่ 4 ครั้งต่อนาทีขึ้นไป</asp:ListItem> 
              </asp:RadioButtonList>
          </div>

        </div>  
            </div>
        <div class="row">
             <div class="col-md-12">
          <div class="form-group">
            <label class="text-blue">การประเมินแรงหรือภาระงานในส่วนของขาหรือเท้า</label>
               <asp:RadioButtonList ID="optRULA14" runat="server" RepeatDirection="Vertical" AutoPostBack="True">
                  <asp:ListItem Value="0">ภาระงานที่ใช้มีค่าน้อยกว่า 2 กก. อย่างไม่ต่อเนื่อง</asp:ListItem>
 <asp:ListItem Value="1">ภาระงานที่ใช้มีค่าระหว่าง 2-10 กก.อย่างไม่ต่อเนื่อง</asp:ListItem>
 <asp:ListItem Value="2">ภาระงานที่ใช้มีค่าระหว่าง 2-10 กก.โดยออกแรงแบบสถิต หรือเกิดขึ้นซ้ำๆตั้งแต่ 4 ครั้งต่อนาทีขึ้นไป</asp:ListItem>
 <asp:ListItem Value="3">ภาระงานที่ใช้มีค่ามากกกว่า 10 กก. โดยออกแรงแบบสถิต หรือ เกิดขึ้นซ้ำๆ หรือมีการออกแรงแบบกระแทก หรือกระชาก</asp:ListItem>

                    
              </asp:RadioButtonList>
          </div>

        </div>  
            </div>
 </div>
      </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>               
         <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Summary Score         </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

<div class="row">
         <div class="col-md-2">
          <div class="form-group">
            <label class="text-blue">ค่าคะแนนการประเมิน</label>
            <h3> <asp:Label ID="lblFinalScore" runat="server" Text="" CssClass="text-blue text-bold" ></asp:Label></h3> 
          </div>

        </div> 
     <div class="col-md-10">
          <div class="form-group">
            <label class="text-blue">สรุปแปลผล</label>
            <h3>  <asp:Label ID="lblFinalResult" runat="server" Text=""  CssClass="text-blue text-bold" ></asp:Label></h3>
          </div>

        </div> 

  </div>     

    </div>
             </div>
</ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="optRULA1" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRULA2" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRULA3" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRULA4" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRULA6" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRULA7" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRULA9" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRULA10" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRULA11" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRULA13" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="optRULA14" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRulaAdd1" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRulaAdd2" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRulaAdd3" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRulaAdd9" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="chkRulaAdd10" EventName="SelectedIndexChanged" />
            </Triggers>

        </asp:UpdatePanel>   
        


  <div class="row"> 

        <div class="col-md-12 text-center">

          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="บันทึก" Width="120px" /> 
             &nbsp;<asp:Button ID="cmdCancel" CssClass="btn btn-default" runat="server" Text="ยกเลิก" Width="120px" /> 
              <asp:Button ID="cmdDelete" CssClass="btn btn-danger" runat="server" Text="ลบ" Width="120px" /> 
        </div>
      </div>
</section>
</asp:Content>