﻿<%@ Page Title="ข้อมูลหลักสูตร" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CourseModify.aspx.vb" Inherits="Ergonomic.CourseModify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

      <section class="content-header">
      <h1><%: Title %>        
        <small>ข้อมูลหลักสูตร</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Course</li>
      </ol>
    </section>

    <section class="content">   


  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">จัดการข้อมูลหลักสูตร<asp:HiddenField ID="hdCompUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-1">
          <div class="form-group">
            <label>Course ID</label>
            <asp:TextBox ID="txtCourseID" runat="server" cssclass="form-control" ReadOnly="true" placeholder="รหัสหลักสูตร">
            </asp:TextBox>
          </div>

        </div>
      
        <div class="col-md-11">
          <div class="form-group">
            <label>ชื่อหลักสูตร</label>
            <asp:TextBox ID="txtName" runat="server" cssclass="form-control" placeholder="ชื่อหลักสูตร"></asp:TextBox>
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>ประเภท</label>
            <asp:DropDownList ID="ddlMediaType" runat="server" cssclass="form-control select2" Width="100%">
                <asp:ListItem Value="VDO">Video</asp:ListItem>
                <asp:ListItem Value="PPT">Power Point Presentration</asp:ListItem>
                <asp:ListItem Value="PDF">PDF File</asp:ListItem>
              </asp:DropDownList>
          </div>

        </div>
        <div class="col-md-1">
          <div class="form-group">
            <label>ความยาว/จำนวนหน้า</label>
            <asp:TextBox ID="txtMediaCount" runat="server" cssclass="form-control" placeholder=""></asp:TextBox>
          </div>

        </div> 
        <div class="col-md-1">
          <div class="form-group">
            <label>Display Order</label>
            <asp:TextBox ID="txtSort" runat="server" cssclass="form-control" placeholder=""></asp:TextBox>
          </div>
        </div>
      </div>
          
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Media Link</label>
            <asp:TextBox ID="txtLink" runat="server" cssclass="form-control" placeholder="URL ชื่อไฟล์บทเรียน"></asp:TextBox>
          </div>
        </div>

      </div>

          <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Upload รูปหน้าปก (ขนาด กว้าง 500 x ยาว 300 pixel)</label>
              <asp:FileUpload ID="FileUploadImg" runat="server" Width="300px" />               
          </div>
        </div>
 <div class="col-md-3">
          <div class="form-group">
            <label>รูปปัจจุบัน</label>
              <br />
              <asp:HyperLink ID="hlnkImage" CssClass="label label-success" runat="server" Target="_blank">[hlnkImage]</asp:HyperLink>
 </div>
        </div>
</div>          
      
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Status</label>
              <asp:CheckBox ID="chkStatus" runat="server" Text="   Active" />
          </div>
        </div>

        <div class="col-md-12 text-center">

          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
            <asp:Button ID="cmdDelete" CssClass="btn btn-danger" runat="server" Text="ลบ" Width="120px" /> 
        </div>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
        </section>
</asp:Content>