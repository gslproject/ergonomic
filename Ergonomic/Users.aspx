﻿<%@ Page Title="User Account" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Users.aspx.vb" Inherits="Ergonomic.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>User Account</h1>     
    </section>
<section class="content"> 
    
        <div class="row no-print">
        <div class="col-xs-12">
          <a href="UserModify?ActionType=user&ItemType=u" target="_blank" class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> เพิ่มใหม่</a>
        </div>
      </div> 
    <br /> 
  
      <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">ผู้ใช้งานทั้งหมด</h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body">       
                   <table border="0" cellspacing="1" cellpadding="0">
            <tr>
                 <td width="40" >บริษัท</td>
                 <td   > <asp:DropDownList ID="ddlCompany"  runat="server"  AutoPostBack="True"  CssClass="form-control select2">                  
                                                  </asp:DropDownList></td>

              <td width="90" >User Group</td>
               <td> <asp:DropDownList ID="ddlGroupFind"  runat="server"  AutoPostBack="True"  CssClass="form-control select2">
                     <asp:ListItem Value="0">---ทั้งหมด---</asp:ListItem> 
            <asp:ListItem Value="1">User</asp:ListItem>             
            <asp:ListItem Value="2">Customer Admin</asp:ListItem>
            <asp:ListItem Value="3">NPC-SE Admin</asp:ListItem>
             <asp:ListItem Value="9">Super Administrator</asp:ListItem>
                                                  </asp:DropDownList> </td>
             <td width="50" class="text-center" >ค้นหา</td>  <td>
                  <asp:TextBox ID="txtSearch" runat="server" Width="250px" CssClass="form-control" placeholder="สามารถค้นหาได้จาก username,ชื่อ" ></asp:TextBox>
                </td>
              <td width="120" class="text-center">
                  <asp:Button ID="cmdFind" runat="server" CssClass="btn btn-primary" Text="Search" Width="100px" />                </td>
            </tr>
            
          </table>

                <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-bordered table-striped" 
                             Font-Bold="False" PageSize="15">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                            </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="No." DataField="nRow">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                            </asp:BoundField>
                        <asp:BoundField DataField="Username" HeaderText="Username" />
                            <asp:BoundField HeaderText="ชื่อ-สกุล" DataField="Name" />
                        <asp:BoundField DataField="CompanyName" HeaderText="หน่วยงาน" >
                            <HeaderStyle HorizontalAlign="Left" />                            </asp:BoundField>
                            <asp:BoundField DataField="LastLogin" HeaderText="Last Login" />
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" 
                                    Visible='<%# IIf(DataBinder.Eval(Container.DataItem, "StatusFlag") = "A", True, False) %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
 
     </section>
</asp:Content>
