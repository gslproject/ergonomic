﻿Public Class JobOwner
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlP As New PersonController
    Dim ctlE As New TaskController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            If Not Request("tid") Is Nothing Then
                LoadTaskData(Request("tid"))
            End If
            LoadOwner()

        End If
    End Sub

    Private Sub LoadTaskData(TaskUID As Integer)
        dt = ctlE.Task_GetByUID(TaskUID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdUID.Value = String.Concat(.Item("UID"))
                lblCode.Text = String.Concat(.Item("TaskNo"))
                lblTaskName.Text = String.Concat(.Item("TaskName"))
                LoadJobOwner(hdUID.Value)
            End With
        Else
        End If
    End Sub

    Private Sub LoadJobOwner(TaskUID As Integer)
        dt = ctlE.JobOwner_Get(Request.Cookies("Ergo")("LoginCompanyUID"), TaskUID)
        grdData.DataSource = dt
        grdData.DataBind()
    End Sub
    Private Sub LoadOwner()
        ddlPerson.Items.Clear()
        dt = ctlP.Person_GetByCompany(Request.Cookies("Ergo")("LoginCompanyUID"))
        With ddlPerson
            .Enabled = True
            .DataSource = dt
            .DataTextField = "PersonName"
            .DataValueField = "PersonUID"
            .DataBind()
            .Visible = True
        End With
        dt = Nothing
    End Sub

    Protected Sub cmdAdd_Click(sender As Object, e As EventArgs) Handles cmdAdd.Click

        If ddlPerson.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือก Owner');", True)
            Exit Sub
        End If
        If ddlLevel.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือก Level');", True)
            Exit Sub
        End If

        ctlE.JobOwner_Save(StrNull2Zero(hdOwnerUID.Value), StrNull2Zero(hdUID.Value), StrNull2Zero(ddlPerson.SelectedValue), Request.Cookies("Ergo")("LoginCompanyUID"), StrNull2Zero(ddlLevel.SelectedValue))

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        LoadJobOwner(hdUID.Value)
    End Sub

    Protected Sub grdCompany_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlE.JobOwner_Delete(e.CommandArgument) Then
                        'Dim acc As New UserController
                        'acc.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_DEL, "Division", "ลบชื่อ Owner Assign :{personid=" & hdPersonUID.Value & "}{cid=" & e.CommandArgument() & "}", "")
                        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบข้อมูลเรียบร้อย');", True)
                        LoadJobOwner(hdUID.Value)
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If
            End Select
        End If
    End Sub
    Private Sub EditData(ByVal pID As String)
        dt = ctlE.JobOwner_GetByUID(pID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                ddlPerson.SelectedValue = String.Concat(dt.Rows(0)("PersonUID"))
                ddlLevel.SelectedValue = String.Concat(dt.Rows(0)("LevelID"))
            End With
        End If
        dt = Nothing
    End Sub


    Private Sub grdCompany_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(1).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d9edf7';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
End Class