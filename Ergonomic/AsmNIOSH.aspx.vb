﻿Imports System.Drawing
Public Class AsmNIOSH
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlA As New AssessmentController
    Dim ctlP As New PersonController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadPerson()
            LoadTaskData()
            optAsmType.SelectedValue = Session("asmtype")
            txtAsmDate.Text = Today.Date.ToShortDateString()

            If Session("asmtype") = "O" Then
                ddlPerson.Enabled = True
            Else
                ddlPerson.SelectedValue = Request.Cookies("Ergo")("LoginPersonUID")
                ddlPerson.Enabled = False
            End If

            If Not Request("id") Is Nothing Then
                LoadAssessmentData()
            End If

        End If

        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")
    End Sub
    Private Sub LoadPerson()
        Dim dtE As New DataTable
        dtE = ctlP.Person_GetByCompany(Request.Cookies("Ergo")("LoginCompanyUID"))
        ddlPerson.DataSource = dtE
        ddlPerson.DataTextField = "PersonName"
        ddlPerson.DataValueField = "PersonUID"
        ddlPerson.DataBind()
    End Sub
    Private Sub LoadTaskData()
        Dim ctlT As New TaskController
        dt = ctlT.Task_GetByUID(Session("asmtask"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                txtTaskNo.Text = String.Concat(.Item("TaskNo"))
                txtTaskName.Text = String.Concat(.Item("TaskName"))
            End With
        Else
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtAsmDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุวันที่ประเมินก่อน');", True)
            Exit Sub
        End If
        'If txtName.Text = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อบริษัท');", True)
        '    DisplayMessage(Me.Page, "กรุณาระบุชื่อบริษัท")
        'End If

        If StrNull2Zero(txtL.Text) <= 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุ Load weight ก่อน');", True)
            Exit Sub
        End If

        If StrNull2Zero(txtH.Text) < 25 Or StrNull2Zero(txtH.Text) > 64 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุ H ระหว่าง 25-64');", True)
            Exit Sub
        End If

        If StrNull2Zero(txtV.Text) < 0 Or StrNull2Zero(txtH.Text) > 178 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุ V ระหว่าง 0-178');", True)
            Exit Sub
        End If

        If StrNull2Zero(txtD.Text) < 25 Or StrNull2Zero(txtH.Text) > 178 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุ D ระหว่าง 25-178');", True)
            Exit Sub
        End If



        Dim L, H, V, D, A, C, Dur, F As Double
        Dim LC, HM, VM, DM, AM, CM, FM, RWL, LI, FI, FILI As Double

        'LC ญ=23 ช=51
        LC = 23
        H = StrNull2Double(txtH.Text)
        V = StrNull2Double(txtV.Text)
        D = StrNull2Double(txtD.Text)
        A = StrNull2Double(ddlA.Text)
        F = StrNull2Double(ddlF.Text)
        C = StrNull2Double(optC.SelectedValue)
        Dur = StrNull2Double(optDur.SelectedValue)
        L = StrNull2Double(txtL.Text)

        If H < 25 Then H = 25
        If H > 63 Then H = 63

        HM = 25 / H
        VM = 1 - (0.003 * Math.Abs(V - 75))

        If V > 175 Then VM = 0

        DM = 0.82 + (4.5 / D)

        If D < 25 Then DM = 1
        If D > 175 Then DM = 0

        AM = 1 - (0.0032 * A)
        AM = StrNull2Double(ddlA.SelectedValue)
        If A > 135 Then AM = 0

        If F > 15 Then FM = 0
        'FM เปิดตาราง
        FM = ctlA.NIOSH_GetFMScore(F, Dur, V)

        Select Case C
            Case 1
                If V < 75 Then
                    CM = 1
                Else
                    CM = 1
                End If
            Case 2
                If V < 75 Then
                    CM = 0.95
                Else
                    CM = 1
                End If

            Case 3
                If V < 75 Then
                    CM = 0.9
                Else
                    CM = 0.9
                End If
        End Select



        RWL = LC * HM * VM * DM * AM * FM * CM
        LI = L / RWL

        'lblFinalScore.Text = ScoreFinal.ToString()

        lblHM.Text = Math.Round(HM, 2).ToString("0.#0")
        lblVM.Text = Math.Round(VM, 2).ToString("0.#0")
        lblDM.Text = Math.Round(DM, 2).ToString("0.#0")
        lblAM.Text = Math.Round(AM, 2).ToString("0.#0")
        lblCM.Text = Math.Round(CM, 2).ToString("0.#0")
        lblFM.Text = Math.Round(FM, 2).ToString("0.#0")
        txtRWL.Text = Math.Round(RWL, 2).ToString("0.#0")
        txtLI.Text = Math.Round(LI, 2).ToString("0.#0")

        txtRecommend.TextMode = TextBoxMode.SingleLine
        txtRecommend.Height = 45
        'If LI <= 1 Then
        '    txtRecommend.Text = "ความเสี่ยงน้อยมาก"
        '    txtRecommend.ForeColor = ColorTranslator.FromHtml("#e0a246")
        '    txtRecommend.BackColor = ColorTranslator.FromHtml("#cff3dd")
        'ElseIf LI >= 1.01 And LI <= 1.5 Then
        '    txtRecommend.Text = "ความเสี่ยงน้อย"
        '    txtRecommend.ForeColor = Color.Green
        '    txtRecommend.BackColor = ColorTranslator.FromHtml("#cff3dd")
        'ElseIf LI >= 1.51 And LI <= 2 Then
        '    txtRecommend.Text = "ความเสี่ยงปานกลาง"
        '    txtRecommend.ForeColor = ColorTranslator.FromHtml("#fcd101")
        '    txtRecommend.BackColor = ColorTranslator.FromHtml("#fbf4b6")
        'ElseIf LI >= 2.01 And LI <= 3 Then
        '    txtRecommend.Text = "ความเสี่ยงสูง"
        '    txtRecommend.ForeColor = ColorTranslator.FromHtml("#e42c6e")
        '    txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
        'Else
        '    txtRecommend.ForeColor = Color.Red
        '    txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
        '    If RWL = 0 Then
        '        txtRecommend.Text = "A result of Unacceptable indicates there is a high level of risk for this job task and it is recommended that control measures be implemented immediately."
        '        txtRecommend.TextMode = TextBoxMode.MultiLine
        '        txtRecommend.Height = 60
        '    Else
        '        txtRecommend.Text = "ความเสี่ยงสูงมาก"
        '        txtRecommend.ForeColor = Color.Red
        '        txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
        '    End If

        'End If

        If LI <= 1 Then
            txtRecommend.Text = "ความเสี่ยงน้อยมาก"
            txtRecommend.ForeColor = ColorTranslator.FromHtml("#e0a246")
            txtRecommend.BackColor = ColorTranslator.FromHtml("#cff3dd")
        ElseIf LI >= 1.01 And LI <= 1.5 Then
            txtRecommend.Text = "ภาวะที่ยอมรับได้"
            txtRecommend.ForeColor = Color.Green
            txtRecommend.BackColor = ColorTranslator.FromHtml("#cff3dd")
        ElseIf LI >= 1.51 And LI <= 2 Then
            txtRecommend.Text = "งานนั้นควรมีการตรวจสอบและติดตาม"
            txtRecommend.ForeColor = ColorTranslator.FromHtml("#fcd101")
            txtRecommend.BackColor = ColorTranslator.FromHtml("#fbf4b6")
        ElseIf LI >= 2.01 And LI <= 3 Then
            txtRecommend.Text = "งานนั้นเริ่มมีปัญหาควรตรวจสอบเพื่อปรับปรุง"
            txtRecommend.ForeColor = ColorTranslator.FromHtml("#e42c6e")
            txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
        Else
            txtRecommend.ForeColor = Color.Red
            txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
            If RWL = 0 Then
                txtRecommend.Text = "A result of Unacceptable indicates there is a high level of risk for this job task and it is recommended that control measures be implemented immediately."
                txtRecommend.TextMode = TextBoxMode.MultiLine
                txtRecommend.Height = 60
            Else
                txtRecommend.Text = "งานนั้นมีปัญหาควรแก้ไขและปรับปรุงโดยทันที"
                txtRecommend.ForeColor = Color.Red
                txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
            End If

        End If

        FI = RWL / FM
        FILI = L / FI

        txtFI.Text = FI.ToString("0.#0")
        txtFILI.Text = FILI.ToString("0.#0")


        ctlA.AsmNIOSH_Save(StrNull2Zero(hdUID.Value), txtTaskNo.Text, optAsmType.SelectedValue, ddlPerson.SelectedValue, ConvertStrDate2ShortDateTH(txtAsmDate.Text), txtRemark.Text, L, H, V, D, A, C, Dur, F, LC, HM, VM, DM, AM, CM, FM, RWL, LI, txtRecommend.Text, Request.Cookies("Ergo")("userid"), StrNull2Double(txtFI.Text), StrNull2Double(txtFILI.Text))


        'Dim ctlU As New UserController
        'ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_UPD, "Customer", "บันทึก/แก้ไข ประวัติส่วนตัว :{uid=" & hdCompUID.Value & "}{code=" & txtCompanyID.Text & "}", "")

        'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Response.Redirect("AsmResult.aspx")
    End Sub

    Private Sub CalculateScore()
        Dim L, H, V, D, A, C, Dur, F As Double
        Dim LC, HM, VM, DM, AM, CM, FM, RWL, LI, FI, FILI As Double

        'LC ญ=23 ช=51
        LC = 23
        H = StrNull2Double(txtH.Text)
        V = StrNull2Double(txtV.Text)
        D = StrNull2Double(txtD.Text)
        A = StrNull2Double(ddlA.Text)
        F = StrNull2Double(ddlF.Text)
        C = StrNull2Double(optC.SelectedValue)
        Dur = StrNull2Double(optDur.SelectedValue)
        L = StrNull2Double(txtL.Text)

        If H < 25 Then H = 25
        If H > 63 Then H = 63

        HM = 25 / H
        VM = 1 - (0.003 * Math.Abs(V - 75))

        If V > 175 Then VM = 0

        DM = 0.82 + (4.5 / D)

        If D < 25 Then DM = 1
        If D > 175 Then DM = 0

        AM = 1 - (0.0032 * A)
        AM = StrNull2Double(ddlA.SelectedValue)
        If A > 135 Then AM = 0

        If F > 15 Then FM = 0
        'FM เปิดตาราง
        FM = ctlA.NIOSH_GetFMScore(F, Dur, V)

        Select Case C
            Case 1
                If V < 75 Then
                    CM = 1
                Else
                    CM = 1
                End If
            Case 2
                If V < 75 Then
                    CM = 0.95
                Else
                    CM = 1
                End If

            Case 3
                If V < 75 Then
                    CM = 0.9
                Else
                    CM = 0.9
                End If
        End Select

        RWL = LC * HM * VM * DM * AM * FM * CM
        LI = L / RWL

        'lblFinalScore.Text = ScoreFinal.ToString()

        lblHM.Text = Math.Round(HM, 2).ToString("0.#0")
        lblVM.Text = Math.Round(VM, 2).ToString("0.#0")
        lblDM.Text = Math.Round(DM, 2).ToString("0.#0")
        lblAM.Text = Math.Round(AM, 2).ToString("0.#0")
        lblCM.Text = Math.Round(CM, 2).ToString("0.#0")
        lblFM.Text = Math.Round(FM, 2).ToString("0.#0")
        txtRWL.Text = Math.Round(RWL, 2).ToString("0.#0")
        txtLI.Text = Math.Round(LI, 2).ToString("0.#0")

        txtRecommend.TextMode = TextBoxMode.SingleLine
        txtRecommend.Height = 45
        'If LI <= 1 Then
        '    txtRecommend.Text = "ความเสี่ยงน้อยมาก"
        '    txtRecommend.ForeColor = ColorTranslator.FromHtml("#e0a246")
        '    txtRecommend.BackColor = ColorTranslator.FromHtml("#cff3dd")
        'ElseIf LI >= 1.01 And LI <= 1.5 Then
        '    txtRecommend.Text = "ความเสี่ยงน้อย"
        '    txtRecommend.ForeColor = Color.Green
        '    txtRecommend.BackColor = ColorTranslator.FromHtml("#cff3dd")
        'ElseIf LI >= 1.51 And LI <= 2 Then
        '    txtRecommend.Text = "ความเสี่ยงปานกลาง"
        '    txtRecommend.ForeColor = ColorTranslator.FromHtml("#fcd101")
        '    txtRecommend.BackColor = ColorTranslator.FromHtml("#fbf4b6")
        'ElseIf LI >= 2.01 And LI <= 3 Then
        '    txtRecommend.Text = "ความเสี่ยงสูง"
        '    txtRecommend.ForeColor = ColorTranslator.FromHtml("#e42c6e")
        '    txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
        'Else
        '    txtRecommend.ForeColor = Color.Red
        '    txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
        '    If RWL = 0 Then
        '        txtRecommend.Text = "A result of Unacceptable indicates there is a high level of risk for this job task and it is recommended that control measures be implemented immediately."
        '        txtRecommend.TextMode = TextBoxMode.MultiLine
        '        txtRecommend.Height = 60
        '    Else
        '        txtRecommend.Text = "ความเสี่ยงสูงมาก"
        '        txtRecommend.ForeColor = Color.Red
        '        txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
        '    End If

        'End If

        If LI <= 1 Then
            txtRecommend.Text = "ความเสี่ยงน้อยมาก"
            txtRecommend.ForeColor = ColorTranslator.FromHtml("#e0a246")
            txtRecommend.BackColor = ColorTranslator.FromHtml("#cff3dd")
        ElseIf LI >= 1.01 And LI <= 1.5 Then
            txtRecommend.Text = "ภาวะที่ยอมรับได้"
            txtRecommend.ForeColor = Color.Green
            txtRecommend.BackColor = ColorTranslator.FromHtml("#cff3dd")
        ElseIf LI >= 1.51 And LI <= 2 Then
            txtRecommend.Text = "งานนั้นควรมีการตรวจสอบและติดตาม"
            txtRecommend.ForeColor = ColorTranslator.FromHtml("#fcd101")
            txtRecommend.BackColor = ColorTranslator.FromHtml("#fbf4b6")
        ElseIf LI >= 2.01 And LI <= 3 Then
            txtRecommend.Text = "งานนั้นเริ่มมีปัญหาควรตรวจสอบเพื่อปรับปรุง"
            txtRecommend.ForeColor = ColorTranslator.FromHtml("#e42c6e")
            txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
        Else
            txtRecommend.ForeColor = Color.Red
            txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
            If RWL = 0 Then
                txtRecommend.Text = "A result of Unacceptable indicates there is a high level of risk for this job task and it is recommended that control measures be implemented immediately."
                txtRecommend.TextMode = TextBoxMode.MultiLine
                txtRecommend.Height = 60
            Else
                txtRecommend.Text = "งานนั้นมีปัญหาควรแก้ไขและปรับปรุงโดยทันที"
                txtRecommend.ForeColor = Color.Red
                txtRecommend.BackColor = ColorTranslator.FromHtml("#fdedf3")
            End If

        End If


        FI = RWL / FM
        FILI = L / FI

        txtFI.Text = FI.ToString("0.#0")
        txtFILI.Text = FILI.ToString("0.#0")


    End Sub

    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Response.Redirect("Evaluated.aspx")
    End Sub

    Protected Sub txtH_TextChanged(sender As Object, e As EventArgs) Handles txtH.TextChanged
        CalculateScore()
    End Sub

    Protected Sub txtV_TextChanged(sender As Object, e As EventArgs) Handles txtV.TextChanged
        CalculateScore()
    End Sub

    Protected Sub txtD_TextChanged(sender As Object, e As EventArgs) Handles txtD.TextChanged
        CalculateScore()
    End Sub

    Protected Sub ddlA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlA.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optC_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optC.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub optDur_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optDur.SelectedIndexChanged
        CalculateScore()
    End Sub

    Protected Sub ddlF_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlF.SelectedIndexChanged
        CalculateScore()
    End Sub
    Private Sub LoadAssessmentData()
        dt = ctlA.Assessment_GetByUID("NIOSH", Request("id"))

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdUID.Value = .Item("UID")
                txtTaskNo.Text = .Item("TaskNo")
                optAsmType.SelectedValue = .Item("AsmType")
                ddlPerson.SelectedValue = .Item("PersonUID")
                txtAsmDate.Text = .Item("AsmDate")
                txtRemark.Text = .Item("Remark")
                txtL.Text = .Item("L")
                txtH.Text = .Item("H")
                txtV.Text = .Item("V")
                txtD.Text = .Item("D")

                ddlA.Text = .Item("A")
                optC.SelectedValue = .Item("C")
                optDur.SelectedValue = .Item("W")
                ddlF.Text = .Item("F")

                txtRWL.Text = .Item("RWL")
                txtLI.Text = .Item("LI")

                txtFI.Text = String.Concat(.Item("FIRWL"))
                txtFILI.Text = String.Concat(.Item("FILI"))

                txtRecommend.Text = .Item("RecommendText")
                CalculateScore()

            End With
        End If


    End Sub

    Protected Sub optType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAsmType.SelectedIndexChanged
        If optAsmType.SelectedValue = "O" Then
            ddlPerson.Enabled = True
        Else
            ddlPerson.SelectedValue = Request.Cookies("Ergo")("LoginPersonUID")
            ddlPerson.Enabled = False
        End If
    End Sub

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlA.AsmNIOSH_Delete(StrNull2Zero(hdUID.Value))
    End Sub
    Protected Sub txtL_TextChanged(sender As Object, e As EventArgs) Handles txtL.TextChanged
        CalculateScore()
    End Sub
End Class