﻿
Public Class ReportCompany
    Inherits System.Web.UI.Page

    Dim ctlM As New MasterController
    Dim dt As New DataTable
    Dim acc As New UserController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadCompany()
            LoadDivision()
            LoadDepartment()
            txtStartDate.Text = Today.Date.ToString("dd/MM/yyyy")
            txtEndDate.Text = Today.Date.ToString("dd/MM/yyyy")

            Select Case Request("r")
                Case "hracomp"
                    lblTitle.Text = "MSDs Report"
                Case "actdt"
                    lblTitle.Text = "Task Action by Date"
                Case "comprog"
                    lblTitle.Text = "Total Prograssion"
            End Select
        End If

    End Sub

    Private Sub LoadCompany()
        Dim ctlC As New CompanyController

        If Request.Cookies("Ergo")("ROLE_SPA") = True Or Request.Cookies("Ergo")("ROLE_ADM") = True Then
            dt = ctlC.Company_GetActive()
        Else
            dt = ctlC.Company_GetByUID(Request.Cookies("Ergo")("LoginCompanyUID"))
        End If

        With ddlCompany
            .DataSource = dt
            .DataTextField = "CompanyName"
            .DataValueField = "UID"
            .DataBind()
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub LoadDivision()
        ddlDivision.DataSource = ctlM.Division_Get4Select(ddlCompany.SelectedValue)
        ddlDivision.DataTextField = "DivisionName"
        ddlDivision.DataValueField = "DivisionUID"
        ddlDivision.DataBind()
    End Sub
    Private Sub LoadDepartment()
        ddlDepartment.DataSource = ctlM.Department_Get4Select(ddlCompany.SelectedValue, ddlDivision.SelectedValue)
        ddlDepartment.DataTextField = "DepartmentName"
        ddlDepartment.DataValueField = "DepartmentUID"
        ddlDepartment.DataBind()
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadDivision()
    End Sub
    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        PrintReport("PDF")
    End Sub
    Private Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click
        PrintReport("EXCEL")
    End Sub

    Private Sub PrintReport(Optional ReportType As String = "PDF")
        Dim Bdate, Edate As String
        Select Case Request("r")
            Case "actdt"
                If txtStartDate.Text = "" Or txtEndDate.Text = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุช่วงวันที่ก่อน');", True)
                    Exit Sub
                End If

                Bdate = Right(txtStartDate.Text, 4) + Mid(txtStartDate.Text, 4, 2) + Left(txtStartDate.Text, 2)
                Edate = Right(txtEndDate.Text, 4) + Mid(txtEndDate.Text, 4, 2) + Left(txtEndDate.Text, 2)

            Case "comprog"


        End Select


        Select Case Request("r")
            Case "actdt"
                Bdate = Right(txtStartDate.Text, 4) + Mid(txtStartDate.Text, 4, 2) + ConvertYearEN(CInt(Left(txtStartDate.Text, 2)))
                Edate = Right(txtEndDate.Text, 4) + Mid(txtEndDate.Text, 4, 2) + ConvertYearEN(CInt(Left(txtEndDate.Text, 2)))

            Case "comprog"


        End Select

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('Report/ReportViewer?R=" & Request("r") & "&COM=" & ddlCompany.SelectedValue & "&BDT=" & Bdate & "&EDT=" & Edate & "&DIVUID=" & ddlDivision.SelectedValue & "&DEPUID=" & ddlDepartment.SelectedValue & "&RPTTYPE=" & ReportType & "','_blank');", True)

    End Sub

    Private Sub ddlDivision_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDivision.SelectedIndexChanged
        LoadDepartment()
    End Sub
End Class

