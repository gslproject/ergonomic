﻿Imports System.Data
Public Class Task
    Inherits System.Web.UI.Page
    Dim ctlEmp As New TaskController
    Public dtTask As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadTask()
        End If
    End Sub
    Private Sub LoadTask()
        dtTask = ctlEmp.Task_GetByUser(Request.Cookies("Ergo")("userid"))
    End Sub

End Class