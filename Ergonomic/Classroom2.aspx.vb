﻿Public Class Classroom2
    Inherits System.Web.UI.Page
    Public dtCourse2 As New DataTable
    Dim dtP As New DataTable
    Dim ctlP As New PersonController
    'Dim ctlM As New MasterController
    Dim ctlE As New ElearningController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            lblAlert.Visible = False
            cmdSave.Visible = False
            cmdReload.Visible = False

            If Not Request("CID") Is Nothing Then
                LoadCourse(Request.Cookies("Ergo")("LoginPersonUID"), Request("CID"))
            End If
        End If
    End Sub
    'Private Sub LoadPersonData(PersonUID As Integer)
    '    dtP = ctlP.Person_GetByPersonID(PersonUID)
    '    If dtP.Rows.Count > 0 Then
    '        With dtP.Rows(0)
    '            hdUID.Value = String.Concat(.Item("PersonUID"))
    '            lblCode.Text = String.Concat(.Item("Code"))
    '            lblPersonName.Text = String.Concat(.Item("PersonName"))
    '            lblPosition.Text = String.Concat(.Item("PositionName"))
    '            lblDepartment.Text = String.Concat(.Item("DepartmentName"))
    '            lblSection.Text = String.Concat(.Item("SectionName"))
    '            LoadCourseAssign(hdUID.Value)

    '        End With
    '    Else
    '    End If
    'End Sub


    Private Sub LoadCourse(PersonUID As Integer, CourseUID As Integer)
        dtCourse2 = ctlE.Course_GetForClassroom(PersonUID, CourseUID)

        If dtCourse2.Rows.Count > 0 Then
            With dtCourse2.Rows(0)
                lblCourseName.Text = String.Concat(.Item("CourseName"))
            End With
            'cmdSave.Visible = True
            cmdReload.Visible = True
        Else
            lblAlert.Text = "เกิดข้อผิดพลาดบางอย่าง ขออภัยในความไม่สะดวก"
            lblAlert.Visible = True
        End If

        'grdCompany.DataSource = dtCourse
        'grdCompany.DataBind()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Response.Redirect("PostTest?CID=" & Request("CID"))
    End Sub

    Protected Sub cmdReload_Click(sender As Object, e As EventArgs) Handles cmdReload.Click
        Response.Redirect("Classroom2?CID=" & Request("CID"))
    End Sub
    'Private Sub LoadCourse()
    '    ddlCourse.Items.Clear()
    '    dt = ctlE.Course_GetActive()
    '    With ddlCourse
    '        .Enabled = True
    '        .DataSource = dt
    '        .DataTextField = "Name"
    '        .DataValueField = "UID"
    '        .DataBind()
    '        .Visible = True
    '    End With
    '    dt = Nothing
    'End Sub

    'Private Sub grdCompany_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdCompany.RowDataBound
    '    'If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

    '    '    Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
    '    '    Dim imgD As Image = e.Row.Cells(1).FindControl("imgDel")
    '    '    imgD.Attributes.Add("onClick", scriptString)

    '    'End If

    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d9edf7';")
    '        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

    '    End If
    'End Sub
End Class