﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PersonModify.aspx.vb" Inherits="Ergonomic.PersonModify" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>Person
        <small>ข้อมูลส่วนบุคคล</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Person</li>
      </ol>
    </section>

    <!-- Main content -->   
      <section class="content"> 
           <asp:Label ID="lblAlert1" runat="server" cssclass="alert alert-error alert-dismissible show" Text="ไม่สามารถเพิ่มข้อมูลได้อีก เนื่องจากเกินจำนวนที่กำหนดแล้ว (Max Employee Limit)"></asp:Label>  

<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Personal Information<asp:HiddenField ID="hdPersonUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">


      <div class="row">
        <div class="col-md-1">
          <div class="form-group">
            <label>Code</label>
            <asp:TextBox ID="txtCode" runat="server" cssclass="form-control text-center" placeholder="รหัสพนักงาน"></asp:TextBox>
          </div>

        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Prefix</label>
               <asp:DropDownList ID="ddlPrefix" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>
        </div>                

        <div class="col-md-3">
          <div class="form-group">
            <label>Sex</label>
            <asp:RadioButtonList ID="optSex" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="M">Male</asp:ListItem>
              <asp:ListItem Value="F">Female</asp:ListItem>
            </asp:RadioButtonList>
          </div>

        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Date of Birth</label>
              <br />
               <asp:DropDownList ID="ddlDay" runat="server" CssClass="Objcontrol" Width="50"></asp:DropDownList>
<asp:DropDownList ID="ddlMonth" runat="server" CssClass="Objcontrol"></asp:DropDownList>
<asp:DropDownList ID="ddlYear" runat="server" CssClass="Objcontrol" AutoPostBack="True"></asp:DropDownList>  

          </div>

        </div>

        <div class="col-md-1">
          <div class="form-group">
            <label>Age</label>
            <asp:TextBox ID="txtAge" runat="server" MaxLength="3" cssclass="form-control text-center" placeholder="อายุ"></asp:TextBox>
          </div>

        </div>
    
      </div>
      

      <div class="row">

        <div class="col-md-6">
          <div class="form-group">
            <label>ชื่อ</label>
            <asp:TextBox ID="txtFirstNameTH" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
            &nbsp;
          </div>

        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>นามสกุล</label>
            <asp:TextBox ID="txtLastNameTH" runat="server" cssclass="form-control" placeholder="นามสกุลภาษาไทย"></asp:TextBox>
            &nbsp;
          </div>

        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>First Name</label>
            <asp:TextBox ID="txtFNameEN" runat="server" cssclass="form-control" placeholder="ชื่อภาษาอังกฤษ"></asp:TextBox>
          </div>

        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Last Name</label>
            <asp:TextBox ID="txtLNameEN" runat="server" cssclass="form-control" placeholder="นามสกุลภาษาอังกฤษ"></asp:TextBox>
            &nbsp;
          </div>

        </div>

      </div>
          <div class="row">            
                <div class="col-md-6">
          <div class="form-group">
            <label>ระดับการศึกษาสูงสุด</label>
              <asp:DropDownList ID="ddlEdu" cssclass="form-control select2" runat="server" RepeatDirection="Horizontal">
                  <asp:ListItem Selected="True" Value=""></asp:ListItem>
                  <asp:ListItem Value="1">มัธยมศึกษาตอนต้น (ม.3)</asp:ListItem>
                  <asp:ListItem Value="2">มัธยมศึกษาตอนปลายหรือเทียบเท่า (ม.6/ปวช.)</asp:ListItem>
                  <asp:ListItem Value="3">อนุปริญญาตรี / ปวส.</asp:ListItem>
                  <asp:ListItem Value="4">ปริญญาตรี</asp:ListItem>
                  <asp:ListItem Value="5">ปริญญาโทหรือสูงกว่า</asp:ListItem>
                  <asp:ListItem Value="6">อื่นๆ (ระบุ)</asp:ListItem>
              </asp:DropDownList>
          </div>

        </div> 
                <div class="col-md-6">
          <div class="form-group">
            <label>ระบุ</label>
            <asp:TextBox ID="txtEduRemark" runat="server" cssclass="form-control" placeholder="ระดับการศึกษาอื่นๆ"></asp:TextBox>
            &nbsp;
          </div>

        </div>
            </div>
    

    
    </div>
    <!-- /.box-body -->
  </div>
  </section>
</div>
        <div class="row">
    <section class="col-lg-12 connectedSortable"> 
          <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">Contact Info.</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                                     
                
      <div class="row">

        <div class="col-md-3">
          <div class="form-group">
            <label>Address no.</label>
            <asp:TextBox ID="txtAddressNo" runat="server" cssclass="form-control" placeholder="บ้านเลขที่">
            </asp:TextBox>
          </div>

        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Lane</label>
            <asp:TextBox ID="txtLane" runat="server" cssclass="form-control" placeholder="ซอย"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Road</label>
            <asp:TextBox ID="txtRoad" runat="server" cssclass="form-control" placeholder="ถนน"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Sub District</label>
            <asp:TextBox ID="txtSubDistrict" runat="server" cssclass="form-control" placeholder="ตำบล/แขวง">
            </asp:TextBox>
          </div>
        </div>
      </div>
      <div class="row">

        <div class="col-md-3">
          <div class="form-group">
            <label>district</label>
            <asp:TextBox ID="txtDistrict" runat="server" cssclass="form-control" placeholder="อำเภอ"></asp:TextBox>
          </div>

        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Province</label>
            <asp:DropDownList ID="ddlProvince" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>

          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Zip Code</label>
            <asp:TextBox ID="txtZipcode" runat="server" cssclass="form-control" placeholder="รหัสไปรษณีย์">
            </asp:TextBox>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Country</label>
            <asp:DropDownList ID="ddlCountry" runat="server" cssclass="form-control select2" Width="100%">
              <asp:ListItem Selected="True" Value="TH">ประเทศไทย</asp:ListItem>
            </asp:DropDownList>

          </div>
        </div>
      </div>
      <div class="row">

        <div class="col-md-3">
          <div class="form-group">
            <label>Telephone</label>
            <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>E-mail</label>
            <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
          </div>
        </div>

      </div>
      <h5 class="text-blue">Address display (for report)</h5>
      <div class="row">

        <div class="col-md-6">
          <div class="form-group">
            <label>ภาษาไทย</label>
            <asp:TextBox ID="txtAddressTha" runat="server" cssclass="form-control" placeholder="ที่อยู่ภาษาไทย">
            </asp:TextBox>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>English</label>
            <asp:TextBox ID="txtAddressEng" runat="server" cssclass="form-control" placeholder="ที่อยู่ภาษาอังกฤษ">
            </asp:TextBox>
          </div>
        </div>
      </div>
</div> 
        <div class="box-footer clearfix">           
                
            </div>
          </div>


        </section>
                       
                </div>

  
            <div class="row">
    <section class="col-lg-12 connectedSortable"> 
          <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">Job Description</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 
                    <div class="row">

         <div class="col-md-9">
          <div class="form-group">
            <label>Company</label>
            <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>

        </div>
       <div class="col-md-3">
          <div class="form-group">
            <label>Status</label>
            <asp:DropDownList ID="ddlWorkStatus" runat="server" cssclass="form-control select2" Width="100%">
                <asp:ListItem Selected="True" Value="1">ยังทำงานอยู่</asp:ListItem>
                <asp:ListItem Value="2">พ้นสภาพ</asp:ListItem>
                <asp:ListItem Value="0">ลาออก</asp:ListItem>
            </asp:DropDownList>
          </div>

        </div>

                        </div>
                  <div class="row">
      
        <div class="col-md-4">
          <div class="form-group">
            <label>Section</label>
            <asp:DropDownList ID="ddlDivision" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>

        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label>Department</label>
            <asp:DropDownList ID="ddlDepartment" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>

        </div>
           <div class="col-md-4">
          <div class="form-group">
            <label>Position</label>
            <asp:DropDownList ID="ddlPosition" runat="server" cssclass="form-control select2" Width="100%">
            </asp:DropDownList>
          </div>

        </div>
                       </div>
                  <div class="row">

        <div class="col-md-3">
          <div class="form-group">
            <label>Start Date</label>
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </div>
              <!-- autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"-->
            
          </div>

        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>End Date</label>
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </div>
          </div>

        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label>Work Period</label>
            <asp:DropDownList ID="ddlWorkPeriod" runat="server" cssclass="form-control select2" Width="100%">
                <asp:ListItem Selected="True" Value="D">Daily</asp:ListItem>
                <asp:ListItem Value="M">Shit time</asp:ListItem> 
            </asp:DropDownList>
          </div>

        </div>

   <div class="col-md-3">
          <div class="form-group">
            <label>Work day per week</label>
            <asp:TextBox ID="txtWorkDayPerWeek" runat="server" cssclass="form-control" placeholder=""></asp:TextBox>
          </div>
        </div>
      </div>
</div> 
        <div class="box-footer clearfix"> 
            </div>
          </div>        
          <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search-plus"></i>

              <h3 class="box-title">Health history</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body">  
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Blood Group</label>
              <asp:DropDownList ID="ddlBloodGroup" runat="server" CssClass="form-control select2" placeholder="กรุ๊ปเลือด" Width="100%">
                  <asp:ListItem Selected="True" Value="-"> </asp:ListItem>
                  <asp:ListItem>A</asp:ListItem>
                  <asp:ListItem>B</asp:ListItem>
                  <asp:ListItem>AB</asp:ListItem>
                  <asp:ListItem>O</asp:ListItem>
              </asp:DropDownList>          
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Medical History</label>
            <asp:TextBox ID="txtMedicalHistory" runat="server" cssclass="form-control" placeholder="โรคประจำตัว"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Drug allergy</label>
            <asp:TextBox ID="txtAllergy" runat="server" cssclass="form-control" placeholder="ประวัติแพ้ยา"></asp:TextBox>
          </div>
        </div>
      </div>  
</div> 
        <div class="box-footer clearfix">
            </div>
          </div>
        </section>
                </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Status</label>
              <asp:CheckBox ID="chkStatus" runat="server" Text="   Active" Checked="True" />
          </div>
        </div>
        <div class="col-md-6">
          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />
            <asp:Button ID="cmdDel" CssClass="btn btn-danger" runat="server" Text="Delete" Width="120px" />
        </div>
      </div>
 <asp:Label ID="lblAlert2" runat="server" cssclass="alert alert-error alert-dismissible show" Text="ไม่สามารถเพิ่มข้อมูลได้อีก เนื่องจากเกินจำนวนที่กำหนดแล้ว (Max Employee Limit)"></asp:Label>  
          </section>
</asp:Content>