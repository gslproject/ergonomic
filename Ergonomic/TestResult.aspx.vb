﻿
Public Class TestResult
    Inherits System.Web.UI.Page

    Dim dtP As New DataTable
    Dim ctlP As New PersonController
    'Dim ctlM As New MasterController
    Dim ctlE As New ElearningController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            cmdPrint.Visible = False
            If Not Request("CID") Is Nothing Then
                hdCourseUID.Value = Request("CID")
                LoadResult(Request.Cookies("Ergo")("LoginPersonUID"), Request("CID"))
            End If
        End If
    End Sub

    Private Sub LoadResult(PersonUID As Integer, CourseUID As Integer)
        dtP = ctlE.PersonTest_Get(PersonUID, CourseUID)

        If dtP.Rows.Count > 0 Then
            With dtP.Rows(0)
                lblCourseName.Text = String.Concat(.Item("CourseName"))

                If .Item("ResultStatus") = "P" Then
                    lblResult.Text = "ผ่าน"
                    cmdPrint.Visible = True
                    lblResult.ForeColor = System.Drawing.Color.Green
                Else
                    lblResult.Text = "ไม่ผ่าน"
                    cmdPrint.Visible = False
                    lblResult.ForeColor = System.Drawing.Color.Red
                End If
            End With
        End If

    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('Report/ReportViewer?R=cert&PID=" & Request.Cookies("Ergo")("LoginPersonUID") & "&CID=" & Request("CID") & "&RPTTYPE=PDF" & "','_blank');", True)
    End Sub
End Class