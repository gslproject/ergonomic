﻿
Public Class ReportPersonal
    Inherits System.Web.UI.Page

    Dim ctlP As New PersonController
    Dim dt As New DataTable
    Dim acc As New UserController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            LoadCompany()
            ddlCompany.SelectedIndex = 0
            If ddlCompany.SelectedValue = 0 Then
                ddlCompany.SelectedIndex = 1
            End If

            LoadPerson()


            If Request.Cookies("Ergo")("ROLE_CUS") = True Then
                ddlPerson.SelectedValue = Request.Cookies("Ergo")("LoginPersonUID")
                ddlPerson.Enabled = False
                ddlCompany.Enabled = False
            Else
                ddlPerson.Enabled = True
                ddlCompany.Enabled = True
            End If
        End If

    End Sub

    Private Sub LoadCompany()
        Dim ctlC As New CompanyController

        If Request.Cookies("Ergo")("ROLE_SPA") = True Or Request.Cookies("Ergo")("ROLE_ADM") = True Then
            dt = ctlC.Company_GetActive()
        Else
            dt = ctlC.Company_GetByUID(Request.Cookies("Ergo")("LoginCompanyUID"))
        End If

        With ddlCompany
            .DataSource = dt
            .DataTextField = "CompanyName"
            .DataValueField = "UID"
            .DataBind()
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub LoadPerson()
        Dim dtE As New DataTable
        dtE = ctlP.Person_GetByCompany(ddlCompany.SelectedValue)
        ddlPerson.DataSource = dtE
        ddlPerson.DataTextField = "PersonName"
        ddlPerson.DataValueField = "PersonUID"
        ddlPerson.DataBind()
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        PrintReport("PDF")
    End Sub

    Private Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadPerson()
    End Sub
    Private Sub PrintReport(Optional ReportType As String = "PDF")
        If ddlPerson.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกพนักงานก่อน');", True)
            Exit Sub
        End If

        'Dim Bdate, Edate, Cond As String
        'Bdate = Right(txtStartDate.Text, 4) + Mid(txtStartDate.Text, 4, 2) + Left(txtStartDate.Text, 2)
        'Edate = Right(txtEndDate.Text, 4) + Mid(txtEndDate.Text, 4, 2) + Left(txtEndDate.Text, 2)

        'If ddlPerson.SelectedValue = "ALL" Then
        '    Cond = "ALL"
        'Else
        '    Cond = "LAST"
        'End If

        Select Case Request("r")
            Case "plearn"
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('Report/ReportViewer?R=plearn&COM=" & ddlCompany.SelectedValue & "&EMP=" & ddlPerson.SelectedValue & "&RPTTYPE=" & ReportType & "','_blank');", True)
            Case Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('Report/ReportViewer?R=personal&COM=" & ddlCompany.SelectedValue & "&EMP=" & ddlPerson.SelectedValue & "&RPTTYPE=" & ReportType & "','_blank');", True)

        End Select


        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Report", "window.open('Report/ReportViewer?R=personal&COM=" & ddlCompany.SelectedValue & "&EMP=" & ddlPerson.SelectedValue & "&RPTTYPE=" & ReportType & "','_blank');", True)


        'Response.Redirect("Report/ReportViewer?R=tasktotal&COM=" & ddlCompany.SelectedValue & "&COND=" & ddlFillter.SelectedValue & "&BDT=" & Bdate & "&EDT=" & Edate & "&RPTTYPE=" & ReportType)


    End Sub

    Private Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click
        PrintReport("EXCEL")
    End Sub
End Class

