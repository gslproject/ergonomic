﻿Imports System.Data
Public Class TaskAction
    Inherits System.Web.UI.Page
    Dim ctlEmp As New TaskController
    Public dtTask As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadTask()
        End If
    End Sub
    Private Sub LoadTask()
        dtTask = ctlEmp.TaskAction_Get(Request.Cookies("Ergo")("LoginCompanyUID"))
    End Sub

End Class