﻿Imports System.Drawing
Public Class AsmHRA
    Inherits System.Web.UI.Page
    Dim ctlEmp As New PersonController
    Dim ctlA As New AssessmentController
    Public dtPersonRisk As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If


        If Not IsPostBack Then
            hdUID.Value = 0
            hdPersonUID.Value = 0

            If Not Request("id") Is Nothing Then
                hdUID.Value = Request("id")
                Dim dtH As New DataTable
                dtH = ctlA.AsmHRA_GetByUID(hdUID.Value)

                If dtH.Rows.Count > 0 Then
                    hdPersonUID.Value = dtH.Rows(0)("PersonUID")
                    hdUID.Value = dtH.Rows(0)("UID")
                End If
            Else
                If Not Request("pid") Is Nothing Then
                    hdPersonUID.Value = Request("pid")
                End If
            End If

            LoadPersonData()
            LoadPersonErgonomicRisk()
            CalculatingScore(hdPersonUID.Value)
        End If
    End Sub

    Private Sub LoadPersonErgonomicRisk()
        dtPersonRisk = ctlEmp.PersonBodyRisk_Get(hdPersonUID.Value)

        'grdData.DataSource = dtPersonRisk
        'grdData.DataBind()

        'For i = 0 To grdData.Rows.Count - 1
        '    If grdData.Rows(i).Cells(1).Text >= 3 Then
        '        grdData.Rows(i).Cells(1).ForeColor = Color.Red
        '    End If

        '    If grdData.Rows(i).Cells(2).Text >= 3 Then
        '        grdData.Rows(i).Cells(2).ForeColor = Color.Red
        '    End If


        'Next



    End Sub
    Private Sub LoadPersonData()
        Dim dtP As New DataTable
        dtP = ctlEmp.Person_GetByPersonID(hdPersonUID.Value)
        If dtP.Rows.Count > 0 Then
            With dtP.Rows(0)

                hdPersonUID.Value = .Item("PersonUID")
                txtCode.Text = .Item("Code")
                txtName.Text = .Item("PersonName")
                txtPosition.Text = .Item("PositionName")
                txtDepartment.Text = String.Concat(.Item("DepartmentName"))
                txtSection.Text = String.Concat(.Item("SectionName"))
            End With


        End If
    End Sub

    Private Sub CalculatingScore(PersonUID As Integer)
        Dim dtScore As New DataTable
        Dim BodyRisk, ErgoRisk, RiskLevel As Double

        dtScore = ctlEmp.PersonBodyRisk_GetMax(PersonUID)

        If dtScore.Rows.Count > 0 Then
            BodyRisk = DBNull2Dbl(dtScore.Rows(0)("Score")) * DBNull2Dbl(dtScore.Rows(0)("Freq"))
        Else
            BodyRisk = 0
        End If

        RiskLevel = ctlEmp.PersonErgonomic_GetMax(PersonUID)
        ErgoRisk = BodyRisk * RiskLevel


        txtBodyScore.Text = BodyRisk
        txtBodyRisk.Text = ""


        If BodyRisk <= 0 Then
            txtBodyRisk.Text = "ระดับ 0 ไม่รู้สึกรับรู้ความไม่สบาย"
            txtBodyRisk.ForeColor = ColorTranslator.FromHtml("#004085")
            txtBodyRisk.BackColor = ColorTranslator.FromHtml("#cce5ff")
            txtBodyRisk.BorderColor = ColorTranslator.FromHtml("#b8daff")
        ElseIf BodyRisk >= 1 And BodyRisk <= 2 Then
            txtBodyRisk.Text = "ระดับ 1 รู้สึกไม่สบายรุนแรงเล็กน้อย"
            txtBodyRisk.ForeColor = ColorTranslator.FromHtml("#008d4c")
            txtBodyRisk.BackColor = ColorTranslator.FromHtml("#d4edda")
            txtBodyRisk.BorderColor = ColorTranslator.FromHtml("#c3e6cb")
        ElseIf BodyRisk >= 3 And BodyRisk <= 4 Then
            txtBodyRisk.Text = "ระดับ 2 รู้สึกไม่สบายรุนแรงปานกลาง"
            txtBodyRisk.ForeColor = ColorTranslator.FromHtml("#ff7701")
            txtBodyRisk.BackColor = ColorTranslator.FromHtml("#fff3cd")
            txtBodyRisk.BorderColor = ColorTranslator.FromHtml("#ffeeba")

        ElseIf BodyRisk >= 5 And BodyRisk <= 8 Then
            txtBodyRisk.Text = "ระดับ 3 รู้สึกไม่สบายรุนแรงมาก"
            txtBodyRisk.ForeColor = ColorTranslator.FromHtml("#ff0000")
            txtBodyRisk.BackColor = ColorTranslator.FromHtml("#f8d7da")
            txtBodyRisk.BorderColor = ColorTranslator.FromHtml("#f5c6cb")
        Else
            txtBodyRisk.Text = "ระดับ 4 รู้สึกไม่สบายรุนแรงมากเกินทนไหว"
            txtBodyRisk.ForeColor = Color.White
            txtBodyRisk.BackColor = ColorTranslator.FromHtml("#d33724")
            txtBodyRisk.BorderColor = ColorTranslator.FromHtml("#d33724")
        End If


        txtErgoScore.Text = ErgoRisk
        txtErgoRisk.Text = ""

        If ErgoRisk <= 0 Then
            txtErgoRisk.Text = "ระดับ 0"
            lblRecommend.Text = "1. อบรมให้ความรู้เรื่องการยศาสตร์ในการทำงาน"

            txtErgoRisk.ForeColor = ColorTranslator.FromHtml("#004085")
            txtErgoRisk.BackColor = ColorTranslator.FromHtml("#cce5ff")
            txtErgoRisk.BorderColor = ColorTranslator.FromHtml("#b8daff")


            lblRecommend.ForeColor = ColorTranslator.FromHtml("#004085")
            lblRecommend.BackColor = ColorTranslator.FromHtml("#cce5ff")
            lblRecommend.BorderColor = ColorTranslator.FromHtml("#b8daff")


        ElseIf ErgoRisk >= 1 And ErgoRisk <= 2 Then
            txtErgoRisk.Text = "ระดับ 1"
            lblRecommend.Text = "1. อบรมให้ความรู้เรื่องการยศาสตร์ในการทำงาน"


            txtErgoRisk.ForeColor = ColorTranslator.FromHtml("#008d4c")
            txtErgoRisk.BackColor = ColorTranslator.FromHtml("#d4edda")
            txtErgoRisk.BorderColor = ColorTranslator.FromHtml("#c3e6cb")

            lblRecommend.ForeColor = ColorTranslator.FromHtml("#008d4c")
            lblRecommend.BackColor = ColorTranslator.FromHtml("#d4edda")
            lblRecommend.BorderColor = ColorTranslator.FromHtml("#c3e6cb")

        ElseIf ErgoRisk >= 3 And ErgoRisk <= 4 Then

            txtErgoRisk.Text = "ระดับ 2"
            lblRecommend.Text = "1. กำหนดโปรแกรมการบริหารร่างกายระหว่างช่วงเวลาทำงาน <br/>2. อบรมให้ความรู้เรื่องการยศาสตร์ในการทำงาน"
            lblRecommend.Height = 50

            txtErgoRisk.ForeColor = ColorTranslator.FromHtml("#ff7701")
            txtErgoRisk.BackColor = ColorTranslator.FromHtml("#fff3cd")
            txtErgoRisk.BorderColor = ColorTranslator.FromHtml("#ffeeba")

            lblRecommend.ForeColor = ColorTranslator.FromHtml("#ff7701")
            lblRecommend.BackColor = ColorTranslator.FromHtml("#fff3cd")
            lblRecommend.BorderColor = ColorTranslator.FromHtml("#ffeeba")


        ElseIf ErgoRisk >= 5 And ErgoRisk <= 8 Then
            txtErgoRisk.Text = "ระดับ 3"
            lblRecommend.Text = "1. ปรับปรุงสถานีงานให้เหมาะสมตามหลักการยศาสตร์ เช่น การปรับเปลี่ยนเก้าอี้หรือโต๊ะทำงาน และคอมพิวเตอร์ เป็นต้น<br/>
2. กำหนดโปรแกรมการบริหารร่างกายระหว่างช่วงเวลาทำงาน<br/>
3. อบรมให้ความรู้เรื่องการยศาสตร์ในการทำงาน<br/>
4. จัดให้มีการตรวจสอบและประเมินท่าทางการทำงานตามหลักการยศาสตร์ เป็นประจำ"

            txtErgoRisk.ForeColor = ColorTranslator.FromHtml("#ff0000")
            txtErgoRisk.BackColor = ColorTranslator.FromHtml("#f8d7da")
            txtErgoRisk.BorderColor = ColorTranslator.FromHtml("#f5c6cb")

            lblRecommend.ForeColor = ColorTranslator.FromHtml("#ff0000")
            lblRecommend.BackColor = ColorTranslator.FromHtml("#f8d7da")
            lblRecommend.BorderColor = ColorTranslator.FromHtml("#f5c6cb")

            lblRecommend.Height = 100
        Else
            txtErgoRisk.Text = "ระดับ 4"
            lblRecommend.Text = "1. ปรับปรุงสถานีงานให้เหมาะสมตามหลักการยศาสตร์ เช่น การปรับเปลี่ยนเก้าอี้หรือโต๊ะทำงาน และคอมพิวเตอร์ เป็นต้น<br/>
2. ดำเนินการประเมินความเสี่ยงอีกครั้งหลังจากมีการปรับปรุงสถานีงาน<br/>
3. กำหนดโปรแกรมการบริหารร่างกายระหว่างช่วงเวลาทำงาน<br/>
4. อบรมให้ความรู้เรื่องการยศาสตร์ในการทำงาน<br/>
5. จัดให้มีการตรวจสอบและประเมินท่าทางการทำงานตามหลักการยศาสตร์ เป็นประจำ"


            txtErgoRisk.ForeColor = Color.White
            txtErgoRisk.BackColor = ColorTranslator.FromHtml("#d33724")
            txtErgoRisk.BorderColor = ColorTranslator.FromHtml("#d33724")

            lblRecommend.ForeColor = Color.White
            lblRecommend.BackColor = ColorTranslator.FromHtml("#d33724")
            lblRecommend.BorderColor = ColorTranslator.FromHtml("#d33724")


            lblRecommend.Height = 150
        End If



        lblRecommend.NavigateUrl = "#"


        dtScore = Nothing

    End Sub


    Protected Sub cmdBack_Click(sender As Object, e As EventArgs) Handles cmdBack.Click
        Response.Redirect("PersonSelect?ActionType=hraasm")
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        ctlA.AsmHRA_Save(StrNull2Zero(hdUID.Value), StrNull2Zero(hdPersonUID.Value), StrNull2Double(txtBodyScore.Text), txtBodyRisk.Text, StrNull2Double(txtErgoScore.Text), txtErgoRisk.Text, txtRemark.Text, Request.Cookies("Ergo")("userid"))

        Response.Redirect("HRA?ActionType=hraasm")

    End Sub

End Class