﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="HRAAssessment.aspx.vb" Inherits="Ergonomic.HRAAssessment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

       <section class="content-header">
      <h1>HRA Assessment</h1>     
    </section>
    <section class="content">  

  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">HRA Assessment<asp:HiddenField ID="hdCompUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">


      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Task No</label>
               <asp:DropDownList ID="ddlTask" runat="server" cssclass="form-control select2"  placeholder="Select Task" Width="100%">
            </asp:DropDownList> 
          </div>

        </div>
      
      </div>
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>Person</label>
              <asp:DropDownList ID="ddlPerson" runat="server" cssclass="form-control select2"  placeholder="Select Person" Width="100%">
            </asp:DropDownList> 
          </div>

        </div> 

      </div>
           
      
      <div class="row"> 

        <div class="col-md-12 text-center">

          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="เริ่มการประเมิน" Width="120px" />
             &nbsp;<asp:Button ID="Button1" CssClass="btn btn-success" runat="server" Text="แก้ไข" Width="120px" />
             &nbsp;<asp:Button ID="Button2" CssClass="btn btn-default" runat="server" Text="ยกเลิก" Width="120px" /> 

        </div>
      </div>
    </div>
    <!-- /.box-body -->
  </div>

        </section>
</asp:Content>