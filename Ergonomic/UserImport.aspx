﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="UserImport.aspx.vb" Inherits="Ergonomic.UserImport" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">    
        function openModals(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#btnConfirm').attr('onclick', "$('#mdEditPerson').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            $('.editemp').click(function () {
             var fname = $(this).attr('data-fname');
             $('#fname').val(fname);
             $('#mdEditPerson').modal('show');
            });
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
     <section class="content-header">
      <h1>Person & User Import</h1>     
    </section>
    <section class="content">  
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Company Quota</h3>
            </div>
            <div class="box-body"> 
                  <div class="row">
                       <div class="col-md-8">
          <div class="form-group">
            <label>Company</label>
                  <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%" AutoPostBack="True">
            </asp:DropDownList>
          </div>

        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Company Quota</label>
            <asp:label ID="lblQuota" runat="server" cssclass="form-control text-center" ></asp:label>
          </div>
        </div>
                      <div class="col-md-2">
          <div class="form-group">
            <label>Remain</label>
            <asp:label ID="lblRemain" runat="server" cssclass="form-control text-center" ></asp:label>
          </div>
        </div>

                      </div>

            </div>
               <div class="box-footer clearfix">
                   <asp:Label ID="lblAlert" runat="server" Text="" CssClass="alert alert-danger"  Width="100%"></asp:Label>
                   </div>
          </div>
     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-upload"></i>

              <h3 class="box-title">Import Person &amp; User from Excel file</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 

                              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="table table-condensed">
       <tr>
         <td>             
                      <asp:FileUpload ID="FileUploadFile" runat="server" /> 
         </td>
         </tr>
       <tr>
         <td>
             <asp:Button ID="cmdImport" runat="server" CssClass="btn btn-primary" Text="import" Width="100px" />           </td>
         </tr>
       <tr>
         <td>
             <asp:Label ID="lblResult" runat="server" CssClass="alert alert-success" Width="100%"></asp:Label>           </td>
         </tr>
     </table>             
</div>
            <div class="box-footer clearfix">
             <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
             <ProgressTemplate>
<img alt="" src="images/progress_bar.gif" height="25" />             </ProgressTemplate>
         </asp:UpdateProgress>   
            </div>
          </div>

        
 
    </section>
</asp:Content>
