﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="JobOwner.aspx.vb" Inherits="Ergonomic.JobOwner" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>Job Owner Assignment
        <small>กำหนด Job Owner </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Job Owner</li>
      </ol>
    </section>

    <!-- Main content -->   
      <section class="content">  
<div class="row">
   <section class="col-lg-12 connectedSortable">
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Task Information<asp:HiddenField ID="hdUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-1">
          <div class="form-group">
            <label>Task no</label>
            <asp:Label ID="lblCode" runat="server" cssclass="form-control text-center"></asp:Label>
          </div>

        </div>
        <div class="col-md-11">
          <div class="form-group">
            <label>Name</label>
               <asp:Label ID="lblTaskName" runat="server" cssclass="form-control text-center"></asp:Label> 
          </div>
        </div>      
      </div>    
    </div>
  </div>
  </section>
</div>
   
            <div class="row">
    <section class="col-lg-12 connectedSortable"> 

          <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Owner Assignment<asp:HiddenField ID="hdOwnerUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

          <div class="row">
            <div class="col-md-6">
          <div class="form-group">
            <label>Person</label>
            <asp:DropDownList ID="ddlPerson" runat="server" cssclass="form-control select2"  placeholder="--- เลือก Owner---" Width="100%"> </asp:DropDownList>
          </div>
        </div>
                  <div class="col-md-3">
          <div class="form-group">
            <label>Level</label>
            <asp:DropDownList ID="ddlLevel" runat="server" cssclass="form-control select2"  placeholder="--- เลือก Owner---" Width="100%">
                <asp:ListItem Value="1">Manager</asp:ListItem>
                <asp:ListItem Value="2">Supervisor</asp:ListItem>
                <asp:ListItem Value="3">Owner</asp:ListItem>
              </asp:DropDownList>
          </div>
        </div>              
<div class="col-md-1">
          <div class="form-group">
              <br />
<asp:Button ID="cmdAdd" runat="server" Text="Save" Width="100" CssClass="btn btn-primary" />
 </div>
        </div>

</div>
     <div class="row text-center">

         </div>

  <div class="row">
 
                <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" CssClass="table table-bordered table-striped" 
                             Font-Bold="False" PageSize="15">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' /> 
                                </ItemTemplate>
                                <ItemStyle Width="20px" />
                            </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgDel" runat="server" ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="No." DataField="nRow">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="40px" />                            </asp:BoundField>
                        <asp:BoundField DataField="PersonName" HeaderText="Owner" />
                            <asp:BoundField DataField="Email" HeaderText="E-mail" />
                            <asp:BoundField HeaderText="Level" DataField="LevelName" />
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView> 
     </div>        
         
          </div>
    </div>




        </section>
<section class="col-lg-6 connectedSortable"> 
       
        </section>
                </div>
      <div class="row">
        <div class="col-md-9">
        </div>
      </div>

          </section>
</asp:Content>