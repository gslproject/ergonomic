﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class TaskTracking
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New TaskController
    Dim ctlR As New ReferenceValueController


    'Private Const UploadDirectory As String = "~/imgTask/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            'Request.Cookies("Ergo")("taskimg")="blankimage.png"
            hdUID.Value = "0"

            LoadAction()

            If Not Request("tid") Is Nothing Then
                LoadTaskData()
                LoadOwner()
            End If
        End If
    End Sub
    Private Sub LoadAction()
        Dim ctlC As New CompanyController
        ddlAction.DataSource = ctlR.ReferenceValue_GetByDomainCode("ACTTASK")
        ddlAction.DataTextField = "DisplayName"
        ddlAction.DataValueField = "UID"
        ddlAction.DataBind()
    End Sub

    Private Sub LoadOwner()
        ddlOwner.DataSource = ctlE.JobOwner_Get(StrNull2Zero(Request.Cookies("Ergo")("LoginCompanyUID")), StrNull2Zero(hdTaskUID.Value))
        ddlOwner.DataTextField = "PersonName"
        ddlOwner.DataValueField = "PersonUID"
        ddlOwner.DataBind()
    End Sub

    Private Sub LoadTaskData()
        dt = ctlE.Task_GetByUID(Request("tid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdTaskUID.Value = String.Concat(.Item("UID"))
                txtTaskNo.Text = String.Concat(.Item("TaskNo"))
                txtName.Text = String.Concat(.Item("TaskName"))
                lblMachine.Text = String.Concat(.Item("MachineName"))
                LoadTaskActionData()
            End With

        Else
        End If
    End Sub

    Private Sub LoadTaskActionData()
        Dim dtTA As New DataTable
        dtTA = ctlE.TaskAction_GetByTaskUID(hdTaskUID.Value)
        If dtTA.Rows.Count > 0 Then
            With dtTA.Rows(0)
                hdUID.Value = String.Concat(.Item("UID"))
                ddlAction.SelectedValue = String.Concat(.Item("ActionUID"))
                ddlOwner.SelectedValue = String.Concat(.Item("OwnerUID"))
                ddlStatus.SelectedValue = String.Concat(.Item("ActionStatus"))
                txtDueDate.Text = String.Concat(.Item("Duedate"))
                txtComment.Text = String.Concat(.Item("Comment"))

            End With

        Else
        End If
    End Sub


    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Dim Duedate As String

        Duedate = txtDueDate.Text

        If Duedate = "" Or Duedate = "dd/mm/yyyy" Then
            Duedate = ""
        Else
            Duedate = ConvertStrDate2ShortDateEN(txtDueDate.Text)
        End If


        ctlE.TaskAction_Save(StrNull2Zero(hdUID.Value), Request.Cookies("Ergo")("LoginCompanyUID"), StrNull2Zero(hdTaskUID.Value), StrNull2Zero(ddlAction.SelectedValue), StrNull2Zero(ddlOwner.SelectedValue), Duedate, ddlStatus.SelectedValue, txtComment.Text)

        Dim TaskUID As Integer
        TaskUID = StrNull2Zero(hdTaskUID.Value) 'ctlE.Task_GetUIDByCode(ddlTask.SelectedValue)

        Dim ctlU As New UserController
        ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_UPD, "Task", "เพิ่ม Task Action :{TaskUID=" & TaskUID & "}{Action=" & ddlAction.SelectedValue & "}{Owner=" & ddlOwner.SelectedValue & "}", "")

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        'Response.Redirect("ResultPage.aspx?p=tska")
    End Sub

    Protected Sub txtDueDate_TextChanged(sender As Object, e As EventArgs)

    End Sub
End Class