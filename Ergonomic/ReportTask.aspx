﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReportTask.aspx.vb" Inherits="Ergonomic.ReportTask" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
      <h1>Total Task Report<small></small></h1>   
</section>

<section class="content">  

         <div class="box box-solid">
            <div class="box-header">           
            </div>
            <div class="box-body"> 
 <div class="col-lg-2"></div>

<div class="col-lg-8" style="background-color:#14539a;color: white">
      <br />   <br /> 
                
   <div class="row"> 
     
   <div class="col-md-12">
          <div class="form-group">
            <label>Company</label>
              <asp:DropDownList ID="ddlCompany" runat="server" cssclass="form-control select2" Width="100%">                   
            </asp:DropDownList>
          </div>

        </div>
          </div>
         <div class="row"> 
           <div class="col-md-6">
          <div class="form-group">
            <label>Condition</label>
                <asp:DropDownList ID="ddlFillter" runat="server" cssclass="form-control select2" Width="100%">
                    <asp:ListItem Selected="True">ALL</asp:ListItem>
                    <asp:ListItem Value="L">เฉพาะครั้งล่าสุด</asp:ListItem>
            </asp:DropDownList>
          </div>

        </div>
    
              

            <div class="col-md-3">
          <div class="form-group">
            <label>Start Date</label>
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control"  autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </div>
              <!-- data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask -->
            
          </div>

        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>End Date</label>
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </div>
          </div>

        </div>

       </div>
          
      
      <br />  
  <div class="row">
   <div class="col-md-12 text-center">
     <asp:Button ID="cmdExcel" runat="server" CssClass="btn btn-info" Text="Export to Excel" />       
       </div>
      </div>

      <br />   <br />
    </div>
 <div class="col-lg-2"></div>
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>            
  
</section>   
</asp:Content>
