﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucMenu.ascx.vb" Inherits="Ergonomic.ucMenu" %>
   <link rel="stylesheet" type="text/css" href="css/rajchasistyles.css"> 
 
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li> 

  <!--    < % If Request("actionType") = "d"  %>
            <li class="active"><a href="Dashboard.aspx?actionType=d"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        < % Else % >
            <li><a href="Dashboard.aspx?actionType=d"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li> 
        < % End If % >  --> 

        <% If Request("actionType") = "h"  %>
            <li class="active"><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> <span>Home</span></a></li>
        <% Else %>
            <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> <span>Home</span></a></li> 
        <% End If %>        

        <% If Request.QueryString("ActionType") = "bio" Then%>
                    <li class="active"><a href="PersonModify?ActionType=bio&t=m"><i class="fa fa-user"></i> <span>Personal data</span></a></li>
        <% Else%>
                    <li><a href="PersonModify?ActionType=bio&t=m"><i class="fa fa-user"></i> <span>Personal data</span></a></li>
        <% End If%>  
         
        <% If Request("ActionType") = "g" Then%>
            <li class="active"><a class="active" href="PersonalHealth?ActionType=g&t=m"><i class="fa fa-stethoscope"></i> <span>Health data</span></a></li>
        <% Else%>
            <li><a href="PersonalHealth?ActionType=g&t=m"><i class="fa fa-stethoscope"></i> <span>Health data</span></a></li>  
        <% End If%>  
        <% If Request("ActionType") = "agrpt" Then%>
            <li class="active"><a class="active" href="Evaluated?ActionType=agrpt&pid=<% =Request.Cookies("Ergo")("LoginPersonUID") %>"><i class="fa fa-desktop"></i> <span>Ergonomic Report</span></a></li>
        <% Else%>
            <li><a href="Evaluated?ActionType=agrpt&pid=<% =Request.Cookies("Ergo")("LoginPersonUID") %>"><i class="fa fa-desktop"></i> <span>Ergonomic Report</span></a></li>  
        <% End If%> 
        <% If Request("ActionType") = "hrarpt" Then%>
            <li class="active"><a class="active" href="HRAResult?ActionType=hrarpt&pid=<% =Request.Cookies("Ergo")("LoginPersonUID") %>"><i class="fa fa-child"></i> <span>MSDs & RA result</span></a></li>
        <% Else%>
            <li><a href="HRAResult?ActionType=hrarpt&pid=<% =Request.Cookies("Ergo")("LoginPersonUID") %>"><i class="fa fa-child"></i> <span>MSDs & RA result</span></a></li>  
        <% End If%>     
    <!-- End Person -->
     <!-- Start of Admin and Super Admin -->
        <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Then %>    
            <% If Request.QueryString("ActionType") = "comp" Then%>
                    <li class="active"><a href="Company.aspx?ActionType=comp"><i class="fa fa-hospital-o"></i> <span>Company</span></a></li>
            <% Else%>
                    <li><a href="Company.aspx?ActionType=comp"><i class="fa fa-hospital-o"></i> <span>Company</span></a></li>
            <% End If%>  
        <% End If%>  

        <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_CAD")) = True Then%>     
    
            <% If Request.QueryString("ActionType") = "emp" Then%>
                    <li class="active"><a href="Person.aspx?ActionType=emp"><i class="fa fa-users"></i> <span>ข้อมูลบุคคล</span></a></li>
                <% Else%>
                    <li><a href="Person.aspx?ActionType=emp"><i class="fa fa-users"></i> <span>ข้อมูลบุคคล</span></a></li>
            <% End If%>  
         
            <% If Request("ActionType") = "u" Then%>
                            <li class="active"><a class="active" href="Users.aspx?ActionType=u"><i class="fa fa-user-plus"></i> <span>User</span></a></li>
            <% Else%>
                            <li><a href="Users.aspx?ActionType=u"><i class="fa fa-user-plus"></i> <span>Users</span></a></li>  
            <% End If%>  

       <% If Request("ActionType") = "i" Then%>
                            <li class="active"><a class="active" href="UserImport.aspx?ActionType=i"><i class="fa fa-file-excel-o"></i> <span>Person & User Import</span></a></li>
            <% Else%>
                            <li><a href="UserImport.aspx?ActionType=i"><i class="fa fa-file-excel-o"></i> <span>Person & User Import</span></a></li>  
            <% End If%>  

            <% If Request("ActionType") = "health" Then%>
                            <li class="active"><a class="active" href="PersonSelect?ActionType=health"><i class="fa fa-stethoscope"></i> <span>Person Health data</span></a></li>
            <% Else%>
                            <li><a href="PersonSelect?ActionType=health"><i class="fa fa-stethoscope"></i> <span>Person Health data</span></a></li>  
            <% End If%>          

            <% If Request.QueryString("ActionType") = "tsk" Then%>
                    <li class="active"><a class="active" href="Task.aspx?ActionType=tsk"><i class="fa fa-list"></i> <span>Task</span></a></li>
            <% Else%>
                    <li><a href="Task.aspx?ActionType=tsk"><i class="fa fa-list"></i> <span>Task</span></a></li>  
            <% End If%>               
    
            <% If Request.QueryString("ActionType") = "asm" Then  %>
                <li class="active treeview">
                <% Else %>
                 <li class="treeview">
                <% End If %>        
                  <a href="#">
                    <i class="fa fa-hospital-o"></i> <span>Assessment</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">                	        
            
                <% If Request("ItemType") = "ega" Then%>
                <li class="active"><a href="Evaluated?ActionType=asm&ItemType=ega"> <i class="fa fa-tv text-primary"></i>Ergonomic</a></li>
                <% Else %>
                <li><a href="Evaluated?ActionType=asm&ItemType=ega"><i class="fa fa-tv"></i>Ergonomic</a></li>
                <% End If %>
                 <% If Request("ItemType") = "prisk" Then%>
                <li class="active"><a href="HRA?ActionType=asm&ItemType=prisk"> <i class="fa fa-header text-primary"></i>RA</a></li>
                <% Else %>
                <li><a href="HRA?ActionType=asm&ItemType=prisk"><i class="fa fa-header"></i>RA</a></li>
                <% End If %>
                <% If Request("ItemType") = "hraresult" Then%>
                    <li class="active"><a href="HRAResult?ActionType=hraresult"> <i class="fa fa-bank text-trophy"></i>MSDs & RA result</a></li>
                  <% Else %>
                    <li><a href="HRAResult?ActionType=hraresult"><i class="fa fa-trophy"></i>MSDs & RA result</a></li>
                  <% End If %>   

            </ul>     
                </li>    

            <% If Request.QueryString("ActionType") = "atsk" Then%>
                    <li class="active"><a class="active" href="TaskAction.aspx?ActionType=atsk"><i class="fa fa-history"></i> <span>Action Tracking</span></a></li>
            <% Else%>
                    <li><a href="TaskAction.aspx?ActionType=atsk"><i class="fa fa-history"></i> <span>Action Tracking</span></a></li>  
            <% End If%>

     <% End If%>

   


   
    
     <% If Request.QueryString("ActionType") = "eln" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-youtube-play"></i> <span>E-Learning</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">     
              
        <% If Request("ActionType") = "e" Then%>
         <li class="active"><a class="active" href="CoursePerson?ActionType=e"><i class="fa fa-star"></i><span>My Course</span></a></li>
     <% Else%>
         <li><a href="CoursePerson?ActionType=e"><i class="fa fa-star"></i> <span>My Course</span></a></li>  
     <% End If%>    
              
 <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Then %>
      <% If Request("ItemType") = "cs" Then%>
        <li class="active"><a href="Course.aspx?ActionType=eln&ItemType=cs"> <i class="fa fa-book text-primary"></i>Course</a></li>
      <% Else %>
        <li><a href="Course.aspx?ActionType=eln&ItemType=cs"><i class="fa fa-book"></i>Course</a></li>
      <% End If %>             
     
     <% End If %>       


 <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_CAD")) = True Then %>
  <% If Request("ItemType") = "csa" Then%>
        <li class="active"><a href="Person.aspx?ActionType=eln&ItemType=csa"> <i class="fa fa-map-marker text-primary"></i>Course Assign</a></li>
      <% Else %>
        <li><a href="Person.aspx?ActionType=eln&ItemType=csa"><i class="fa fa-map-marker"></i>Course Assign</a></li>
      <% End If %>  
     <% End If %>   

        <% If Request("ItemType") = "elr" Then%>
        <li class="active"><a href="ReportPersonal.aspx?r=plearn&ActionType=eln&ItemType=elr"> <i class="fa fa-area-chart text-primary"></i>Personal Learning Report</a></li>
      <% Else %>
        <li><a href="ReportPersonal.aspx?r=plearn&ActionType=eln&ItemType=elr"><i class="fa fa-area-chart"></i>Personal Learning Report</a></li>
      <% End If %>


 <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_CAD")) = True Then %>   

      <% If Request("ItemType") = "comprog" Then%>
        <li class="active"><a href="ReportCompany?r=comprog"> <i class="fa fa-pie-chart text-primary"></i>Total Progressing</a></li>
      <% Else %>
        <li><a href="ReportCompany?r=comprog"><i class="fa fa-pie-chart"></i>Total Progressing</a></li>
      <% End If %>

      <% If Request("ItemType") = "csprog" Then%>
        <li class="active"><a href="ReportCourse?r=csprog"> <i class="fa fa-line-chart text-primary"></i>Course progressing</a></li>
      <% Else %>
        <li><a href="ReportCourse?r=csprog"><i class="fa fa-line-chart"></i>Course progressing</a></li>
      <% End If %>             
     
     <% End If %>  

              
 <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Then %>   

      <% If Request("ItemType") = "eva" Then%>
        <li class="active"><a href="EvaluationTopic?ActionType=eln&ItemType=eva"> <i class="fa fa-check-square-o text-primary"></i>จัดการแบบทดสอบ</a></li>
      <% Else %>
        <li><a href="EvaluationTopic?ActionType=eln&ItemType=eva"><i class="fa fa-check-square-o"></i>จัดการแบบทดสอบ</a></li>
      <% End If %>
         
     
     <% End If %>  


          </ul>     
            </li>  

    
 <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_CAD")) = True Then %> 
     
     <% If Request.QueryString("ActionType") = "rpt" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-registered"></i> <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">                	        
            
      <% If Request("ItemType") = "tsk" Then%>
        <li class="active"><a href="ReportTask.aspx?ActionType=rpt&ItemType=tsk"> <i class="fa fa-pie-chart text-primary"></i>Total Task </a></li>
      <% Else %>
        <li><a href="ReportTask.aspx?ActionType=rpt&ItemType=tsk"><i class="fa fa-pie-chart"></i> Total Task </a></li>
      <% End If %>       
      <% If Request("ItemType") = "acttask" Then%>
        <li class="active"><a href="ReportAction?ActionType=rpt&r=acttask&ItemType=acttask"> <i class="fa fa-line-chart text-primary"></i>Task Action by Job</a></li>
      <% Else %>
        <li><a href="ReportAction?ActionType=rpt&r=acttask&ItemType=acttask"><i class="fa fa-line-chart"></i> Task Action by Job</a></li>
      <% End If %>       
      <% If Request("ItemType") = "actdt" Then%>
        <li class="active"><a href="ReportCompany?ActionType=rpt&r=actdt&ItemType=actdt"> <i class="fa fa-calendar text-primary"></i>Task Action by Date</a></li>
      <% Else %>
        <li><a href="ReportCompany?ActionType=rpt&r=actdt&ItemType=actdt"><i class="fa fa-calendar"></i> Task Action by Date</a></li>
      <% End If %>
                 
      <% If Request("ItemType") = "prpt" Then%>
        <li class="active"><a href="ReportPersonal.aspx?ActionType=rpt&ItemType=prpt"><i class="fa fa-heartbeat"></i>Personal Report</a></li>
      <% Else %>
        <li><a href="ReportPersonal.aspx?ActionType=rpt&ItemType=prpt"><i class="fa fa-heartbeat"></i>Personal Report</a></li>
      <% End If %>  

        <% If Request("ItemType") = "hracomp" Then%>
        <li class="active"><a href="ReportCompany?ActionType=rpt&r=hracomp&ItemType=hracomp"><i class="fa fa-stethoscope"></i>MSDs & RA Report</a></li>
      <% Else %>
        <li><a href="ReportCompany?ActionType=rpt&r=hracomp&ItemType=hracomp"><i class="fa fa-stethoscope"></i>MSDs & RA Report</a></li>
      <% End If %>  

       <% If Request("ItemType") = "msd" Then%>
        <li class="active"><a href="ReportCompany1?ActionType=rpt&r=msd&ItemType=msd"><i class="fa fa-bar-chart"></i>MSDs</a></li>
      <% Else %>
        <li><a href="ReportCompany1?ActionType=rpt&r=msd&ItemType=msd"><i class="fa fa-bar-chart"></i>MSDs</a></li>
      <% End If %>  

          </ul>     
            </li>


    
     <% If Request.QueryString("ActionType") = "ghp" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-pie-chart"></i> <span>สรุปผลการดำเนินการ</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">                	        
            
      <% If Request("ItemType") = "1" Then%>
        <li class="active"><a href="Chart1.aspx?ActionType=ghp&ItemType=1"> <i class="fa fa-pie-chart text-primary"></i>การตอบแบบประเมินสุขภาพ</a></li>
      <% Else %>
        <li><a href="Chart1.aspx?ActionType=ghp&ItemType=1"><i class="fa fa-pie-chart"></i>การตอบแบบประเมินสุขภาพ</a></li>
      <% End If %>       

      <% If Request("ItemType") = "2" Then%>
        <li class="active"><a href="Chart2?ActionType=ghp&r=acttask&ItemType=2"> <i class="fa fa-line-chart text-primary"></i>อาการแสดงที่บ่งชี้ว่ามีโอกาสเกิด MSDs</a></li>
      <% Else %>
        <li><a href="Chart2?ActionType=ghp&r=acttask&ItemType=2"><i class="fa fa-line-chart"></i>อาการแสดงที่บ่งชี้ว่ามีโอกาสเกิด MSDs</a></li>
      <% End If %>       
      <% If Request("ItemType") = "3" Then%>
        <li class="active"><a href="Chart3?ActionType=ghp&r=actdt&ItemType=3"> <i class="fa fa-calendar text-primary"></i>ผลความเสี่ยงด้านสุขภาพด้าน MSDs</a></li>
      <% Else %>
        <li><a href="Chart3?ActionType=ghp&r=actdt&ItemType=3"><i class="fa fa-calendar"></i>ผลความเสี่ยงด้านสุขภาพด้าน MSDs</a></li>
      <% End If %>
                 
      <% If Request("ItemType") = "4" Then%>
        <li class="active"><a href="Chart4.aspx?ActionType=ghp&ItemType=4"><i class="fa fa-heartbeat"></i>ผลประเมินความเสี่ยงทางการยศาสตร์</a></li>
      <% Else %>
        <li><a href="Chart4.aspx?ActionType=ghp&ItemType=4"><i class="fa fa-heartbeat"></i>ผลประเมินความเสี่ยงทางการยศาสตร์</a></li>
      <% End If %>  

      <% If Request("ItemType") = "5" Then%>
        <li class="active"><a href="Chart5.aspx?ActionType=ghp&ItemType=5"><i class="fa fa-heartbeat"></i>การรับรู้อาการผิดปกติทางระบบ<br />โครงร่างและกล้ามเนื้อ (MSDs)</a></li>
      <% Else %>
        <li><a href="Chart5.aspx?ActionType=ghp&ItemType=5"><i class="fa fa-heartbeat"></i>การรับรู้อาการผิดปกติทางระบบ<br />โครงร่างและกล้ามเนื้อ (MSDs)</a></li>
      <% End If %>  

          </ul>     
            </li>

     
     <% If Request("ActionType") = "setup" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-gears"></i> <span>ตั้งค่า</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">     

          
      <% If Request("ItemType") = "org" Then%>
        <li class="active"><a href="Organization?ActionType=setup&ItemType=org"> <i class="fa fa-hospital-o  text-primary"></i>ข้อมูลองค์กร</a></li>
      <% Else %>
        <li><a href="Organization?ActionType=setup&ItemType=org"><i class="fa fa-hospital-o"></i> ข้อมูลองค์กร</a></li>
      <% End If %>

      <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Then%>  
       
      <% If Request("ItemType") = "comp" Then%>
        <li class="active"><a href="Company?ActionType=setup&ItemType=comp"> <i class="fa fa-home  text-primary"></i>Customer Company</a></li>
      <% Else %>
        <li><a href="Company?ActionType=setup&ItemType=comp"><i class="fa fa-home"></i> Customer Company</a></li>
      <% End If %>
               
       <% If Request("ItemType") = "pre" Then%>
        <li class="active"><a href="Prefix.aspx?ActionType=setup&ItemType=pre"><i class="fa fa-star"></i>คำนำหน้าชื่อ</a></li>
      <% Else %>
        <li><a href="Prefix.aspx?ActionType=setup&ItemType=pre"><i class="fa fa-star"></i>คำนำหน้าชื่อ</a></li>
      <% End If %> 
           
              
 <% End If %>


      <% If Request("ItemType") = "div" Then%>
        <li class="active"><a href="Division.aspx?ActionType=setup&ItemType=div"><i class="fa fa-angle-right text-primary"></i>ฝ่าย</a></li>
      <% Else %>
        <li><a href="Division.aspx?ActionType=setup&ItemType=div"><i class="fa fa-angle-right"></i>ฝ่าย</a></li>
      <% End If %>  
       <% If Request("ItemType") = "dep" Then%>
        <li class="active"><a href="Department.aspx?ActionType=setup&ItemType=dep"><i class="fa fa-angle-right text-primary"></i>แผนก</a></li>
      <% Else %>
        <li><a href="Department.aspx?ActionType=setup&ItemType=dep"><i class="fa fa-angle-right"></i>แผนก</a></li>
      <% End If %>  

 <% If Request("ItemType") = "pos" Then%>
        <li class="active"><a href="Position.aspx?ActionType=setup&ItemType=pos"> <i class="fa fa-odnoklassniki"></i>ชื่อตำแหน่ง</a></li>
      <% Else %>
        <li><a href="Position.aspx?ActionType=setup&ItemType=pos"> <i class="fa fa-odnoklassniki"></i>ชื่อตำแหน่ง</a></li>
      <% End If %>

                <% If Request("ItemType") = "mac" Then%>
        <li class="active"><a href="Machine.aspx?ActionType=setup&ItemType=mac"><i class="fa fa-wrench text-primary"></i>Machine</a></li>
      <% Else %>
        <li><a href="Machine.aspx?ActionType=setup&ItemType=mac"><i class="fa fa-wrench"></i>Machine</a></li>
      <% End If %>  

            <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Then%>   

                 <% If Request("ItemType") = "act" Then%>
        <li class="active"><a href="ActionStatus.aspx?ActionType=setup&ItemType=act"><i class="fa fa-spinner text-primary"></i>Actions Status</a></li>
      <% Else %>
        <li><a href="ActionStatus.aspx?ActionType=setup&ItemType=act"><i class="fa fa-spinner"></i>Actions Status</a></li>
      <% End If %>  

    <% End If %>  

          </ul></li>
    

   <% End If %>  


        <% If Request.QueryString("ActionType") = "chg" Then%>
            <li class="active"><a href="ChangePassword.aspx?ActionType=chg"><i class="fa fa-key"></i> <span>เปลี่ยนรหัสผ่าน</span></a></li>
        <% Else%>
            <li><a href="ChangePassword.aspx?ActionType=chg"><i class="fa fa-key"></i> <span>เปลี่ยนรหัสผ่าน</span></a></li>
        <% End If%> 
                 
        <li><a href="Default.aspx?logout=y"><i class="fa fa-power-off"></i> <span>ออกจากระบบ</span></a></li>
</ul>
