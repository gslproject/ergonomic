﻿Imports System.Drawing
Imports System.IO
Imports DevExpress.Web
Imports DevExpress.Web.Internal
Public Class TaskModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New TaskController
    Dim ctlM As New MasterController

    Private Const UploadDirectory As String = "~/imgTask/"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            'Request.Cookies("Ergo")("taskimg")="blankimage.png"
            Request.Cookies("Ergo")("taskimg1") = ""
            Request.Cookies("Ergo")("picname1") = ""
            Request.Cookies("Ergo")("taskimg2") = ""
            Request.Cookies("Ergo")("picname2") = ""
            Request.Cookies("Ergo")("taskimg3") = ""
            Request.Cookies("Ergo")("picname3") = ""
            LoadCompany()
            LoadDivision()
            LoadDepartment()
            LoadPosition()
            LoadMachine()

            If Request("tid") Is Nothing Then
                txtTaskNo.Text = ctlM.RunningNumber_New(StrNull2Zero(Request.Cookies("Ergo")("LoginCompanyUID")), CODE_TASK)
            Else
                LoadTaskData()
                ddlCompany.Enabled = False
            End If

            If Request.Cookies("Ergo")("ROLE_ADM") = False And Request.Cookies("Ergo")("ROLE_SPA") = False Then
                ddlCompany.Enabled = False
            Else
                ddlCompany.Enabled = True
            End If

        End If


        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")

    End Sub
    Private Sub LoadCompany()
        Dim ctlC As New CompanyController
        ddlCompany.DataSource = ctlC.Company_GetActive()
        ddlCompany.DataTextField = "CompanyName"
        ddlCompany.DataValueField = "UID"
        ddlCompany.DataBind()
        ddlCompany.SelectedValue = Request.Cookies("Ergo")("LoginCompanyUID")
    End Sub
    Private Sub LoadMachine()
        ddlMachine.DataSource = ctlM.Machine_GetActive()
        ddlMachine.DataTextField = "MachineName"
        ddlMachine.DataValueField = "UID"
        ddlMachine.DataBind()
    End Sub

    'Private Sub LoadBusinessUnit()
    '    Dim ctlOrg As New OrganizationController
    '    ddlBU.DataSource = ctlOrg.Organization_Get()
    '    ddlBU.DataTextField = "UnitName"
    '    ddlBU.DataValueField = "OrganizationCode"
    '    ddlBU.DataBind()
    'End Sub
    'Private Sub LoadMachine()
    '    ddl.DataSource = ctlM.Prefix_GetActive()
    '    ddlPrefix.DataTextField = "Name"
    '    ddlPrefix.DataValueField = "UID"
    '    ddlPrefix.DataBind()
    'End Sub

    Private Sub LoadDivision()
        ddlDivision.DataSource = ctlM.Division_Get(Request.Cookies("Ergo")("LoginCompanyUID"))
        ddlDivision.DataTextField = "DivisionName"
        ddlDivision.DataValueField = "DivisionUID"
        ddlDivision.DataBind()
    End Sub
    Private Sub LoadDepartment()
        ddlDepartment.DataSource = ctlM.Department_GetByDivisionUID(Request.Cookies("Ergo")("LoginCompanyUID"), ddlDivision.SelectedValue)
        ddlDepartment.DataTextField = "DepartmentName"
        ddlDepartment.DataValueField = "DepartmentUID"
        ddlDepartment.DataBind()
    End Sub
    Private Sub LoadPosition()
        ddlPosition.DataSource = ctlM.Position_Get(Request.Cookies("Ergo")("LoginCompanyUID"))
        ddlPosition.DataTextField = "PositionName"
        ddlPosition.DataValueField = "PositionUID"
        ddlPosition.DataBind()
    End Sub

    Private Sub LoadTaskData()
        dt = ctlE.Task_GetByUID(Request("tid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                hdTaskUID.Value = String.Concat(.Item("UID"))
                txtTaskNo.Text = String.Concat(.Item("TaskNo"))

                txtName.Text = String.Concat(.Item("TaskName"))
                txtDescription.Text = String.Concat(.Item("Descriptions"))
                ddlMachine.SelectedValue = String.Concat(.Item("MachineUID"))
                txtPPE.Text = String.Concat(.Item("PPERequirement"))
                ddlCompany.SelectedValue = String.Concat(.Item("CompanyUID"))
                ddlDivision.SelectedValue = String.Concat(.Item("SectionUID"))
                LoadDepartment()
                ddlDepartment.SelectedValue = String.Concat(.Item("DepartmentUID"))
                ddlPosition.SelectedValue = String.Concat(.Item("PositionUID"))

                If String.Concat(.Item("Hot")) = "Y" Then
                    chkHazard.Items(0).Selected = True
                Else
                    chkHazard.Items(0).Selected = False
                End If

                If String.Concat(.Item("Cold")) = "Y" Then
                    chkHazard.Items(1).Selected = True
                Else
                    chkHazard.Items(1).Selected = False
                End If

                If String.Concat(.Item("Noise")) = "Y" Then
                    chkHazard.Items(2).Selected = True
                Else
                    chkHazard.Items(2).Selected = False
                End If

                If String.Concat(.Item("Chemical")) = "Y" Then
                    chkHazard.Items(3).Selected = True
                Else
                    chkHazard.Items(3).Selected = False
                End If

                If String.Concat(.Item("Biological")) = "Y" Then
                    chkHazard.Items(4).Selected = True
                Else
                    chkHazard.Items(4).Selected = False
                End If


                If String.Concat(.Item("REBA")) = "Y" Then
                    chkTools.Items(0).Selected = True
                Else
                    chkHazard.Items(0).Selected = False
                End If


                If String.Concat(.Item("ROSA")) = "Y" Then
                    chkTools.Items(1).Selected = True
                Else
                    chkHazard.Items(1).Selected = False
                End If



                If String.Concat(.Item("RULA")) = "Y" Then
                    chkTools.Items(2).Selected = True
                Else
                    chkHazard.Items(2).Selected = False
                End If



                If String.Concat(.Item("NIOSH")) = "Y" Then
                    chkTools.Items(3).Selected = True
                Else
                    chkHazard.Items(3).Selected = False
                End If

                LoadTaskImage(DBNull2Zero(.Item("UID")))


                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(.Item("StatusFlag")))

            End With

        Else
        End If
    End Sub

    Private Sub LoadTaskImage(TaskUID As Integer)
        Dim dtPic As New DataTable
        Dim str() As String
        dtPic = ctlE.TaskImage_Get(TaskUID)
        If dtPic.Rows.Count > 0 Then
            For i = 0 To dtPic.Rows.Count - 1

                If String.Concat(dtPic.Rows(i)("ImagePath")) <> "" Then
                    str = Split(dtPic.Rows(i)("ImagePath"), ".")

                    Select Case Right(str(0), 1)
                        Case "1"
                            Request.Cookies("Ergo")("taskimg1") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                            Request.Cookies("Ergo")("picname1") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                        Case "2"
                            Request.Cookies("Ergo")("taskimg2") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                            Request.Cookies("Ergo")("picname2") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                        Case "3"
                            Request.Cookies("Ergo")("taskimg3") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                            Request.Cookies("Ergo")("picname3") = String.Concat(dtPic.Rows(i).Item("ImagePath"))
                    End Select
                End If


            Next
        End If

    End Sub

    Protected Sub ddlDivision_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDivision.SelectedIndexChanged
        LoadDepartment()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        'Dim BDate As String = ddlDay.SelectedValue
        'BDate = ddlYear.SelectedValue & ddlMonth.SelectedValue & BDate
        'If txtGPAX.Text <> "" Then
        '    If Not IsNumeric(txtGPAX.Text) Then
        '        DisplayMessage(Me.Page, "GPAX ไม่ถูกต้อง")
        '        Exit Sub
        '    End If
        'End If

        If txtName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ระบุ Task name');", True)
            Exit Sub
        End If

        Dim sHot, sCold, sNoise, sChemical, sBiological As String

        sHot = "N"
        sCold = "N"
        sNoise = "N"
        sChemical = "N"
        sBiological = "N"

        For i = 0 To chkHazard.Items.Count - 1

            Select Case chkHazard.Items(i).Value
                Case "Hot"
                    If chkHazard.Items(i).Selected = True Then
                        sHot = "Y"
                    End If
                Case "Cold"
                    If chkHazard.Items(i).Selected = True Then
                        sCold = "Y"
                    End If
                Case "Noise"
                    If chkHazard.Items(i).Selected = True Then
                        sNoise = "Y"
                    End If
                Case "Chemical"
                    If chkHazard.Items(i).Selected = True Then
                        sChemical = "Y"
                    End If
                Case "Biological"
                    If chkHazard.Items(i).Selected = True Then
                        sBiological = "Y"
                    End If
            End Select

        Next


        Dim sREBA, sROSA, sRULA, sNIOSH As String
        sREBA = "N"
        sROSA = "N"
        sRULA = "N"
        sNIOSH = "N"

        For i = 0 To chkTools.Items.Count - 1

            Select Case chkTools.Items(i).Value
                Case "REBA"
                    If chkTools.Items(i).Selected = True Then
                        sREBA = "Y"
                    End If
                Case "ROSA"
                    If chkTools.Items(i).Selected = True Then
                        sROSA = "Y"
                    End If
                Case "RULA"
                    If chkTools.Items(i).Selected = True Then
                        sRULA = "Y"
                    End If
                Case "NIOSH"
                    If chkTools.Items(i).Selected = True Then
                        sNIOSH = "Y"
                    End If
            End Select

        Next


        ctlE.Task_Save(StrNull2Zero(hdTaskUID.Value), txtTaskNo.Text, txtName.Text, ddlMachine.SelectedValue, txtDescription.Text, StrNull2Zero(ddlCompany.SelectedValue), StrNull2Zero(ddlDivision.SelectedValue), StrNull2Zero(ddlDepartment.SelectedValue), StrNull2Zero(ddlPosition.SelectedValue), txtPPE.Text, sHot, sCold, sNoise, sChemical, sBiological, sREBA, sROSA, sRULA, sNIOSH, ConvertBoolean2StatusFlag(chkStatus.Checked), Request.Cookies("Ergo")("userid"))


        If StrNull2Zero(hdTaskUID.Value) = 0 Then
            ctlM.RunningNumber_Update(Request.Cookies("Ergo")("LoginCompanyUID"), CODE_TASK)
        End If

        Dim imgName As String

        Dim TaskUID As Integer
        TaskUID = ctlE.Task_GetUIDByCode(Request.Cookies("Ergo")("LoginCompanyUID"), txtTaskNo.Text)

        imgName = txtTaskNo.Text & Format(Request.Cookies("Ergo")("LoginCompanyUID"), "00#")

        If Not Request.Cookies("Ergo")("picname1") Is Nothing And Request.Cookies("Ergo")("picname1") <> "" Then
            ctlE.Task_UpdateFileName(TaskUID, txtTaskNo.Text, imgName & "01" & Path.GetExtension(Request.Cookies("Ergo")("picname1")))
            'UploadFile(FileUpload1, lblPersonID.Text & Path.GetExtension(FileUpload1.PostedFile.FileName))
            RenamePictureName(imgName & "01" & Path.GetExtension(Request.Cookies("Ergo")("picname1")), Request.Cookies("Ergo")("picname1"))
        Else
            ctlE.TaskImage_Delete(TaskUID, txtTaskNo.Text, imgName & "01" & Path.GetExtension(Request.Cookies("Ergo")("picname1")))
        End If

        If Not Request.Cookies("Ergo")("picname2") Is Nothing And Request.Cookies("Ergo")("picname2") <> "" Then
            ctlE.Task_UpdateFileName(TaskUID, txtTaskNo.Text, imgName & "02" & Path.GetExtension(Request.Cookies("Ergo")("picname2")))
            'UploadFile(FileUpload1, lblPersonID.Text & Path.GetExtension(FileUpload1.PostedFile.FileName))
            RenamePictureName(imgName & "02" & Path.GetExtension(Request.Cookies("Ergo")("picname2")), Request.Cookies("Ergo")("picname2"))
        Else
            ctlE.TaskImage_Delete(TaskUID, txtTaskNo.Text, imgName & "02" & Path.GetExtension(Request.Cookies("Ergo")("picname2")))
        End If

        If Not Request.Cookies("Ergo")("picname3") Is Nothing And Request.Cookies("Ergo")("picname3") <> "" Then
            ctlE.Task_UpdateFileName(TaskUID, txtTaskNo.Text, imgName & "03" & Path.GetExtension(Request.Cookies("Ergo")("picname3")))
            'UploadFile(FileUpload1, lblPersonID.Text & Path.GetExtension(FileUpload1.PostedFile.FileName))
            RenamePictureName(imgName & "03" & Path.GetExtension(Request.Cookies("Ergo")("picname3")), Request.Cookies("Ergo")("picname3"))
        Else
            ctlE.TaskImage_Delete(TaskUID, txtTaskNo.Text, imgName & "03" & Path.GetExtension(Request.Cookies("Ergo")("picname3")))
        End If


        Dim ctlU As New UserController
        ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_UPD, "Task", "บันทึก/แก้ไข Task :{TaskUID=" & hdTaskUID.Value & "}{TaskNo=" & txtTaskNo.Text & "}", "")

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Response.Redirect("Task.aspx?ActionType=tsk")
    End Sub

    Private Sub RenamePictureName(fname As String, pname As String)

        If pname <> "" Then
            Dim Path As String = Server.MapPath(UploadDirectory)
            Dim Fromfile As String = Path + pname
            Dim Tofile As String = Path + fname


            Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & PictureTask & "/" & fname))
            If fname <> pname Then
                If objfile.Exists Then
                    objfile.Delete()
                End If

                File.Move(Fromfile, Tofile)
            End If
        End If
    End Sub
    Sub UploadFile(ByVal Fileupload As Object, sName As String)
        Dim FileFullName As String = Fileupload.PostedFile.FileName
        Dim FileNameInfo As String = Path.GetFileName(FileFullName)
        Dim filename As String = Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As FileInfo = New FileInfo(Server.MapPath("../" & PictureTask & "/"))
        If FileNameInfo <> "" Then
            'Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & filename)
            Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & sName)
        End If
        objfile = Nothing
    End Sub


    Protected Sub UploadControl_FileUploadComplete1(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)

        If chkFileExist(Server.MapPath(UploadDirectory + Request.Cookies("Ergo")("picname1"))) Then
            File.Delete(Server.MapPath(UploadDirectory + Request.Cookies("Ergo")("picname1")))
        End If

        e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory, Path.GetRandomFileName())
        Request.Cookies("Ergo")("picname1") = e.CallbackData
    End Sub

    Protected Sub UploadControl_FileUploadComplete2(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)

        If chkFileExist(Server.MapPath(UploadDirectory + Request.Cookies("Ergo")("picname2"))) Then
            File.Delete(Server.MapPath(UploadDirectory + Request.Cookies("Ergo")("picname2")))
        End If

        e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory, Path.GetRandomFileName())
        Request.Cookies("Ergo")("picname2") = e.CallbackData
    End Sub

    Protected Sub UploadControl_FileUploadComplete3(ByVal sender As Object, ByVal e As FileUploadCompleteEventArgs)

        If chkFileExist(Server.MapPath(UploadDirectory + Request.Cookies("Ergo")("picname3"))) Then
            File.Delete(Server.MapPath(UploadDirectory + Request.Cookies("Ergo")("picname3")))
        End If

        e.CallbackData = SavePostedFile(e.UploadedFile, UploadDirectory, Path.GetRandomFileName())
        Request.Cookies("Ergo")("picname3") = e.CallbackData
    End Sub


    Protected Function SavePostedFile(ByVal uploadedFile As UploadedFile, ByVal UploadPath As String, ByVal UploadFileName As String) As String
        If (Not uploadedFile.IsValid) Then
            Return String.Empty
        End If

        Dim fileName As String = Path.ChangeExtension(UploadFileName, ".jpg")

        Dim fullFileName As String = CombinePath(UploadPath, fileName)
        Using original As Image = Image.FromStream(uploadedFile.FileContent)
            Using thumbnail As Image = New ImageThumbnailCreator(original).CreateImageThumbnail(New Size(350, 350))
                ImageUtils.SaveToJpeg(CType(thumbnail, Bitmap), fullFileName)
            End Using
        End Using
        UploadingUtils.RemoveFileWithDelay(fileName, fullFileName, 5)
        Return fileName
    End Function
    Protected Function CombinePath(ByVal UploadPath As String, ByVal fileName As String) As String
        Return Path.Combine(Server.MapPath(UploadPath), fileName)
    End Function

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlE.Task_Delete(StrNull2Zero(hdTaskUID.Value))
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Success','ลบ Task เรียบร้อย');", True)
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        txtTaskNo.Text = ctlM.RunningNumber_New(StrNull2Zero(ddlCompany.SelectedValue), CODE_TASK)
    End Sub
End Class