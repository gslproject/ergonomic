﻿Public Class CompanyModify
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlE As New CompanyController
    Dim ctlM As New MasterController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
          If IsNothing(Request.Cookies("Ergo")) Then
            Response.Redirect("Default.aspx")
        End If

        If Request.Cookies("Ergo")("ROLE_ADM") = False And Request.Cookies("Ergo")("ROLE_SPA") = False Then
            Response.Redirect("Error.aspx")
        End If

        If Not IsPostBack Then
            LoadProvince()
            LoadCompanyData()
        End If

        cmdDelete.Attributes.Add("onClick", "javascript:return confirm(""ต้องการลบข้อมูลนี้ใช่หรือไม่?"");")
        txtMaxPerson.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
    End Sub
    Private Sub LoadProvince()
        ddlProvince.DataSource = ctlM.Province_Get()
        ddlProvince.DataTextField = "ProvinceName"
        ddlProvince.DataValueField = "ProvinceID"
        ddlProvince.DataBind()
    End Sub
    Private Sub LoadCompanyData()
        dt = ctlE.Company_GetByUID(Request("cid"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Dim dr As DataRow = dt.Rows(0)

                hdCompUID.Value = String.Concat(dr("UID"))
                txtCompanyID.Text = String.Concat(dr("CompanyCode"))
                txtNameTH.Text = String.Concat(dr("NameTH"))
                txtNameEN.Text = String.Concat(dr("NameEN"))
                txtBranch.Text = String.Concat(dr("Branch"))
                txtTaxID.Text = String.Concat(dr("VATID"))
                txtAddressNo.Text = String.Concat(dr("AddressNumber"))
                txtDistrict.Text = String.Concat(dr("District"))
                txtEmail.Text = String.Concat(dr("Email"))
                txtFax.Text = String.Concat(dr("Fax"))
                txtLane.Text = String.Concat(dr("Lane"))
                txtRoad.Text = String.Concat(dr("Road"))
                txtSubDistrict.Text = String.Concat(dr("SubDistrict"))
                txtTel.Text = String.Concat(dr("Telephone"))
                txtWebsite.Text = String.Concat(dr("Website"))
                txtZipcode.Text = String.Concat(dr("Zipcode"))
                ddlCountry.SelectedValue = String.Concat(dr("Country"))
                ddlProvince.SelectedValue = String.Concat(dr("ProvinceID"))
                chkStatus.Checked = ConvertStatusFlag2CHK(String.Concat(dr("StatusFlag")))

                txtMaxPerson.Text = String.Concat(dr("MAXPERSON"))

                dr = Nothing


            End With

        Else

        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtCompanyID.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุรหัสบริษัท');", True)
            Exit Sub
        End If
        If txtNameTH.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อบริษัท');", True)
            Exit Sub
        End If

        If StrNull2Zero(txtMaxPerson.Text) <= 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุจำนวนพนักงานสูงสุด (Max Person)');", True)
            Exit Sub
        End If



        ctlE.Company_Save(StrNull2Zero(hdCompUID.Value), txtCompanyID.Text, txtNameTH.Text, txtNameEN.Text, txtBranch.Text, txtTaxID.Text, txtAddressNo.Text, txtLane.Text, txtRoad.Text, txtSubDistrict.Text, txtDistrict.Text, ddlProvince.SelectedValue, txtZipcode.Text, ddlCountry.SelectedValue, txtTel.Text, txtFax.Text, txtEmail.Text, txtWebsite.Text, ConvertBoolean2StatusFlag(chkStatus.Checked), StrNull2Zero(txtMaxPerson.Text), Request.Cookies("Ergo")("userid"))

        'Dim ctlU As New UserController
        'ctlU.User_GenLogfile(Request.Cookies("Ergo")("username"), ACTTYPE_UPD, "Customer", "บันทึก/แก้ไข ประวัติส่วนตัว :{uid=" & hdCompUID.Value & "}{code=" & txtCompanyID.Text & "}", "")

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Response.Redirect("Company.aspx?p=complete")
    End Sub

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlE.Company_Delete(StrNull2Zero(hdCompUID.Value))
    End Sub
End Class