﻿<%@ Page Title="Personal Risk" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PersonalHealth.aspx.vb" Inherits="Ergonomic.PersonalHealth" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <section class="content-header">
      <h1>Health data</h1>     
    </section>
<section class="content"> 
     <div class="row">
            <section class="col-lg-9 connectedSortable"> 
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Personal Infomation
        <asp:HiddenField ID="hdPersonUID" runat="server" />
      </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-2">
          <div class="form-group">
            <label>Code</label>
            <asp:Label ID="lblCode" runat="server" cssclass="form-control"></asp:Label>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Name</label>
            <asp:Label ID="lblName" runat="server" cssclass="form-control" placeholder="ชื่อ"></asp:Label>

          </div>

        </div>    

        <div class="col-md-2">
          <div class="form-group">
            <label>Sex</label>
               <asp:Label ID="lblSex" runat="server" cssclass="form-control"></asp:Label>            
          </div>

        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Age</label>
            <asp:Label ID="lblAge" runat="server" cssclass="form-control"></asp:Label>
          </div>

        </div>

      </div>

      <div class="row">

        <div class="col-md-3">
          <div class="form-group">
            <label>Position</label>
              <asp:Label ID="lblPosition" runat="server" cssclass="form-control" Text=""></asp:Label>

          </div>

        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label>Section</label>
             <asp:Label ID="lblDivision" runat="server" cssclass="form-control" Text=""></asp:Label>
          </div>

        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label>Department</label>
              <asp:Label ID="lblDepartment" runat="server" cssclass="form-control" Text=""></asp:Label>
          </div>

        </div>
       

        <div class="col-md-2">
          <div class="form-group">
            <label>อายุงาน</label>
            <asp:Label ID="lblWY" runat="server" cssclass="form-control"></asp:Label>
          </div>
        </div>
      <div class="col-md-2">
          <div class="form-group">
            <label>Work period</label>
             <asp:Label ID="lblWorkPeriod" runat="server" cssclass="form-control" Text=""></asp:Label>
          </div>
        </div>
      </div>



    </div>
    <!-- /.box-body -->
  </div>
   </section>
   <section class="col-lg-3 connectedSortable"> 

        <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Health info.</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
       
        <div class="col-md-6">
          <div class="form-group">
            <label>Hight</label>
            <asp:TextBox ID="txtHight" runat="server" cssclass="form-control" placeholder="ส่วนสูง (Cm.)" ></asp:TextBox>
          </div>

        </div>

        <div class="col-md-6">
          <div class="form-group">
            <label>Weight</label>
            <asp:TextBox ID="txtWeight" runat="server" cssclass="form-control" placeholder="น้ำหนัก (Kg.)"></asp:TextBox>
          </div>
        </div>

  </div>
    <!-- /.box-body -->
  </div>


       </section>


</div>
              <div class="row">
                   <section class="col-lg-12 connectedSortable"> 
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Activity/Behavior
  
      </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">

        <div class="col-md-4">
          <div class="form-group">
            <label>มีอาชีพเสริมหรืองานอดิเรกนอกเหนือจากงานปัจจุบันหรือไม่</label>
            <asp:RadioButtonList ID="optHobby" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่มี</asp:ListItem>
              <asp:ListItem Value="Y">มี</asp:ListItem>
            </asp:RadioButtonList>
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label>ระบุ</label>
            <asp:TextBox ID="txtHobby" runat="server" cssclass="form-control" placeholder="" />
          </div>
        </div>


      </div>
      <div class="row">

        <div class="col-md-4">
          <div class="form-group">
            <label>กิจกรรมส่วนตัว</label>
            <asp:RadioButtonList ID="optActivity" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่ออกกำลัง</asp:ListItem>
              <asp:ListItem Value="Y">ออกกำลังกาย</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label> วันละ (นาที)</label>
            <asp:TextBox ID="txtActivityTime" runat="server" cssclass="form-control" placeholder="" />
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
            <label> สัปดาห์ละ (วัน)</label>
            <asp:TextBox ID="txtActivityFeq" runat="server" cssclass="form-control" placeholder="" />
          </div>
        </div>


      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>อุปกรณ์ป้องกันอันตรายส่วนบุคคลที่ใช้ประจำ</label>
            <asp:CheckBoxList ID="chkPPE" runat="server" RepeatColumns="5"></asp:CheckBoxList>
          </div>
        </div>


      </div>


    </div>
  </div>
</section>
</div>
              <div class="row">
                      <section class="col-lg-12 connectedSortable"> 
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">การรับรู้ภาระงาน
      </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div>  คำชี้แจง  จงพิจารณา ค่าคะแนนตั้งแต่ 0 ถึง 10 คะแนน ว่าในขณะที่ท่านทำงานท่านมีความรู้สึกต่างๆ เหล่านี้
มากน้อยเท่าใด พร้อมทำเครื่องหมายวงกลม          ลงบนตัวเลขตามความรู้สึกของท่าน
</div>
        <br />
      <table class="table table-hover">
        <tr>
          <th>Description</th>
          <th>Score</th>
        </tr>
        <tr>
          <td>ความรู้สึกเหนื่อยล้า</td>
          <td>
            <asp:RadioButtonList ID="optROWL1" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True">0</asp:ListItem><asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
              <asp:ListItem>4</asp:ListItem>
              <asp:ListItem>5</asp:ListItem>
              <asp:ListItem>6</asp:ListItem>
              <asp:ListItem>7</asp:ListItem>
              <asp:ListItem>8</asp:ListItem>
              <asp:ListItem>9</asp:ListItem>
              <asp:ListItem>10</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td>ความเสี่ยงอันตราย หรือ อุบัติเหตุ</td>

          <td>
            <asp:RadioButtonList ID="optROWL2" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True">0</asp:ListItem><asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
              <asp:ListItem>4</asp:ListItem>
              <asp:ListItem>5</asp:ListItem>
              <asp:ListItem>6</asp:ListItem>
              <asp:ListItem>7</asp:ListItem>
              <asp:ListItem>8</asp:ListItem>
              <asp:ListItem>9</asp:ListItem>
              <asp:ListItem>10</asp:ListItem>
            </asp:RadioButtonList>
          </td>

        </tr>
        <tr>
          <td>ความคร่ำเคร่งในการทำงาน</td>
          <td>
            <asp:RadioButtonList ID="optROWL3" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True">0</asp:ListItem><asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
              <asp:ListItem>4</asp:ListItem>
              <asp:ListItem>5</asp:ListItem>
              <asp:ListItem>6</asp:ListItem>
              <asp:ListItem>7</asp:ListItem>
              <asp:ListItem>8</asp:ListItem>
              <asp:ListItem>9</asp:ListItem>
              <asp:ListItem>10</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td>ความยากและซับซ้อนของงาน</td>
          <td>
            <asp:RadioButtonList ID="optROWL4" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True">0</asp:ListItem><asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
              <asp:ListItem>4</asp:ListItem>
              <asp:ListItem>5</asp:ListItem>
              <asp:ListItem>6</asp:ListItem>
              <asp:ListItem>7</asp:ListItem>
              <asp:ListItem>8</asp:ListItem>
              <asp:ListItem>9</asp:ListItem>
              <asp:ListItem>10</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td>ความเหมาะสมของจังหวะในการทำงาน</td>
          <td>
            <asp:RadioButtonList ID="optROWL5" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True">0</asp:ListItem><asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
              <asp:ListItem>4</asp:ListItem>
              <asp:ListItem>5</asp:ListItem>
              <asp:ListItem>6</asp:ListItem>
              <asp:ListItem>7</asp:ListItem>
              <asp:ListItem>8</asp:ListItem>
              <asp:ListItem>9</asp:ListItem>
              <asp:ListItem>10</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td>ความรับผิดชอบในการงานที่ทำ</td>
          <td>
            <asp:RadioButtonList ID="optROWL6" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True">0</asp:ListItem><asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
              <asp:ListItem>4</asp:ListItem>
              <asp:ListItem>5</asp:ListItem>
              <asp:ListItem>6</asp:ListItem>
              <asp:ListItem>7</asp:ListItem>
              <asp:ListItem>8</asp:ListItem>
              <asp:ListItem>9</asp:ListItem>
              <asp:ListItem>10</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td>ความพึงพอใจต่องาน</td>
          <td>
            <asp:RadioButtonList ID="optROWL7" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True">0</asp:ListItem><asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
              <asp:ListItem>4</asp:ListItem>
              <asp:ListItem>5</asp:ListItem>
              <asp:ListItem>6</asp:ListItem>
              <asp:ListItem>7</asp:ListItem>
              <asp:ListItem>8</asp:ListItem>
              <asp:ListItem>9</asp:ListItem>
              <asp:ListItem>10</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td>ความเป็นอิสระในการทำงาน</td>
          <td>
            <asp:RadioButtonList ID="optROWL8" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True">0</asp:ListItem><asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
              <asp:ListItem>4</asp:ListItem>
              <asp:ListItem>5</asp:ListItem>
              <asp:ListItem>6</asp:ListItem>
              <asp:ListItem>7</asp:ListItem>
              <asp:ListItem>8</asp:ListItem>
              <asp:ListItem>9</asp:ListItem>
              <asp:ListItem>10</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>


      </table>
    </div>
  </div>
</section>
</div>
              <div class="row">
                      <section class="col-lg-12 connectedSortable"> 
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">ประวัติสุขภาพ</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">


      <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label>ประวัติการสูบบุหรี่</label>
            <asp:RadioButtonList ID="optSmoking" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่สูบ</asp:ListItem>
              <asp:ListItem Value="Y">สูบ</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label> วันละ (มวน/วัน)</label>
            <asp:TextBox ID="txtSmokeQTY" runat="server" cssclass="form-control" placeholder="" />
          </div>
        </div>

      </div>
      <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label>ในระยะเวลา 7 วันที่ผ่านมา ท่านเคยมี อาการปวด/ ชา / เมื่อยล้า ตามส่วนของร่างกาย ข้างต้น
              ที่เกิดจากการทำงาน หรือไม่</label>
            <asp:RadioButtonList ID="optBodyHurt" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่เคย</asp:ListItem>
              <asp:ListItem Value="Y">เคย</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label> ระบุ</label>
            <asp:TextBox ID="txtBodyHurt" runat="server" cssclass="form-control" placeholder="" />
          </div>
        </div>


      </div>

      <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label>มีอาการปวด/ ชา / เมื่อยล้า เหล่านั้นเกิดจากการทำงาน ในโรงงานเท่านั้น หรือไม่</label>
            <asp:RadioButtonList ID="optLegHurt" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่มี</asp:ListItem>
              <asp:ListItem Value="Y">มี</asp:ListItem>
              <asp:ListItem Value="M">ไม่แน่ใจ</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label> ระบุสาเหตุ</label>
            <asp:TextBox ID="txtLegHurt" runat="server" cssclass="form-control" placeholder="" />
          </div>
        </div>


      </div>



      <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label>ประวัติโรคประจำตัว</label>
            <asp:RadioButtonList ID="optIsMedicalHistory" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่มี</asp:ListItem>
              <asp:ListItem Value="Y">มี</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label> ระบุ</label>
            <asp:TextBox ID="txtMedicalHistory" runat="server" cssclass="form-control" placeholder="" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label>โรคประจำตัวนั้นเกี่ยวกับ กระดูก กล้ามเนื้อ คอ ไหล่ หลัง แขน มือ ข้อมือ ขา</label>
            <asp:RadioButtonList ID="optIsErgoMedical" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่มี</asp:ListItem>
              <asp:ListItem Value="Y">มี</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label> ระบุ</label>
            <asp:TextBox ID="txtErgoMedical" runat="server" cssclass="form-control" placeholder="" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label>เคยมีอุบัติเหตุที่กระทบต่อ กระดูก กล้ามเนื้อ คอ ไหล่ หลัง แขน มือ ข้อมือ ขา</label>
            <asp:RadioButtonList ID="optIsAccident" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่มี</asp:ListItem>
              <asp:ListItem Value="Y">มี</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label> ระบุ</label>
            <asp:TextBox ID="txtAccident" runat="server" cssclass="form-control" placeholder="" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label>อุบัติเหตุนั้นส่งผลต่องานหรือไม่</label>
            <asp:RadioButtonList ID="optJobAccident" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่มี</asp:ListItem>
              <asp:ListItem Value="Y">มี</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label> ระบุ</label>
            <asp:TextBox ID="txtJobAccident" runat="server" cssclass="form-control" placeholder="" />
          </div>
        </div>
      </div>
         <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label>ท่านมีการทำงานซ้ำๆ ในท่าเดิมๆ หรือไม่</label>
            <asp:RadioButtonList ID="optWorkRepeatedly" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่ใช่</asp:ListItem>
              <asp:ListItem Value="Y">ใช่</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label> ถ้ามี</label>
             <asp:CheckBoxList ID="chkWorkRepeatedly" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">ต้องนั่งติดต่อกันนานกว่า 2 ชม/วัน</asp:ListItem>
              <asp:ListItem Value="2">ต้องยืนหรือเดินติดต่อกันนานกว่า 2 ชม/วัน</asp:ListItem>
            </asp:CheckBoxList>
          </div>
        </div>
      </div>
          <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label>ท่านมีการใช้สายตาเพ่งชิ้นงานหรือไม่</label>
            <asp:RadioButtonList ID="optHardLook" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่ใช้สายตาเพ่ง</asp:ListItem>
              <asp:ListItem Value="Y">ใช้สายตาเพ่ง</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>        
      </div>
         <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label>ลักษณะงานของท่านต้องมีการยกของเป็นบางครั้งหรือไม่</label>
            <asp:RadioButtonList ID="optLifting" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="N">ไม่มี</asp:ListItem>
              <asp:ListItem Value="Y">มี</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>
        <div class="col-md-5">
          <div class="form-group">
            <label>โดยยกจำนวนกี่ครั้ง/วัน </label>
              <asp:TextBox ID="txtLiftTime" runat="server" cssclass="form-control" placeholder="" />
          </div>

               <div class="form-group">
            <label>ระบุน้ำหนักสูงสุด (กก.)</label>
              <asp:TextBox ID="txtLiftLoad" runat="server" cssclass="form-control" placeholder="" />
          </div>


        </div>
      </div>

          <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>ลักษณะงานของท่านโดยส่วนใหญ่อยู่ในท่าใด</label>
            <asp:RadioButtonList ID="optBodyPosition" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">ท่านั่ง</asp:ListItem>
              <asp:ListItem Value="2">ท่ายืน</asp:ListItem>
                 <asp:ListItem Value="3">ท่านั่งและยืน/เดิน</asp:ListItem>
            </asp:RadioButtonList>

          </div>
        </div>
        
      </div>



    </div>
  </div>
</section>
</div>
              <div class="row">
                      <section class="col-lg-12 connectedSortable"> 
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">อาการผิดปกติทางระบบโครงร่างและกล้ามเนื้อ</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div>  คำชี้แจง  โปรดดูรูปทางซ้ายมือเพื่อประกอบการระบุส่วนของร่างกายที่ท่านเคยรู้สึกว่ามีอาการปวด/ ชา/ เมื่อยล้า ที่เกิดจากการทำงานในโรงงานนี้ ในกรณีที่เคยมีอาการในรอบ 1 เดือนที่ผ่านมา  ให้ท่านทำเครื่องหมาย  ลงในช่องของระดับความรุนแรง (ข้อ 1.) และระดับความถี่ (ข้อ 2.) ให้ตรงกับส่วนของร่างกายที่เคยมีอาการเท่านั้น 

</div><br />
        <table class="table table-responsive" style="width: 100%;">
            <tr>
               <td class="text-center">
                          <img src="images/body.png" /> 
                      </td>
                <td>
      <table class="table table-hover">
        <tr>
          <th>ส่วน</th>
          <th>ด้าน</th>
          <th class="text-center">1. อาการปวดเมื่อยล้า</th>
          <th class="text-center">2. ความถี่ของอาการปวด </th>
        </tr>
        <tr>
          <td rowspan="2">1.คอ</td>
          <td>
            ซ้าย</td>
          <td>
            <asp:RadioButtonList ID="optNeck_Score_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
          <td>
            <asp:RadioButtonList ID="optNeck_Freq_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>

          <td>
            ขวา</td>

          <td>
            <asp:RadioButtonList ID="optNeck_Score_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>

          <td>
            <asp:RadioButtonList ID="optNeck_Freq_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>

        </tr>
        <tr>
          <td rowspan="2">2.ไหล่

          </td>
          <td>
            ซ้าย</td>
          <td>
            <asp:RadioButtonList ID="optShoulder_Score_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
          <td>
            <asp:RadioButtonList ID="optShoulder_Freq_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>

          <td>
            ขวา</td>

          <td>
            <asp:RadioButtonList ID="optShoulder_Score_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>

          <td>
            <asp:RadioButtonList ID="optShoulder_Freq_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td rowspan="2">3.หลังส่วนบน </td>
          <td>
            ซ้าย</td>
          <td>
            <asp:RadioButtonList ID="optUpperBack_Score_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
          <td>
            <asp:RadioButtonList ID="optUpperBack_Freq_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>

          <td>
            ขวา</td>

          <td>
            <asp:RadioButtonList ID="optUpperBack_Score_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>

          <td>
            <asp:RadioButtonList ID="optUpperBack_Freq_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td rowspan="2">4.หลังส่วนล่าง </td>
          <td>
            ซ้าย</td>
          <td>
            <asp:RadioButtonList ID="optLowerBack_Score_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
          <td>
            <asp:RadioButtonList ID="optLowerBack_Freq_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>

          <td>
            ขวา</td>

          <td>
            <asp:RadioButtonList ID="optLowerBack_Score_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>

          <td>
            <asp:RadioButtonList ID="optLowerBack_Freq_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>

        <tr>
          <td rowspan="2">5.แขนท่อนล่าง

          </td>
          <td>
            ซ้าย</td>
          <td>
            <asp:RadioButtonList ID="optForearm_Score_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
          <td>
            <asp:RadioButtonList ID="optForearm_Freq_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>

          <td>
            ขวา</td>

          <td>
            <asp:RadioButtonList ID="optForearm_Score_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>

          <td>
            <asp:RadioButtonList ID="optForearm_Freq_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>

        <tr>
          <td rowspan="2">6.มือและข้อมือ </td>
          <td>
            ซ้าย</td>
          <td>
            <asp:RadioButtonList ID="optHand_Score_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
          <td>
            <asp:RadioButtonList ID="optHand_Freq_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>

          <td>
            ขวา</td>

          <td>
            <asp:RadioButtonList ID="optHand_Score_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>

          <td>
            <asp:RadioButtonList ID="optHand_Freq_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>

        <tr>
          <td rowspan="2">7.สะโพก </td>
          <td>
            ซ้าย</td>
          <td>
            <asp:RadioButtonList ID="optHip_Score_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
          <td>
            <asp:RadioButtonList ID="optHip_Freq_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>

          <td>
            ขวา</td>

          <td>
            <asp:RadioButtonList ID="optHip_Score_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>

          <td>
            <asp:RadioButtonList ID="optHip_Freq_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>

        <tr>
          <td rowspan="2">8.เข่า </td>
          <td>
            ซ้าย</td>
          <td>
            <asp:RadioButtonList ID="optKnee_Score_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
          <td>
            <asp:RadioButtonList ID="optKnee_Freq_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>

          <td>
            ขวา</td>

          <td>
            <asp:RadioButtonList ID="optKnee_Score_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>

          <td>
            <asp:RadioButtonList ID="optKnee_Freq_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>

        <tr>
          <td rowspan="2">9.น่อง </td>
          <td>
            ซ้าย</td>
          <td>
            <asp:RadioButtonList ID="optCalf_Score_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
          <td>
            <asp:RadioButtonList ID="optCalf_Freq_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>

          <td>
            ขวา</td>

          <td>
            <asp:RadioButtonList ID="optCalf_Score_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>

          <td>
            <asp:RadioButtonList ID="optCalf_Freq_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>

        <tr>
          <td rowspan="2">10.เท้าและข้อเท้า </td>
          <td>
            ซ้าย</td>
          <td>
            <asp:RadioButtonList ID="optFeet_Score_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
          <td>
            <asp:RadioButtonList ID="optFeet_Freq_L" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>

          <td>
            ขวา</td>

          <td>
            <asp:RadioButtonList ID="optFeet_Score_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">เล็กน้อย </asp:ListItem>
              <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
              <asp:ListItem Value="3">มาก</asp:ListItem>
              <asp:ListItem Value="4">มากเกินทนไหว</asp:ListItem>
            </asp:RadioButtonList>
          </td>

          <td>
            <asp:RadioButtonList ID="optFeet_Freq_R" runat="server" RepeatDirection="Horizontal">
              <asp:ListItem Selected="True" Value="1">1-2 ครั้ง/สัปดาห์
              </asp:ListItem>
              <asp:ListItem Value="2">3-4 ครั้ง/สัปดาห์</asp:ListItem>
              <asp:ListItem Value="3">1 ครั้ง/วัน </asp:ListItem>
              <asp:ListItem Value="4">หลายครั้งในทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>



      </table>
  </td>
            </tr>           
        </table>
        
    </div>
  </div>
                          </section>
</div> 

  <div class="row" align="center">


    <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="Save" Width="120px" />

  </div>
  <div class="row">
      
   <section class="col-lg-12 connectedSortable">
    หมายเหตุ : <br />
    - ปวดเล็กน้อย คือ ปวดแปล๊บๆ หรือปวดๆ หายๆ ระหว่างทำงาน <br />
    - ปวดปานกลาง คือ ปวดจนต้องเปลี่ยนแปลงท่าทาง หรือปวดต่อเนื่องเป็นพัก ๆ <br />
    - ปวดมาก คือ ปวดจนต้องหยุดการทำงานชั่วขณะ <br />
    - ปวดมากเกินทนไหว คือ ถึงขั้นหยุดงานอย่างน้อย 1 วัน
</section>
  </div>
    </section>
</asp:Content>