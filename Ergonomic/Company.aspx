﻿<%@ Page Title="Company" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Company.aspx.vb" Inherits="Ergonomic.Company" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">    
        function openModals(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#btnConfirm').attr('onclick', "$('#mdEditPerson').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            $('.editemp').click(function () {
             var fname = $(this).attr('data-fname');
             $('#fname').val(fname);
             $('#mdEditPerson').modal('show');

            });
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
     <section class="content-header">
      <h1><%: Title %>        
        <small>ข้อมูลบริษัท</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Company</li>
      </ol>
    </section>

    <section class="content">   

        <div class="row no-print">
        <div class="col-xs-12">
          <a href="CompanyModify?ActionType=comp"  class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> เพิ่มบริษัทใหม่</a>
        </div>
      </div>
        <br />
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">ข้อมูลรายชื่อบริษัท</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
              <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled"></th>    
                    <th>Company ID</th>
                      <th>Code</th>
                  <th>Name</th>
                  <th>Branch</th>
                  <th>Address</th>  
                    <th>Max Emp.</th>
                     <th>Active</th>
                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtC.Rows %>
                <tr>
                 <td width="50px"><a class="editemp" href="CompanyModify?cid=<% =String.Concat(row("UID")) %>" ><img src="images/icon-edit.png"/></a>
                    </td>
                    <td><% =String.Concat(row("UID")) %></td>
                  <td><% =String.Concat(row("CompanyCode")) %></td>
                  <td><% =String.Concat(row("CompanyName")) %>    </td>
                  <td><% =String.Concat(row("Branch")) %></td>
                  <td><% =String.Concat(row("CompanyAddress")) %></td>
                    <td><% =String.Concat(row("MAXPERSON")) %></td>
                    <td><% =IIf(String.Concat(row("StatusFlag")) = "A", "<img src='images/icon-ok.png'>", "") %> </td>
                   
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div>
  
    </section>
</asp:Content>
