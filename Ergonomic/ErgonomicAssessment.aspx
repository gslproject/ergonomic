﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ErgonomicAssessment.aspx.vb" Inherits="Ergonomic.ErgonomicAssessment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <section class="content-header">
      <h1>Ergonomic Assessment</h1>     
    </section>
    <section class="content">  

  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">ErgonomicAssessment<asp:HiddenField ID="hdCompUID" runat="server" />
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>Task No</label>
               <asp:DropDownList ID="ddlTask" runat="server" cssclass="form-control select2"  placeholder="Select Task" Width="100%">
            </asp:DropDownList> 
          </div>

        </div>
      
      </div>
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>Type</label>
              <asp:RadioButtonList ID="optAsmType" runat="server" RepeatDirection="Horizontal">
                  <asp:ListItem Value="S" Selected="True">Self-assessment</asp:ListItem>
                  <asp:ListItem Value="O">Observer assessment</asp:ListItem>
              </asp:RadioButtonList>
          </div>

        </div> 

      </div>
           
      
      <div class="row"> 

        <div class="col-md-12 text-center">

          <asp:Button ID="cmdSave" CssClass="btn btn-primary" runat="server" Text="เริ่มการประเมิน" Width="120px" />

        </div>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
</section>
</asp:Content>