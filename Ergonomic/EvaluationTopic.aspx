﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="EvaluationTopic.aspx.vb" Inherits="Ergonomic.EvaluationTopic" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">  
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<section class="content-header">
      <h1>หัวข้อการประเมิน</h1> <small>ข้อสอบ</small> 
    </section>

<section class="content">  

       <div class="row no-print">
        <div class="col-xs-12">
          <a href="EvaluationTopicModify?ActionType=eva"  class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> เพิ่มใหม่</a>
        </div>
      </div>     
    <br />
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-list"></i>

              <h3 class="box-title">หัวข้อการประเมิน</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                                 
            </div>
            <div class="box-body"> 

         <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled"></th>    
                    <th>ID</th>
                  <th>หัวข้อ/คำถาม</th>
                  <th>หลักสูตร</th>                               
                     <th>Active</th>                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtT.Rows %>
                <tr>
                 <td width="30px"><a class="editemp" href="EvaluationTopicModify?id=<% =String.Concat(row("UID")) %>" ><img src="images/icon-edit.png"/></a>
                    </td>
                  <td><% =String.Concat(row("UID")) %></td>
                  <td><% =String.Concat(row("Descriptions")) %>    </td>
                  <td><% =String.Concat(row("CourseName")) %></td>
                    <td><% =IIf(String.Concat(row("StatusFlag")) = "A", "<img src='images/icon-ok.png'>", "") %> </td>
                   
                </tr>
            <%  Next %>
                </tbody>               
              </table> 
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

     
   </section>   
</asp:Content>
