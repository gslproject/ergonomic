﻿<%@ Page Title="Documentation" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Documentation.aspx.vb" Inherits="Ergonomic.Documentation" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
     <section class="content-header">
      <h1>Documentation      
        <small>เอกสารประกอบ</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Documentation</li>
      </ol>
    </section>
    <section class="content"> 
        
      <!-- START ACCORDION & CAROUSEL-->
    

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">แนวทางการปรับปรุงการยศาสตร์โดยใช้หลัก Hierarchy of Control</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box box-success">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        หมวดที่ 1 Elimination/Substitution การกำจัด/การแทน
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="box-body">
                         <dl class="dl-horizontal">
                <dt>มาตรการควบคุม</dt>
                <dd><b>ใช้ระบบอัตโนมัติในการยกหรือแบก</b></dd>
                <dt>แนวทางในการแก้ไขปัญหา</dt>
                <dd><a href="elearning-resource/docs/แนวทางที่ 1.1 ตัวอย่างระบบลำเลียงแบบลูกกลิ้ง.pdf" target="_blank">1.1 ตัวอย่าง ระบบลาเลียงแบบลูกกลิ้ง (Roller Conveyor)</a></dd>
                <dd><a href="elearning-resource/docs/แนวทางที่ 1.2 รูปภาพตัวอย่างการใช้หุ่นยนต์.pdf" target="_blank">1.2 ตัวอย่าง Robot</a></dd>                 
                <dt>อ้างอิง</dt>
                <dd> 
                    <ul>
                <li>http://www.thaiintermat.com</li>
                        <li>http://www.logisticsmart.net</li>
                        <li>https://techsauce.co</li>                
                        </ul>
                </dd>
              </dl>
                    </div>
                  </div>
                </div>
                <div class="panel box box-success">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        หมวดที่ 2
Engineering controls
การควบคุมทางวิศวกรรม
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse in">
                    <div class="box-body">
                        <dl class="dl-horizontal">
                <dt>มาตรการควบคุม</dt>
                <dd><b>การใช้เครื่องจักรช่วย
และเครื่องมือในการกาจัดหรือลดการออกแรง</b></dd>
                <dt>แนวทางในการแก้ไขปัญหา</dt>  
                <dd><a href="elearning-resource/docs/แนวทางที่ 2.1 การกําหนดขนาดเครื่องมือ.pdf" target="_blank">แนวทางที่ 2.1 การกาหนดขนาดเครื่องมือและด้ามจับในเหมาะสมกับขนาดมือ</a></dd>
                <dd><a href="elearning-resource/docs/แนวทางที่ 2.2 การกําหนดรูปทรงของเครื่องมือ.pdf" target="_blank">แนวทางที่ 2.2 การกาหนดรูปทรงของเครื่องมือ ด้ามจับ และท่าทางในการใช้งาน</a></dd>                 
                <dt>อ้างอิง</dt>
                <dd> 
                    <ul>
                <li>เอกสารการสอนชุดวิชาการยศาสตร์และจิตวิทยาอุตสาหกรรม มหาวิทยาลัยสุโขทัยธรรมาธิราช</li>                                   
                        </ul>
                </dd>
<hr />
 <dt>มาตรการควบคุม</dt>
                <dd><b>เลือกเครื่องมือหรือออกแบบใหม่ลดกระบวนการทางานเพื่อลดการออกแรงหรือปรับปรุงท่าทางการทางาน</b></dd>
                            <dt>แนวทางในการแก้ไขปัญหา</dt> 
                <dd><a href="elearning-resource/docs/แนวทางที่ 2.3 เกณฑ์ในการออกแบบเครื่องมือ.pdf" target="_blank">แนวทางที่ 2.3 เกณฑ์ในการออกแบบเครื่องมือด้วยหลักการทางชีวกลศาสตร์</a></dd>
                <dd><a href="elearning-resource/docs/แนวทางที่ 2.4 แนวทางในการออกแบบอุปกรณ์ควบคุม.pdf" target="_blank">แนวทางที่ 2.4 แนวทางในการออกแบบอุปกรณ์ควบคุม</a></dd>                 
                <dt>อ้างอิง</dt>
                <dd> 
                    <ul>
                <li>เอกสารการสอนชุดวิชาการยศาสตร์และจิตวิทยาอุตสาหกรรม มหาวิทยาลัยสุโขทัยธรรมาธิราช</li>
<li>รัตนาภรณ์ อมรรัตนไพจิตร และสุดธิดา กรุงไกรวงศ์. การยศาสตร์ในสถานที่ทางาน. กรุงเทพฯ : บริษัท เรียงสาม กราฟฟิค ดีไซน์ จากัด, 2544.</li>
<li>http://old-book.ru.ac.th/</li>                                   
                        </ul>
                </dd>

<hr />
 <dt>มาตรการควบคุม</dt>
                <dd><b>ใช้อุปกรณ์ให้ถูกต้องตามหลักสรีรศาสตร์ และให้ผู้ใช้สามารถปรับได้เวิร์กสเตชันเพื่อปรับปรุงท่าทางการทางาน</b></dd>
                            <dt>แนวทางในการแก้ไขปัญหา</dt> 
                <dd><a href="elearning-resource/docs/แนวทางที่ 2.5 การออกแบบสถานีงาน (Workstation Design).pdf" target="_blank">แนวทางที่ 2.5 การออกแบบสถานีงาน</a></dd>
                <dd><a href="elearning-resource/docs/แนวทางที่ 2.6 การจัดพื้นที่ทำงานนั่ง.pdf" target="_blank">แนวทางที่ 2.6 การจัดพื้นที่ทางานแบบนั่ง</a></dd>   
                <dd><a href="elearning-resource/docs/แนวทางที่ 2.7 แนวทางในการออกแบบพื้นที่ทำงาน.pdf" target="_blank">แนวทางที่ 2.7 แนวทางในการออกแบบพื้นที่ทางานสาหรับงานยืน</a></dd>   
                <dd><a href="elearning-resource/docs/แนวทางที่ 2.8 สถานีงานสําหรับงานที่ต้องปฏิบัติ.pdf" target="_blank">แนวทางที่ 2.8 สถานีงานสาหรับงานที่ต้องปฏิบัติในท่านั่งสลับยืน</a></dd>       

                <dt>อ้างอิง</dt>
                <dd> 
                    <ul>
                <li>คู่มือการฝึกอบรมหลักสูตรเจ้าหน้าที่ความปลอดภัยในการทางาน ระดับเทคนิคขั้นสูง : หมวดวิชาที่ 5 การยศาสตร์และการปรับปรุงสภาพการทางาน: กรมสวัสดิการและคุ้มครองแรงงานกระทรวงแรงงาน</li> 
                        </ul>
                </dd>

              </dl>
                    </div>
                  </div>
                </div>
                <div class="panel box box-success">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                       หมวดที่ 3
Administrative หลักการบริการจัดการ
                      </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="box-body">
                        <dl class="dl-horizontal">
                <dt>มาตรการควบคุม</dt>
                <dd><b>กาหนดเวลาพักผ่อนให้เพียงพอ หยุดพักระหว่างงาน</b></dd>
                <dt>แนวทางในการแก้ไขปัญหา</dt>
                <dd><a href="elearning-resource/docs/แนวทางที่ 3.1 แบบสำรวจทางด้านการยศาสตร์ในสถานประกอบการ.pdf" target="_blank">แนวทางที่ 3.1 การใช้แบบสารวจทางด้านการยศาสตร์ในสถานประกอบการ</a></dd>
                <dd><a href="elearning-resource/docs/แนวทางที่ 3.2 แนวทางการปรับปรุงงานยกและเคลื่อนย้ายวัสดุด้วยแรงกายตามหลักการยศาสตร์.pdf" target="_blank">แนวทางที่ 3.2 แนวทางการปรับปรุงงานยกและเคลื่อนย้ายวัสดุด้วยแรงกายตามหลักการยศาสตร์</a></dd>                 
                <dt>อ้างอิง</dt>
                <dd> 
                    <ul>
                <li>http://old-book.ru.ac.th</li>
                <li>มาตรฐานการยกและเคลื่อนย้ายวัสดุด้วยแรงกายตามหลักการยศาสตร์ (มปอ.302: 2561) สสปท. ;กระทรวงแรงงาน</li>                           
                        </ul>
                </dd>

<hr />
 <dt>มาตรการควบคุม</dt>
                <dd><b>การปรับสภาพแวดล้อมในการทางาน</b></dd>
                            <dt>แนวทางในการแก้ไขปัญหา</dt>                           
                <dd><a href="elearning-resource/docs/แนวทางที่ 3.3 การจัดสถานีงานคอมพิวเตอร์ (VDT Workstation).pdf" target="_blank">แนวทางที่ 3.3 การจัดสถานีงานคอมพิวเตอร์ (VDT Workstation)</a></dd>
                <dd><a href="elearning-resource/docs/แนวทางที่ 3.4 การจัดสภาพแวดล้อมการทำงานสำหรับงานคอมพิวเตอร์.pdf" target="_blank">แนวทางที่ 3.4 การจัดสภาพแวดล้อมการทางานสาหรับงานคอมพิวเตอร์</a></dd>   
                <dd><a href="elearning-resource/docs/แนวทางที่ 3.5 สภาพแวดล้อมในสานักงานที่เหมาะสมตามหลักการยศาสตร์.pdf" target="_blank">แนวทางที่ 3.5 สภาพแวดล้อมในสานักงานที่เหมาะสมตามหลักการยศาสตร์</a></dd>   
                 

                <dt>อ้างอิง</dt>
                <dd> 
                    <ul>
                <li>เอกสารการสอนชุดวิชาการยศาสตร์และจิตวิทยาอุตสาหกรรม มหาวิทยาลัยสุโขทัยธรรมาธิราช</li> 
                        <li>https://colonychiro.com/office-ergonomics</li> 
                        </ul>
                </dd>

<hr />
 <dt>มาตรการควบคุม</dt>
                <dd><b>การหมุนเวียนงานเพื่อลดการเคลื่อนไหวซ้า ๆ</b></dd>
                            <dt>แนวทางในการแก้ไขปัญหา</dt> 
                <dd><a href="elearning-resource/docs/แนวทางที่ 3.6 แนวทางในการจัดระบบงาน.pdf" target="_blank">แนวทางที่ 3.6 แนวทางในการจัดระบบงาน</a></dd>     

                <dt>อ้างอิง</dt>
                <dd> 
                    <ul>
                <li>สุนทรี คาเพ็ง, 2545</li>  <li>https://colonychiro.com/office-ergonomics</li> 
                        </ul>
                </dd>

<hr />
 <dt>มาตรการควบคุม</dt>
                <dd><b>การฝึกอบรมเพื่อเพิ่ม
ตระหนักถึงการยศาสตร์
ความเสี่ยงและความรู้</b></dd>
                            <dt>แนวทางในการแก้ไขปัญหา</dt> 
                <dd><a href="elearning-resource/docs/แนวทางที่ 3.7 การฝึกอบรมแนะนำเทคนิคการยกย้ายที่ปลอดภัยให้แก่ผู้ปฏิบัติงาน.pdf" target="_blank">แนวทางที่ 3.7 การฝึกอบรมแนะนำเทคนิคการยกย้ายที่ปลอดภัยให้แก่ผู้ปฏิบัติงาน</a></dd>
                <dd><a href="elearning-resource/docs/แนวทางที่ 3.8 ท่านั่งปฏิบัติงานคอมพิวเตอร์ในสานักงานอย่างเหมาะสม.pdf" target="_blank">แนวทางที่ 3.8 ท่านั่งปฏิบัติงานคอมพิวเตอร์ในสานักงานอย่างเหมาะสม</a></dd>  

                <dt>อ้างอิง</dt>
                <dd> 
                    <ul>
                <li>เอกสารการสอนชุดวิชาการยศาสตร์และจิตวิทยาอุตสาหกรรม มหาวิทยาลัยสุโขทัยธรรมาธิราช</li> 
<li>มาตรฐานการยกและเคลื่อนย้ายวัสดุด้วยแรงกายตามหลักการยศาสตร์ (มปอ.302: 2561) สสปท. ;กระทรวงแรงงาน</li> 
                        </ul>
                </dd>

<hr />
 <dt>มาตรการควบคุม</dt>
                <dd><b>ปรับจังหวะการทางาน</b></dd>
                            <dt>แนวทางในการแก้ไขปัญหา</dt> 
                <dd><a href="elearning-resource/docs/แนวทางที่ 3.9 ท่าทางที่ถูกต้องสาหรับการยก-วางสิ่งของ.pdf" target="_blank">แนวทางที่ 3.9 ท่าทางที่ถูกต้องสาหรับการยก-วางสิ่งของ</a></dd>
                <dd><a href="elearning-resource/docs/แนวทางที่ 3.10 งานยกและเคลื่อนย้ายวัสดุด้วยแรงกาย.pdf" target="_blank">แนวทางที่ 3.10 งานยกและเคลื่อนย้ายวัสดุด้วยแรงกาย</a></dd>  
 <dd><a href="elearning-resource/docs/แนวทางที่ 3.11 ท่าบริหารร่างกายสาหรับผู้ปฏิบัติงานยกและเคลื่อนย้ายวัสดุด้วยแรงกาย.pdf" target="_blank">แนวทางที่ 3.11 ท่าบริหารร่างกายสาหรับผู้ปฏิบัติงานยกและเคลื่อนย้ายวัสดุด้วยแรงกาย</a></dd> 
                <dt>อ้างอิง</dt>
                <dd> 
                    <ul>
                <li>http://old-book.ru.ac.th</li>  
                        </ul>
                </dd>
              </dl>
                    </div>
                  </div>
                </div>
                  
                <div class="panel box box-success">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                      หมวดที่ 4
Personal protective
การป้องกันส่วนบุคคล
อุปกรณ์ (PPE)
                      </a>
                    </h4>
                  </div>
                  <div id="collapse4" class="panel-collapse collapse in">
                    <div class="box-body">
                        <dl class="dl-horizontal">
                <dt>มาตรการควบคุม</dt>
                <dd><b>PPE กับร่างกายทั้งหมดเช่นการป้องกันการสั่นสะเทือน</b></dd>
                <dt>แนวทางในการแก้ไขปัญหา</dt>
                <dd><a href="elearning-resource/docs/แนวทางที่ 4.1 ตัวอย่างการใช้ถุงมือป้องกันการสั่นสะเทือนเพื่อลดปัญหาด้านการยศาสตร์.pdf" target="_blank">แนวทางที่ 4.1 ตัวอย่างการใช้ถุงมือป้องกันการสั่นสะเทือนเพื่อลดปัญหาด้านการยศาสตร์</a></dd>
                        
                <dt>อ้างอิง</dt>
                <dd> 
                    <ul>
                <li>http://old-book.ru.ac.th</li>                
                        </ul>
                </dd>
              </dl>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->      
      </div>
      <!-- /.row -->
      <!-- END ACCORDION & CAROUSEL-->

           
    </section>
</asp:Content>
