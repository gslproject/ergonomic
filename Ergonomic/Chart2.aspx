﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Chart2.aspx.vb" Inherits="Ergonomic.Chart2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
       <link href="assets/styles.css" rel="stylesheet" />   

    <script>
      window.Promise ||
        document.write(
          '<script src="assets/polyfill.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/classList.min.js"><\/script>'
        )
      window.Promise ||
        document.write(
          '<script src="assets/findindex_polyfill_mdn.js"><\/script>'
        )
    </script>
    
    <script src="assets/apexcharts.js"></script>
    

    <script>
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
         <script>
    var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ]
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     
   <section class="content-header">
    <h1>
      อาการแสดงที่บ่งชี้ว่ามีโอกาสเกิด MSDs
      <small></small>
    </h1>  
  </section>

  <!-- Main content -->
  <section class="content">   
    <div class="row"> 
      <section class="col-lg-12 connectedSortable">

          <div class="box box-solid">  
    <div class="box-body">             
                  <div id="pie1"></div>                 
              </div>
          </div>
</section>   
      
        </div>    
</section>
     
    <script>
      
        var options = {
            series: [<%=datapie %>],
            labels: ['อาการชี้บ่ง 1 อาการ','อาการชี้บ่ง 2 อาการ','อาการชี้บ่ง 3 อาการ','อาการชี้บ่ง 4 อาการ','อาการชี้บ่ง 5 ขึ้นไป'],
			legend: {position: 'bottom'},
          chart: {
          width: 480,
          type: 'pie',
            },         
        responsive: [{
          breakpoint: 380,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var pie1 = new ApexCharts(document.querySelector("#pie1"), options);
        pie1.render();
      
      
    </script>
                
</asp:Content>