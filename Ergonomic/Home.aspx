﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Home.aspx.vb" Inherits="Ergonomic.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <section class="content">

    <div class="row">
      <section class="col-lg-7 connectedSortable">
        <div class="box box-nobg no-border">
          <div class="box-body no-padding">

<div class="row"> 

        <h4 class="content-header text-black">Personal</h4>
  
        <div class="col-md-2">
         <a class="icon-menu1" href="PersonModify?ActionType=bio&t=m">
             <div class="icon-imenu1"><div><img src="images/menu/card_id.png" alt="" width="64"></div>
              <div class="icon-menu1-name1">Personal<br /> data</div>
          </div></a>

</div>       
  
          <div class="col-md-2">
            <a class="icon-menu1" href="PersonalHealth?ActionType=g&t=m"><div class="icon-imenu1"><div><img src="images/menu/checkup1.png" alt="" width="64" /></div>
            <div class="icon-menu1-name1">Health data</div>
          </div></a></div>

          <div class="col-md-2">
            <a class="icon-menu1" href="Evaluated?ActionType=agrpt&pid=<% =Request.Cookies("Ergo")("LoginPersonUID") %>">
                 <div class="icon-imenu1">
                <div><img src="images/menu/bench_over_head.png" alt="" width="64" /></div>
            <div class="icon-menu1-name1">Ergonomic<br />Report</div>
          </div></a></div>

          <div class="col-md-2">
            <a class="icon-menu1" href="HRAResult?ActionType=hrarpt&pid=<% =Request.Cookies("Ergo")("LoginPersonUID") %>"><div class="icon-imenu1"><div><img
                src="images/menu/hrar.png" alt="" width="64" /></div>
            <div class="icon-menu1-name1">MSDs & RA result</div>
          </div></a></div>

          <div class="col-md-2">
            <a class="icon-menu1" href="ChangePassword.aspx?ActionType=chg">
                <div class="icon-imenu1"><div><img src="images/menu/keepass.png" alt="" width="64" /></div>
            <div class="icon-menu1-name1">เปลี่ยน<br />รหัสผ่าน</div>
          </div></a></div>

 </div>
            
          <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_CAD")) = True Then%>
 <div class="row">
        <h3 class="content-header text-black">Employee</h3>
        
          
          <div class="col-md-2">
            <a class="icon-menu1" href="Person?ActionType=emp">
                <div class="icon-imenu1">
                    <div><img src="images/menu/person.png" width="64" alt="ข้อมูลบุคคล"></div>
            <div class="icon-menu1-name1">Person</div>
          </div></a>
              </div>
           

          <div class="col-md-2">
            <a class="icon-menu1" href="Users?ActionType=u"><div class="icon-imenu1"><div><img src="images/menu/user.png" alt="" width="64" /></div>
            <div class="icon-menu1-name1">User<br />Account</div>
          </div></a></div>

          <div class="col-md-2">
            <a class="icon-menu1" href="PersonSelect?ActionType=health"><div class="icon-imenu1"><div><img src="images/menu/checkup.png" alt=""    width="64" /></div>
            <div class="icon-menu1-name1">Person<br />Health data</div>
          </div></a></div>

     
  <div class="col-md-2">
            <a class="icon-menu1" href="UserImport?ActionType=i"><div class="icon-imenu1"><div><img src="images/menu/excel.png" alt="" width="64" /></div>
            <div class="icon-menu1-name1">Personal &<br />User Import</div>
          </div></a></div>

     
        
</div>
              <div class="row">
        <h3 class="content-header text-black">Task & Evaluated</h3>

        
          <div class="col-md-2">
            <a class="icon-menu1" href="Task?ActionType=tsk"><div class="icon-imenu1"><div><img src="images/menu/task.png" alt="" width="64" /></div>
            <div class="icon-menu1-name2">Task</div>
          </div></a></div>
          <div class="col-md-2">
            <a class="icon-menu1" href="Evaluated?ActionType=asm&ItemType=ega"><div class="icon-imenu1"><div><img src="images/menu/asm.png" alt=""
                width="64" /></div>
            <div class="icon-menu1-name2">Ergonomic<br />Evaluated</div>
          </div></a></div>   

          <div class="col-md-2">
            <a class="icon-menu1" href="HRA?ActionType=hraresult"><div class="icon-imenu1"><div><img src="images/menu/hra.png" alt=""
                width="64" /></div>
            <div class="icon-menu1-name2">MSDs & RA result</div>
          </div></a></div>


          <div class="col-md-2">
            <a class="icon-menu1" href="TaskAction?ActionType=atsk"><div class="icon-imenu1"><div><img src="images/menu/taskaction.png" alt=""            width="64" /></div>
            <div class="icon-menu1-name2">Action<br />Tracking</div>
          </div></a></div>
         
         
        

    <% End If %>

</div>
              <div class="row">
        <h3 class="content-header text-black">E-Learning</h3>

              
          <div class="col-md-2">
            <a class="icon-menu1" href="CoursePerson?ActionType=e"><div class="icon-imenu1"><div><img src="images/menu/video.png" alt="" width="64" /></div>
            <div class="icon-menu1-name3">My Course</div>
          </div></a></div>   

    <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Then %>
          <div class="col-md-2">
            <a class="icon-menu1" href="Course?ActionType=eln&ItemType=cs"><div class="icon-imenu1"><div><img src="images/menu/courses.png" alt="" width="64" /></div>
            <div class="icon-menu1-name3">Course</div>
          </div></a></div>
  <% End If %>

    <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_CAD")) = True Then%>
          <div class="col-md-2">
            <a class="icon-menu1" href="Person?ActionType=emp"><div class="icon-imenu1"><div><img src="images/menu/address_book.png" alt="" width="64" /></div>
            <div class="icon-menu1-name3">Course Assign</div>
          </div></a></div>
 <% End If %>

            
    <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Then %>
          <div class="col-md-2">
            <a class="icon-menu1" href="EvaluationTopic?ActionType=eln&ItemType=eva"><div class="icon-imenu1"><div><img src="images/menu/abc.png" alt="" width="64" /></div>
            <div class="icon-menu1-name3">จัดการ<br />แบบทดสอบ</div>
          </div></a></div>
  <% End If %>
   
          <div class="col-md-2">
            <a class="icon-menu1" href="ReportPersonal?r=plearn"><div class="icon-imenu1"><div><img src="images/menu/motarboard.png" alt=""
                width="64" /></div>
            <div class="icon-menu1-name3">Personal <br />Learning Report</div>
          </div></a></div>
             

    <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_CAD")) = True Then%>
          <div class="col-md-2">
            <a class="icon-menu1" href="ReportCompany?r=comprog"><div class="icon-imenu1"><div><img src="images/menu/bullish.png" alt=""
                width="64" /></div>
            <div class="icon-menu1-name3">Total <br />Progressing</div>
          </div></a></div>

          <div class="col-md-2">
            <a class="icon-menu1" href="ReportCourse?r=csprog"><div class="icon-imenu1"><div><img src="images/menu/police_station.png" alt="" width="64" /></div>
            <div class="icon-menu1-name3">Course <br />progressing</div>
          </div></a></div>

</div>
              <div class="row">
        <h3 class="content-header text-black">Report</h3>

        
          <div class="col-md-2">
            <a class="icon-menu1" href="ReportTask"><div class="icon-imenu1"><div><img src="images/menu/doughnut_chart.png" alt="" width="64" /></div>
            <div class="icon-menu1-name4">Total Task</div>
          </div></a></div>

          <div class="col-md-2">
            <a class="icon-menu1" href="ReportPersonal"><div class="icon-imenu1"><div><img src="images/menu/personalreport.png" width="64"         alt=""></div>
            <div class="icon-menu1-name4">Personal<br />Report</div>
          </div></a></div>

          <div class="col-md-2">
            <a class="icon-menu1" href="ReportCompany?r=hracomp"><div class="icon-imenu1"><div><img src="images/menu/graph2.png" width="64" alt=""></div>
            <div class="icon-menu1-name4" >MSDs</div>
          </div></a></div>


             <div class="col-md-2">
            <a class="icon-menu1" href="ReportCompany1?r=msd"><div class="icon-imenu1"><div><img src="images/menu/hor_chart.png" width="64"
                alt=""></div>
            <div class="icon-menu1-name4">MSDs</div>
          </div></a></div>

          <div class="col-md-2">
            <a class="icon-menu1" href="ReportAction?r=acttask"><div class="icon-imenu1"><div><img src="images/menu/graph.png" alt=""
                width="64" /></div>
            <div class="icon-menu1-name4">Task Action<br /> by Job</div>
          </div></a></div>
          <div class="col-md-2">
            <a class="icon-menu1" href="ReportCompany?r=actdt"><div class="icon-imenu1"><div><img src="images/menu/graph3.png" width="64"
                alt=""></div>
            <div class="icon-menu1-name4">Task Action<br /> by Date</div>
          </div></a></div>
        

          <% End If %>
  
  

    <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_CAD")) = True Then%>  
        
         
</div>
              <div class="row">
    <h3 class="content-header text-black">Master data</h3>
        <div class="col-md-2">
            <a class="icon-menu1" href="Organization?ActionType=setup&ItemType=org"><div class="icon-imenu1"><div><img src="images/menu/org.png"
                alt="จัดการข้อมูลองค์กร" width="64" /></div>
            <div class="icon-menu1-name5">Organization</div>
            </div></a></div>
        <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Then%>     
                  
          <div class="col-md-2">
            <a class="icon-menu1" href="Company?ActionType=setup&ItemType=comp"><div class="icon-imenu1"><div><img src="images/menu/company.png" alt=""     width="64" /></div>
            <div class="icon-menu1-name5">Customer <br />Company</div>
          </div></a></div>
           
            
            <div class="col-md-2">
            <a class="icon-menu1" href="Prefix?ActionType=setup&ItemType=pre"><div class="icon-imenu1"><div><img src="images/menu/medal.png"
                alt="" width="64" /></div>
            <div class="icon-menu1-name5">คำนำหน้าชื่อ</div>
            </div></a></div>
         
        <% End If %>

        <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_CAD")) = True Then%>  
            <div class="col-md-2">
            <a class="icon-menu1" href="Division?ActionType=org&ItemType=div"><div class="icon-imenu1"><div><img src="images/menu/division.png"
                alt="ฝ่าย" width="64" /></div>
            <div class="icon-menu1-name5">Section</div>
            </div></a></div>
            <div class="col-md-2">
            <a class="icon-menu1" href="Department?ActionType=org&ItemType=dep"><div class="icon-imenu1"><div><img src="images/menu/dept.png"
                alt="แผนก" width="64" /></div>
            <div class="icon-menu1-name5">Department</div>
            </div></a></div> 
               <div class="col-md-2">
            <a class="icon-menu1" href="Position?ActionType=org&ItemType=pos"><div class="icon-imenu1"><div><img src="images/menu/pos.png"
                alt="ตำแหน่งงาน" width="64" /></div>
            <div class="icon-menu1-name5">Position</div>
            </div></a></div>
             <div class="col-md-2">
            <a class="icon-menu1" href="Machine?ActionType=mac"><div class="icon-imenu1"><div><img src="images/menu/robot.png" alt="" width="64" /></div>
            <div class="icon-menu1-name5">Machine</div>
          </div></a></div>

               <% If Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_SPA")) = True Or Convert.ToBoolean(Request.Cookies("Ergo")("ROLE_ADM")) = True Then%>        
                       <div class="col-md-2">
            <a class="icon-menu1" href="ActionStatus?ActionType=t"><div class="icon-imenu1"><div><img src="images/menu/define_location.png" alt=""
                width="64" /></div>
            <div class="icon-menu1-name5">Actions<br /> Status</div>
          </div></a></div>
               <% End If %>
            
        <% End If %>
  </div> 
  <% End If %>
      </div>
        </div>    
      </section>
      <section class="col-lg-5 connectedSortable">
        <div class="box box-primary">
          <div class="box-body no-padding"> 
            <div id="calendar"></div>
          </div>
        </div>

            <div class="box box-solid">
                   <div class="box-header">
              <i class="fa fa-pie-chart"></i>
              <h3 class="box-title">สรุปผลการดำเนินการ</h3>                                            
            </div>
          <div class="box-body">
              <a href="Chart1.aspx?ActionType=ghp&ItemType=1" class="btn btn-app text-blue">
                <i class="fa fa-pie-chart"></i>สรุปผลการตอบแบบประเมินสุขภาพ
              </a> 
           
               <a href="Chart2.aspx?ActionType=ghp&ItemType=2" class="btn btn-app text-blue">
                <i class="fa fa-bar-chart-o"></i>สรุปอาการแสดงที่บ่งชี้ว่ามีโอกาสเกิด MSDs
              </a>
             
               <a href="Chart3.aspx?ActionType=ghp&ItemType=3" class="btn btn-app text-blue">
                <i class="fa fa-pie-chart"></i>สรุปประเมินความเสี่ยงสุขภาพด้าน MSDs
              </a>
                 <a href="Chart4.aspx?ActionType=ghp&ItemType=4" class="btn btn-app text-blue">
                <i class="fa fa-line-chart"></i>สรุปผลการประเมินความเสี่ยงทางการยศาสตร์
              </a>
              <a href="Chart5.aspx?ActionType=ghp&ItemType=5" class="btn btn-app text-blue">
                <i class="fa fa-bar-chart-o"></i>การรับรู้อาการผิดปกติทางระบบโครงร่างและกล้ามเนื้อ (MSDs)
              </a>
          </div>
        </div>


      </section>
    </div>
</section>
</asp:Content>